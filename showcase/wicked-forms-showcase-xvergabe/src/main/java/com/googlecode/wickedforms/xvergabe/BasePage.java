package com.googlecode.wickedforms.xvergabe;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class BasePage extends WebPage {

	public BasePage(){
		add(new BookmarkablePageLink<WebPage>("homePage", HomePage.class));
	}
}
