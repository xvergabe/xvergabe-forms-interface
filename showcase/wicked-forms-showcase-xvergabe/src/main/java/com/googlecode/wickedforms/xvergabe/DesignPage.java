package com.googlecode.wickedforms.xvergabe;



import java.util.ArrayList;
import java.util.Arrays;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.xvergabe.xsd.forms.components.x20.DisplayInformation;




public class DesignPage extends BasePage {
	
	ArrayList<String> addedComponents = new ArrayList<String>();
	ArrayList<DisplayInformation> displayInformations = new ArrayList<DisplayInformation>();
	
	DisplayInformation newDisplayInformation = DisplayInformation.Factory.newInstance();
	String selected;
	 
	
	@SuppressWarnings({ "serial", "rawtypes", "unchecked" })
	public DesignPage() {	
	
		//Components for Metadata
		CheckBox chkSellerField = new CheckBox("sellerField", Model.of(Boolean.FALSE));
		CheckBox chkVisible = new CheckBox("visible", Model.of(Boolean.FALSE));
		CheckBox chkRequired = new CheckBox("required", Model.of(Boolean.FALSE));
		CheckBox chkStatic = new CheckBox("static", Model.of(Boolean.FALSE));	
		//final TextField inlang = new TextField("inlang", new PropertyModel<String>(newDisplayInformation, "lang"));
		final TextField inlabel = new TextField("inlabel", new PropertyModel<String>(newDisplayInformation, "label"));
		final TextField inquickInfo = new TextField("inquickInfo", new PropertyModel<String>(newDisplayInformation, "quickInfo"));

		final DropDownChoice inlang = new DropDownChoice<String>("inlang",
				new PropertyModel<String>(newDisplayInformation, "lang"),
				Arrays.asList(new StringResourceModel("lang",this, null).getObject().split(";")));
		
		
		ListView displayInfo = new ListView("displayInfo",displayInformations) {
			@Override
		    protected void populateItem(ListItem item) {
				DisplayInformation i = (DisplayInformation)item.getModelObject();
		        item.add(new Label("lang", i.getLang().toString()));
		        item.add(new Label("label", i.getLabel().getStringValue()));
		        item.add(new Label("quickInfo", i.getQuickInfo().getStringValue()));
		        item.add(new Button("remove"){
		    		@Override
		    		public void onSubmit(){
		    			getParent().remove();
		    		}
		    	});
		    }
		};

		
		DropDownChoice dropDownComponentToAdd = new DropDownChoice<String>("componentToAdd",
				new PropertyModel<String>(this, "selected"),
				Arrays.asList(new StringResourceModel("components",this, null).getObject().split(";")));
		
		ListView addedComps = new ListView("addedComps",addedComponents) {
			@Override
		    protected void populateItem(final ListItem item) {
		        item.add(new Label("label", item.getModel()));
		    	item.add(new Button("remove"){
		    		@Override
		    		public void onSubmit(){
		    			getParent().remove();
		    		}
		    	});
		    }
		};

		add(new Label("sidebartext",new StringResourceModel("sidebartext",this, null).getObject()));
		Form form = new Form("homeform");

		//Meta part 
		form.add(chkSellerField);
		form.add(chkRequired);
		form.add(chkStatic);
		form.add(chkVisible);
			
		form.add(inlang);
		form.add(inlabel);
		form.add(inquickInfo);
		form.add(new Button("addDisplayInformation"){
			@Override
			public void onSubmit() {
				//TODO
 			}
		});
		form.add(displayInfo);
		
		
		//Attributes part
		form.add(addedComps);
		form.add(dropDownComponentToAdd);
		form.add(new Button("addComponent"){
			@Override
			public void onSubmit() {
				addedComponents.add(selected);
 			}
		});
		
		add(form);
	}

}
