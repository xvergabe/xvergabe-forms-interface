package com.googlecode.wickedforms.xvergabe;

import java.io.Serializable;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;

/**
 * Contains all parameters that have influence on how a form is displayed and
 * what it contains.
 * 
 * @author hombergs
 * 
 */
public class FormParameters implements Serializable {

	private RoleType.Enum displayMode = RoleType.PROVIDER;

	private Locale locale = Locale.GERMAN;

	private String xml, pdfPath;

	public RoleType.Enum getDisplayMode() {
		return this.displayMode;
	}

	public Locale getLocale() {
		return this.locale;
	}

	public String getXml() {
		return this.xml;
	}

	public void setDisplayMode(final RoleType.Enum displayMode) {
		this.displayMode = displayMode;
	}

	public void setLocale(final Locale locale) {
		this.locale = locale;
	}

	public void setXml(final String xml) {
		this.xml = xml;
	}
	
	public void setPdfPath(final String pdfPath) {
		this.pdfPath = pdfPath;
	}

	public String getPdfPath(final String pdfPath) {
		return this.pdfPath;
	}
}
