package com.googlecode.wickedforms.xvergabe;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public enum Preset {

	AUFFORDERUNG_ABGABE_ANGEBOT_VOL_A("Aufforderung-Abgabe-Angebot-VOL-A.xml",
			"Aufforderung zur Abgabe eines Angebotes VOL/A"),
			
	AUFFORDERUNG_ABGABE_ANGEBOT_VOL_A_MIT_TOGGLES("Aufforderung-Abgabe-Angebot-VOL-A_mit_Toggles.xml",
					"Aufforderung zur Abgabe eines Angebotes VOL/A mit Toggles"),

	FORMBLATT_332_L_MIT_ULV("Formblatt-332-L-mit-ULV.xml", "Formblatt 332-L mit ULV"),
	
	METADATEN_BEISPIEL("Metadaten-Beispiel.xml", "Metadaten Beispiel");
					
	private final String filename;

	private final String title;

	private Preset(final String filename, final String title) {
		this.filename = filename;
		this.title = title;
	}

	public Reader getReaderUtf8() {
		try {
			InputStream in = Preset.class
					.getResourceAsStream("/com/googlecode/wickedforms/xvergabe/presets/"
							+ this.filename);
			Reader reader;
			reader = new InputStreamReader(in, "UTF-8");
			return reader;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public Reader getReader() {
		InputStream in = Preset.class
				.getResourceAsStream("/com/googlecode/wickedforms/xvergabe/presets/"
						+ this.filename);
		Reader reader;
		reader = new InputStreamReader(in);
		return reader;
	}

	public String getTitle() {
		return this.title;
	}

}
