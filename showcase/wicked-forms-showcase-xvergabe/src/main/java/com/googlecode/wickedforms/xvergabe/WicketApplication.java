package com.googlecode.wickedforms.xvergabe;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.apache.wicket.ConverterLocator;
import org.apache.wicket.IConverterLocator;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.util.convert.converter.CalendarConverter;
import org.apache.wicket.util.convert.converter.DateConverter;

/**
 * Application object for your web application. If you want to run this
 * application without deploying, run the Start class.
 * 
 * @see com.googlecode.wickedforms.xvergabe.Start#main(String[])
 */
public class WicketApplication extends WebApplication {
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<HomePage> getHomePage() {
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init() {
		super.init();
		mountPage("DesignPage", DesignPage.class);
		mountPage("HomePage", HomePage.class);
		// add your configuration here
	}

	@Override
	protected IConverterLocator newConverterLocator() {
		
		DateConverter dateConverter = new DateConverter(){
			@Override
			public DateFormat getDateFormat(Locale locale) {
				return new SimpleDateFormat("yyyy-MM-dd");
			}
		};
		
		ConverterLocator locator = new ConverterLocator();
		locator.set(Calendar.class, new CalendarConverter(dateConverter));
		return locator;
	}
}
