package com.googlecode.wickedforms.xvergabe.modelers;

import java.io.Serializable;
import java.util.Locale;

import org.apache.xmlbeans.XmlObject;
import org.xvergabe.xsd.forms.components.x20.RoleType;

public interface XmlModeller<X extends XmlObject, M> extends Serializable{
	
	M createModelFromXml(X xmlObject, Locale locale, RoleType.Enum displayMode);

}
