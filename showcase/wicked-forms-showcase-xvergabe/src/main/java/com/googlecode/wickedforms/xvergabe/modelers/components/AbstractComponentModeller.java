package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.AbstractComponent;
import org.xvergabe.xsd.forms.components.x20.Address;
import org.xvergabe.xsd.forms.components.x20.Agreement;
import org.xvergabe.xsd.forms.components.x20.Agreements;
import org.xvergabe.xsd.forms.components.x20.Contact;
import org.xvergabe.xsd.forms.components.x20.Deadline;
import org.xvergabe.xsd.forms.components.x20.Description;
import org.xvergabe.xsd.forms.components.x20.Discount;
import org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup;
import org.xvergabe.xsd.forms.components.x20.InReferenceTo;
import org.xvergabe.xsd.forms.components.x20.Lot;
import org.xvergabe.xsd.forms.components.x20.Lots;
import org.xvergabe.xsd.forms.components.x20.NaturalPerson;
import org.xvergabe.xsd.forms.components.x20.OrSection;
import org.xvergabe.xsd.forms.components.x20.Organization;
import org.xvergabe.xsd.forms.components.x20.Prequalification;
import org.xvergabe.xsd.forms.components.x20.Prequalifications;
import org.xvergabe.xsd.forms.components.x20.Price;
import org.xvergabe.xsd.forms.components.x20.ProvidedDocuments;
import org.xvergabe.xsd.forms.components.x20.Remission;
import org.xvergabe.xsd.forms.components.x20.RoleType;
import org.xvergabe.xsd.forms.components.x20.Section;
import org.xvergabe.xsd.forms.components.x20.Signature;
import org.xvergabe.xsd.forms.components.x20.XorSection;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.xvergabe.modelers.FormModeller;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.DynamicCheckGroupModeller;

public class AbstractComponentModeller implements XmlModeller<AbstractComponent, SectionModel> {

	private final OrganizationModeller organizationModeller = new OrganizationModeller();

	private final NaturalPersonModeller naturalPersonModeller = new NaturalPersonModeller();

	private final ContactModeller contactModeller = new ContactModeller();

	private final AddressModeller addressModeller = new AddressModeller();

	private final DescriptionModeller descriptionModeller = new DescriptionModeller();

	private final DeadlineModeller deadlineModeller = new DeadlineModeller();

	private final PriceModeller priceModeller = new PriceModeller();

	private final SectionModeller sectionModeller;
	private final XorSectionModeller xorSectionModeller;
	private final OrSectionModeller orSectionModeller;

	private final SignatureModeller signatureModeller = new SignatureModeller();

	private final AgreementModeller agreementModeller = new AgreementModeller();

	private final RemissionModeller remissionModeller = new RemissionModeller();

	private final DiscountModeller discountModeller = new DiscountModeller();

	private final LotModeller lotModeller = new LotModeller();

	private final PrequalificationModeller prequalificationModeller = new PrequalificationModeller();

	private final InReferenceToModeller inReferenceToModeller = new InReferenceToModeller();

	private final ProvidedDocumentsModeller providedDocumentsModeller = new ProvidedDocumentsModeller();

	private final PrequalificationsModeller prequalificationsModeller = new PrequalificationsModeller();

	private final AgreementsModeller agreementsModeller = new AgreementsModeller();

	private final LotsModeller lotsModeller = new LotsModeller();
	
	private final DynamicCheckGroupModeller dynamicCheckGroupModeller = new DynamicCheckGroupModeller();

	public AbstractComponentModeller(FormModeller formModeller){
		this.sectionModeller = new SectionModeller(formModeller);
		this.orSectionModeller = new OrSectionModeller(formModeller);
		this.xorSectionModeller = new XorSectionModeller(formModeller);
	}

	@Override
	public SectionModel createModelFromXml(final AbstractComponent xmlObject, final Locale lang, final RoleType.Enum sellerMode) {

		SectionModel model = new SectionModel();

		if (xmlObject instanceof Organization) {
			model.add(this.organizationModeller.createModelFromXml((Organization) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof NaturalPerson) {
			model.add(this.naturalPersonModeller.createModelFromXml((NaturalPerson) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Contact) {
			model.add(this.contactModeller.createModelFromXml((Contact) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Address) {
			model.add(this.addressModeller.createModelFromXml((Address) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Description) {
			model.add(this.descriptionModeller.createModelFromXml((Description) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Deadline) {
			model.add(this.deadlineModeller.createModelFromXml((Deadline) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Price) {
			model.add(this.priceModeller.createModelFromXml((Price) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Signature) {
			model.add(this.signatureModeller.createModelFromXml((Signature) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Agreement) {
			model.add(agreementModeller.createModelFromXml((Agreement) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Remission) {
			model.add(this.remissionModeller.createModelFromXml((Remission) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Discount) {
			model.add(this.discountModeller.createModelFromXml((Discount) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Prequalification) {
			model.add(this.prequalificationModeller.createModelFromXml((Prequalification) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Prequalifications) {
			model.add(this.prequalificationsModeller.createModelFromXml((Prequalifications) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof InReferenceTo) {
			model.add(this.inReferenceToModeller.createModelFromXml((InReferenceTo) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof ProvidedDocuments) {
			model.add(this.providedDocumentsModeller.createModelFromXml((ProvidedDocuments) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Lots) {
			model.add(this.lotsModeller.createModelFromXml((Lots) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Agreements) {
			model.add(this.agreementsModeller.createModelFromXml((Agreements) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Lot) {
			model.add(this.lotModeller.createModelFromXml((Lot) xmlObject, lang, sellerMode));
		} else if (xmlObject instanceof Section) {
			return this.sectionModeller.createModelFromXml((Section) xmlObject, lang, sellerMode);
		} else if (xmlObject instanceof XorSection) {
			return this.xorSectionModeller.createModelFromXml((XorSection) xmlObject, lang, sellerMode);
		} else if (xmlObject instanceof OrSection) {
			return this.orSectionModeller.createModelFromXml((OrSection) xmlObject, lang, sellerMode);
		} else if(xmlObject instanceof DynamicCheckGroup){
			return this.dynamicCheckGroupModeller.createModelFromXml((DynamicCheckGroup) xmlObject, lang, sellerMode);
		} else {
			throw new RuntimeException("Unknown component! Cannot create model for xml component of type " + xmlObject.getClass());
		}
		return model;
	}

}
