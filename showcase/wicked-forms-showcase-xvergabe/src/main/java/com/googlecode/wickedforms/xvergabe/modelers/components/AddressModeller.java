package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Address;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class AddressModeller implements
		XmlModeller<Address, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();

	@Override
	public SectionModel createModelFromXml(final Address xmlObject,
			final Locale locale, final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.ADDRESS.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.isSetStreet()
				&& XMLBeansUtil.isVisible(xmlObject.getStreet().getMetadata())){
			TextFieldModel<String> streetField = this.textFieldModeller
					.createModelFromXml(xmlObject.getStreet(), locale,
							sellerMode);
			section.add(streetField);
		}

		if (xmlObject.isSetNumber()
				&& XMLBeansUtil.isVisible(xmlObject.getNumber().getMetadata())) {
			TextFieldModel<String> streetNumberField = this.textFieldModeller
					.createModelFromXml(xmlObject.getNumber(), locale,
							sellerMode);
			section.add(streetNumberField);
		}

		if (xmlObject.isSetPostalCode()
				&& XMLBeansUtil.isVisible(xmlObject.getPostalCode().getMetadata())) {
			TextFieldModel<String> postalCodeField = this.textFieldModeller
					.createModelFromXml(xmlObject.getPostalCode(), locale,
							sellerMode);
			section.add(postalCodeField);
		}

		if (xmlObject.isSetPostOfficeBox()
				&& XMLBeansUtil.isVisible(xmlObject.getPostOfficeBox().getMetadata())) {
			TextFieldModel<String> postOfficeBoxField = this.textFieldModeller
					.createModelFromXml(xmlObject.getPostOfficeBox(), locale,
							sellerMode);
			section.add(postOfficeBoxField);
		}
		
		if (xmlObject.isSetPostOfficeBoxCode()
				&& XMLBeansUtil.isVisible(xmlObject.getPostOfficeBoxCode().getMetadata())) {
			TextFieldModel<String> postOfficeBoxCodeField = this.textFieldModeller
					.createModelFromXml(xmlObject.getPostOfficeBoxCode(), locale,
							sellerMode);
			section.add(postOfficeBoxCodeField);
		}

		if (xmlObject.isSetCity() && XMLBeansUtil.isVisible(xmlObject.getCity().getMetadata())) {
			TextFieldModel<String> cityField = this.textFieldModeller
					.createModelFromXml(xmlObject.getCity(), locale, sellerMode);
			section.add(cityField);
		}

		if (xmlObject.isSetDistrict()
				&& XMLBeansUtil.isVisible(xmlObject.getDistrict().getMetadata())) {
			TextFieldModel<String> districtField = this.textFieldModeller
					.createModelFromXml(xmlObject.getDistrict(), locale,
							sellerMode);
			section.add(districtField);
		}

		if (xmlObject.isSetFederalState()
				&& XMLBeansUtil.isVisible(xmlObject.getFederalState().getMetadata())) {
			TextFieldModel<String> federalStateField = this.textFieldModeller
					.createModelFromXml(xmlObject.getFederalState(), locale,
							sellerMode);
			section.add(federalStateField);
		}

		if (xmlObject.isSetState()
				&& XMLBeansUtil.isVisible(xmlObject.getState().getMetadata())) {
			TextFieldModel<String> stateField = this.textFieldModeller
					.createModelFromXml(xmlObject.getState(), locale,
							sellerMode);
			section.add(stateField);
		}

		if (xmlObject.isSetFurtherInformation()
				&& XMLBeansUtil.isVisible(xmlObject.getFurtherInformation().getMetadata())) {
			TextFieldModel<String> furtherInformationField = this.textFieldModeller
					.createModelFromXml(xmlObject.getFurtherInformation(),
							locale, sellerMode);
			section.add(furtherInformationField);
		}

		/*
		 * TODO-> Implementieren der Komplexen Komponenten Komplexe Komponenten
		 * -> in eigenen Untersectionen
		 */

		// AnsprechpartnerInformationen

		// KontaktdatenInformationen
		return section;
	}

}
