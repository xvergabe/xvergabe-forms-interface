package com.googlecode.wickedforms.xvergabe.modelers.components;


import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.AbstractField;
import org.xvergabe.xsd.forms.components.x20.Agreement;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.CheckboxModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.AbstractFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.BooleanFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.RadioGroupModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextElementModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class AgreementModeller implements
		XmlModeller<Agreement, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();

	BooleanFieldModeller boolFieldModeller = new BooleanFieldModeller();

	TextElementModeller textElementModeller = new TextElementModeller();

	AbstractFieldModeller abstractFieldModeller = new AbstractFieldModeller();
	
	RadioGroupModeller  radioGroupModeller = new RadioGroupModeller();
	
		
	@Override
	public SectionModel createModelFromXml(Agreement xmlObject, Locale locale,
			RoleType.Enum displayMode) {

		String title = "Erklärung";
		if (xmlObject.getMetadata() != null) {
			title = XMLBeansUtil.getLabel(xmlObject.getMetadata(), locale);
		}

		SectionModel section = new SectionModel(title);
		
		if (XMLBeansUtil.isVisible(xmlObject.getMetadata())) {

			if (xmlObject.getMetadata() == null
					|| xmlObject.getAgreed().getMetadata().getFillingRole().equals(RoleType.PROVIDER)) {
				
				CheckboxModel agreedField;
				org.xvergabe.xsd.forms.components.x20.Boolean agreed = org.xvergabe.xsd.forms.components.x20.Boolean.Factory.newInstance();
				agreed.setValue(false);
				
				if(null!=xmlObject.getAgreed())
					agreedField = this.boolFieldModeller.createModelFromXml(xmlObject.getAgreed(), locale, displayMode);
				else
					agreedField = this.boolFieldModeller.createModelFromXml(agreed, locale, displayMode);

				agreedField.setLabel(xmlObject.getExplanation().getValue());
				section.add(agreedField);
			} else {
				TextFieldModel<String> textField = this.textFieldModeller
						.createModelFromXml(xmlObject.getExplanation(), locale,
								displayMode);
				section.add(textField);
			}
			
			for (AbstractField field : xmlObject.getAdditionalFieldArray()){
				section.add(this.abstractFieldModeller
						.createModelFromXml(field, locale, displayMode));
			}
		}
		return section;
	}
}
