package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Agreement;
import org.xvergabe.xsd.forms.components.x20.Agreements;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.BooleanFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextElementModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class AgreementsModeller implements
		XmlModeller<Agreements, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();

	BooleanFieldModeller boolFieldModeller = new BooleanFieldModeller();

	TextElementModeller textElementModeller = new TextElementModeller();

	private AgreementModeller agreementModeller = new AgreementModeller();

	@Override
	public SectionModel createModelFromXml(Agreements xmlObject,
			Locale locale, RoleType.Enum displayMode) {

		String title = "Erklärungen";
		if (xmlObject.getMetadata() != null) {
			title = XMLBeansUtil.getLabel(xmlObject.getMetadata(), locale);
		}

		SectionModel section = new SectionModel(title);

		for (Agreement agreement : xmlObject.getAgreementArray()) {
			section.add(agreementModeller.createModelFromXml(agreement, locale,
					displayMode));
		}

		return section;
	}

}
