package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Bidder;
import org.xvergabe.xsd.forms.components.x20.Organization;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class BidderModeller extends OrganizationModeller {

	private final TextFieldModeller textFieldModeller = new TextFieldModeller();
	private final CountHistoryModeller countHistoryModeller = new CountHistoryModeller();
	private final AddressModeller addressModeller = new AddressModeller(); 

	@Override
	public SectionModel createModelFromXml(final Organization xmlObject, final Locale locale, final RoleType.Enum displayMode) {

		super.createModelFromXml(xmlObject, locale, displayMode);

		Bidder bidder = (Bidder) xmlObject;

		SectionModel section = new SectionModel(ComponentType.ORGANIZATION.getMessage(locale));

		if (bidder.isSetCustomsNumber() && XMLBeansUtil.isVisible(bidder.getCustomsNumber().getMetadata())) {
			section.add(textFieldModeller.createModelFromXml(bidder.getCustomsNumber(), locale, displayMode));
		}

		if (bidder.isSetTaxId() && XMLBeansUtil.isVisible(bidder.getTaxId().getMetadata())) {
			section.add(textFieldModeller.createModelFromXml(bidder.getTaxId(), locale, displayMode));
		}

		if (bidder.isSetEmployeeHistory() && XMLBeansUtil.isVisible(bidder.getEmployeeHistory().getMetadata())) {
			section.add(countHistoryModeller.createModelFromXml(bidder.getEmployeeHistory(), locale, displayMode));
		}

		if (bidder.isSetTaxId() && XMLBeansUtil.isVisible(bidder.getBusinessBranch().getMetadata())) {
			section.add(textFieldModeller.createModelFromXml(bidder.getBusinessBranch(), locale, displayMode));
		}
		
		if (bidder.isSetTaxId() && XMLBeansUtil.isVisible(bidder.getCustomerNumber().getMetadata())) {
			section.add(textFieldModeller.createModelFromXml(bidder.getCustomerNumber(), locale, displayMode));
		}
		
		if (bidder.isSetTaxId() && XMLBeansUtil.isVisible(bidder.getCustomerNumber2().getMetadata())) {
			section.add(textFieldModeller.createModelFromXml(bidder.getCustomerNumber2(), locale, displayMode));
		}
		
		if (bidder.isSetTaxId() && XMLBeansUtil.isVisible(bidder.getBusinessType().getMetadata())) {
			section.add(textFieldModeller.createModelFromXml(bidder.getBusinessType(), locale, displayMode));
		}
		
		if (bidder.isSetTaxId() && XMLBeansUtil.isVisible(bidder.getRevenueHistory().getMetadata())) {
			section.add(textFieldModeller.createModelFromXml(bidder.getRevenueHistory(), locale, displayMode));
		}
		
		if (bidder.isSetTaxId() && XMLBeansUtil.isVisible(bidder.getTradeRegisterNumber().getMetadata())) {
			section.add(textFieldModeller.createModelFromXml(bidder.getTradeRegisterNumber(), locale, displayMode));
		}

		if (bidder.isSetTaxId() && XMLBeansUtil.isVisible(bidder.getAddress().getMetadata())) {
			section.add(addressModeller.createModelFromXml(bidder.getAddress(), locale, displayMode));
		}
		
		return section;
	}
}
