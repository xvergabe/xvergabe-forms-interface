package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Contact;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.URLFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class ContactModeller implements
		XmlModeller<Contact, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();

	URLFieldModeller urlFieldModeller = new URLFieldModeller();

	@Override
	public SectionModel createModelFromXml(final Contact xmlObject,
			final Locale locale, final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.CONTACT.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.isSetPhone()
				&& XMLBeansUtil.isVisible(xmlObject.getPhone().getMetadata())) {
			TextFieldModel<String> phoneField = this.textFieldModeller
					.createModelFromXml(xmlObject.getPhone(), locale,
							sellerMode);
			section.add(phoneField);
		}

		if (xmlObject.isSetFax()
				&& XMLBeansUtil.isVisible(xmlObject.getFax().getMetadata())) {
			TextFieldModel<String> faxField = this.textFieldModeller
					.createModelFromXml(xmlObject.getFax(), locale, sellerMode);
			section.add(faxField);
		}

		if (xmlObject.isSetMail()
				&& XMLBeansUtil.isVisible(xmlObject.getMail().getMetadata())) {
			TextFieldModel<String> mailField = this.textFieldModeller
					.createModelFromXml(xmlObject.getMail(), locale, sellerMode);
			section.add(mailField);
		}

		if (xmlObject.isSetUrl()
				&& XMLBeansUtil.isVisible(xmlObject.getUrl().getMetadata())) {
			TextFieldModel<String> urlField = this.urlFieldModeller
					.createModelFromXml(xmlObject.getUrl(), locale, sellerMode);
			section.add(urlField);
		}

		return section;
	}

}
