package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.ArrayList;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.CountHistory;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.IntegerFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class CountHistoryModeller implements XmlModeller<CountHistory, SectionModel> {

	TextFieldModeller stringFieldModeller = new TextFieldModeller();
	IntegerFieldModeller integerFieldModeller = new IntegerFieldModeller();
	
	@Override
	public SectionModel createModelFromXml(CountHistory xmlObject, Locale locale, RoleType.Enum displayMode) {

		String title = ComponentType.COUNTHISTORY.getMessage(locale);
		if (xmlObject.getMetadata() != null) {
			title = XMLBeansUtil.getLabel(xmlObject.getMetadata(), locale);
		}

		SectionModel section = new SectionModel(title);

		ArrayList<org.xvergabe.xsd.forms.components.x20.Integer> counts = new ArrayList<org.xvergabe.xsd.forms.components.x20.Integer>();
		ArrayList<org.xvergabe.xsd.forms.components.x20.Integer> years = new ArrayList<org.xvergabe.xsd.forms.components.x20.Integer>();
		
		for (int i = 0; i<counts.size(); i++) {
			
			org.xvergabe.xsd.forms.components.x20.String line = new org.xvergabe.xsd.forms.components.x20.impl.StringImpl(null);
			line.setValue(years.get(i) + ": " + counts.get(i));
			
			section.add(stringFieldModeller
					.createModelFromXml(line, locale, displayMode));
		}
		return null;
	}
}