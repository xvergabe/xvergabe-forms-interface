package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Calendar;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Deadline;
import org.xvergabe.xsd.forms.components.x20.DeadlineType;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.CheckboxModel;
import com.googlecode.wickedforms.model.elements.fields.DropDownModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.BooleanFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.DateFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.DeadlineTypeFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TimeFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class DeadlineModeller implements
		XmlModeller<Deadline, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();

	BooleanFieldModeller booleanFieldModeller = new BooleanFieldModeller();

	DateFieldModeller dateFieldModeller = new DateFieldModeller();

	TimeFieldModeller timeFieldModeller = new TimeFieldModeller();

	DeadlineTypeFieldModeller deadlineTypeFieldModeller = new DeadlineTypeFieldModeller();

	@Override
	public SectionModel createModelFromXml(final Deadline xmlObject,
			final Locale locale, final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.DEADLINE.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		// boolean-Type
		if (xmlObject.isSetExclusionCriterion()
				&& XMLBeansUtil.isVisible(xmlObject.getExclusionCriterion()
						.getMetadata())) {
			CheckboxModel exCriterionField = this.booleanFieldModeller
					.createModelFromXml(xmlObject.getExclusionCriterion(),
							locale, sellerMode);
			section.add(exCriterionField);
		}

		// date-Type
		if (XMLBeansUtil.isVisible(xmlObject.getDate().getMetadata())) {
			TextFieldModel<Calendar> dateField = this.dateFieldModeller
					.createModelFromXml(xmlObject.getDate(), locale, sellerMode);
			section.add(dateField);
		}

		// time-Type
		if (xmlObject.isSetTime()
				&& XMLBeansUtil.isVisible(xmlObject.getTime().getMetadata())) {
			TextFieldModel<Calendar> timeField = this.timeFieldModeller
					.createModelFromXml(xmlObject.getTime(), locale, sellerMode);
			section.add(timeField);
		}

		if (xmlObject.isSetDescription()
				&& XMLBeansUtil.isVisible(xmlObject.getDescription()
						.getMetadata())) {
			TextFieldModel<String> descriptionField = this.textFieldModeller
					.createModelFromXml(xmlObject.getDescription(), locale,
							sellerMode);
			section.add(descriptionField);
		}

		// Deadlinetype-Type
		if (xmlObject.getType() != null
				&& XMLBeansUtil.isVisible(xmlObject.getType().getMetadata())) {
			DropDownModel<DeadlineType> deadlineTypeField = this.deadlineTypeFieldModeller
					.createModelFromXml(xmlObject.getType(), locale, sellerMode);
			section.add(deadlineTypeField);
		}

		return section;
	}
}
