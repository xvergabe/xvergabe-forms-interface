package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Description;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextAreaModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextAreaModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class DescriptionModeller implements
		XmlModeller<Description, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();
	
	TextAreaModeller textAreaModeller = new TextAreaModeller();

	@Override
	public SectionModel createModelFromXml(
			final Description xmlObject, final Locale locale,
			final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.DESCRIPTION.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.isSetShortDescription()
				&& XMLBeansUtil.isVisible(xmlObject.getShortDescription()
						.getMetadata())) {
			TextFieldModel<String> shortDescField = this.textFieldModeller
					.createModelFromXml(xmlObject.getShortDescription(),
							locale, sellerMode);
			section.add(shortDescField);
		}

		if (xmlObject.isSetLongDescription()
				&& XMLBeansUtil.isVisible(xmlObject.getLongDescription()
						.getMetadata())) {
			TextAreaModel longDescField = this.textAreaModeller
					.createModelFromXml(xmlObject.getLongDescription(), locale,
							sellerMode);
			section.add(longDescField);
		}

		return section;
	}

}
