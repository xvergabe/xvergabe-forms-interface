package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Discount;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.IntegerFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.PercentageFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class DiscountModeller implements
		XmlModeller<Discount, SectionModel> {

	private final PercentageFieldModeller percentageFieldModeller = new PercentageFieldModeller();

	private final IntegerFieldModeller integerFieldModeller = new IntegerFieldModeller();

	@Override
	public SectionModel createModelFromXml(final Discount xmlObject,
			final Locale locale, final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.DISCOUNT.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		// Integer-Type
		if (XMLBeansUtil.isVisible(xmlObject.getDays().getMetadata())) {
			TextFieldModel<Integer> dayField = this.integerFieldModeller
					.createModelFromXml(xmlObject.getDays(), locale, sellerMode);
			section.add(dayField);
		}

		// Percentage-Type -> float
		if (XMLBeansUtil.isVisible(xmlObject.getPercentage().getMetadata())) {
			TextFieldModel<Float> percentageField = this.percentageFieldModeller
					.createModelFromXml(xmlObject.getPercentage(), locale,
							sellerMode);
			section.add(percentageField);
		}

		return section;
	}

}
