package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Calendar;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.InReferenceTo;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.DateFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class InReferenceToModeller implements
		XmlModeller<InReferenceTo, SectionModel> {

	private final DateFieldModeller dateFieldModeller = new DateFieldModeller();

	private final TextFieldModeller textFieldModeller = new TextFieldModeller();
	
	private final DescriptionModeller descriptionModeller = new DescriptionModeller();

	@Override
	public SectionModel createModelFromXml(
			final InReferenceTo xmlObject, final Locale locale,
			final RoleType.Enum displayMode) {

		SectionModel section = new SectionModel(
				ComponentType.INREFERENCETO.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.isSetDateOfReference()
				&& XMLBeansUtil.isVisible(xmlObject.getDateOfReference()
						.getMetadata())) {
			TextFieldModel<Calendar> refDateField = this.dateFieldModeller
					.createModelFromXml(xmlObject.getDateOfReference(), locale,
							displayMode);
			section.add(refDateField);
		}

		if (xmlObject.isSetReferenceNumber()
				&& XMLBeansUtil.isVisible(xmlObject.getReferenceNumber()
						.getMetadata())) {
			TextFieldModel<String> refNumberField = this.textFieldModeller
					.createModelFromXml(xmlObject.getReferenceNumber(), locale,
							displayMode);
			section.add(refNumberField);
		}

		if (xmlObject.isSetActivityNumber()
				&& XMLBeansUtil.isVisible(xmlObject.getActivityNumber()
						.getMetadata())) {
			TextFieldModel<String> field = this.textFieldModeller
					.createModelFromXml(xmlObject.getActivityNumber(), locale,
							displayMode);
			section.add(field);
		}

		if (xmlObject.isSetActivityName()
				&& XMLBeansUtil.isVisible(xmlObject.getActivityName()
						.getMetadata())) {
			TextFieldModel<String> field = this.textFieldModeller
					.createModelFromXml(xmlObject.getActivityName(), locale,
							displayMode);
			section.add(field);
		}

		if (xmlObject.isSetTenderingNumber()
				&& XMLBeansUtil.isVisible(xmlObject.getTenderingNumber()
						.getMetadata())) {
			TextFieldModel<String> field = this.textFieldModeller
					.createModelFromXml(xmlObject.getTenderingNumber(), locale,
							displayMode);
			section.add(field);
		}

		if (xmlObject.isSetServiceDescription()
				&& XMLBeansUtil.isVisible(xmlObject.getServiceDescription()
						.getMetadata())) {
			SectionModel field = this.descriptionModeller
					.createModelFromXml(xmlObject.getServiceDescription(), locale,
							displayMode);
			section.add(field);
		}

		return section;
	}

}
