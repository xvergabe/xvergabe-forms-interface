package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Lot;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.IntegerFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class LotModeller implements XmlModeller<Lot, SectionModel> {

	private final IntegerFieldModeller integerFieldModeller = new IntegerFieldModeller();

	private final PriceModeller priceModeller = new PriceModeller();

	@Override
	public SectionModel createModelFromXml(final Lot xmlObject,
			final Locale locale, final RoleType.Enum displayMode) {

		SectionModel section = new SectionModel(
				ComponentType.LOT.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.getLotNumber() != null
				&& XMLBeansUtil.isVisible(xmlObject.getLotNumber()
						.getMetadata())) {
			TextFieldModel<Integer> lotNumberField = this.integerFieldModeller
					.createModelFromXml(xmlObject.getLotNumber(), locale,
							displayMode);
			lotNumberField.setEnabled(false);
			section.add(lotNumberField);
		}

		if (xmlObject.getPrice() != null) {
			section.add(this.priceModeller.createModelFromXml(
					xmlObject.getPrice(), locale, displayMode));
		}

		if (xmlObject.getAlternativeOffers() != null
				&& XMLBeansUtil.isVisible(xmlObject.getAlternativeOffers()
						.getMetadata())) {
			TextFieldModel<Integer> alterOfferField = this.integerFieldModeller
					.createModelFromXml(xmlObject.getAlternativeOffers(),
							locale, displayMode);
			section.add(alterOfferField);
		}

		return section;
	}

}
