package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.NaturalPerson;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class NaturalPersonModeller implements
		XmlModeller<NaturalPerson, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();

	@Override
	public SectionModel createModelFromXml(
			final NaturalPerson xmlObject, final Locale locale,
			final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.NATURALPERSON.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		// TODO: Auswahlbox einbauen mit enum{Herr;Frau;etc}
		if (xmlObject.isSetSalutation()
				&& XMLBeansUtil.isVisible(xmlObject.getSalutation()
						.getMetadata())) {
			TextFieldModel<String> salutationField = this.textFieldModeller
					.createModelFromXml(xmlObject.getSalutation(), locale,
							sellerMode);

			section.add(salutationField);
		}

		if (xmlObject.isSetAcademicTitle()
				&& XMLBeansUtil.isVisible(xmlObject.getAcademicTitle()
						.getMetadata())) {
			TextFieldModel<String> titleField = this.textFieldModeller
					.createModelFromXml(xmlObject.getAcademicTitle(), locale,
							sellerMode);
			section.add(titleField);
		}

		if (xmlObject.isSetFamilyName()
				&& XMLBeansUtil.isVisible(xmlObject.getFamilyName()
						.getMetadata())) {
			TextFieldModel<String> familyNameField = this.textFieldModeller
					.createModelFromXml(xmlObject.getFamilyName(), locale,
							sellerMode);
			section.add(familyNameField);
		}

		if (xmlObject.isSetFirstName()
				&& XMLBeansUtil.isVisible(xmlObject.getFirstName()
						.getMetadata())) {
			TextFieldModel<String> firstNameField = this.textFieldModeller
					.createModelFromXml(xmlObject.getFirstName(), locale,
							sellerMode);
			section.add(firstNameField);
		}

		if (xmlObject.isSetOrganizationalUnit()
				&& XMLBeansUtil.isVisible(xmlObject.getOrganizationalUnit()
						.getMetadata())) {
			TextFieldModel<String> orgUnitField = this.textFieldModeller
					.createModelFromXml(xmlObject.getOrganizationalUnit(),
							locale, sellerMode);
			section.add(orgUnitField);
		}
		/*
		 * TODO-> Integration der Komponentenelemente aus den komplexen Typen
		 * Komplexe Typen, die aus anderen Komponenten entstehen
		 */
		// AdressInformationen
		if (xmlObject.isSetOfficeAddress()) {
			TextFieldModel<String> officeAddressField = this.textFieldModeller
					.createModelFromXml(xmlObject.getOfficeAddress()
							.getStreet(), locale, sellerMode);
			section.add(officeAddressField);
		}

	
		// KontaktInformationen
		if (xmlObject.getContactDataArray().length > 0
				&& XMLBeansUtil.isVisible(xmlObject.getContactDataArray(0)
						.getPhone().getMetadata())) {
			TextFieldModel<String> contactField = this.textFieldModeller
					.createModelFromXml(xmlObject.getContactDataArray(0)
							.getPhone(), locale, sellerMode);
			section.add(contactField);
		}

		return section;
	}

}
