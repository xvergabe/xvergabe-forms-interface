package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Contact;
import org.xvergabe.xsd.forms.components.x20.Organization;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class OrganizationModeller implements
		XmlModeller<Organization, SectionModel> {

	private final AddressModeller addressModeller = new AddressModeller();

	private final DescriptionModeller descriptionModeller = new DescriptionModeller();

	private final ContactModeller contactModeller = new ContactModeller();

	@Override
	public SectionModel createModelFromXml(
			final Organization xmlObject, final Locale locale,
			final RoleType.Enum displayMode) {
		SectionModel section = new SectionModel(
				ComponentType.ORGANIZATION.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (XMLBeansUtil.isVisible(xmlObject.getDescription())) {
			section.add(this.descriptionModeller.createModelFromXml(
					xmlObject.getDescription(), locale, displayMode));
		}
		if (XMLBeansUtil.isVisible(xmlObject.getAddress())) {
			section.add(this.addressModeller.createModelFromXml(
					xmlObject.getAddress(), locale, displayMode));
		}

		for (Contact contact : xmlObject.getContactDataArray()) {
			section.add(this.contactModeller.createModelFromXml(contact,
					locale, displayMode));
		}

		return section;
	}
}
