package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Prequalification;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.buttons.AddSectionButtonModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class PrequalificationModeller implements
		XmlModeller<Prequalification, SectionModel> {

	private final TextFieldModeller textFieldModeller = new TextFieldModeller();

	@Override
	public SectionModel createModelFromXml(
			final Prequalification xmlObject, final Locale locale,
			final RoleType.Enum displayMode) {

		SectionModel section = new SectionModel(
				ComponentType.PREQUALIFIKATION.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.getPrequalificationInstance() != null
				&& XMLBeansUtil.isVisible(xmlObject.getPrequalificationInstance().getMetadata())) {
			TextFieldModel<String> prequNameField = this.textFieldModeller
					.createModelFromXml(xmlObject.getPrequalificationInstance(), locale,
							displayMode);
			section.add(prequNameField);
		}

		if (xmlObject.getPrequalifiedOrganization() != null
				&& XMLBeansUtil
						.isVisible(xmlObject.getPrequalifiedOrganization().getMetadata())) {
			TextFieldModel<String> pqNumberField = this.textFieldModeller
					.createModelFromXml(xmlObject.getPrequalifiedOrganization(), locale,
							displayMode);
			section.add(pqNumberField);
		}
		
		if (xmlObject.getPqNumber() != null
				&& XMLBeansUtil
						.isVisible(xmlObject.getPqNumber().getMetadata())) {
			TextFieldModel<String> pqNumberField = this.textFieldModeller
					.createModelFromXml(xmlObject.getPqNumber(), locale,
							displayMode);
			section.add(pqNumberField);
		}
		
		if (xmlObject.getUlvNumber() != null
				&& XMLBeansUtil
						.isVisible(xmlObject.getUlvNumber().getMetadata())) {
			TextFieldModel<String> ulvNumberField = this.textFieldModeller
					.createModelFromXml(xmlObject.getUlvNumber(), locale,
							displayMode);
			section.add(ulvNumberField);
		}

		return section;
	}

}
