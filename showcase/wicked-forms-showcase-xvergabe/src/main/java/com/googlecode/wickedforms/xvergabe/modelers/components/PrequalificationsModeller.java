package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.apache.xmlbeans.XmlObject;
import org.xvergabe.xsd.forms.components.x20.Prequalification;
import org.xvergabe.xsd.forms.components.x20.Prequalifications;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.buttons.AddSectionButtonModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class PrequalificationsModeller implements XmlModeller<Prequalifications, SectionModel> {

	private PrequalificationModeller prequalificationModeller = new PrequalificationModeller();

	@Override
	public SectionModel createModelFromXml(final Prequalifications xmlObject, final Locale locale, final RoleType.Enum displayMode) {

		String title = "Präqualifikationen";
		if (xmlObject.getMetadata() != null) {
			title = XMLBeansUtil.getLabel(xmlObject.getMetadata(), locale);
		}

		SectionModel section = new SectionModel(title);

		for (Prequalification prequalification : xmlObject.getPrequalificationArray()) {
			section.add(prequalificationModeller.createModelFromXml(prequalification, locale, displayMode));
		}


		section.add(new AddSectionButtonModel("weitere Präqualifikation") {

			@Override
			public SectionModel createSection() {

				Prequalification p = Prequalification.Factory.newInstance();
				p.set(xmlObject.getPrequalificationArray(0));//set uses a copy, not a reference. 
				return prequalificationModeller.createModelFromXml(p, locale, displayMode);
			}
		});

		return section;
	}

}
