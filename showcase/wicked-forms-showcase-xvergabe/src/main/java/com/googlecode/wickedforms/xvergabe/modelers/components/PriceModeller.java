package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Price;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.DropDownModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.CurrencyTypeModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.DecimalFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.PercentageFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.types.CurrencyType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class PriceModeller implements XmlModeller<Price, SectionModel> {

	private final DecimalFieldModeller decimalFieldModeller = new DecimalFieldModeller();

	private final PercentageFieldModeller percentageFieldModeller = new PercentageFieldModeller();

	private final CurrencyTypeModeller currencyTypeModeller = new CurrencyTypeModeller();

	private final DiscountModeller discountModeller = new DiscountModeller();

	private final RemissionModeller remissionModeller = new RemissionModeller();

	@Override
	public SectionModel createModelFromXml(final Price xmlObject,
			final Locale locale, final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.PRICE.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.isSetGrossValue()
				&& XMLBeansUtil.isVisible(xmlObject.getGrossValue()
						.getMetadata())) {
			TextFieldModel<Float> grossValueField = this.decimalFieldModeller
					.createModelFromXml(xmlObject.getGrossValue(), locale,
							sellerMode);
			section.add(grossValueField);
		}

		if (xmlObject.isSetNetValue()
				&& XMLBeansUtil
						.isVisible(xmlObject.getNetValue().getMetadata())) {
			TextFieldModel<Float> netValueField = this.decimalFieldModeller
					.createModelFromXml(xmlObject.getNetValue(), locale,
							sellerMode);
			section.add(netValueField);
		}

		if (xmlObject.isSetTaxRate()
				&& XMLBeansUtil.isVisible(xmlObject.getTaxRate().getMetadata())) {
			TextFieldModel<Float> taxRateField = this.percentageFieldModeller
					.createModelFromXml(xmlObject.getTaxRate(), locale,
							sellerMode);
			section.add(taxRateField);
		}

		if (xmlObject.isSetCurrency()
				&& XMLBeansUtil
						.isVisible(xmlObject.getCurrency().getMetadata())) {
			DropDownModel<org.xvergabe.xsd.forms.components.x20.CurrencyType> currencyField = this.currencyTypeModeller
					.createModelFromXml(xmlObject.getCurrency(), locale,
							sellerMode);
			section.add(currencyField);
		}

		if (xmlObject.isSetDiscount()) {
			section.add(this.discountModeller.createModelFromXml(
					xmlObject.getDiscount(), locale, sellerMode));
		}

		if (xmlObject.isSetRemission()) {
			section.add(this.remissionModeller.createModelFromXml(
					xmlObject.getRemission(), locale, sellerMode));
		}

		return section;
	}
}
