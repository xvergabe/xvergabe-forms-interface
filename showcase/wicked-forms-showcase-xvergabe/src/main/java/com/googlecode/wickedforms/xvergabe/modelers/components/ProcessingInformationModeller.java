package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Calendar;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.ProcessingInformation;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.DateFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class ProcessingInformationModeller implements
		XmlModeller<ProcessingInformation, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();

	DateFieldModeller dateFieldModeller = new DateFieldModeller();

	@Override
	public SectionModel createModelFromXml(
			final ProcessingInformation xmlObject,
			final Locale locale, final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.PROCESSING_INFORMATION.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.isSetLocation()
				&& XMLBeansUtil
						.isVisible(xmlObject.getLocation().getMetadata())) {
			TextFieldModel<String> locationField = this.textFieldModeller
					.createModelFromXml(xmlObject.getLocation(), locale,
							sellerMode);
			section.add(locationField);
		}

		if (xmlObject.isSetSendingDate()
				&& XMLBeansUtil.isVisible(xmlObject.getSendingDate()
						.getMetadata())) {
			TextFieldModel<Calendar> dateField = this.dateFieldModeller
					.createModelFromXml(xmlObject.getSendingDate(), locale,
							sellerMode);
			section.add(dateField);
		}

		return section;
	}

}
