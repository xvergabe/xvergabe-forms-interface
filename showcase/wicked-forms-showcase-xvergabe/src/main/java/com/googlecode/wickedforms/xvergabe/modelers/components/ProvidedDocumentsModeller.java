package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.ProvidedDocuments;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class ProvidedDocumentsModeller implements
		XmlModeller<ProvidedDocuments, SectionModel> {

	TextFieldModeller stringFieldModeller = new TextFieldModeller();

	@Override
	public SectionModel createModelFromXml(ProvidedDocuments xmlObject,
			Locale locale, RoleType.Enum displayMode) {

		String title = ComponentType.PROVIDEDDOCUMENTS.getMessage(locale);
		if (xmlObject.getMetadata() != null) {
			title = XMLBeansUtil.getLabel(xmlObject.getMetadata(), locale);
		}

		SectionModel section = new SectionModel(title);

		for (org.xvergabe.xsd.forms.components.x20.String stringField : xmlObject.getDocumentArray()) {
			section.add(stringFieldModeller.createModelFromXml(stringField,
					locale, displayMode));
		}

		return section;
	}

}
