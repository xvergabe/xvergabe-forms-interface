package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Remission;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.DecimalFieldModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.PercentageFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class RemissionModeller implements
		XmlModeller<Remission, SectionModel> {

	private final DecimalFieldModeller decimalFieldModeller = new DecimalFieldModeller();

	private final PercentageFieldModeller percentageFieldModeller = new PercentageFieldModeller();

	@Override
	public SectionModel createModelFromXml(final Remission xmlObject,
			final Locale locale, final RoleType.Enum sellerMode) {
		SectionModel section = new SectionModel(
				ComponentType.REMISSION.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (xmlObject.getAbsoluteValue() != null
				&& XMLBeansUtil.isVisible(xmlObject.getAbsoluteValue()
						.getMetadata())) {
			TextFieldModel<Float> absValueField = this.decimalFieldModeller
					.createModelFromXml(xmlObject.getAbsoluteValue(), locale,
							sellerMode);
			section.add(absValueField);
		}

		// Percentage-Type
		if (xmlObject.getPercentage() != null
				&& XMLBeansUtil.isVisible(xmlObject.getPercentage()
						.getMetadata())) {
			TextFieldModel<Float> percentageField = this.percentageFieldModeller
					.createModelFromXml(xmlObject.getPercentage(), locale,
							sellerMode);
			section.add(percentageField);
		}

		return section;
	}
}
