package com.googlecode.wickedforms.xvergabe.modelers.components;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;
import org.xvergabe.xsd.forms.components.x20.Signature;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.modelers.fields.TextFieldModeller;
import com.googlecode.wickedforms.xvergabe.types.ComponentType;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class SignatureModeller implements XmlModeller<Signature, SectionModel> {

	TextFieldModeller textFieldModeller = new TextFieldModeller();

	@Override
	public SectionModel createModelFromXml(final Signature xmlObject,
	    final Locale locale, final RoleType.Enum sellerMode) {

		SectionModel section = new SectionModel(
		    ComponentType.SIGNATURE.getMessage(locale));

		if (xmlObject.getMetadata() != null) {
			section.setLabel(XMLBeansUtil.getLabel(xmlObject.getMetadata(),
					locale));
		}

		if (XMLBeansUtil.isVisible(xmlObject.getLocation())) {
			TextFieldModel<String> shortDescField = this.textFieldModeller
			    .createModelFromXml(xmlObject.getLocation(), locale, sellerMode);
			section.add(shortDescField);
		}
		
		if (XMLBeansUtil.isVisible(xmlObject.getAuthorization())) {
			TextFieldModel<String> longDescField = this.textFieldModeller
			    .createModelFromXml(xmlObject.getAuthorization(), locale, sellerMode);
			section.add(longDescField);
		}

		if (XMLBeansUtil.isVisible(xmlObject.getName())) {
			TextFieldModel<String> longDescField = this.textFieldModeller
			    .createModelFromXml(xmlObject.getName(), locale, sellerMode);
			section.add(longDescField);
		}
		return section;
	}
}
