package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.AbstractField;
import org.xvergabe.xsd.forms.components.x20.CheckGroup;
import org.xvergabe.xsd.forms.components.x20.Checkbox;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.elements.AbstractFormElementModel;

public class AbstractFieldModeller {

	private final TextFieldModeller textFieldModeller = new TextFieldModeller();

	private final BooleanFieldModeller booleanFieldModeller = new BooleanFieldModeller();

	private final DecimalFieldModeller decimalFieldModeller = new DecimalFieldModeller();

	private final DateFieldModeller dateFieldModeller = new DateFieldModeller();

	private final DeadlineTypeFieldModeller deadlineTypeFieldModeller = new DeadlineTypeFieldModeller();

	private final IntegerFieldModeller integerFieldModeller = new IntegerFieldModeller();

	private final PercentageFieldModeller percentageFieldModeller = new PercentageFieldModeller();

	private final TenderTypeFieldModeller tenderTypeFieldModeller = new TenderTypeFieldModeller();

	private final TimeFieldModeller timeFieldModeller = new TimeFieldModeller();

	private final URLFieldModeller urlFieldModeller = new URLFieldModeller();

	private final TextElementModeller textElementModeller = new TextElementModeller();

	private final NoteFieldModeller noteFieldModeller = new NoteFieldModeller();

	private final CurrencyTypeModeller currencyTypeModeller = new CurrencyTypeModeller();

	private final TextAreaModeller textAreaModeller = new TextAreaModeller();

	private final RadioGroupModeller radioGroupModeller = new RadioGroupModeller();

	private final DropDownModeller dropDownModeller = new DropDownModeller();

	private final CheckGroupModeller checkgroupModeller = new CheckGroupModeller();
	
	private final CheckboxModeller checkboxModeller = new CheckboxModeller();

	public AbstractFormElementModel createModelFromXml(
			final AbstractField xmlObject, final Locale locale,
			final RoleType.Enum displayMode) {

		if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.String) {
			return this.textFieldModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.String) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.Boolean) {
			return this.booleanFieldModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.Boolean) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.Note) {
			return this.noteFieldModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.Note) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.Decimal) {
			return this.decimalFieldModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.Decimal) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.Date) {
			return this.dateFieldModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.Date) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.DeadlineChoice) {
			return this.deadlineTypeFieldModeller
					.createModelFromXml(
							(org.xvergabe.xsd.forms.components.x20.DeadlineChoice) xmlObject,
							locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.Integer) {
			return this.integerFieldModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.Integer) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.Percentage) {
			return this.percentageFieldModeller
					.createModelFromXml(
							(org.xvergabe.xsd.forms.components.x20.Percentage) xmlObject,
							locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.TenderTypeChoice) {
			return this.tenderTypeFieldModeller
					.createModelFromXml(
							(org.xvergabe.xsd.forms.components.x20.TenderTypeChoice) xmlObject,
							locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.Time) {
			return this.timeFieldModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.Time) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.Url) {
			return this.urlFieldModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.Url) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.CurrencyChoice) {
			return this.currencyTypeModeller
					.createModelFromXml(
							(org.xvergabe.xsd.forms.components.x20.CurrencyChoice) xmlObject,
							locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.TextArea) {
			return this.textAreaModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.String) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.RadioGroup) {
			return this.radioGroupModeller
					.createModelFromXml(
							(org.xvergabe.xsd.forms.components.x20.RadioGroup) xmlObject,
							locale, displayMode);
		} else if (xmlObject instanceof org.xvergabe.xsd.forms.components.x20.DropDown) {
			return this.dropDownModeller.createModelFromXml(
					(org.xvergabe.xsd.forms.components.x20.DropDown) xmlObject,
					locale, displayMode);
		} else if (xmlObject instanceof CheckGroup) {
			return this.checkgroupModeller.createModelFromXml(
					(CheckGroup) xmlObject, locale, displayMode);
		} else if (xmlObject instanceof Checkbox) {
			return this.checkboxModeller.createModelFromXml(
					(Checkbox) xmlObject, locale, displayMode);
		} else {
			throw new IllegalArgumentException(
					"Cannot create form model for unknown xml field type "
							+ xmlObject.getClass());
		}

	}
}
