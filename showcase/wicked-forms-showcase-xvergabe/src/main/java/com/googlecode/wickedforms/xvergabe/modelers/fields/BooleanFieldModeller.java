package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Boolean;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.elements.fields.CheckboxModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;


public class BooleanFieldModeller implements
		XmlModeller<Boolean, CheckboxModel> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public CheckboxModel createModelFromXml(Boolean bool,
			Locale locale, RoleType.Enum displayMode) {

		CheckboxModel checkboxField = new CheckboxModel();
		checkboxField.setId(bool.getId());
//		checkboxField.setBinding(new PropertyBinding<Boolean>(bool,
//				"value"));
		
		metadataModeller.addMetaDataToModel(bool.getMetadata(),
				checkboxField, locale, displayMode);
		
		return checkboxField;
	}
}
