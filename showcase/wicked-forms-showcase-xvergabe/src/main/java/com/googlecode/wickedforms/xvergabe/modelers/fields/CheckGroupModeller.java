package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Arrays;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.CheckGroup;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.elements.fields.CheckboxGroupModel;
import com.googlecode.wickedforms.model.elements.fields.ChoiceLabeller;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class CheckGroupModeller
		implements
		XmlModeller<CheckGroup, CheckboxGroupModel<org.xvergabe.xsd.forms.components.x20.Checkbox>> {

	@Override
	public CheckboxGroupModel<org.xvergabe.xsd.forms.components.x20.Checkbox> createModelFromXml(
			final CheckGroup xmlObject, final Locale locale,
			final RoleType.Enum displayMode) {

		CheckboxGroupModel<org.xvergabe.xsd.forms.components.x20.Checkbox> checkgroup = new CheckboxGroupModel<org.xvergabe.xsd.forms.components.x20.Checkbox>(
				XMLBeansUtil.getLabel(xmlObject, locale),
				Arrays.asList(xmlObject.getOptionArray()),
				new ChoiceLabeller<org.xvergabe.xsd.forms.components.x20.Checkbox>() {
					@Override
					public String getLabel(
							org.xvergabe.xsd.forms.components.x20.Checkbox choice) {
						return choice.getValue().getValue();

					}
				});
		checkgroup.setId(xmlObject.getId());

		if (xmlObject.isSetMetadata()) {
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(),
					checkgroup, locale, displayMode);
		}

		return checkgroup;
	}
}
