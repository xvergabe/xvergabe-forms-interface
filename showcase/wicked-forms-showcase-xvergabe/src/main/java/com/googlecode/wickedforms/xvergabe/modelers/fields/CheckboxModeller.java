package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Checkbox;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.elements.fields.CheckboxModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

/**
 * @author schiller
 *
 */
public class CheckboxModeller implements XmlModeller<Checkbox, CheckboxModel> {

	@Override
	public CheckboxModel createModelFromXml(final Checkbox xmlObject, final Locale locale, final RoleType.Enum displayMode) {
		
		 CheckboxModel checkbox = new CheckboxModel(
				 XMLBeansUtil.getLabel(xmlObject, locale));
		 checkbox.setId(xmlObject.getId());

		if(xmlObject.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(), checkbox, locale, displayMode);
		}
		
		

		return checkbox;
	}
}
