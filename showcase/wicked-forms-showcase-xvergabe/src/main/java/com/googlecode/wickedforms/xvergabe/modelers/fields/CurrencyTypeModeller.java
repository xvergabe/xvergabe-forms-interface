package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.CurrencyType;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.elements.fields.DropDownModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;


public class CurrencyTypeModeller implements XmlModeller<org.xvergabe.xsd.forms.components.x20.CurrencyChoice, DropDownModel<CurrencyType>> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public DropDownModel<CurrencyType> createModelFromXml(final org.xvergabe.xsd.forms.components.x20.CurrencyChoice xmlObject,
			final Locale locale, final RoleType.Enum displayMode) {

		// TODO: PropertyBinding
//		DropDownModel<CurrencyType> modelfield = new DropDownModel<CurrencyType>("Währung",
//				Arrays.asList(CurrencyType.values()), CurrencyType.choiceLabeller(), CurrencyType.EUR,
//				CurrencyType.class);
//
//		this.metadataModeller.addMetaDataToModel(xmlObject.getMetadata(),
//				modelfield, locale, displayMode);

		return null;
	}

}
