package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Calendar;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Date;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;

public class DateFieldModeller implements XmlModeller<Date, TextFieldModel<Calendar>> {


	@Override
	public TextFieldModel<Calendar> createModelFromXml(final Date date,
	    final Locale locale, final RoleType.Enum displayMode) {

		TextFieldModel<Calendar> modelfield = new TextFieldModel<Calendar>(Calendar.class);
		modelfield.setBinding(new PropertyBinding<Calendar>(date,
		      "value"));
		modelfield.setId(date.getId());

		if(date.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(date.getMetadata(), modelfield, locale, displayMode);
		}
		
		return modelfield;
	}
}
