package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Arrays;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.DeadlineChoice;
import org.xvergabe.xsd.forms.components.x20.DeadlineType;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.elements.fields.DropDownModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;



public class DeadlineTypeFieldModeller implements XmlModeller<org.xvergabe.xsd.forms.components.x20.DeadlineChoice, DropDownModel<DeadlineType>> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public DropDownModel<DeadlineType> createModelFromXml(final org.xvergabe.xsd.forms.components.x20.DeadlineChoice xmlObject,
			final Locale locale, final RoleType.Enum displayMode) {

		// TODO: PropertyBinding korrekt implementieren
//		DropDownModel<DeadLineType> modelfield = new DropDownModel<DeadLineType>("Deadline-Type",
//				Arrays.asList(DeadLineType.values()), DeadLineType.choiceLabeller(), DeadLineType.ANGEBOTSFRIST,
//				DeadLineType.class);
//
//		this.metadataModeller.addMetaDataToModel(xmlObject.getMetadata(),
//				modelfield, locale, displayMode);

		return null;
	}

}
