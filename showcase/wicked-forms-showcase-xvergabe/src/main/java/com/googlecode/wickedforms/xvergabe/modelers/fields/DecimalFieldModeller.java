package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;

public class DecimalFieldModeller implements XmlModeller<org.xvergabe.xsd.forms.components.x20.Decimal, TextFieldModel<Float>> {

	@Override
	public TextFieldModel<Float> createModelFromXml(final org.xvergabe.xsd.forms.components.x20.Decimal xmlObject,
	    final Locale locale, final RoleType.Enum displayMode) {

		TextFieldModel<Float> modelfield = new TextFieldModel<Float>(Float.class);
		modelfield.setBinding(new PropertyBinding<Float>(xmlObject,
		          "value"));
		modelfield.setId(xmlObject.getId());

		if(xmlObject.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(), modelfield, locale, displayMode);
		}
		
		return modelfield;
	}

}
