package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Arrays;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.DropDown;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.elements.fields.ChoiceLabeller;
import com.googlecode.wickedforms.model.elements.fields.DropDownModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class DropDownModeller implements XmlModeller<DropDown, DropDownModel<org.xvergabe.xsd.forms.components.x20.AbstractField>> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public DropDownModel<org.xvergabe.xsd.forms.components.x20.AbstractField> createModelFromXml(final DropDown xmlObject, final Locale locale, final RoleType.Enum displayMode) {
		DropDownModel dropDown = new DropDownModel<org.xvergabe.xsd.forms.components.x20.AbstractField>(XMLBeansUtil.getLabel(xmlObject.getMetadata(), locale), Arrays.asList(xmlObject.getOptionArray()), new ChoiceLabeller<org.xvergabe.xsd.forms.components.x20.AbstractField>() {
			@Override
			public String getLabel(org.xvergabe.xsd.forms.components.x20.AbstractField choice) {
				return XMLBeansUtil.getLabel(choice.getMetadata(), locale);
			}
		}, org.xvergabe.xsd.forms.components.x20.AbstractField.class);
		dropDown.setId(xmlObject.getId());
		
		if(xmlObject.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(), dropDown, locale, displayMode);
		}
		
		return dropDown;
	}
}
