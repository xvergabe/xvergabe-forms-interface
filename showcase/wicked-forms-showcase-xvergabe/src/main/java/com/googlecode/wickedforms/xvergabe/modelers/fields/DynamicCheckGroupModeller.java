package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Arrays;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.Checkbox;
import org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.SectionModel;
import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.buttons.AddSectionButtonModel;
import com.googlecode.wickedforms.model.elements.fields.CheckboxGroupModel;
import com.googlecode.wickedforms.model.elements.fields.ChoiceLabeller;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class DynamicCheckGroupModeller implements
		XmlModeller<DynamicCheckGroup, SectionModel> {

	@Override
	public SectionModel createModelFromXml(final DynamicCheckGroup xmlObject,
			final Locale locale, final RoleType.Enum displayMode) {

		SectionModel section = new SectionModel(XMLBeansUtil.getLabel(
				xmlObject.getMetadata(), locale));

		if (displayMode == RoleType.PROVIDER) {
			// nur Textfelder anzeigen
			Checkbox[] options = xmlObject.getOptionArray();
			for (Checkbox option : options) {
				TextFieldModel<String> textField = new TextFieldModel<String>(
						"", new PropertyBinding<String>(option, "value"),
						String.class);
				textField.setId(xmlObject.getId());
				section.add(textField);
			}
			section.add(new AddSectionButtonModel(
					"Weiteres Textfeld hinzufügen") {
				@Override
				public SectionModel createSection() {
					return null;
				}
			});
		} else if (displayMode == RoleType.CONSUMER) {

			CheckboxGroupModel<Checkbox> checkBoxModel = new CheckboxGroupModel<Checkbox>(
					"", Arrays.asList(xmlObject.getOptionArray()),
					new ChoiceLabeller<Checkbox>() {
						public String getLabel(Checkbox choice) {
							return choice.getValue().getValue();
						}
					}, Arrays.asList(xmlObject.getOptionArray()));
			checkBoxModel.setEnabled(false);
			checkBoxModel.setId(xmlObject.getId());
			section.add(checkBoxModel);
			section.add(new AddSectionButtonModel(
					"Weiteres Dokument hinzufügen") {
				@Override
				public SectionModel createSection() {
					return null;
				}
			});
		}

		return section;

	}
}
