package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.io.Serializable;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.FieldMetaData;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.elements.fields.AbstractInputFieldModel;
import com.lowagie.text.Meta;

public class GeneralMetadataModeller implements Serializable {

	/**
	 * Takes in the model of an input field and the xml bean defining its
	 * metadata to add the metadata to the model accordingly.
	 * 
	 * @param metadata
	 *            the metadata to add to the input field model
	 * @param inputFieldModel
	 *            the input field model
	 */
	public static void addMetaDataToModel(FieldMetaData metadata,
			AbstractInputFieldModel<?> inputFieldModel, Locale locale,
			RoleType.Enum displayMode) {

		if (metadata == null) {
			return;
		}

		for (org.xvergabe.xsd.forms.components.x20.DisplayInformation metaInformation : metadata
				.getDisplayInformationArray()) {

			if (metaInformation.getLang().isEmpty()) {
				metaInformation.setLang("en");
			}

			if (locale.toString().equals(metaInformation.getLang())) {
				inputFieldModel.setLabel(metaInformation.getLabel().getStringValue());
				if (metaInformation.isSetQuickInfo()) {
					inputFieldModel.setHint(metaInformation.getQuickInfo().getStringValue());
				}
			}
		}

		inputFieldModel.setRequired(metadata.getRequired());
		inputFieldModel.setEnabled(isEnabled(displayMode, metadata));

	}

	private static boolean isEnabled(RoleType.Enum displayMode,
			FieldMetaData fieldMetadata) {
		
		if(displayMode == fieldMetadata.getFillingRole()){
			if(fieldMetadata.isSetActive()){
				return fieldMetadata.getActive(); //correct fillingRole, isEnabled depends on "actvie"'s value
			}else
				return true; //default
		}
		return false; //false fillingRole disables field
	}

}
