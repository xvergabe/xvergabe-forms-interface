package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.TextModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;

public class NoteFieldModeller implements
		XmlModeller<org.xvergabe.xsd.forms.components.x20.Note, TextModel> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public TextModel createModelFromXml(
			final org.xvergabe.xsd.forms.components.x20.Note note,
			final Locale locale, final RoleType.Enum displayMode) {

		TextModel modelfield = new TextModel(note.getValue().getValue());
		modelfield.setBinding(new PropertyBinding<String>(note.getValue(),
				"value"));
		modelfield.setId(note.getId());

		return modelfield;
	}

}
