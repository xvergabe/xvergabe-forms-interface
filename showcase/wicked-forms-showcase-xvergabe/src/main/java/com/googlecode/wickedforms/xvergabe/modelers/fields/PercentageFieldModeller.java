package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.DecimalMetaData;
import org.xvergabe.xsd.forms.components.x20.Percentage;
import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.validation.PercentageValidator;

public class PercentageFieldModeller implements
		XmlModeller<Percentage, TextFieldModel<Float>> {

	@Override
	public TextFieldModel<Float> createModelFromXml(
			final Percentage percentage, final Locale locale,
			final RoleType.Enum displayMode) {

		TextFieldModel<Float> modelfield = new TextFieldModel<Float>(
				Float.class);
		modelfield.setId(percentage.getId());
		modelfield.setBinding(new PropertyBinding<Float>(percentage, "value"));

		DecimalMetaData decimalMetaData = percentage.getDecimalMetaData();

		if (decimalMetaData == null) {
			PercentageValidator<Float> validator = new PercentageValidator<Float>(
					100f, 0f, locale);
			modelfield.add(validator);
		} else {
			PercentageValidator<Float> validator = new PercentageValidator<Float>(
					decimalMetaData.getMax(), decimalMetaData.getMin(), locale);
			modelfield.add(validator);
		}

		if (percentage.isSetMetadata()) {
			GeneralMetadataModeller.addMetaDataToModel(
					percentage.getMetadata(), modelfield, locale, displayMode);
		}

		return modelfield;
	}

}
