package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RadioGroup;
import org.xvergabe.xsd.forms.components.x20.RadioOption;
import org.xvergabe.xsd.forms.components.x20.RoleType;
import org.xvergabe.xsd.forms.components.x20.String;

import com.googlecode.wickedforms.model.elements.fields.ChoiceLabeller;
import com.googlecode.wickedforms.model.elements.fields.RadioGroupModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.util.XMLBeansUtil;

public class RadioGroupModeller implements XmlModeller<RadioGroup, RadioGroupModel<org.xvergabe.xsd.forms.components.x20.String>> {

	@Override
	public RadioGroupModel<org.xvergabe.xsd.forms.components.x20.String> createModelFromXml(final RadioGroup xmlObject, final Locale locale, final RoleType.Enum displayMode) {

		List<org.xvergabe.xsd.forms.components.x20.String> choices = new ArrayList<org.xvergabe.xsd.forms.components.x20.String>();
		for(RadioOption choice : xmlObject.getOptionArray()){
			choices.add(choice.getChoice());
		}
		
		RadioGroupModel<org.xvergabe.xsd.forms.components.x20.String> model = new RadioGroupModel<org.xvergabe.xsd.forms.components.x20.String>(
				XMLBeansUtil.getLabel(xmlObject.getMetadata(), locale),
				choices, new ChoiceLabeller<String>() {
			@Override
			public java.lang.String getLabel(String choice) {
				return choice.getValue();
			}
		}, String.class);
		model.setId(xmlObject.getId());

		if(xmlObject.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(), model, locale, displayMode);
		}
		
		return model;
	}
}
