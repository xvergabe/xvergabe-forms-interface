package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;
import org.xvergabe.xsd.forms.components.x20.TenderType;
import org.xvergabe.xsd.forms.components.x20.TenderTypeChoice;

import com.googlecode.wickedforms.model.elements.fields.DropDownModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;

public class TenderTypeFieldModeller implements XmlModeller<org.xvergabe.xsd.forms.components.x20.TenderTypeChoice, DropDownModel<TenderType>> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public DropDownModel<TenderType> createModelFromXml(final org.xvergabe.xsd.forms.components.x20.TenderTypeChoice xmlObject,
			final Locale locale, final RoleType.Enum displayMode) {

		// TODO: Property Binding
//		DropDownModel<TenderTypeChoice> modelfield = new DropDownModel<TenderTypeChoice>("Auswahl",
//				Arrays.asList(TenderType.values()), TenderType.choiceLabeller(), TenderType.OEFFENTLICH,
//				TenderType.class);
//
//		this.metadataModeller.addMetaDataToModel(xmlObject.getMetadata(),
//				modelfield, locale, displayMode);
//
		return null;
	}

}
