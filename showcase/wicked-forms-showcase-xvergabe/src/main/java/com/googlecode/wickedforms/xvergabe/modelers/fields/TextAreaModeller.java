package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.fields.TextAreaModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;

public class TextAreaModeller implements XmlModeller<org.xvergabe.xsd.forms.components.x20.String, TextAreaModel> {

	@Override
	public TextAreaModel createModelFromXml(final org.xvergabe.xsd.forms.components.x20.String xmlObject, final Locale locale, final RoleType.Enum displayMode) {

		TextAreaModel modelfield = new TextAreaModel();
		modelfield.setId(xmlObject.getId());
		modelfield.setBinding(new PropertyBinding<String>(xmlObject, "value"));

		if(xmlObject.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(), modelfield, locale, displayMode);
		}

		return modelfield;
	}
}
