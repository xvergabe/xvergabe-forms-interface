package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.TextModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;

public class TextElementModeller implements
	XmlModeller<org.xvergabe.xsd.forms.components.x20.String, TextModel> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public TextModel createModelFromXml(final org.xvergabe.xsd.forms.components.x20.String string,
			final Locale locale, final RoleType.Enum displayMode) {

		TextModel modelfield = new TextModel(string.getValue());
		modelfield.setBinding(new PropertyBinding<String>(string,
				"value"));
		modelfield.setId(string.getId());

		return modelfield;
	}

}
