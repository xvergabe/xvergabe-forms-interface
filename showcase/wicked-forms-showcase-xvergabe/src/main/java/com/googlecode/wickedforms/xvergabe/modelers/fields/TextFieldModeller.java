package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;

public class TextFieldModeller implements
    XmlModeller<org.xvergabe.xsd.forms.components.x20.String, TextFieldModel<String>> {

	@Override
	public TextFieldModel<String> createModelFromXml(final org.xvergabe.xsd.forms.components.x20.String xmlObject,
	    final Locale locale, final RoleType.Enum displayMode) {

		TextFieldModel<String> modelfield = new TextFieldModel<String>(String.class);
		modelfield.setBinding(new PropertyBinding<String>(xmlObject,
		    "value"));
		modelfield.setId(xmlObject.getId());
		
		if(xmlObject.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(), modelfield, locale, displayMode);
		} 

		return modelfield;
	}
}
