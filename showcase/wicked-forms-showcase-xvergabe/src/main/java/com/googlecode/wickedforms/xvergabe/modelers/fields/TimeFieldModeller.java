package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Calendar;
import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;

public class TimeFieldModeller implements XmlModeller<org.xvergabe.xsd.forms.components.x20.Time, TextFieldModel<Calendar>> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public TextFieldModel<Calendar> createModelFromXml(final org.xvergabe.xsd.forms.components.x20.Time xmlObject,
	    final Locale locale, final RoleType.Enum displayMode) {

		TextFieldModel<Calendar> modelfield = new TextFieldModel<Calendar>(Calendar.class);
		modelfield.setBinding(new PropertyBinding<Calendar>(xmlObject,
		      "value"));
		modelfield.setId(xmlObject.getId());

		this.metadataModeller.addMetaDataToModel(xmlObject.getMetadata(),
		      modelfield, locale, displayMode);

		if(xmlObject.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(), modelfield, locale, displayMode);
		}
		return modelfield;
	}

}
