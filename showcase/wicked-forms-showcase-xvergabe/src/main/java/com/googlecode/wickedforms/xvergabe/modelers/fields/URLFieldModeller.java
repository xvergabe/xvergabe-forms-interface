package com.googlecode.wickedforms.xvergabe.modelers.fields;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.RoleType;
import org.xvergabe.xsd.forms.components.x20.Url;

import com.googlecode.wickedforms.model.binding.PropertyBinding;
import com.googlecode.wickedforms.model.elements.fields.TextFieldModel;
import com.googlecode.wickedforms.xvergabe.modelers.XmlModeller;
import com.googlecode.wickedforms.xvergabe.validation.URLValidator;

public class URLFieldModeller implements XmlModeller<Url, TextFieldModel<String>> {

	GeneralMetadataModeller metadataModeller = new GeneralMetadataModeller();

	@Override
	public TextFieldModel<String> createModelFromXml(final Url xmlObject,
	    final Locale locale, final RoleType.Enum displayMode) {

		TextFieldModel<String> modelfield = new TextFieldModel<String>(String.class);
		modelfield.setBinding(new PropertyBinding<String>(xmlObject,
		    "value"));
		modelfield.setId(xmlObject.getId());

		this.metadataModeller.addMetaDataToModel(xmlObject.getMetadata(),
		    modelfield, locale, displayMode);

		URLValidator validator = new URLValidator(xmlObject.getValue(),
		      locale);
		modelfield.add(validator);

		if(xmlObject.isSetMetadata()){
			GeneralMetadataModeller.addMetaDataToModel(xmlObject.getMetadata(), modelfield, locale, displayMode);
		}
		
		return modelfield;
	}

}
