package com.googlecode.wickedforms.xvergabe.types;

import java.util.Locale;

import com.googlecode.wickedforms.xvergabe.util.ResourceMessage;
import com.googlecode.wickedforms.xvergabe.util.ResourceUtil;

public enum ComponentType implements ResourceMessage {

	DESCRIPTION,
	CONTACT,
	NATURALPERSON,
	ADDRESS,
	ORGANIZATION,
	FORMDESCRIPTION,
	DEADLINE,
	DOCUMENTHEADER,
	REFERENCEINFORMATION,
	INREFERENCETO,
	SIGNATURE,
	AGREEMENT,
	REMISSION,
	DISCOUNT,
	PRICE,
	FIELD,
	SECTION,
	LOT,
	LOTS,
	PREQUALIFIKATION,
	PROCESSING_INFORMATION,
	ORGANIZATION_FEATURES,
	PROVIDEDDOCUMENTS,
	COUNTHISTORY,
	XORSECTION,
	ORSECTION;

	@Override
	public String getMessage() {
		return ResourceUtil.getMessageString(this);
	}

	@Override
	public String getMessage(final Locale locale) {
		return ResourceUtil.getMessageString(this, locale);
	}

	@Override
	public String getMessage(final Locale locale, final Object... params) {
		return ResourceUtil.getMessageString(this, locale, params);
	}

	@Override
	public String getMessage(final Object... params) {
		return ResourceUtil.getMessageString(this, params);
	}

}
