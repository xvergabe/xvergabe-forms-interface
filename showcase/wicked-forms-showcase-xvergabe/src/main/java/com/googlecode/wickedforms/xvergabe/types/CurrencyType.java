package com.googlecode.wickedforms.xvergabe.types;

import java.io.Serializable;

import com.googlecode.wickedforms.model.elements.fields.ChoiceLabeller;

public enum CurrencyType implements Serializable {

	EUR("Euro"),
	USD("US-Dollar"),
	GBP("British Pound");

	private final String label;

	private CurrencyType(final String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}

	public static ChoiceLabeller<CurrencyType> choiceLabeller() {
		return new ChoiceLabeller<CurrencyType>() {

			@Override
			public String getLabel(final CurrencyType choice) {
				return choice.getLabel();
			}
		};
	}

}
