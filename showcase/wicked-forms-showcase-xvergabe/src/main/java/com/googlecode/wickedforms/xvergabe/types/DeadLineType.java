package com.googlecode.wickedforms.xvergabe.types;

import java.io.Serializable;

import com.googlecode.wickedforms.model.elements.fields.ChoiceLabeller;

public enum DeadLineType implements Serializable {

	BINDEFRIST("Bindefrist"),
	ANGEBOTSFRIST("Angebotsfrist");

	private final String label;

	private DeadLineType(final String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}

	public static ChoiceLabeller<DeadLineType> choiceLabeller() {
		return new ChoiceLabeller<DeadLineType>() {

			@Override
			public String getLabel(final DeadLineType choice) {
				return choice.getLabel();
			}
		};
	}

}
