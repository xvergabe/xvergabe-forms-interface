package com.googlecode.wickedforms.xvergabe.types;

import java.io.Serializable;

import com.googlecode.wickedforms.model.elements.fields.ChoiceLabeller;

public enum TenderType implements Serializable {

	OEFFENTLICH("Oeffentliche Ausschreibung"),
	BESCHRAENKT("Beschraenkte Ausschreibung"),
	OEFFENTLICH_MIT_TEILNAHMEWETTBEWERB("Beschraenkte Ausschreibung mit Teilnahmewettbewerb"),
	FREIHAENDIG("Freihaendige Vergabe"),
	FREIHAENDIG_MIT_TEILNAHMEWETTBEWERB("Freihaendige Vergabe mit Teilnahmewettbewerb");

	private final String label;

	private TenderType(final String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}

	public static ChoiceLabeller<TenderType> choiceLabeller() {
		return new ChoiceLabeller<TenderType>() {

			@Override
			public String getLabel(final TenderType choice) {
				return choice.getLabel();
			}
		};
	}

}
