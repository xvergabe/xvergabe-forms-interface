package com.googlecode.wickedforms.xvergabe.util;

import java.io.Serializable;
import java.util.Locale;

public interface ResourceMessage extends Serializable {

	public String getMessage();

	public String getMessage(final Locale locale);

	public String getMessage(final Locale locale, final Object... params);

	public String getMessage(final Object... params);

}
