package com.googlecode.wickedforms.xvergabe.util;

import java.util.Locale;

import org.xvergabe.xsd.forms.components.x20.AbstractComponent;
import org.xvergabe.xsd.forms.components.x20.AbstractField;
import org.xvergabe.xsd.forms.components.x20.ComponentMetaData;
import org.xvergabe.xsd.forms.components.x20.DisplayInformation;
import org.xvergabe.xsd.forms.components.x20.FieldMetaData;


public class XMLBeansUtil {

	/**
	 * Utility method to access the label defined for a specific locale within a
	 * metadata object.
	 * 
	 * @param metadata
	 *            the metadate to read the label from
	 * @param locale
	 *            the locale in which to retrieve the label
	 * @return the label or null if no label in the specified locale was
	 *         defined.
	 */
	public static String getLabel(FieldMetaData metadata, Locale locale) {
		if (metadata != null && metadata.getDisplayInformationArray() != null) {
			for (DisplayInformation display : metadata.getDisplayInformationArray()) {
				if (locale.getLanguage().equals(display.getLang())) {
					return display.getLabel().getStringValue();
				}
			}
		}
		return null;
	}
	
	public static String getLabel(AbstractField field, Locale locale){
		if(field.isSetLabelled() && !field.getLabelled()){
			return "";
		}else{
			return getLabel(field.getMetadata(), locale);
		}
	}
	
	/**
	 * Utility method to access the label defined for a specific locale within a
	 * metadata object.
	 * 
	 * @param metadata
	 *            the metadate to read the label from
	 * @param locale
	 *            the locale in which to retrieve the label
	 * @return the label or null if no label in the specified locale was
	 *         defined.
	 */
	public static String getLabel(ComponentMetaData metadata, Locale locale) {
		if (metadata != null && metadata.getDisplayInformationArray() != null) {
			for (DisplayInformation display : metadata.getDisplayInformationArray()) {
				if (locale.getLanguage().equals(display.getLang())) {
					return display.getLabel().getStringValue();
				}
			}
		}
		return null;
	}
	
	/**
	 * Determines if a component should be displayed in a form or not.
	 * 
	 * @param component
	 *            the component whose visibility to determine
	 * @return true, if the component should be visible to the user, false if
	 *         not.
	 */
	public static boolean isVisible(AbstractComponent component) {
		if (component == null) {
			return false;
		} else {
			return isVisible(component.getMetadata());
		}
	}
	
	public static boolean isVisible(AbstractField field) {
		if (field == null) {
			return false;
		} else {
			return isVisible(field.getMetadata());
		}
	}

	public static boolean isVisible(ComponentMetaData metadata) {
		if (metadata == null || !metadata.isSetVisible()) {
			// default if not specified
			return true;
		} else {
			return metadata.getVisible();
		}
	}
	
	public static boolean isVisible(FieldMetaData metadata) {
		if (metadata == null || !metadata.isSetVisible()) {
			// default if not specified
			return true;
		} else {
			return metadata.getVisible();
		}
	}

}
