package com.googlecode.wickedforms.xvergabe.validation;

import java.util.Locale;

import com.googlecode.wickedforms.model.elements.fields.AbstractInputFieldModel;
import com.googlecode.wickedforms.model.validation.FieldValidatorModel;
import com.googlecode.wickedforms.model.validation.ValidationFeedback;

public class MaximumValidator<T extends Number> implements FieldValidatorModel<T> {

	private final T max;

	private final Locale locale;

	public MaximumValidator(final T max, final Locale locale) {
		this.max = max;
		this.locale = locale;
	}

	@Override
	public void validate(final AbstractInputFieldModel<T> inputField, final T value, final ValidationFeedback feedback) {
		if (value.doubleValue() > this.max.doubleValue()) {
			feedback.error(ValidationMessage.VALUE_ABOVE_MAXIMUM.getMessage(this.locale, inputField.getLabel(), this.max));
		}

	}
}
