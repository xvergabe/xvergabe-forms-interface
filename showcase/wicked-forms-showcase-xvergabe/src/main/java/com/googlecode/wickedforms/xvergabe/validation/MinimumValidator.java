package com.googlecode.wickedforms.xvergabe.validation;

import java.util.Locale;

import com.googlecode.wickedforms.model.elements.fields.AbstractInputFieldModel;
import com.googlecode.wickedforms.model.validation.FieldValidatorModel;
import com.googlecode.wickedforms.model.validation.ValidationFeedback;

public class MinimumValidator<T extends Number> implements FieldValidatorModel<T> {

	private final T min;

	private final Locale locale;

	public MinimumValidator(final T min, final Locale locale) {
		this.min = min;
		this.locale = locale;
	}

	@Override
	public void validate(final AbstractInputFieldModel<T> inputField, final T value, final ValidationFeedback feedback) {
		if (value.doubleValue() < this.min.doubleValue()) {
			feedback.error(ValidationMessage.VALUE_BELOW_MINIMUM.getMessage(this.locale, inputField.getLabel(), this.min));
		}

	}
}
