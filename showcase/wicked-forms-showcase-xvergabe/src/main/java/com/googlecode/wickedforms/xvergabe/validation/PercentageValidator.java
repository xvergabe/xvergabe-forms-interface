package com.googlecode.wickedforms.xvergabe.validation;

import java.util.Locale;

import com.googlecode.wickedforms.model.elements.fields.AbstractInputFieldModel;
import com.googlecode.wickedforms.model.validation.FieldValidatorModel;
import com.googlecode.wickedforms.model.validation.ValidationFeedback;

public class PercentageValidator<T extends Number> implements FieldValidatorModel<T> {

	private final T max;

	private final T min;

	private final Locale locale;

	public PercentageValidator(final T max, final T min, final Locale locale) {
		this.max = max;
		this.min = min;
		this.locale = locale;
	}

	@Override
	public void validate(final AbstractInputFieldModel<T> inputField, final T value, final ValidationFeedback feedback) {
		if (value.doubleValue() > this.max.doubleValue()) {
			feedback.error(ValidationMessage.VALUE_NOT_PERCENT_MAX.getMessage(this.locale, inputField.getLabel(), this.max));
		} else if (value.doubleValue() < this.min.doubleValue()) {
			feedback.error(ValidationMessage.VALUE_NOT_PERCENT_MIN.getMessage(this.locale, inputField.getLabel(), this.max));
		}

	}
}
