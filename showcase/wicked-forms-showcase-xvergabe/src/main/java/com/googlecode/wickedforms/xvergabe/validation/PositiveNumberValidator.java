package com.googlecode.wickedforms.xvergabe.validation;

import java.util.Locale;

import com.googlecode.wickedforms.model.elements.fields.AbstractInputFieldModel;
import com.googlecode.wickedforms.model.validation.FieldValidatorModel;
import com.googlecode.wickedforms.model.validation.ValidationFeedback;

public class PositiveNumberValidator<T extends Number> implements FieldValidatorModel<T> {

	private final Locale locale;

	public PositiveNumberValidator(final T number, final Locale locale) {
		this.locale = locale;
	}

	@Override
	public void validate(final AbstractInputFieldModel<T> inputField, final T value, final ValidationFeedback feedback) {
		if (value.doubleValue() < 0) {
			feedback.error(ValidationMessage.VALUE_BELOW_ZERO.getMessage(this.locale, inputField.getLabel()));
		}

	}
}
