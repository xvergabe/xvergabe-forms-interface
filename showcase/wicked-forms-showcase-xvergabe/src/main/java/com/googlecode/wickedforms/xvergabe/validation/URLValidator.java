package com.googlecode.wickedforms.xvergabe.validation;

import java.util.Locale;

import com.googlecode.wickedforms.model.elements.fields.AbstractInputFieldModel;
import com.googlecode.wickedforms.model.validation.FieldValidatorModel;
import com.googlecode.wickedforms.model.validation.ValidationFeedback;

public class URLValidator implements FieldValidatorModel<String> {

	private final String url;

	private final Locale locale;

	public URLValidator(final String url, final Locale locale) {
		this.url = url;
		this.locale = locale;
	}

	@Override
	public void validate(final AbstractInputFieldModel<String> inputField, final String value,
	    final ValidationFeedback feedback) {
		if (!value
		    .matches("(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?")) {
			feedback.error(ValidationMessage.VALUE_NOT_VALID_URL.getMessage(this.locale, this.url));
		}
	}

}
