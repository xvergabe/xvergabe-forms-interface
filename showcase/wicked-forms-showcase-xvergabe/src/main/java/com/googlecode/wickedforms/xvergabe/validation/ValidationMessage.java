package com.googlecode.wickedforms.xvergabe.validation;

import java.util.Locale;

import com.googlecode.wickedforms.xvergabe.util.ResourceMessage;
import com.googlecode.wickedforms.xvergabe.util.ResourceUtil;

public enum ValidationMessage implements ResourceMessage {

	VALUE_ABOVE_MAXIMUM,
	VALUE_BELOW_MINIMUM,
	VALUE_BELOW_ZERO,
	VALUE_NOT_PERCENT_MAX,
	VALUE_NOT_PERCENT_MIN,
	VALUE_NOT_VALID_URL;

	@Override
	public String getMessage() {
		return ResourceUtil.getMessageString(this);
	}

	@Override
	public String getMessage(final Locale locale) {
		return ResourceUtil.getMessageString(this, locale);
	}

	@Override
	public String getMessage(final Locale locale, final Object... params) {
		return ResourceUtil.getMessageString(this, locale, params);
	}

	@Override
	public String getMessage(final Object... params) {
		return ResourceUtil.getMessageString(this, params);
	}

}
