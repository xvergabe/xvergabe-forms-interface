package com.googlecode.wickedforms.xvergabe.xml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.apache.xmlbeans.XmlValidationError;
import org.xvergabe.xsd.forms.components.x20.FormDocument;
import org.xvergabe.xsd.forms.components.x20.FormDocument.Form;

/**
 * Reads XML from a given file.
 * 
 * @author hombergs
 * 
 */
public class FormXmlFileReader implements FormXmlReader {

	private final File xmlFile;

	public FormXmlFileReader(File xmlFile) {
		this.xmlFile = xmlFile;
	}

	@Override
	public Form readFromXml() throws XmlException, IOException {
		FormDocument xmlFormDocument = FormDocument.Factory.parse(xmlFile);
		return xmlFormDocument.getForm();
	}

	@Override
	public List<String> validate() throws XmlException, IOException {
		List<XmlValidationError> validationErrors = new ArrayList<XmlValidationError>();
		XmlOptions options = new XmlOptions();
		options.setErrorListener(validationErrors);
		FormDocument xmlFormDocument = FormDocument.Factory.parse(xmlFile);
		xmlFormDocument.validate(options);

		List<String> resultList = new ArrayList<String>();
		for (XmlValidationError error : validationErrors) {
			resultList.add(error.getMessage() + " (line " + error.getLine() + ")");
		}

		return resultList;
	}

}
