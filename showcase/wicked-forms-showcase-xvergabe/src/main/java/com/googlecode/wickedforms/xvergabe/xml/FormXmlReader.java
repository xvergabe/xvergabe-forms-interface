package com.googlecode.wickedforms.xvergabe.xml;

import java.io.IOException;
import java.util.List;

import org.apache.xmlbeans.XmlException;
import org.xvergabe.xsd.forms.components.x20.FormDocument.Form;

public interface FormXmlReader {

	/**
	 * Reads an xml stream from a datasource defined by the implementation and
	 * returns the root element of type form as java bean.
	 * 
	 * @return the {@link Form} bean representing the xml root element.
	 */
	Form readFromXml() throws XmlException, IOException;

	/**
	 * Validates the form against the XSD.
	 * 
	 * @return list of the validation errors. If this list is empty the
	 *         validation was successful.
	 */
	List<String> validate() throws XmlException, IOException;
}
