package com.googlecode.wickedforms.xvergabe.util;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;

public class ResourceUtilTest {

	@Test
	public void testDe() {
		Assert.assertEquals("test_de_1",
		    TestMessage.MESSAGE1.getMessage(Locale.GERMAN));
	}

	@Test
	public void testEn() {
		Assert.assertEquals("test_en_1",
		    TestMessage.MESSAGE1.getMessage(Locale.ENGLISH));
	}

}
