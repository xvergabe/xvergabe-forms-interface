package com.googlecode.wickedforms.xvergabe.util;

import java.util.Locale;

public enum TestMessage implements ResourceMessage {

	MESSAGE1,

	MESSAGE2;

	@Override
	public String getMessage() {
		return ResourceUtil.getMessageString(this);
	}

	@Override
	public String getMessage(final Locale locale) {
		return ResourceUtil.getMessageString(this, locale);
	}

	@Override
	public String getMessage(final Locale locale, final Object... params) {
		return ResourceUtil.getMessageString(this, locale, params);
	}

	@Override
	public String getMessage(final Object... params) {
		return ResourceUtil.getMessageString(this, params);
	}

}
