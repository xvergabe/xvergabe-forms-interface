package com.googlecode.wickedforms.xvergabe.validator;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;

import com.googlecode.wickedforms.xvergabe.validation.ValidationMessage;

public class ValidationMessageTest {

	/**
	 * This method tests if all enumeration constants of
	 * {@link ValidationMessage} have a default value, a german value and an
	 * english value listed in the respective properties files.
	 */
	@Test
	public void test() {
		String className = ValidationMessage.class.getSimpleName();
		for (ValidationMessage message : ValidationMessage.values()) {
			Assert.assertNotNull(className + "." + message + " is missing in " + className + ".properties!",
					message.getMessage());
			Assert.assertNotNull(className + "." + message + " is missing in " + className + ".properties!",
					message.getMessage(Locale.GERMAN));
			Assert.assertNotNull(className + "." + message + " is missing in " + className + ".properties!",
					message.getMessage(Locale.ENGLISH));
		}
	}

}
