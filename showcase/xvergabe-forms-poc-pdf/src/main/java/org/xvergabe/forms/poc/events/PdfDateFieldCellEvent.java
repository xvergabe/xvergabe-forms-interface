package org.xvergabe.forms.poc.events;

import java.io.IOException;

import org.xvergabe.forms.poc.helper.JavaScriptGenerator;
import org.xvergabe.xsd.forms.components.x20.Date;
import org.xvergabe.xsd.forms.components.x20.String;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TextField;

/**
 * einer {@link cell} zugef�gt bewirkt dieses Event die Darstellung eines
 * Datumseingabefeldes in dieser Zelle
 * 
 * @author schiller
 * 
 */
public class PdfDateFieldCellEvent implements PdfPCellEvent {

	Date date;

	public PdfDateFieldCellEvent(Date date) {
		this.date = date;
	}

	@Override
	public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {

		PdfWriter writer = canvases[0].getPdfWriter();
		TextField textfield = new TextField(writer, position, date.getId());
		
		if(date.isSetValue())
			textfield.setText(date.getValue().toString());
		else
			textfield.setText("dd.mm.yyyy");
		
		//wenn Feld initial nicht aktiv, wird ein JS gesetzt welches die komponente initial deaktiviert 
		if(date.isSetMetadata() && date.getMetadata().isSetActive() && !date.getMetadata().getActive())
			JavaScriptGenerator.getInstance().addInitialConfig("getField(\""+date.getId()+"\").readonly = true;");
		
		try {
			PdfFormField datefield = textfield.getTextField();
			datefield.setAdditionalActions(PdfName.V, PdfAction.javaScript(
	                "AFDate_FormatEx( 'dd.mm.yyyy' );", writer));
			writer.addAnnotation(datefield);
			//bzgl. JavaScript: es wird jedem Field eine superfunction zugef�gt. Existiert kein Toggle f�r dieses Feld bleibt diese Funktion leer
			//TODO es ist zu �berlegen ob und auf welche weise nur Elementen mit Togglen eine Funktion zugeschrieben wird
			datefield.setAdditionalActions(PdfName.BL, PdfAction.javaScript(date.getId().replace(".", "")+"()", writer));
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
