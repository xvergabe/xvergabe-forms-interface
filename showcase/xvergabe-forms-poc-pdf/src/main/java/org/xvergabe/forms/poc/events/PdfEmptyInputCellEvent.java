package org.xvergabe.forms.poc.events;

import java.io.IOException;

import org.xvergabe.forms.poc.helper.JavaScriptGenerator;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TextField;

/**
 * einer {@link cell} zugef�gt bewirkt dieses Event die Darstellung eines
 * Eingabefeldes in dieser Zelle
 * 
 * @author schiller
 * 
 */
public class PdfEmptyInputCellEvent implements PdfPCellEvent {

	String field;//id for within the PDF for JavaScript referencing 

	public PdfEmptyInputCellEvent(String field) {
		this.field = field;
	}

	@Override
	public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {

		PdfWriter writer = canvases[0].getPdfWriter();
		TextField textfield = new TextField(writer, position, field);
		
		try {
			PdfFormField text = textfield.getTextField();
			writer.addAnnotation(text);
			//bzgl. JavaScript: es wird jedem Field eine superfunction zugef�gt. Existiert kein Toggle f�r dieses Feld bleibt diese Funktion leer
			//TODO es ist zu �berlegen ob und auf welche weise nur Elementen mit Togglen eine Funktion zugeschrieben wird
			text.setAdditionalActions(PdfName.BL, PdfAction.javaScript(field.replace(".", "")+"()", writer));
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
