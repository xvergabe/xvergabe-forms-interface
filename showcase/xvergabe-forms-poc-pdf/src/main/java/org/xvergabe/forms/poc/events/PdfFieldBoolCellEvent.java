package org.xvergabe.forms.poc.events;

import java.io.IOException;

import org.xvergabe.forms.poc.helper.JavaScriptGenerator;
import org.xvergabe.xsd.forms.components.x20.Boolean;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RadioCheckField;

/**
 * einer {@link cell} zugef�gt bewirkt dieses Event die Darstellung einer
 * Checkbox in dieser Zelle
 * 
 * @author schiller
 * 
 */
public class PdfFieldBoolCellEvent implements PdfPCellEvent {

	Boolean field;
	
	public PdfFieldBoolCellEvent(Boolean field) {
		this.field = field;
	}

	@Override
	public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {
		PdfWriter writer = canvases[0].getPdfWriter();
		RadioCheckField checkbox = new RadioCheckField(writer, position, field.getId(), "on");
		checkbox.setChecked(field.getValue());
		checkbox.setCheckType(RadioCheckField.TYPE_CHECK);
		
		
		//wenn Feld initial nicht aktiv, wird ein JS gesetzt welches die komponente initial deaktiviert 
		if(field.isSetMetadata() && field.getMetadata().isSetActive() && !field.getMetadata().getActive())
			JavaScriptGenerator.getInstance().addInitialConfig("getField(\""+field.getId()+"\").readonly = true;");
		
		try {
			PdfFormField field = checkbox.getCheckField();
			writer.addAnnotation(field);
			//bzgl. JavaScript: es wird jedem Field eine superfunction zugef�gt. Existiert kein Toggle f�r dieses Feld bleibt diese Funktion leer
			//TODO es ist zu �berlegen ob und auf welche weise nur Elementen mit Togglen eine Funktion zugeschrieben wird
			field.setAction(PdfAction.javaScript(this.field.getId().replace(".", "")+"()", writer));
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
