package org.xvergabe.forms.poc.events;

import java.io.IOException;

import org.xvergabe.forms.poc.helper.JavaScriptGenerator;
import org.xvergabe.xsd.forms.components.x20.Integer;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TextField;

/**
 * einer {@link cell} zugef�gt bewirkt dieses Event die Darstellung eines
 * Ganzzahl-Eingabefeldes in dieser Zelle
 * 
 * @author schiller
 * 
 */
public class PdfIntegerFieldCellEvent implements PdfPCellEvent {

	Integer integer;

	public PdfIntegerFieldCellEvent(Integer integer) {
		
		this.integer = integer;
	}

	@Override
	public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {

		PdfWriter writer = canvases[0].getPdfWriter();
		TextField textfield = new TextField(writer, position, integer.getId());
		
		if(integer.isSetValue())
			textfield.setText(java.lang.Integer.toString(integer.getValue()));
		
		//wenn Feld initial nicht aktiv, wird ein JS gesetzt welches die komponente initial deaktiviert 
		if(integer.isSetMetadata() && integer.getMetadata().isSetActive() && !integer.getMetadata().getActive())
			JavaScriptGenerator.getInstance().addInitialConfig("getField(\""+integer.getId()+"\").readonly = true;");
		
		try {
			PdfFormField datefield = textfield.getTextField();
			//TODO JavaScript Pr�fung auf Integer implementieren. Dazu an dieser Stelle den folgenden Aufruf al 2. Paramter eine geeignete JS Function zuf�gen: 
//			JavaScriptGenerator.getInstance().addFunction(integer.getId(), "");
			writer.addAnnotation(datefield);
			//bzgl. JavaScript: es wird jedem Field eine superfunction zugef�gt. Existiert kein Toggle f�r dieses Feld bleibt diese Funktion leer
			//TODO es ist zu �berlegen ob und auf welche weise nur Elementen mit Togglen eine Funktion zugeschrieben wird
			datefield.setAdditionalActions(PdfName.BL, PdfAction.javaScript(integer.getId().replace(".", "")+"()", writer));
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
