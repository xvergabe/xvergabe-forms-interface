package org.xvergabe.forms.poc.events;

import java.io.IOException;

import org.xvergabe.forms.poc.helper.JavaScriptGenerator;
import org.xvergabe.xsd.forms.components.x20.Integer;
import org.xvergabe.xsd.forms.components.x20.Percentage;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TextField;

/**
 * einer {@link cell} zugef�gt bewirkt dieses Event die Darstellung eines
 * Prozent-Eingabefeldes in dieser Zelle
 * 
 * @author schiller
 * 
 */
public class PdfPercentageFieldCellEvent implements PdfPCellEvent {

	Percentage percent;

	public PdfPercentageFieldCellEvent(Percentage percent) {
		
		this.percent = percent;
	}

	@Override
	public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases) {

		PdfWriter writer = canvases[0].getPdfWriter();
		TextField textfield = new TextField(writer, position, percent.getId());
		
		if(percent.isSetValue())
			textfield.setText(java.lang.Double.toString(percent.getValue()));
		
		//wenn Feld initial nicht aktiv, wird ein JS gesetzt welches die komponente initial deaktiviert 
		if(percent.isSetMetadata() && percent.getMetadata().isSetActive() && !percent.getMetadata().getActive())
			JavaScriptGenerator.getInstance().addInitialConfig("getField(\""+percent.getId()+"\").readonly = true;");
		
		try {
			PdfFormField percentagefield = textfield.getTextField();
			//TODO JavaScript Pr�fung auf percentage implementieren. Dazu an dieser Stelle den folgenden Aufruf al 2. Paramter eine geeignete JS Function zuf�gen: 
//			JavaScriptGenerator.getInstance().addFunction(percentage.getId(), "");
			writer.addAnnotation(percentagefield);
			//bzgl. JavaScript: es wird jedem Field eine superfunction zugef�gt. Existiert kein Toggle f�r dieses Feld bleibt diese Funktion leer
			//TODO es ist zu �berlegen ob und auf welche weise nur Elementen mit Togglen eine Funktion zugeschrieben wird
			percentagefield.setAdditionalActions(PdfName.BL, PdfAction.javaScript(percent.getId().replace(".", "")+"()", writer));
		} catch (DocumentException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
