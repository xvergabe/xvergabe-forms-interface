package org.xvergabe.forms.poc.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.xvergabe.xsd.forms.components.x20.FormMetaData;
import org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle;

/**
 * This class generates all JavaScript functions derived from the given
 * FormMetaDatas Toggles
 * 
 * Every Field in the PDF has its own Super-Function which is called on an
 * action. This Functions calls different Sub-Functions each representing one
 * toggle corresponding to those configured in the Forms Metadata
 * 
 * @author schiller
 * 
 */
public class JavaScriptGenerator {

	private HashMap<String, StringBuilder> functionCatalog; 
	private ArrayList<String> initialConfigs; 
	private static JavaScriptGenerator instance = null;
	
	private JavaScriptGenerator(){
		functionCatalog = new HashMap<String, StringBuilder>();
		initialConfigs = new ArrayList<String>();
	}
	
	public static JavaScriptGenerator getInstance(){
		if (instance == null)
			instance = new JavaScriptGenerator();
		return instance;
	}
	
	/**
	 * Add JavaScript functioncode to a superfunction of a Field
	 * @param superfunctionName named like the the field 
	 * @param subfunctionCode the JavaScript code to be executed in the fields standard case
	 * Textfields = onBlur
	 * Radiobutton/Checkbox onMouseUp
	 * 
	 */
	public void addFunction(String superfunctionName, String subfunctionCode){
		addSubFunctionToCatalog(superfunctionName, subfunctionCode);
	}
	
	public String getInitialConfigs(){
		StringBuilder configs = new StringBuilder();
		for(String config : initialConfigs){
			configs.append(config+"\n");
		}
		return configs.toString();
	}
	
	public void addInitialConfig(String config){
		initialConfigs.add(config);
	}
	/**
	 * generates all Super-Functions and Sub-Functions derived from the given
	 * FormMetaDatas Toggles.
	 * 
	 * 
	 * @param formMetaData
	 * @return
	 */
	public String getFunctions(FormMetaData formMetaData) {

		//dynamic implementation of the subfunctions, implicitly creating the superFunctionscatalog 
		String subFunctions = getSubFunctions(formMetaData);
		 
		//finilaze the superfunctions
		String superFunctions = getSuperFunctions();

		return superFunctions + subFunctions;
	}

	/**
	 * This function creates the Super-Functions for the Formfields. It calls all
	 * Sub-Functions each corresponding to one Toggle.
	 * 
	 * @param formMetaData
	 * @return
	 */
	private  String getSuperFunctions() {

		//iteration through the functioncatalog. the superfunctionbodies allready exist.
		//they were created during the generation of the subfunctions
		//they only have to be finalized for the JS syntax
		Iterator it = functionCatalog.values().iterator();
		String superFunctions = new String();
		while (it.hasNext()) {
			StringBuilder function = (StringBuilder) it.next();
			superFunctions = superFunctions + function.toString() + "}\n";
		}

		return superFunctions;
	}

	/**
	 * 
	 * This function generates the real implementations of the toggles
	 * 
	 * @param formMetaData
	 * @return
	 */
	private String getSubFunctions(FormMetaData formMetaData) {

		ActivationToggle[] activationToggles = formMetaData.getActivationToggleArray();
		ActivationToggle activationToggle;
		String functions = new String();
		for (int i = 0; i < activationToggles.length; i++) {
			activationToggle = activationToggles[i];
			if(activationToggle.isSetTriggerValue())
				functions = functions + getActivateOnValueToggle(activationToggle) ;
			else //if there is no triggervalue for now it is assumed the toggle is for a checkbox/radiobutton
				functions = functions + getActivateOnTriggerActive(activationToggle) ;
		}
		return functions;
	}

	private String getActivateOnTriggerActive(ActivationToggle activationToggle) {
		
		String subFunctionName;

		String function = new String();
		//create individual subfunctionname
		subFunctionName = activationToggle.getTriggerField() + "_activate_" + activationToggle.getTargetField() + "_on_trigger_active";
		subFunctionName = subFunctionName.replace(".", "");
		
		//add subfunctionname to the superfunction entry in the catalog (the key is the triggerFields Id)
		addSubFunctionToCatalog(activationToggle.getTriggerField(), subFunctionName);
		
		//implementation of the subfunction
		function = function + "function " + subFunctionName + "(){" + "if(getField(\"" + activationToggle.getTriggerField() + "\").value==\"on\"){getField(\"" + activationToggle.getTargetField() + "\").readonly = false;}else{getField(\""
				+ activationToggle.getTargetField() + "\").readonly = true;}}\n";
		
		return function;
	}

	/**
	 * gets the implementation of an activation toggle.  
	 * @param activationToggle
	 * @return
	 */
	private String getActivateOnValueToggle(ActivationToggle activationToggle) {
		
		String subFunctionName;

		String function = new String();
		//create individual subfunctionname
		subFunctionName = activationToggle.getTriggerField() + "_activate_" + activationToggle.getTargetField() + "_on_" + activationToggle.getTriggerValue();
		subFunctionName = subFunctionName.replace(".", "");
		
		//add subfunctionname to the superfunction entry in the catalog (the key is the triggerFieldId)
		addSubFunctionToCatalog(activationToggle.getTriggerField(), subFunctionName);
		
		//implementation of the subfunction
		function = function + "function " + subFunctionName + "(){" + "if(getField(\"" + activationToggle.getTriggerField() + "\").value==\"" + activationToggle.getTriggerValue() + "\"){" + "getField(\"" + activationToggle.getTargetField() + "\").readonly = false;" + "}else{" + "getField(\""
				+ activationToggle.getTargetField() + "\").readonly = true;}}\n";
		
		return function;
	}
	
	/**
	 * adds an entry to the superfunctionscatalog. 
	 * Structure: HashMap<String, Stringbuilder>
	 * 				- key/the string is the name of the superfunction
	 * 				- value/Stringbuilder are the subfunctionnames/calls. the new subfunction is appended in JS Syntax be called at runtime. 
	 * @param superFunction
	 * @param subFunction
	 */
	private void addSubFunctionToCatalog(String superFunction, String subFunction){
		superFunction = superFunction.replace(".", "");
		//Entry for superfunction allready exists, the subfunction call is added to the Stringbuilder
		if (functionCatalog.containsKey(superFunction)) {
			functionCatalog.get(superFunction).append(subFunction+"();");
		//first appearence of a superfunction, the Stringbuilder is created with function header and first subfunction call
		} else {
			functionCatalog.put(superFunction, new StringBuilder("function " + superFunction + "(){" + subFunction +"();"));
		}
	}

}
