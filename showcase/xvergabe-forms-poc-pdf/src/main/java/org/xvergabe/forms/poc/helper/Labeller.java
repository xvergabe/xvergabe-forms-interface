package org.xvergabe.forms.poc.helper;

import org.xvergabe.xsd.forms.components.x20.AbstractComponent;
import org.xvergabe.xsd.forms.components.x20.AbstractField;
import org.xvergabe.xsd.forms.components.x20.ComponentMetaData;
import org.xvergabe.xsd.forms.components.x20.FieldMetaData;

import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;

public class Labeller {

	public static String getLabelFor(AbstractComponent element) {
		ComponentMetaData metadata = null;
		metadata = element.getMetadata();

		if (element.getLabelled()) {
			if (!(metadata == null)) {
				if (!metadata.getDisplayInformationArray(0).getLabel()
						.getStringValue().equals(null))
					return metadata.getDisplayInformationArray(0).getLabel()
							.getStringValue();
			} else if (!(element.getId() == null)) {
				return element.getId();
			}
		}
		return "";
	}

	public static String getLabelFor(AbstractField element) {
		FieldMetaData metadata = null;
		metadata = element.getMetadata();

		if (element.getLabelled()) {
			if (!(metadata == null)) {
				if (!metadata.getDisplayInformationArray(0).getLabel()
						.getStringValue().equals(null))
					return metadata.getDisplayInformationArray(0).getLabel()
							.getStringValue();
			} else if (!(element.getId() == null)) {
				return element.getId();
			}
		}
		return "";
	}

	public static Paragraph getLabelParagraphFor(AbstractField element) {
		FieldMetaData metadata = null;
		metadata = element.getMetadata();
		String label = new String();
		Paragraph p = new Paragraph();
		Font font = new Font();
		font.setSize(10);
		font.setStyle(Font.BOLD);
		p.setFont(font);

		if (element.getLabelled()) {
			if (!(metadata == null) && !(metadata.getDisplayInformationArray() == null)) {
				if (!(metadata.getDisplayInformationArray(0).getLabel()
						.getStringValue().equals(null)))
					label = metadata.getDisplayInformationArray(0).getLabel()
							.getStringValue();
			} else if (!(element.getId() == null)) {
				label = element.getId();
			}
		}

		p.add(label);
		return p;
	}

	public static Paragraph getLabelParagraphFor(AbstractComponent element) {
		ComponentMetaData metadata = null;
		metadata = element.getMetadata();
		String label = new String();
		Paragraph p = new Paragraph();
		Font font = new Font();
		font.setSize(10);
		font.setStyle(Font.BOLD);
		p.setFont(font);

		if (element.getLabelled()) {
			if (!(metadata == null)) {
				if (!metadata.getDisplayInformationArray(0).getLabel()
						.getStringValue().equals(null))
					label = metadata.getDisplayInformationArray(0).getLabel()
							.getStringValue();
			} else if (!(element.getId() == null)) {
				label = element.getId();
			}
		}

		p.add(label);
		return p;
	}

}
