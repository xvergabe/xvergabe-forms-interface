package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.AbstractComponent;
import org.xvergabe.xsd.forms.components.x20.Address;
import org.xvergabe.xsd.forms.components.x20.Agreement;
import org.xvergabe.xsd.forms.components.x20.Agreements;
import org.xvergabe.xsd.forms.components.x20.Bidder;
import org.xvergabe.xsd.forms.components.x20.Contact;
import org.xvergabe.xsd.forms.components.x20.ContractingAuthority;
import org.xvergabe.xsd.forms.components.x20.Description;
import org.xvergabe.xsd.forms.components.x20.Discount;
import org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup;
import org.xvergabe.xsd.forms.components.x20.InquiryOffice;
import org.xvergabe.xsd.forms.components.x20.Lots;
import org.xvergabe.xsd.forms.components.x20.Organization;
import org.xvergabe.xsd.forms.components.x20.Prequalification;
import org.xvergabe.xsd.forms.components.x20.Prequalifications;
import org.xvergabe.xsd.forms.components.x20.ProvidedDocuments;
import org.xvergabe.xsd.forms.components.x20.Signature;
import org.xvergabe.xsd.forms.components.x20.impl.AddressImpl;
import org.xvergabe.xsd.forms.components.x20.impl.AgreementImpl;
import org.xvergabe.xsd.forms.components.x20.impl.AgreementsImpl;
import org.xvergabe.xsd.forms.components.x20.impl.BidderImpl;
import org.xvergabe.xsd.forms.components.x20.impl.ContactImpl;
import org.xvergabe.xsd.forms.components.x20.impl.ContractingAuthorityImpl;
import org.xvergabe.xsd.forms.components.x20.impl.DescriptionImpl;
import org.xvergabe.xsd.forms.components.x20.impl.DiscountImpl;
import org.xvergabe.xsd.forms.components.x20.impl.InquiryOfficeImpl;
import org.xvergabe.xsd.forms.components.x20.impl.LotsImpl;
import org.xvergabe.xsd.forms.components.x20.impl.OrganizationImpl;
import org.xvergabe.xsd.forms.components.x20.impl.PrequalificationImpl;
import org.xvergabe.xsd.forms.components.x20.impl.PrequalificationsImpl;
import org.xvergabe.xsd.forms.components.x20.impl.ProvidedDocumentsImpl;
import org.xvergabe.xsd.forms.components.x20.impl.SignatureImpl;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * This class is a general PDFWriter that delegates a component to its
 * corresponding PDFWriter
 * 
 * @author schiller
 * 
 */
public class AbstractComponentPdfWriter implements
		XmlPdfWriter<AbstractComponent> {

	AgreementPdfWriter agreementPdfWriter = new AgreementPdfWriter();
	AgreementsPdfWriter agreementsPdfWriter = new AgreementsPdfWriter();
	ContactPdfWriter contactPdfWriter = new ContactPdfWriter();
	ProvidedDocumentsPdfWriter providedDocumentsPdfWriter = new ProvidedDocumentsPdfWriter();
	InquiryOfficePdfWriter inquiryOfficePdfWriter = new InquiryOfficePdfWriter();
	OrganizationPdfWriter organizationPdfWriter = new OrganizationPdfWriter();
	AddressPdfWriter addressPdfWriter = new AddressPdfWriter();
	DescriptionPdfWriter descriptionPdfWriter = new DescriptionPdfWriter();
	SignaturePdfWriter signaturePdfWriter = new SignaturePdfWriter();
	DynamicCheckGroupPdfWriter dynamicCheckGroupPdfWriter = new DynamicCheckGroupPdfWriter();
	LotsPdfWriter lotsPdfWriter = new LotsPdfWriter();
	DiscountPdfWriter discountPdfWriter = new DiscountPdfWriter();
	PrequalificationPdfWriter prequalificationPdfWriter = new PrequalificationPdfWriter();
	PrequalificationsPdfWriter prequalificationsPdfWriter = new PrequalificationsPdfWriter();
	BidderPdfWriter bidderPdfWriter = new BidderPdfWriter();
	ContractingAuthorityPdfWriter contractingAuthorityPdfWriter = new ContractingAuthorityPdfWriter();
	
	@Override
	public void writePdfFromXml(AbstractComponent abstractComponent,
			PdfWriter writer, Document document) {

		if (abstractComponent instanceof AgreementImpl)
			agreementPdfWriter.writePdfFromXml((Agreement) abstractComponent,
					writer, document);
		else if (abstractComponent instanceof AgreementsImpl)
			agreementsPdfWriter.writePdfFromXml((Agreements) abstractComponent,
					writer, document);
		else if (abstractComponent instanceof ContactImpl)
			contactPdfWriter.writePdfFromXml((Contact) abstractComponent,
					writer, document);
		else if (abstractComponent instanceof ProvidedDocumentsImpl)
			providedDocumentsPdfWriter.writePdfFromXml(
					(ProvidedDocuments) abstractComponent, writer, document);
		else if (abstractComponent instanceof InquiryOfficeImpl)
			inquiryOfficePdfWriter.writePdfFromXml(
					(InquiryOffice) abstractComponent, writer, document);
		else if (abstractComponent instanceof ContractingAuthorityImpl)
			contractingAuthorityPdfWriter.writePdfFromXml(
					(ContractingAuthority) abstractComponent, writer, document);
		else if (abstractComponent instanceof BidderImpl)
			bidderPdfWriter.writePdfFromXml(
					(Bidder) abstractComponent, writer, document);
		else if (abstractComponent instanceof OrganizationImpl)
			organizationPdfWriter.writePdfFromXml(
					(Organization) abstractComponent, writer, document);
		else if (abstractComponent instanceof AddressImpl)
			addressPdfWriter.writePdfFromXml((Address) abstractComponent,
					writer, document);
		else if (abstractComponent instanceof DescriptionImpl)
			descriptionPdfWriter.writePdfFromXml(
					(Description) abstractComponent, writer, document);
		else if (abstractComponent instanceof DiscountImpl)
			discountPdfWriter.writePdfFromXml(
					(Discount) abstractComponent, writer, document);
		else if (abstractComponent instanceof SignatureImpl)
			signaturePdfWriter.writePdfFromXml((Signature) abstractComponent,
					writer, document);
		else if (abstractComponent instanceof LotsImpl)
			lotsPdfWriter.writePdfFromXml((Lots) abstractComponent,
					writer, document);
		else if (abstractComponent instanceof PrequalificationImpl)
			prequalificationPdfWriter.writePdfFromXml((Prequalification) abstractComponent,
					writer, document);
		else if (abstractComponent instanceof PrequalificationsImpl)
			prequalificationsPdfWriter.writePdfFromXml((Prequalifications) abstractComponent,
					writer, document);
		else if (abstractComponent instanceof DynamicCheckGroup) {
			dynamicCheckGroupPdfWriter.writePdfFromXml(
					(DynamicCheckGroup) abstractComponent, writer, document);
			
		}

	}
}
