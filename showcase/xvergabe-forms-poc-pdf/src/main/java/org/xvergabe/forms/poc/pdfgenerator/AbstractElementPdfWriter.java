package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.AbstractComponent;
import org.xvergabe.xsd.forms.components.x20.AbstractElement;
import org.xvergabe.xsd.forms.components.x20.AbstractField;
import org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl;
import org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * This class is a general PDFWriter that delegates an Element to its corresponding PDFWriter.
 * Every Element (Field or Component) can be given.
 * @author schiller
 *
 */
public class AbstractElementPdfWriter implements XmlPdfWriter<AbstractElement> {

	AbstractFieldPdfWriter abstractFieldPdfWriter = new AbstractFieldPdfWriter();
	AbstractComponentPdfWriter abstractComponentPdfWriter = new AbstractComponentPdfWriter();
		
	@Override
	public void writePdfFromXml(AbstractElement abstractElement, PdfWriter writer, Document document) {

		if (abstractElement instanceof AbstractComponentImpl) {
			abstractComponentPdfWriter.writePdfFromXml((AbstractComponent) abstractElement, writer, document);
		} else if (abstractElement instanceof AbstractFieldImpl) {
			abstractFieldPdfWriter.writePdfFromXml((AbstractField) abstractElement, writer, document);
		}
		
	}
}
