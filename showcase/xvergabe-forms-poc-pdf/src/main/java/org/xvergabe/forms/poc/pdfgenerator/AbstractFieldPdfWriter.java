package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.AbstractField;
import org.xvergabe.xsd.forms.components.x20.CheckGroup;
import org.xvergabe.xsd.forms.components.x20.Checkbox;
import org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup;
import org.xvergabe.xsd.forms.components.x20.RadioGroup;
import org.xvergabe.xsd.forms.components.x20.TenderTypeChoice;
import org.xvergabe.xsd.forms.components.x20.impl.TenderTypeChoiceImpl;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * This class is a general PDFWriter that delegates a field to its corresponding PDFWriter.
 * @author schiller
 *
 */
public class AbstractFieldPdfWriter implements XmlPdfWriter<AbstractField>{

	TenderTypeChoicePdfWriter tenderTypeChoicePdfWriter = new TenderTypeChoicePdfWriter();
	StringInputFieldPdfWriter stringFieldPdfWriter = new StringInputFieldPdfWriter();
	DateInputFieldPdfWriter dateInputFieldPdfWriter = new DateInputFieldPdfWriter(); 
	DynamicCheckGroupPdfWriter dynamicCheckGroupPdfWriter = new DynamicCheckGroupPdfWriter();
	CheckGroupPdfWriter checkGroupPdfWriter = new CheckGroupPdfWriter();
	CheckboxPdfWriter checkboxPdfWriter = new CheckboxPdfWriter();
	RadioGroupPdfWriter radioGroupPdfWriter = new RadioGroupPdfWriter();
	NumberFieldPdfWriter numberFieldPdfWriter = new NumberFieldPdfWriter();
	NotePdfWriter notePdfWriter = new NotePdfWriter();
	
	@Override
	public void writePdfFromXml(AbstractField abstractField, PdfWriter writer, Document document) {
		
		if(abstractField instanceof TenderTypeChoiceImpl)
			tenderTypeChoicePdfWriter.writePdfFromXml((TenderTypeChoice)abstractField, writer, document);
		else if(abstractField instanceof org.xvergabe.xsd.forms.components.x20.String)
			stringFieldPdfWriter.writePdfFromXml((org.xvergabe.xsd.forms.components.x20.String)abstractField, writer, document);
		else if(abstractField instanceof org.xvergabe.xsd.forms.components.x20.Note)
			notePdfWriter.writePdfFromXml((org.xvergabe.xsd.forms.components.x20.Note)abstractField, writer, document);
		else if(abstractField instanceof org.xvergabe.xsd.forms.components.x20.Integer ||
				abstractField instanceof org.xvergabe.xsd.forms.components.x20.Decimal ||
				abstractField instanceof org.xvergabe.xsd.forms.components.x20.Percentage)
			numberFieldPdfWriter.writePdfFromXml(abstractField, writer, document);
		else if(abstractField instanceof org.xvergabe.xsd.forms.components.x20.Date)
			dateInputFieldPdfWriter.writePdfFromXml((org.xvergabe.xsd.forms.components.x20.Date)abstractField, writer, document);
		else if(abstractField instanceof org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup)
			dynamicCheckGroupPdfWriter.writePdfFromXml((DynamicCheckGroup)abstractField, writer, document);
		else if(abstractField instanceof org.xvergabe.xsd.forms.components.x20.CheckGroup)
			checkGroupPdfWriter.writePdfFromXml((CheckGroup)abstractField, writer, document);
		else if(abstractField instanceof org.xvergabe.xsd.forms.components.x20.Checkbox)
			checkboxPdfWriter.writePdfFromXml((Checkbox)abstractField, writer, document);
		else if(abstractField instanceof org.xvergabe.xsd.forms.components.x20.RadioGroup)
			radioGroupPdfWriter.writePdfFromXml((RadioGroup)abstractField, writer, document);
		
	}

}
