package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.Address;
import org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an {@link Address} Component to a PDF 
 * @author schiller
 *
 */
public class AddressPdfWriter implements XmlPdfWriter<Address>{

	@Override
	public void writePdfFromXml(Address address, PdfWriter writer, Document document) {
		
		StringInputFieldPdfWriter stringFieldPdfWriter = new StringInputFieldPdfWriter();
		
		if(address.isSetStreet())
			stringFieldPdfWriter.writePdfFromXml(address.getStreet(), writer, document);
		if(address.isSetNumber())
			stringFieldPdfWriter.writePdfFromXml(address.getNumber(), writer, document);
		if(address.isSetPostalCode())
			stringFieldPdfWriter.writePdfFromXml(address.getPostalCode(), writer, document);
		if(address.isSetCity())
			stringFieldPdfWriter.writePdfFromXml(address.getCity(), writer, document);
	}

}
