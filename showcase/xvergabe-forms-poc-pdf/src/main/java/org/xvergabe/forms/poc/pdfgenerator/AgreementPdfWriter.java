package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.events.PdfFieldBoolCellEvent;
import org.xvergabe.xsd.forms.components.x20.Agreement;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an {@link Agreement} Component to a PDF
 * 
 * @author schiller
 * 
 */
public class AgreementPdfWriter implements XmlPdfWriter<Agreement> {
	@Override
	public void writePdfFromXml(Agreement agreement, PdfWriter writer, Document document) {

		Font f = new Font();
		f.setSize(8);
		
		try {
			PdfPTable table = new PdfPTable(2);
			table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			table.setWidths(new int[] { 1, 20 });
			PdfPCell cell = new PdfPCell();

			//Checkbox
			cell.setCellEvent(new PdfFieldBoolCellEvent(agreement.getAgreed()));
			cell.setBorder(1);
			table.addCell(cell);
			
			//Label/Explanation
			table.addCell(new Phrase(agreement.getExplanation().getValue(),f));
			document.add(table);
			document.add(new Chunk(" "));
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
