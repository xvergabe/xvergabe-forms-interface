package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Agreement;
import org.xvergabe.xsd.forms.components.x20.Agreements;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an Agreements Component to a PDF
 * 
 */
public class AgreementsPdfWriter implements XmlPdfWriter<Agreements> {

	@Override
	public void writePdfFromXml(Agreements agreements, PdfWriter writer, Document document) {

		Agreement[] agreementList = agreements.getAgreementArray();
		AgreementPdfWriter agreementPdfWriter = new AgreementPdfWriter();
		
		try {
			document.add(Labeller.getLabelParagraphFor(agreements));
			for (int i = 0; i < agreementList.length; i++) {
				agreementPdfWriter.writePdfFromXml(agreementList[i], writer, document);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		

	}

}
