package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.Bidder;
import org.xvergabe.xsd.forms.components.x20.InquiryOffice;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an {@link Bidder} Component to a PDF. 
 * @author schiller
 *
 */
public class BidderPdfWriter implements XmlPdfWriter<Bidder>{

	@Override
	public void writePdfFromXml(Bidder bidder, PdfWriter writer, Document document) {
	
		OrganizationPdfWriter organizationPdfWriter = new OrganizationPdfWriter();
		
		organizationPdfWriter.writePdfFromXml(bidder, writer, document);
		
	}

}
