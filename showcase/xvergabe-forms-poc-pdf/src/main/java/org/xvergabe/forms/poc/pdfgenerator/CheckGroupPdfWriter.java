package org.xvergabe.forms.poc.pdfgenerator;


import org.xvergabe.forms.poc.events.PdfFieldBoolCellEvent;
import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Boolean;
import org.xvergabe.xsd.forms.components.x20.CheckGroup;
import org.xvergabe.xsd.forms.components.x20.Checkbox;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;


/**
 * Class to write a Checkgroup Field to a PDF
 * 
 * @author schiller
 *
 */
public class CheckGroupPdfWriter implements XmlPdfWriter<CheckGroup> {

	@Override
	public void writePdfFromXml(CheckGroup checkgroup, PdfWriter writer, Document document) {

		Checkbox[] options = checkgroup.getOptionArray();
		PdfPTable table = new PdfPTable(2);
		PdfPCell cell;

		int[] widths = { 1, 20 };

		try {
			Font f = new Font();
			f.setSize(12);
			f.setStyle(Font.BOLD);
			Phrase label = new Phrase(new Chunk(Labeller.getLabelFor(checkgroup), f));

			table = new PdfPTable(2);
			table.setWidths(widths);
			table.setSpacingAfter(10);

			cell = new PdfPCell(label);
			cell.setColspan(2);
			cell.setBorder(PdfPCell.NO_BORDER);
			table.addCell(cell);
			document.add(table);

			for (int i = 0; i < options.length; i++) {

				table = new PdfPTable(2);
				table.setWidths(widths);

				// Checkbox
				cell = new PdfPCell();
				Boolean b;
				if(options[i].isSetChecked()){
					b = options[i].getChecked();
					b.setId(checkgroup.getId()+".option"+(i+1));
				}else{//wenn kein <form:checked> gesetzt ist
					b = Boolean.Factory.newInstance();
					b.setValue(false);
					b.setId(checkgroup.getId()+".option"+(i+1));
				}
				cell.setCellEvent(new PdfFieldBoolCellEvent(b));
				table.addCell(cell);

				// Label
				f.setSize(8);
				f.setStyle(Font.NORMAL);
				cell = new PdfPCell(new Phrase(options[i].getValue().getValue(),f));
				cell.setBorder(PdfPCell.NO_BORDER);
				table.addCell(cell);

				// Spaceline
				cell = new PdfPCell();
				cell.setBorder(PdfPCell.NO_BORDER);
				cell.setColspan(2);
				cell.setMinimumHeight(5);
				table.addCell(cell);

				document.add(table);

			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
