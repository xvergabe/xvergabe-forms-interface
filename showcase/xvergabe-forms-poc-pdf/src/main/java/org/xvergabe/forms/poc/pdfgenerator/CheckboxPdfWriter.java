package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.events.PdfFieldBoolCellEvent;
import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Boolean;
import org.xvergabe.xsd.forms.components.x20.Checkbox;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a Checkbox Field to a PDF
 * 
 * @author schiller
 * 
 */
public class CheckboxPdfWriter implements XmlPdfWriter<Checkbox> {

	@Override
	public void writePdfFromXml(Checkbox checkbox, PdfWriter writer, Document document) {

		PdfPTable table = new PdfPTable(2);
		PdfPCell cell;

		int[] widths = { 1, 20 };

		try {
			table = new PdfPTable(2);
			table.setWidths(widths);

			// Checkbox
			cell = new PdfPCell();
			Boolean b;
			if (checkbox.isSetChecked()) {
				b = checkbox.getChecked();
				b.setId(checkbox.getId());
			} else {// wenn kein <form:checked> gesetzt ist
				b = Boolean.Factory.newInstance();
				b.setValue(false);
				b.setId(checkbox.getId());
			}
			cell.setCellEvent(new PdfFieldBoolCellEvent(b));
			table.addCell(cell);

			// Label
			Font f = new Font();
			f.setSize(8);
			cell = new PdfPCell(new Phrase(Labeller.getLabelFor(checkbox),f));
			cell.setBorder(PdfPCell.NO_BORDER);
			table.addCell(cell);

			document.add(table);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
