package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.Contact;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 *Class to write a Contact Componentto a PDF
 * 
 * @author schiller
 * 
 */
public class ContactPdfWriter implements XmlPdfWriter<Contact> {

	@Override
	public void writePdfFromXml(Contact contact, PdfWriter writer, Document document) {

		StringInputFieldPdfWriter stringInputFieldPdfWriter = new StringInputFieldPdfWriter();

		stringInputFieldPdfWriter.writePdfFromXml(contact.getPhone(), writer, document);
		stringInputFieldPdfWriter.writePdfFromXml(contact.getFax(), writer, document);
		stringInputFieldPdfWriter.writePdfFromXml(contact.getMail(), writer, document);
		
	}
}
