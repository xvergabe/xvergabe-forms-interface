package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.Bidder;
import org.xvergabe.xsd.forms.components.x20.ContractingAuthority;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an {@link Bidder} Component to a PDF. 
 * @author schiller
 *
 */
public class ContractingAuthorityPdfWriter implements XmlPdfWriter<ContractingAuthority>{

	@Override
	public void writePdfFromXml(ContractingAuthority contractingAuthority, PdfWriter writer, Document document) {
	
		OrganizationPdfWriter organizationPdfWriter = new OrganizationPdfWriter();
		
		organizationPdfWriter.writePdfFromXml(contractingAuthority, writer, document);
		
	}

}
