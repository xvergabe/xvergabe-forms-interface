package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.events.PdfDateFieldCellEvent;
import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Date;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;


/**
 * Class to write a String input field to a PDF
 * 
 * @author schiller
 *
 */
public class DateInputFieldPdfWriter implements XmlPdfWriter<org.xvergabe.xsd.forms.components.x20.Date> {

	int[] tableCellRatio = { 4, 15 };

	/**
	 * function to differ the label:inputfield-ratio in the PDF
	 * 
	 * @param ratio the ratio for label:inputfield
	 */
	public void writePdfFromXml(Date date, PdfWriter writer, Document document, int[] ratio) {
		this.tableCellRatio = ratio;
		writePdfFromXml(date, writer, document);
	}

	@Override
	public void writePdfFromXml(Date date, PdfWriter writer, Document document) {
		
		PdfPCell cell = new PdfPCell();
		PdfPTable table = new PdfPTable(2);

		try {
			
			Font f = new Font();
			f.setSize(8);
			table.setWidths(this.tableCellRatio);
			
			// Label
			cell = new PdfPCell(new Phrase(Labeller.getLabelFor(date),f));
			cell.setBorder(PdfPCell.NO_BORDER);
			table.addCell(cell);

			// InputField
			cell = new PdfPCell();
			cell.setBorder(PdfPCell.BOTTOM);
			cell.setCellEvent(new PdfDateFieldCellEvent(date));
			table.addCell(cell);

			document.add(table);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
