package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Description;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a Description Component to a PDF
 * 
 * @author schiller
 * 
 */
public class DescriptionPdfWriter implements XmlPdfWriter<Description> {

	@Override
	public void writePdfFromXml(Description description, PdfWriter writer, Document document) {

		PdfPCell cell = new PdfPCell();
		PdfPTable table = new PdfPTable(1);

		try {

			Font f = new Font();
			f.setSize(8);

			document.add(Labeller.getLabelParagraphFor(description));

			if (description.isSetShortDescription()) {
				cell = new PdfPCell(new Phrase(description.getShortDescription().getValue(), f));
				cell.setBorder(PdfPCell.NO_BORDER);
				table.addCell(cell);
			}
			
			if (description.isSetLongDescription()) {
				cell = new PdfPCell(new Phrase(description.getLongDescription().getValue(), f));
				cell.setBorder(PdfPCell.NO_BORDER);
				table.addCell(cell);
			}
			
			document.add(table);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
