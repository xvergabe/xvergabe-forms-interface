package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.Discount;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an {@link Discount} Component to a PDF 
 * @author schiller
 *
 */
public class DiscountPdfWriter implements XmlPdfWriter<Discount>{

	@Override
	public void writePdfFromXml(Discount discount, PdfWriter writer, Document document) {
		
		NumberFieldPdfWriter numberFieldPdfWriter = new NumberFieldPdfWriter();
		
		numberFieldPdfWriter.writePdfFromXml(discount.getDays(), writer, document);
		numberFieldPdfWriter.writePdfFromXml(discount.getPercentage(), writer, document);
		
		
	}

}
