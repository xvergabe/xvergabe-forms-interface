package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.FormDocument.Form;
import org.xvergabe.xsd.forms.components.x20.Section;
import org.xvergabe.xsd.forms.components.x20.impl.OrSectionImpl;
import org.xvergabe.xsd.forms.components.x20.impl.SectionImpl;
import org.xvergabe.xsd.forms.components.x20.impl.XorSectionImpl;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a Form to a PDF. The form component is the root node of a
 * Form-XML so this Class is initially needed to start writing every form to PDF
 * 
 * @author schiller
 * 
 */
public class FormPdfWriter implements XmlPdfWriter<Form> {

	@Override
	public void writePdfFromXml(Form form, PdfWriter writer, Document document) {

		SectionPdfWriter sectionPdfWriter = new SectionPdfWriter();

		// Documentheader
		try {
			Font f = new Font();
			f.setSize(18);
			Paragraph p = new Paragraph(new Chunk(form.getMetadata().getDisplayInformationArray(0).getLabel().getStringValue(), f));
			p.setAlignment(Element.ALIGN_CENTER);
			document.add(p);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		try {

			for (int i = 0; i < form.getSectionArray().length; i++) {

				Section section = form.getSectionArray(i);

				if (section instanceof SectionImpl)
					sectionPdfWriter.writePdfFromXml(section, writer, document);
				else if (section instanceof XorSectionImpl)
					;
				// TODO xorsectiongenerator
				else if (section instanceof OrSectionImpl)
					;
				// TODO orsectiongenerator

				document.add(Chunk.NEWLINE);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
