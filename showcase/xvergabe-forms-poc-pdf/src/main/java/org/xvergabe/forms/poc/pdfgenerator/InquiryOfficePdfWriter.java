package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.xsd.forms.components.x20.InquiryOffice;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an InquiryOffice Component to a PDF. 
 * @author schiller
 *
 */
public class InquiryOfficePdfWriter implements XmlPdfWriter<InquiryOffice>{

	@Override
	public void writePdfFromXml(InquiryOffice inquiryOffice, PdfWriter writer, Document document) {
	
		OrganizationPdfWriter organizationPdfWriter = new OrganizationPdfWriter();
		
		organizationPdfWriter.writePdfFromXml(inquiryOffice, writer, document);
		
	}

}
