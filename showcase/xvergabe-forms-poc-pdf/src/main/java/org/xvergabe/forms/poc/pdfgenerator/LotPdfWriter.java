package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.events.PdfDecimalFieldCellEvent;
import org.xvergabe.forms.poc.events.PdfEmptyInputCellEvent;
import org.xvergabe.forms.poc.events.PdfIntegerFieldCellEvent;
import org.xvergabe.forms.poc.events.PdfPercentageFieldCellEvent;
import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Lot;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an Lot Component to a PDF
 * 
 * @author schiller
 * 
 */
public class LotPdfWriter implements XmlPdfWriter<Lot> {

	@Override
	public void writePdfFromXml(Lot lot, PdfWriter writer, Document document) {
		
		StringInputFieldPdfWriter stringInputFieldPdfWriter = new StringInputFieldPdfWriter();
		NumberFieldPdfWriter numberFieldPdfWriter = new NumberFieldPdfWriter();
		
		try {
			
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(50);
			table.getDefaultCell().setMinimumHeight(10);
			table.getDefaultCell().setBorder(PdfCell.NO_BORDER);

			Paragraph p = new Paragraph("Los " + lot.getLotNumber().getValue());
			Font font = new Font();
			font.setSize(8);
			p.setFont(font);
			PdfPCell cell = new PdfPCell(p);
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setColspan(2);
			table.addCell(cell);
			
			numberFieldPdfWriter.writePdfFromXml(lot.getPrice().getNetValue(), writer, document);
			numberFieldPdfWriter.writePdfFromXml(lot.getPrice().getTaxRate(), writer, document);
			numberFieldPdfWriter.writePdfFromXml(lot.getPrice().getGrossValue(), writer, document);
			numberFieldPdfWriter.writePdfFromXml(lot.getAlternativeOffers(), writer, document);
			if(lot.getPrice().getRemission().isSetPercentage())
				numberFieldPdfWriter.writePdfFromXml(lot.getPrice().getRemission().getPercentage(), writer, document);
			if(lot.getPrice().getRemission().isSetAbsoluteValue())
				numberFieldPdfWriter.writePdfFromXml(lot.getPrice().getRemission().getAbsoluteValue(), writer, document);
			// Spaceline
			table.flushContent();
			cell = new PdfPCell();
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setColspan(2);
			cell.setMinimumHeight(5);
			table.addCell(cell);
			document.add(table);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
