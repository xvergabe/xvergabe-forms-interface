package org.xvergabe.forms.poc.pdfgenerator;

import java.awt.Color;

import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Lot;
import org.xvergabe.xsd.forms.components.x20.Lots;

import com.lowagie.text.Anchor;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write an Lots Component to a PDF
 * 
 */
public class LotsPdfWriter implements XmlPdfWriter<Lots> {

	@Override
	public void writePdfFromXml(Lots lots, PdfWriter writer, Document document) {

		Lot[] lotList = lots.getLotArray();
		LotPdfWriter lotPdfWriter = new LotPdfWriter();
		PdfPTable table = new PdfPTable(1);; 

		try {
			document.add(Labeller.getLabelParagraphFor(lots));
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		for (int i = 0; i < lotList.length; i++) {
			lotPdfWriter.writePdfFromXml(lotList[i], writer, document);
		}

		try {
			Font font = new Font();
			font.setColor(new Color(0, 0, 255));
			// erstmal nur als Link, Buttons sind schwierig zu platzieren...
			Anchor link = new Anchor("Weiteren Eintrag hinzufügen", font);
			PdfPCell cell = new PdfPCell();
			cell = new PdfPCell(link);
			cell.setBorder(PdfPCell.NO_BORDER);
			table.addCell(cell);
			document.add(table);
			
			// Spaceline
			table = new PdfPTable(1);
			cell = new PdfPCell();
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setMinimumHeight(5);
			table.addCell(cell);
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		

	}

}
