package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Note;
import org.xvergabe.xsd.forms.components.x20.String;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a Note to a PDF
 * 
 * @author schiller
 * 
 */
public class NotePdfWriter implements XmlPdfWriter<org.xvergabe.xsd.forms.components.x20.Note> {

	@Override
	public void writePdfFromXml(Note note, PdfWriter writer, Document document) {

		PdfPCell cell = new PdfPCell();
		PdfPTable table = new PdfPTable(1);

		try {

			Font f = new Font();
			f.setSize(8);

			// note
			cell = new PdfPCell(new Phrase(note.getValue().getValue()));
			cell.setBorder(PdfPCell.NO_BORDER);
			table.addCell(cell);

			document.add(table);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
