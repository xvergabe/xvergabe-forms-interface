package org.xvergabe.forms.poc.pdfgenerator;

import javax.xml.bind.TypeConstraintException;

import org.xvergabe.forms.poc.events.PdfDecimalFieldCellEvent;
import org.xvergabe.forms.poc.events.PdfIntegerFieldCellEvent;
import org.xvergabe.forms.poc.events.PdfPercentageFieldCellEvent;
import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.AbstractField;
import org.xvergabe.xsd.forms.components.x20.Decimal;
import org.xvergabe.xsd.forms.components.x20.Integer;
import org.xvergabe.xsd.forms.components.x20.Percentage;
import org.xvergabe.xsd.forms.components.x20.impl.DecimalImpl;
import org.xvergabe.xsd.forms.components.x20.impl.IntegerImpl;
import org.xvergabe.xsd.forms.components.x20.impl.PercentageImpl;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;


/**
 * Class to write an Integer input field to a PDF
 * 
 * @author schiller
 *
 */
public class NumberFieldPdfWriter implements XmlPdfWriter<org.xvergabe.xsd.forms.components.x20.AbstractField> {

	int[] tableCellRatio = { 4, 15 };

	/**
	 * function to differ the label:inputfield-ratio in the PDF
	 * 
	 * @param ratio the ratio for label:inputfield
	 */
	public void writePdfFromXml(AbstractField number, PdfWriter writer, Document document, int[] ratio) {
		this.tableCellRatio = ratio;
		writePdfFromXml(number, writer, document);
	}

	@Override
	public void writePdfFromXml(AbstractField number, PdfWriter writer, Document document) {
		
		PdfPCell cell = new PdfPCell();
		PdfPTable table = new PdfPTable(2);
		PdfPCellEvent cellevent; 

		if(number instanceof IntegerImpl)
			cellevent = new PdfIntegerFieldCellEvent((Integer)number);
		else if(number instanceof PercentageImpl)
			cellevent = new PdfPercentageFieldCellEvent((Percentage)number);
		else if(number instanceof DecimalImpl)
			cellevent = new PdfDecimalFieldCellEvent((Decimal)number);
		else
			throw new TypeConstraintException("zu schreibendes Element ist kein Zahlentyp. Objekt:\n"+number.toString());
		
		try {
			
			Font f = new Font();
			f.setSize(8);
			table.setWidths(this.tableCellRatio);
			
			// Label
			cell = new PdfPCell(new Phrase(Labeller.getLabelFor(number),f));
			cell.setBorder(PdfPCell.NO_BORDER);
			table.addCell(cell);

			// InputField
			cell = new PdfPCell();
			cell.setBorder(PdfPCell.BOTTOM);
			cell.setCellEvent(cellevent);
			table.addCell(cell);

			document.add(table);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
