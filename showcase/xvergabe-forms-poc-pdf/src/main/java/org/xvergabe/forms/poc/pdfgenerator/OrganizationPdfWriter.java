package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Address;
import org.xvergabe.xsd.forms.components.x20.Contact;
import org.xvergabe.xsd.forms.components.x20.Organization;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a Organization Component to a PDF
 * 
 * @author schiller
 *
 */
public class OrganizationPdfWriter implements XmlPdfWriter<Organization> {

	@Override
	public void writePdfFromXml(Organization organization, PdfWriter writer, Document document) {

		StringInputFieldPdfWriter stringInputFieldPdfWriter = new StringInputFieldPdfWriter();
		DescriptionPdfWriter descriptionPdfWriter = new DescriptionPdfWriter();
		AddressPdfWriter addressPdfWriter = new AddressPdfWriter();
		ContactPdfWriter contactPdfWriter = new ContactPdfWriter(); 
		
		Contact[] contactdata = organization.getContactDataArray();

			
		try {
			document.add(Labeller.getLabelParagraphFor(organization));
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		if(organization.isSetName())
			stringInputFieldPdfWriter.writePdfFromXml(organization.getName(), writer, document);
		
		if(organization.isSetDescription())
			descriptionPdfWriter.writePdfFromXml(organization.getDescription(), writer, document);
			
		if(organization.isSetAddress())
			addressPdfWriter.writePdfFromXml(organization.getAddress(), writer, document);
		
		
		for(Contact contact : contactdata)
			contactPdfWriter.writePdfFromXml(contact, writer, document);
		

	}

}
