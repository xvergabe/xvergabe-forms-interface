package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.events.PdfFieldStringCellEvent;
import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Prequalification;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a {@link Prequalification} to PDF.
 * 
 * @author schiller
 * 
 */
public class PrequalificationPdfWriter implements
		XmlPdfWriter<Prequalification> {

	@Override
	public void writePdfFromXml(Prequalification prequalification, PdfWriter writer,
			Document document) {

		StringInputFieldPdfWriter stringInputFieldPdfWriter = new StringInputFieldPdfWriter();
		
		PdfPTable table = new PdfPTable(2);
		table.getDefaultCell().setMinimumHeight(10);
		table.getDefaultCell().setBorder(PdfCell.NO_BORDER);

		PdfPCell cell = new PdfPCell();
		cell.setBorder(PdfPCell.NO_BORDER);
		cell.setColspan(2);
		table.addCell(cell);
		
		stringInputFieldPdfWriter.writePdfFromXml(prequalification.getPrequalificationInstance(), writer, document);
		stringInputFieldPdfWriter.writePdfFromXml(prequalification.getPqNumber(), writer, document);
		stringInputFieldPdfWriter.writePdfFromXml(prequalification.getUlvNumber(), writer, document);
		
	}

}
