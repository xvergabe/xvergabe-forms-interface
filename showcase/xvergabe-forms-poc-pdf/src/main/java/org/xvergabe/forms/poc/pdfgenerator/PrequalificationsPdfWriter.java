package org.xvergabe.forms.poc.pdfgenerator;

import java.awt.Color;

import org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup;
import org.xvergabe.xsd.forms.components.x20.Prequalification;
import org.xvergabe.xsd.forms.components.x20.Prequalifications;

import com.lowagie.text.Anchor;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a {@link Prequalifications} to PDF.
 * 
 * @author schiller
 * 
 */
public class PrequalificationsPdfWriter implements
		XmlPdfWriter<Prequalifications> {

	@Override
	public void writePdfFromXml(Prequalifications prequalifications, PdfWriter writer,
			Document document) {

		
		PrequalificationPdfWriter prequalificationPdfWriter = new PrequalificationPdfWriter();
		
		for(Prequalification prequalification : prequalifications.getPrequalificationArray()){
			prequalificationPdfWriter.writePdfFromXml(prequalification, writer, document);
		}
		
		try {
			Font font = new Font();
			font.setColor(new Color(0, 0, 255));	
			// erstmal nur als Link, Buttons sind schwierig zu platzieren...
			Anchor link = new Anchor("Weiteren Eintrag hinzufügen", font);
			PdfPCell cell = new PdfPCell();
			cell = new PdfPCell(link);
			cell.setBorder(PdfPCell.NO_BORDER);
			PdfPTable table = new PdfPTable(1);
			table.addCell(cell);
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
