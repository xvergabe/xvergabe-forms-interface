package org.xvergabe.forms.poc.pdfgenerator;

import java.io.IOException;

import org.xvergabe.forms.poc.events.PdfFieldStringCellEvent;
import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.RadioGroup;
import org.xvergabe.xsd.forms.components.x20.RadioOption;
import org.xvergabe.xsd.forms.components.x20.String;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RadioCheckField;
import com.lowagie.text.pdf.events.FieldPositioningEvents;

/**
 * Class to write a RadioGroup to a PDF
 * 
 * @author schiller
 *
 */
public class RadioGroupPdfWriter implements XmlPdfWriter<RadioGroup> {

	@Override
	public void writePdfFromXml(RadioGroup radiogroup, PdfWriter writer, Document document) {

		Chunk chunk;
		Paragraph paragraph;
		PdfFormField radioOption;
		RadioCheckField radiofield = new RadioCheckField(writer, new Rectangle(0, 0), "", "");
		PdfFormField radioContainer = radiofield.getRadioGroup(false, false);
		PdfPTable table = new PdfPTable(2);
		PdfPCell cell;
		FieldPositioningEvents fieldPositioningEvents = (FieldPositioningEvents) writer.getPageEvent();
		RadioOption[] options = radiogroup.getOptionArray();
		int[] widths = { 1, 20 };
		
		try {
			
			radioContainer.setFieldName(radiogroup.getId());
			table.setWidths(widths);
			table.setKeepTogether(true);
			document.add(Labeller.getLabelParagraphFor(radiogroup));
			
			for (Integer i = 0; i < options.length; i++) {

				java.lang.String radiofieldname = options[i].getId() + i;
				
				//Value of the Radiobutton
				radiofield.setOnValue(i.toString());
				//Fieldname for the genericTag to match
				radiofield.setFieldName(radiofieldname);
				radioOption = radiofield.getRadioField();
				
				fieldPositioningEvents.addField(radiofieldname, radioOption);
				
				radioContainer.addKid(radioOption);
				//bzgl. JavaScript: es wird jede eine superfunction zugef�gt. Existiert kein Toggle f�r dieses Feld bleibt diese Funktion leer
				//TODO es ist zu �berlegen ob und auf welche weise nur Elementen mit Togglen eine Funktion zugeschrieben wird
				radioOption.setAction(PdfAction.javaScript(radiogroup.getId().replace(".", "")+"()", writer));
				
				//ein Radiobutton wird auf einen Chunk gelegt
				//der Chunk muss in einem Paragraph liegen um ihn der Tabelle zuzuf�gen
				chunk = new Chunk("__");
				chunk.setGenericTag(radiofieldname);
				paragraph = new Paragraph(chunk);
				cell = new PdfPCell(paragraph);
				cell.setBorder(PdfPCell.NO_BORDER);
				table.addCell(cell);
				
				//Buttonlabel
				cell = new PdfPCell(new Phrase(options[i].getChoice().getValue()));
				cell.setBorder(PdfPCell.NO_BORDER);
				table.addCell(cell);
				
				//zus�tzliche freie Eintr�ge zu einem Radiobutton
				String[] additions = options[i].getAdditionArray();
				for(int j=0;j<additions.length;j++){
					cell = new PdfPCell();
					cell.setBorder(PdfPCell.NO_BORDER);
					table.addCell(cell);
					cell = new PdfPCell();
					cell.setMinimumHeight(12);
					cell.setBorder(PdfPCell.BOTTOM);
					cell.setCellEvent(new PdfFieldStringCellEvent(additions[j]));
					table.addCell(cell);
				}
			}
			
			document.add(table);
			writer.addAnnotation(radioContainer);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
