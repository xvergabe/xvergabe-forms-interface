package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Section;

import com.lowagie.text.Chapter;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a Section Component to a PDF
 * 
 * @author schiller
 *
 */
public class SectionPdfWriter implements XmlPdfWriter<Section> {

	int chapter = 1;

	@Override
	public void writePdfFromXml(Section section, PdfWriter writer, Document document) {
		
		AbstractElementPdfWriter abstractElementPdfWriter = new AbstractElementPdfWriter();
		
		try {
			Font f = new Font();
			f.setSize(12);
			f.setStyle(Font.BOLD);
			
			Chapter c = new Chapter(new Paragraph(Labeller.getLabelFor(section), f),chapter++);
			c.setTriggerNewPage(false);
			
			document.add(c);
			
		} catch (DocumentException e) {
			throw new RuntimeException(e);
		}
		
		for (int i = 0; i < section.getElementArray().length; i++) {
			abstractElementPdfWriter.writePdfFromXml(section.getElementArray(i), writer, document);
		}
	}
}
