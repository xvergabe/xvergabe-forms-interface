package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.Signature;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a Signature Component to a PDF
 * 
 * @author schiller
 *
 */
public class SignaturePdfWriter implements XmlPdfWriter<Signature>{

	@Override
	public void writePdfFromXml(Signature signature, PdfWriter writer, Document document) {

		StringInputFieldPdfWriter stringInputFieldPdfWriter = new StringInputFieldPdfWriter();
		
		try {
			document.add(Labeller.getLabelParagraphFor(signature));
			stringInputFieldPdfWriter.writePdfFromXml(signature.getLocation(), writer, document);
			stringInputFieldPdfWriter.writePdfFromXml(signature.getName(), writer, document);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

}
