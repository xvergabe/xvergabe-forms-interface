package org.xvergabe.forms.poc.pdfgenerator;

import org.xvergabe.forms.poc.events.PdfFieldStringCellEvent;
import org.xvergabe.forms.poc.helper.Labeller;
import org.xvergabe.xsd.forms.components.x20.String;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Class to write a String input field to a PDF
 * 
 * @author schiller
 * 
 */
public class StringInputFieldPdfWriter implements XmlPdfWriter<org.xvergabe.xsd.forms.components.x20.String> {

	int[] tableCellRatio = { 4, 15 };

	/**
	 * function to differ the label:inputfield-ratio in the PDF
	 * 
	 * @param ratio
	 *            the ratio for label:inputfield
	 */
	public void writePdfFromXml(String string, PdfWriter writer, Document document, int[] ratio) {
		this.tableCellRatio = ratio;
		writePdfFromXml(string, writer, document);
	}

	@Override
	public void writePdfFromXml(String string, PdfWriter writer, Document document) {

		PdfPCell cell = new PdfPCell();
		PdfPTable table = new PdfPTable(2);

		// calculates the ratio label:input field depending on the length of the
		// label
		if (!Labeller.getLabelFor(string).equals("")) {
			int variable = 5;
			tableCellRatio[0] = (Labeller.getLabelFor(string).length() + variable) / variable;
			tableCellRatio[1] = 15;
		}

		try {

			Font f = new Font();
			f.setSize(8);
			table.setWidths(this.tableCellRatio);

			// Label
			cell = new PdfPCell(new Phrase(Labeller.getLabelFor(string), f));
			cell.setBorder(PdfPCell.NO_BORDER);
			table.addCell(cell);

			// InputField
			cell = new PdfPCell();
			cell.setBorder(PdfPCell.BOTTOM);
			cell.setCellEvent(new PdfFieldStringCellEvent(string));
			table.addCell(cell);

			document.add(table);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
