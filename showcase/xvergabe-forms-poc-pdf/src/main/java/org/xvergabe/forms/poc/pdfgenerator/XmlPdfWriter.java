package org.xvergabe.forms.poc.pdfgenerator;

import org.apache.xmlbeans.XmlObject;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

/**
 * Interface for classes that write a form-component or form-field to a PDF
 *  
 * @author schiller
 *
 * @param <X> The Element-Class that has to be written to a PDF
 */
public interface XmlPdfWriter <X extends XmlObject>{
	
	void writePdfFromXml(X xmlObject, PdfWriter writer, Document document);
	
}
