package org.xvergabe.forms.poc.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;
import org.xvergabe.forms.poc.helper.JavaScriptGenerator;
import org.xvergabe.forms.poc.pdfgenerator.FormPdfWriter;
import org.xvergabe.xsd.forms.components.x20.FormDocument;
import org.xvergabe.xsd.forms.components.x20.FormDocument.Form;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.events.FieldPositioningEvents;

public class PdfWriterTest {
	
	
	
	public static void main(String[] args) {

		FormDocument formDocument = null;
		Form form = null;
		FormPdfWriter formPdfWriter = new FormPdfWriter();
		Document doc = new Document();
		PdfWriter writer = null;
		
		
		//Create the PDF
		try {
			formDocument = FormDocument.Factory.parse(new File("Formblatt-332-L-mit-ULV.xml"));
			form = formDocument.getForm();
			writer = PdfWriter.getInstance(doc, new FileOutputStream("Formblatt-332-L-mit-ULV.pdf"));
			doc.open();

			writer.setPageEvent(new FieldPositioningEvents());

			formPdfWriter.writePdfFromXml(form, writer, doc);
			
			System.out.println("done writing PDF");
			
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (XmlException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		writer.addJavaScript(JavaScriptGenerator.getInstance().getInitialConfigs());
		System.out.println("done writing default value JavaScripts");
		
		writer.addJavaScript(JavaScriptGenerator.getInstance().getFunctions(form.getMetadata()));
		System.out.println("done writing JavaScript super- and subfunctions");		
		doc.close();
		writer.close();
		
	}
}
