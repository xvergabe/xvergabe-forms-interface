/*
 * XML Type:  Address
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Address
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML Address(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface Address extends org.xvergabe.xsd.forms.components.x20.AbstractComponent
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Address.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("address1d6dtype");
    
    /**
     * Gets the "street" element
     */
    org.xvergabe.xsd.forms.components.x20.String getStreet();
    
    /**
     * True if has "street" element
     */
    boolean isSetStreet();
    
    /**
     * Sets the "street" element
     */
    void setStreet(org.xvergabe.xsd.forms.components.x20.String street);
    
    /**
     * Appends and returns a new empty "street" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewStreet();
    
    /**
     * Unsets the "street" element
     */
    void unsetStreet();
    
    /**
     * Gets the "number" element
     */
    org.xvergabe.xsd.forms.components.x20.String getNumber();
    
    /**
     * True if has "number" element
     */
    boolean isSetNumber();
    
    /**
     * Sets the "number" element
     */
    void setNumber(org.xvergabe.xsd.forms.components.x20.String number);
    
    /**
     * Appends and returns a new empty "number" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewNumber();
    
    /**
     * Unsets the "number" element
     */
    void unsetNumber();
    
    /**
     * Gets the "postalCode" element
     */
    org.xvergabe.xsd.forms.components.x20.String getPostalCode();
    
    /**
     * True if has "postalCode" element
     */
    boolean isSetPostalCode();
    
    /**
     * Sets the "postalCode" element
     */
    void setPostalCode(org.xvergabe.xsd.forms.components.x20.String postalCode);
    
    /**
     * Appends and returns a new empty "postalCode" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewPostalCode();
    
    /**
     * Unsets the "postalCode" element
     */
    void unsetPostalCode();
    
    /**
     * Gets the "postOfficeBox" element
     */
    org.xvergabe.xsd.forms.components.x20.String getPostOfficeBox();
    
    /**
     * True if has "postOfficeBox" element
     */
    boolean isSetPostOfficeBox();
    
    /**
     * Sets the "postOfficeBox" element
     */
    void setPostOfficeBox(org.xvergabe.xsd.forms.components.x20.String postOfficeBox);
    
    /**
     * Appends and returns a new empty "postOfficeBox" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewPostOfficeBox();
    
    /**
     * Unsets the "postOfficeBox" element
     */
    void unsetPostOfficeBox();
    
    /**
     * Gets the "postOfficeBoxCode" element
     */
    org.xvergabe.xsd.forms.components.x20.String getPostOfficeBoxCode();
    
    /**
     * True if has "postOfficeBoxCode" element
     */
    boolean isSetPostOfficeBoxCode();
    
    /**
     * Sets the "postOfficeBoxCode" element
     */
    void setPostOfficeBoxCode(org.xvergabe.xsd.forms.components.x20.String postOfficeBoxCode);
    
    /**
     * Appends and returns a new empty "postOfficeBoxCode" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewPostOfficeBoxCode();
    
    /**
     * Unsets the "postOfficeBoxCode" element
     */
    void unsetPostOfficeBoxCode();
    
    /**
     * Gets the "city" element
     */
    org.xvergabe.xsd.forms.components.x20.String getCity();
    
    /**
     * True if has "city" element
     */
    boolean isSetCity();
    
    /**
     * Sets the "city" element
     */
    void setCity(org.xvergabe.xsd.forms.components.x20.String city);
    
    /**
     * Appends and returns a new empty "city" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewCity();
    
    /**
     * Unsets the "city" element
     */
    void unsetCity();
    
    /**
     * Gets the "district" element
     */
    org.xvergabe.xsd.forms.components.x20.String getDistrict();
    
    /**
     * True if has "district" element
     */
    boolean isSetDistrict();
    
    /**
     * Sets the "district" element
     */
    void setDistrict(org.xvergabe.xsd.forms.components.x20.String district);
    
    /**
     * Appends and returns a new empty "district" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewDistrict();
    
    /**
     * Unsets the "district" element
     */
    void unsetDistrict();
    
    /**
     * Gets the "federalState" element
     */
    org.xvergabe.xsd.forms.components.x20.String getFederalState();
    
    /**
     * True if has "federalState" element
     */
    boolean isSetFederalState();
    
    /**
     * Sets the "federalState" element
     */
    void setFederalState(org.xvergabe.xsd.forms.components.x20.String federalState);
    
    /**
     * Appends and returns a new empty "federalState" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewFederalState();
    
    /**
     * Unsets the "federalState" element
     */
    void unsetFederalState();
    
    /**
     * Gets the "state" element
     */
    org.xvergabe.xsd.forms.components.x20.String getState();
    
    /**
     * True if has "state" element
     */
    boolean isSetState();
    
    /**
     * Sets the "state" element
     */
    void setState(org.xvergabe.xsd.forms.components.x20.String state);
    
    /**
     * Appends and returns a new empty "state" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewState();
    
    /**
     * Unsets the "state" element
     */
    void unsetState();
    
    /**
     * Gets the "furtherInformation" element
     */
    org.xvergabe.xsd.forms.components.x20.String getFurtherInformation();
    
    /**
     * True if has "furtherInformation" element
     */
    boolean isSetFurtherInformation();
    
    /**
     * Sets the "furtherInformation" element
     */
    void setFurtherInformation(org.xvergabe.xsd.forms.components.x20.String furtherInformation);
    
    /**
     * Appends and returns a new empty "furtherInformation" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewFurtherInformation();
    
    /**
     * Unsets the "furtherInformation" element
     */
    void unsetFurtherInformation();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.Address newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Address parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Address parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Address parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Address) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
