/*
 * XML Type:  Bidder
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Bidder
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML Bidder(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface Bidder extends org.xvergabe.xsd.forms.components.x20.Organization
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Bidder.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("bidder9b3btype");
    
    /**
     * Gets the "customsNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String getCustomsNumber();
    
    /**
     * True if has "customsNumber" element
     */
    boolean isSetCustomsNumber();
    
    /**
     * Sets the "customsNumber" element
     */
    void setCustomsNumber(org.xvergabe.xsd.forms.components.x20.String customsNumber);
    
    /**
     * Appends and returns a new empty "customsNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewCustomsNumber();
    
    /**
     * Unsets the "customsNumber" element
     */
    void unsetCustomsNumber();
    
    /**
     * Gets the "taxId" element
     */
    org.xvergabe.xsd.forms.components.x20.String getTaxId();
    
    /**
     * True if has "taxId" element
     */
    boolean isSetTaxId();
    
    /**
     * Sets the "taxId" element
     */
    void setTaxId(org.xvergabe.xsd.forms.components.x20.String taxId);
    
    /**
     * Appends and returns a new empty "taxId" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewTaxId();
    
    /**
     * Unsets the "taxId" element
     */
    void unsetTaxId();
    
    /**
     * Gets the "employeeHistory" element
     */
    org.xvergabe.xsd.forms.components.x20.CountHistory getEmployeeHistory();
    
    /**
     * True if has "employeeHistory" element
     */
    boolean isSetEmployeeHistory();
    
    /**
     * Sets the "employeeHistory" element
     */
    void setEmployeeHistory(org.xvergabe.xsd.forms.components.x20.CountHistory employeeHistory);
    
    /**
     * Appends and returns a new empty "employeeHistory" element
     */
    org.xvergabe.xsd.forms.components.x20.CountHistory addNewEmployeeHistory();
    
    /**
     * Unsets the "employeeHistory" element
     */
    void unsetEmployeeHistory();
    
    /**
     * Gets the "businessBranch" element
     */
    org.xvergabe.xsd.forms.components.x20.String getBusinessBranch();
    
    /**
     * True if has "businessBranch" element
     */
    boolean isSetBusinessBranch();
    
    /**
     * Sets the "businessBranch" element
     */
    void setBusinessBranch(org.xvergabe.xsd.forms.components.x20.String businessBranch);
    
    /**
     * Appends and returns a new empty "businessBranch" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewBusinessBranch();
    
    /**
     * Unsets the "businessBranch" element
     */
    void unsetBusinessBranch();
    
    /**
     * Gets the "customerNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String getCustomerNumber();
    
    /**
     * True if has "customerNumber" element
     */
    boolean isSetCustomerNumber();
    
    /**
     * Sets the "customerNumber" element
     */
    void setCustomerNumber(org.xvergabe.xsd.forms.components.x20.String customerNumber);
    
    /**
     * Appends and returns a new empty "customerNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewCustomerNumber();
    
    /**
     * Unsets the "customerNumber" element
     */
    void unsetCustomerNumber();
    
    /**
     * Gets the "customerNumber2" element
     */
    org.xvergabe.xsd.forms.components.x20.String getCustomerNumber2();
    
    /**
     * True if has "customerNumber2" element
     */
    boolean isSetCustomerNumber2();
    
    /**
     * Sets the "customerNumber2" element
     */
    void setCustomerNumber2(org.xvergabe.xsd.forms.components.x20.String customerNumber2);
    
    /**
     * Appends and returns a new empty "customerNumber2" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewCustomerNumber2();
    
    /**
     * Unsets the "customerNumber2" element
     */
    void unsetCustomerNumber2();
    
    /**
     * Gets the "businessType" element
     */
    org.xvergabe.xsd.forms.components.x20.String getBusinessType();
    
    /**
     * True if has "businessType" element
     */
    boolean isSetBusinessType();
    
    /**
     * Sets the "businessType" element
     */
    void setBusinessType(org.xvergabe.xsd.forms.components.x20.String businessType);
    
    /**
     * Appends and returns a new empty "businessType" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewBusinessType();
    
    /**
     * Unsets the "businessType" element
     */
    void unsetBusinessType();
    
    /**
     * Gets the "revenueHistory" element
     */
    org.xvergabe.xsd.forms.components.x20.String getRevenueHistory();
    
    /**
     * True if has "revenueHistory" element
     */
    boolean isSetRevenueHistory();
    
    /**
     * Sets the "revenueHistory" element
     */
    void setRevenueHistory(org.xvergabe.xsd.forms.components.x20.String revenueHistory);
    
    /**
     * Appends and returns a new empty "revenueHistory" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewRevenueHistory();
    
    /**
     * Unsets the "revenueHistory" element
     */
    void unsetRevenueHistory();
    
    /**
     * Gets the "tradeRegisterNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String getTradeRegisterNumber();
    
    /**
     * True if has "tradeRegisterNumber" element
     */
    boolean isSetTradeRegisterNumber();
    
    /**
     * Sets the "tradeRegisterNumber" element
     */
    void setTradeRegisterNumber(org.xvergabe.xsd.forms.components.x20.String tradeRegisterNumber);
    
    /**
     * Appends and returns a new empty "tradeRegisterNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewTradeRegisterNumber();
    
    /**
     * Unsets the "tradeRegisterNumber" element
     */
    void unsetTradeRegisterNumber();
    
    /**
     * Gets the "headQuartersAddress" element
     */
    org.xvergabe.xsd.forms.components.x20.Address getHeadQuartersAddress();
    
    /**
     * True if has "headQuartersAddress" element
     */
    boolean isSetHeadQuartersAddress();
    
    /**
     * Sets the "headQuartersAddress" element
     */
    void setHeadQuartersAddress(org.xvergabe.xsd.forms.components.x20.Address headQuartersAddress);
    
    /**
     * Appends and returns a new empty "headQuartersAddress" element
     */
    org.xvergabe.xsd.forms.components.x20.Address addNewHeadQuartersAddress();
    
    /**
     * Unsets the "headQuartersAddress" element
     */
    void unsetHeadQuartersAddress();
    
    /**
     * Gets the "contact" element
     */
    org.xvergabe.xsd.forms.components.x20.Contact getContact();
    
    /**
     * True if has "contact" element
     */
    boolean isSetContact();
    
    /**
     * Sets the "contact" element
     */
    void setContact(org.xvergabe.xsd.forms.components.x20.Contact contact);
    
    /**
     * Appends and returns a new empty "contact" element
     */
    org.xvergabe.xsd.forms.components.x20.Contact addNewContact();
    
    /**
     * Unsets the "contact" element
     */
    void unsetContact();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.Bidder newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Bidder parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Bidder) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
