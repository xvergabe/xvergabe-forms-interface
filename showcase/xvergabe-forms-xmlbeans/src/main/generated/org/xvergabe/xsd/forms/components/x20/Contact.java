/*
 * XML Type:  Contact
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Contact
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML Contact(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface Contact extends org.xvergabe.xsd.forms.components.x20.AbstractComponent
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Contact.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("contact22c1type");
    
    /**
     * Gets the "phone" element
     */
    org.xvergabe.xsd.forms.components.x20.String getPhone();
    
    /**
     * True if has "phone" element
     */
    boolean isSetPhone();
    
    /**
     * Sets the "phone" element
     */
    void setPhone(org.xvergabe.xsd.forms.components.x20.String phone);
    
    /**
     * Appends and returns a new empty "phone" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewPhone();
    
    /**
     * Unsets the "phone" element
     */
    void unsetPhone();
    
    /**
     * Gets the "fax" element
     */
    org.xvergabe.xsd.forms.components.x20.String getFax();
    
    /**
     * True if has "fax" element
     */
    boolean isSetFax();
    
    /**
     * Sets the "fax" element
     */
    void setFax(org.xvergabe.xsd.forms.components.x20.String fax);
    
    /**
     * Appends and returns a new empty "fax" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewFax();
    
    /**
     * Unsets the "fax" element
     */
    void unsetFax();
    
    /**
     * Gets the "mail" element
     */
    org.xvergabe.xsd.forms.components.x20.String getMail();
    
    /**
     * True if has "mail" element
     */
    boolean isSetMail();
    
    /**
     * Sets the "mail" element
     */
    void setMail(org.xvergabe.xsd.forms.components.x20.String mail);
    
    /**
     * Appends and returns a new empty "mail" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewMail();
    
    /**
     * Unsets the "mail" element
     */
    void unsetMail();
    
    /**
     * Gets the "url" element
     */
    org.xvergabe.xsd.forms.components.x20.Url getUrl();
    
    /**
     * True if has "url" element
     */
    boolean isSetUrl();
    
    /**
     * Sets the "url" element
     */
    void setUrl(org.xvergabe.xsd.forms.components.x20.Url url);
    
    /**
     * Appends and returns a new empty "url" element
     */
    org.xvergabe.xsd.forms.components.x20.Url addNewUrl();
    
    /**
     * Unsets the "url" element
     */
    void unsetUrl();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.Contact newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Contact parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Contact) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
