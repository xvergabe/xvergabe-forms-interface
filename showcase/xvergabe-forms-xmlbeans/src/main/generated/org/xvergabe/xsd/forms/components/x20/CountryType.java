/*
 * XML Type:  CountryType
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.CountryType
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML CountryType(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.CountryType.
 */
public interface CountryType extends org.apache.xmlbeans.XmlToken
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CountryType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("countrytype1471type");
    
    org.apache.xmlbeans.StringEnumAbstractBase enumValue();
    void set(org.apache.xmlbeans.StringEnumAbstractBase e);
    
    static final Enum AF = Enum.forString("AF");
    static final Enum AX = Enum.forString("AX");
    static final Enum AL = Enum.forString("AL");
    static final Enum DZ = Enum.forString("DZ");
    static final Enum AS = Enum.forString("AS");
    static final Enum AD = Enum.forString("AD");
    static final Enum AO = Enum.forString("AO");
    static final Enum AI = Enum.forString("AI");
    static final Enum AQ = Enum.forString("AQ");
    static final Enum AG = Enum.forString("AG");
    static final Enum AR = Enum.forString("AR");
    static final Enum AM = Enum.forString("AM");
    static final Enum AW = Enum.forString("AW");
    static final Enum AU = Enum.forString("AU");
    static final Enum AT = Enum.forString("AT");
    static final Enum AZ = Enum.forString("AZ");
    static final Enum BS = Enum.forString("BS");
    static final Enum BH = Enum.forString("BH");
    static final Enum BD = Enum.forString("BD");
    static final Enum BB = Enum.forString("BB");
    static final Enum BY = Enum.forString("BY");
    static final Enum BE = Enum.forString("BE");
    static final Enum BZ = Enum.forString("BZ");
    static final Enum BJ = Enum.forString("BJ");
    static final Enum BM = Enum.forString("BM");
    static final Enum BT = Enum.forString("BT");
    static final Enum BO = Enum.forString("BO");
    static final Enum BQ = Enum.forString("BQ");
    static final Enum BA = Enum.forString("BA");
    static final Enum BW = Enum.forString("BW");
    static final Enum BV = Enum.forString("BV");
    static final Enum BR = Enum.forString("BR");
    static final Enum IO = Enum.forString("IO");
    static final Enum BN = Enum.forString("BN");
    static final Enum BG = Enum.forString("BG");
    static final Enum BF = Enum.forString("BF");
    static final Enum BI = Enum.forString("BI");
    static final Enum KH = Enum.forString("KH");
    static final Enum CM = Enum.forString("CM");
    static final Enum CA = Enum.forString("CA");
    static final Enum CV = Enum.forString("CV");
    static final Enum KY = Enum.forString("KY");
    static final Enum CF = Enum.forString("CF");
    static final Enum TD = Enum.forString("TD");
    static final Enum CL = Enum.forString("CL");
    static final Enum CN = Enum.forString("CN");
    static final Enum CX = Enum.forString("CX");
    static final Enum CC = Enum.forString("CC");
    static final Enum CO = Enum.forString("CO");
    static final Enum KM = Enum.forString("KM");
    static final Enum CG = Enum.forString("CG");
    static final Enum CD = Enum.forString("CD");
    static final Enum CK = Enum.forString("CK");
    static final Enum CR = Enum.forString("CR");
    static final Enum CI = Enum.forString("CI");
    static final Enum HR = Enum.forString("HR");
    static final Enum CU = Enum.forString("CU");
    static final Enum CW = Enum.forString("CW");
    static final Enum CY = Enum.forString("CY");
    static final Enum CZ = Enum.forString("CZ");
    static final Enum DK = Enum.forString("DK");
    static final Enum DJ = Enum.forString("DJ");
    static final Enum DM = Enum.forString("DM");
    static final Enum DO = Enum.forString("DO");
    static final Enum EC = Enum.forString("EC");
    static final Enum EG = Enum.forString("EG");
    static final Enum SV = Enum.forString("SV");
    static final Enum GQ = Enum.forString("GQ");
    static final Enum ER = Enum.forString("ER");
    static final Enum EE = Enum.forString("EE");
    static final Enum ET = Enum.forString("ET");
    static final Enum FK = Enum.forString("FK");
    static final Enum FO = Enum.forString("FO");
    static final Enum FJ = Enum.forString("FJ");
    static final Enum FI = Enum.forString("FI");
    static final Enum FR = Enum.forString("FR");
    static final Enum GF = Enum.forString("GF");
    static final Enum PF = Enum.forString("PF");
    static final Enum TF = Enum.forString("TF");
    static final Enum GA = Enum.forString("GA");
    static final Enum GM = Enum.forString("GM");
    static final Enum GE = Enum.forString("GE");
    static final Enum DE = Enum.forString("DE");
    static final Enum GH = Enum.forString("GH");
    static final Enum GI = Enum.forString("GI");
    static final Enum GR = Enum.forString("GR");
    static final Enum GL = Enum.forString("GL");
    static final Enum GD = Enum.forString("GD");
    static final Enum GP = Enum.forString("GP");
    static final Enum GU = Enum.forString("GU");
    static final Enum GT = Enum.forString("GT");
    static final Enum GG = Enum.forString("GG");
    static final Enum GN = Enum.forString("GN");
    static final Enum GW = Enum.forString("GW");
    static final Enum GY = Enum.forString("GY");
    static final Enum HT = Enum.forString("HT");
    static final Enum HM = Enum.forString("HM");
    static final Enum VA = Enum.forString("VA");
    static final Enum HN = Enum.forString("HN");
    static final Enum HK = Enum.forString("HK");
    static final Enum HU = Enum.forString("HU");
    static final Enum IS = Enum.forString("IS");
    static final Enum IN = Enum.forString("IN");
    static final Enum ID = Enum.forString("ID");
    static final Enum IR = Enum.forString("IR");
    static final Enum IQ = Enum.forString("IQ");
    static final Enum IE = Enum.forString("IE");
    static final Enum IM = Enum.forString("IM");
    static final Enum IL = Enum.forString("IL");
    static final Enum IT = Enum.forString("IT");
    static final Enum JM = Enum.forString("JM");
    static final Enum JP = Enum.forString("JP");
    static final Enum JE = Enum.forString("JE");
    static final Enum JO = Enum.forString("JO");
    static final Enum KZ = Enum.forString("KZ");
    static final Enum KE = Enum.forString("KE");
    static final Enum KI = Enum.forString("KI");
    static final Enum KP = Enum.forString("KP");
    static final Enum KR = Enum.forString("KR");
    static final Enum KW = Enum.forString("KW");
    static final Enum KG = Enum.forString("KG");
    static final Enum LA = Enum.forString("LA");
    static final Enum LV = Enum.forString("LV");
    static final Enum LB = Enum.forString("LB");
    static final Enum LS = Enum.forString("LS");
    static final Enum LR = Enum.forString("LR");
    static final Enum LY = Enum.forString("LY");
    static final Enum LI = Enum.forString("LI");
    static final Enum LT = Enum.forString("LT");
    static final Enum LU = Enum.forString("LU");
    static final Enum MO = Enum.forString("MO");
    static final Enum MK = Enum.forString("MK");
    static final Enum MG = Enum.forString("MG");
    static final Enum MW = Enum.forString("MW");
    static final Enum MY = Enum.forString("MY");
    static final Enum MV = Enum.forString("MV");
    static final Enum ML = Enum.forString("ML");
    static final Enum MT = Enum.forString("MT");
    static final Enum MH = Enum.forString("MH");
    static final Enum MQ = Enum.forString("MQ");
    static final Enum MR = Enum.forString("MR");
    static final Enum MU = Enum.forString("MU");
    static final Enum YT = Enum.forString("YT");
    static final Enum MX = Enum.forString("MX");
    static final Enum FM = Enum.forString("FM");
    static final Enum MD = Enum.forString("MD");
    static final Enum MC = Enum.forString("MC");
    static final Enum MN = Enum.forString("MN");
    static final Enum ME = Enum.forString("ME");
    static final Enum MS = Enum.forString("MS");
    static final Enum MA = Enum.forString("MA");
    static final Enum MZ = Enum.forString("MZ");
    static final Enum MM = Enum.forString("MM");
    static final Enum NA = Enum.forString("NA");
    static final Enum NR = Enum.forString("NR");
    static final Enum NP = Enum.forString("NP");
    static final Enum NL = Enum.forString("NL");
    static final Enum NC = Enum.forString("NC");
    static final Enum NZ = Enum.forString("NZ");
    static final Enum NI = Enum.forString("NI");
    static final Enum NE = Enum.forString("NE");
    static final Enum NG = Enum.forString("NG");
    static final Enum NU = Enum.forString("NU");
    static final Enum NF = Enum.forString("NF");
    static final Enum MP = Enum.forString("MP");
    static final Enum NO = Enum.forString("NO");
    static final Enum OM = Enum.forString("OM");
    static final Enum PK = Enum.forString("PK");
    static final Enum PW = Enum.forString("PW");
    static final Enum PS = Enum.forString("PS");
    static final Enum PA = Enum.forString("PA");
    static final Enum PG = Enum.forString("PG");
    static final Enum PY = Enum.forString("PY");
    static final Enum PE = Enum.forString("PE");
    static final Enum PH = Enum.forString("PH");
    static final Enum PN = Enum.forString("PN");
    static final Enum PL = Enum.forString("PL");
    static final Enum PT = Enum.forString("PT");
    static final Enum PR = Enum.forString("PR");
    static final Enum QA = Enum.forString("QA");
    static final Enum RE = Enum.forString("RE");
    static final Enum RO = Enum.forString("RO");
    static final Enum RU = Enum.forString("RU");
    static final Enum RW = Enum.forString("RW");
    static final Enum BL = Enum.forString("BL");
    static final Enum SH = Enum.forString("SH");
    static final Enum KN = Enum.forString("KN");
    static final Enum LC = Enum.forString("LC");
    static final Enum MF = Enum.forString("MF");
    static final Enum PM = Enum.forString("PM");
    static final Enum VC = Enum.forString("VC");
    static final Enum WS = Enum.forString("WS");
    static final Enum SM = Enum.forString("SM");
    static final Enum ST = Enum.forString("ST");
    static final Enum SA = Enum.forString("SA");
    static final Enum SN = Enum.forString("SN");
    static final Enum RS = Enum.forString("RS");
    static final Enum SC = Enum.forString("SC");
    static final Enum SL = Enum.forString("SL");
    static final Enum SG = Enum.forString("SG");
    static final Enum SX = Enum.forString("SX");
    static final Enum SK = Enum.forString("SK");
    static final Enum SI = Enum.forString("SI");
    static final Enum SB = Enum.forString("SB");
    static final Enum SO = Enum.forString("SO");
    static final Enum ZA = Enum.forString("ZA");
    static final Enum GS = Enum.forString("GS");
    static final Enum SS = Enum.forString("SS");
    static final Enum ES = Enum.forString("ES");
    static final Enum LK = Enum.forString("LK");
    static final Enum SD = Enum.forString("SD");
    static final Enum SR = Enum.forString("SR");
    static final Enum SJ = Enum.forString("SJ");
    static final Enum SZ = Enum.forString("SZ");
    static final Enum SE = Enum.forString("SE");
    static final Enum CH = Enum.forString("CH");
    static final Enum SY = Enum.forString("SY");
    static final Enum TW = Enum.forString("TW");
    static final Enum TJ = Enum.forString("TJ");
    static final Enum TZ = Enum.forString("TZ");
    static final Enum TH = Enum.forString("TH");
    static final Enum TL = Enum.forString("TL");
    static final Enum TG = Enum.forString("TG");
    static final Enum TK = Enum.forString("TK");
    static final Enum TO = Enum.forString("TO");
    static final Enum TT = Enum.forString("TT");
    static final Enum TN = Enum.forString("TN");
    static final Enum TR = Enum.forString("TR");
    static final Enum TM = Enum.forString("TM");
    static final Enum TC = Enum.forString("TC");
    static final Enum TV = Enum.forString("TV");
    static final Enum UG = Enum.forString("UG");
    static final Enum UA = Enum.forString("UA");
    static final Enum AE = Enum.forString("AE");
    static final Enum GB = Enum.forString("GB");
    static final Enum US = Enum.forString("US");
    static final Enum UM = Enum.forString("UM");
    static final Enum UY = Enum.forString("UY");
    static final Enum UZ = Enum.forString("UZ");
    static final Enum VU = Enum.forString("VU");
    static final Enum VE = Enum.forString("VE");
    static final Enum VN = Enum.forString("VN");
    static final Enum VG = Enum.forString("VG");
    static final Enum VI = Enum.forString("VI");
    static final Enum WF = Enum.forString("WF");
    static final Enum EH = Enum.forString("EH");
    static final Enum YE = Enum.forString("YE");
    static final Enum ZM = Enum.forString("ZM");
    static final Enum ZW = Enum.forString("ZW");
    static final Enum X = Enum.forString("");
    
    static final int INT_AF = Enum.INT_AF;
    static final int INT_AX = Enum.INT_AX;
    static final int INT_AL = Enum.INT_AL;
    static final int INT_DZ = Enum.INT_DZ;
    static final int INT_AS = Enum.INT_AS;
    static final int INT_AD = Enum.INT_AD;
    static final int INT_AO = Enum.INT_AO;
    static final int INT_AI = Enum.INT_AI;
    static final int INT_AQ = Enum.INT_AQ;
    static final int INT_AG = Enum.INT_AG;
    static final int INT_AR = Enum.INT_AR;
    static final int INT_AM = Enum.INT_AM;
    static final int INT_AW = Enum.INT_AW;
    static final int INT_AU = Enum.INT_AU;
    static final int INT_AT = Enum.INT_AT;
    static final int INT_AZ = Enum.INT_AZ;
    static final int INT_BS = Enum.INT_BS;
    static final int INT_BH = Enum.INT_BH;
    static final int INT_BD = Enum.INT_BD;
    static final int INT_BB = Enum.INT_BB;
    static final int INT_BY = Enum.INT_BY;
    static final int INT_BE = Enum.INT_BE;
    static final int INT_BZ = Enum.INT_BZ;
    static final int INT_BJ = Enum.INT_BJ;
    static final int INT_BM = Enum.INT_BM;
    static final int INT_BT = Enum.INT_BT;
    static final int INT_BO = Enum.INT_BO;
    static final int INT_BQ = Enum.INT_BQ;
    static final int INT_BA = Enum.INT_BA;
    static final int INT_BW = Enum.INT_BW;
    static final int INT_BV = Enum.INT_BV;
    static final int INT_BR = Enum.INT_BR;
    static final int INT_IO = Enum.INT_IO;
    static final int INT_BN = Enum.INT_BN;
    static final int INT_BG = Enum.INT_BG;
    static final int INT_BF = Enum.INT_BF;
    static final int INT_BI = Enum.INT_BI;
    static final int INT_KH = Enum.INT_KH;
    static final int INT_CM = Enum.INT_CM;
    static final int INT_CA = Enum.INT_CA;
    static final int INT_CV = Enum.INT_CV;
    static final int INT_KY = Enum.INT_KY;
    static final int INT_CF = Enum.INT_CF;
    static final int INT_TD = Enum.INT_TD;
    static final int INT_CL = Enum.INT_CL;
    static final int INT_CN = Enum.INT_CN;
    static final int INT_CX = Enum.INT_CX;
    static final int INT_CC = Enum.INT_CC;
    static final int INT_CO = Enum.INT_CO;
    static final int INT_KM = Enum.INT_KM;
    static final int INT_CG = Enum.INT_CG;
    static final int INT_CD = Enum.INT_CD;
    static final int INT_CK = Enum.INT_CK;
    static final int INT_CR = Enum.INT_CR;
    static final int INT_CI = Enum.INT_CI;
    static final int INT_HR = Enum.INT_HR;
    static final int INT_CU = Enum.INT_CU;
    static final int INT_CW = Enum.INT_CW;
    static final int INT_CY = Enum.INT_CY;
    static final int INT_CZ = Enum.INT_CZ;
    static final int INT_DK = Enum.INT_DK;
    static final int INT_DJ = Enum.INT_DJ;
    static final int INT_DM = Enum.INT_DM;
    static final int INT_DO = Enum.INT_DO;
    static final int INT_EC = Enum.INT_EC;
    static final int INT_EG = Enum.INT_EG;
    static final int INT_SV = Enum.INT_SV;
    static final int INT_GQ = Enum.INT_GQ;
    static final int INT_ER = Enum.INT_ER;
    static final int INT_EE = Enum.INT_EE;
    static final int INT_ET = Enum.INT_ET;
    static final int INT_FK = Enum.INT_FK;
    static final int INT_FO = Enum.INT_FO;
    static final int INT_FJ = Enum.INT_FJ;
    static final int INT_FI = Enum.INT_FI;
    static final int INT_FR = Enum.INT_FR;
    static final int INT_GF = Enum.INT_GF;
    static final int INT_PF = Enum.INT_PF;
    static final int INT_TF = Enum.INT_TF;
    static final int INT_GA = Enum.INT_GA;
    static final int INT_GM = Enum.INT_GM;
    static final int INT_GE = Enum.INT_GE;
    static final int INT_DE = Enum.INT_DE;
    static final int INT_GH = Enum.INT_GH;
    static final int INT_GI = Enum.INT_GI;
    static final int INT_GR = Enum.INT_GR;
    static final int INT_GL = Enum.INT_GL;
    static final int INT_GD = Enum.INT_GD;
    static final int INT_GP = Enum.INT_GP;
    static final int INT_GU = Enum.INT_GU;
    static final int INT_GT = Enum.INT_GT;
    static final int INT_GG = Enum.INT_GG;
    static final int INT_GN = Enum.INT_GN;
    static final int INT_GW = Enum.INT_GW;
    static final int INT_GY = Enum.INT_GY;
    static final int INT_HT = Enum.INT_HT;
    static final int INT_HM = Enum.INT_HM;
    static final int INT_VA = Enum.INT_VA;
    static final int INT_HN = Enum.INT_HN;
    static final int INT_HK = Enum.INT_HK;
    static final int INT_HU = Enum.INT_HU;
    static final int INT_IS = Enum.INT_IS;
    static final int INT_IN = Enum.INT_IN;
    static final int INT_ID = Enum.INT_ID;
    static final int INT_IR = Enum.INT_IR;
    static final int INT_IQ = Enum.INT_IQ;
    static final int INT_IE = Enum.INT_IE;
    static final int INT_IM = Enum.INT_IM;
    static final int INT_IL = Enum.INT_IL;
    static final int INT_IT = Enum.INT_IT;
    static final int INT_JM = Enum.INT_JM;
    static final int INT_JP = Enum.INT_JP;
    static final int INT_JE = Enum.INT_JE;
    static final int INT_JO = Enum.INT_JO;
    static final int INT_KZ = Enum.INT_KZ;
    static final int INT_KE = Enum.INT_KE;
    static final int INT_KI = Enum.INT_KI;
    static final int INT_KP = Enum.INT_KP;
    static final int INT_KR = Enum.INT_KR;
    static final int INT_KW = Enum.INT_KW;
    static final int INT_KG = Enum.INT_KG;
    static final int INT_LA = Enum.INT_LA;
    static final int INT_LV = Enum.INT_LV;
    static final int INT_LB = Enum.INT_LB;
    static final int INT_LS = Enum.INT_LS;
    static final int INT_LR = Enum.INT_LR;
    static final int INT_LY = Enum.INT_LY;
    static final int INT_LI = Enum.INT_LI;
    static final int INT_LT = Enum.INT_LT;
    static final int INT_LU = Enum.INT_LU;
    static final int INT_MO = Enum.INT_MO;
    static final int INT_MK = Enum.INT_MK;
    static final int INT_MG = Enum.INT_MG;
    static final int INT_MW = Enum.INT_MW;
    static final int INT_MY = Enum.INT_MY;
    static final int INT_MV = Enum.INT_MV;
    static final int INT_ML = Enum.INT_ML;
    static final int INT_MT = Enum.INT_MT;
    static final int INT_MH = Enum.INT_MH;
    static final int INT_MQ = Enum.INT_MQ;
    static final int INT_MR = Enum.INT_MR;
    static final int INT_MU = Enum.INT_MU;
    static final int INT_YT = Enum.INT_YT;
    static final int INT_MX = Enum.INT_MX;
    static final int INT_FM = Enum.INT_FM;
    static final int INT_MD = Enum.INT_MD;
    static final int INT_MC = Enum.INT_MC;
    static final int INT_MN = Enum.INT_MN;
    static final int INT_ME = Enum.INT_ME;
    static final int INT_MS = Enum.INT_MS;
    static final int INT_MA = Enum.INT_MA;
    static final int INT_MZ = Enum.INT_MZ;
    static final int INT_MM = Enum.INT_MM;
    static final int INT_NA = Enum.INT_NA;
    static final int INT_NR = Enum.INT_NR;
    static final int INT_NP = Enum.INT_NP;
    static final int INT_NL = Enum.INT_NL;
    static final int INT_NC = Enum.INT_NC;
    static final int INT_NZ = Enum.INT_NZ;
    static final int INT_NI = Enum.INT_NI;
    static final int INT_NE = Enum.INT_NE;
    static final int INT_NG = Enum.INT_NG;
    static final int INT_NU = Enum.INT_NU;
    static final int INT_NF = Enum.INT_NF;
    static final int INT_MP = Enum.INT_MP;
    static final int INT_NO = Enum.INT_NO;
    static final int INT_OM = Enum.INT_OM;
    static final int INT_PK = Enum.INT_PK;
    static final int INT_PW = Enum.INT_PW;
    static final int INT_PS = Enum.INT_PS;
    static final int INT_PA = Enum.INT_PA;
    static final int INT_PG = Enum.INT_PG;
    static final int INT_PY = Enum.INT_PY;
    static final int INT_PE = Enum.INT_PE;
    static final int INT_PH = Enum.INT_PH;
    static final int INT_PN = Enum.INT_PN;
    static final int INT_PL = Enum.INT_PL;
    static final int INT_PT = Enum.INT_PT;
    static final int INT_PR = Enum.INT_PR;
    static final int INT_QA = Enum.INT_QA;
    static final int INT_RE = Enum.INT_RE;
    static final int INT_RO = Enum.INT_RO;
    static final int INT_RU = Enum.INT_RU;
    static final int INT_RW = Enum.INT_RW;
    static final int INT_BL = Enum.INT_BL;
    static final int INT_SH = Enum.INT_SH;
    static final int INT_KN = Enum.INT_KN;
    static final int INT_LC = Enum.INT_LC;
    static final int INT_MF = Enum.INT_MF;
    static final int INT_PM = Enum.INT_PM;
    static final int INT_VC = Enum.INT_VC;
    static final int INT_WS = Enum.INT_WS;
    static final int INT_SM = Enum.INT_SM;
    static final int INT_ST = Enum.INT_ST;
    static final int INT_SA = Enum.INT_SA;
    static final int INT_SN = Enum.INT_SN;
    static final int INT_RS = Enum.INT_RS;
    static final int INT_SC = Enum.INT_SC;
    static final int INT_SL = Enum.INT_SL;
    static final int INT_SG = Enum.INT_SG;
    static final int INT_SX = Enum.INT_SX;
    static final int INT_SK = Enum.INT_SK;
    static final int INT_SI = Enum.INT_SI;
    static final int INT_SB = Enum.INT_SB;
    static final int INT_SO = Enum.INT_SO;
    static final int INT_ZA = Enum.INT_ZA;
    static final int INT_GS = Enum.INT_GS;
    static final int INT_SS = Enum.INT_SS;
    static final int INT_ES = Enum.INT_ES;
    static final int INT_LK = Enum.INT_LK;
    static final int INT_SD = Enum.INT_SD;
    static final int INT_SR = Enum.INT_SR;
    static final int INT_SJ = Enum.INT_SJ;
    static final int INT_SZ = Enum.INT_SZ;
    static final int INT_SE = Enum.INT_SE;
    static final int INT_CH = Enum.INT_CH;
    static final int INT_SY = Enum.INT_SY;
    static final int INT_TW = Enum.INT_TW;
    static final int INT_TJ = Enum.INT_TJ;
    static final int INT_TZ = Enum.INT_TZ;
    static final int INT_TH = Enum.INT_TH;
    static final int INT_TL = Enum.INT_TL;
    static final int INT_TG = Enum.INT_TG;
    static final int INT_TK = Enum.INT_TK;
    static final int INT_TO = Enum.INT_TO;
    static final int INT_TT = Enum.INT_TT;
    static final int INT_TN = Enum.INT_TN;
    static final int INT_TR = Enum.INT_TR;
    static final int INT_TM = Enum.INT_TM;
    static final int INT_TC = Enum.INT_TC;
    static final int INT_TV = Enum.INT_TV;
    static final int INT_UG = Enum.INT_UG;
    static final int INT_UA = Enum.INT_UA;
    static final int INT_AE = Enum.INT_AE;
    static final int INT_GB = Enum.INT_GB;
    static final int INT_US = Enum.INT_US;
    static final int INT_UM = Enum.INT_UM;
    static final int INT_UY = Enum.INT_UY;
    static final int INT_UZ = Enum.INT_UZ;
    static final int INT_VU = Enum.INT_VU;
    static final int INT_VE = Enum.INT_VE;
    static final int INT_VN = Enum.INT_VN;
    static final int INT_VG = Enum.INT_VG;
    static final int INT_VI = Enum.INT_VI;
    static final int INT_WF = Enum.INT_WF;
    static final int INT_EH = Enum.INT_EH;
    static final int INT_YE = Enum.INT_YE;
    static final int INT_ZM = Enum.INT_ZM;
    static final int INT_ZW = Enum.INT_ZW;
    static final int INT_X = Enum.INT_X;
    
    /**
     * Enumeration value class for org.xvergabe.xsd.forms.components.x20.CountryType.
     * These enum values can be used as follows:
     * <pre>
     * enum.toString(); // returns the string value of the enum
     * enum.intValue(); // returns an int value, useful for switches
     * // e.g., case Enum.INT_AF
     * Enum.forString(s); // returns the enum value for a string
     * Enum.forInt(i); // returns the enum value for an int
     * </pre>
     * Enumeration objects are immutable singleton objects that
     * can be compared using == object equality. They have no
     * public constructor. See the constants defined within this
     * class for all the valid values.
     */
    static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
    {
        /**
         * Returns the enum value for a string, or null if none.
         */
        public static Enum forString(java.lang.String s)
            { return (Enum)table.forString(s); }
        /**
         * Returns the enum value corresponding to an int, or null if none.
         */
        public static Enum forInt(int i)
            { return (Enum)table.forInt(i); }
        
        private Enum(java.lang.String s, int i)
            { super(s, i); }
        
        static final int INT_AF = 1;
        static final int INT_AX = 2;
        static final int INT_AL = 3;
        static final int INT_DZ = 4;
        static final int INT_AS = 5;
        static final int INT_AD = 6;
        static final int INT_AO = 7;
        static final int INT_AI = 8;
        static final int INT_AQ = 9;
        static final int INT_AG = 10;
        static final int INT_AR = 11;
        static final int INT_AM = 12;
        static final int INT_AW = 13;
        static final int INT_AU = 14;
        static final int INT_AT = 15;
        static final int INT_AZ = 16;
        static final int INT_BS = 17;
        static final int INT_BH = 18;
        static final int INT_BD = 19;
        static final int INT_BB = 20;
        static final int INT_BY = 21;
        static final int INT_BE = 22;
        static final int INT_BZ = 23;
        static final int INT_BJ = 24;
        static final int INT_BM = 25;
        static final int INT_BT = 26;
        static final int INT_BO = 27;
        static final int INT_BQ = 28;
        static final int INT_BA = 29;
        static final int INT_BW = 30;
        static final int INT_BV = 31;
        static final int INT_BR = 32;
        static final int INT_IO = 33;
        static final int INT_BN = 34;
        static final int INT_BG = 35;
        static final int INT_BF = 36;
        static final int INT_BI = 37;
        static final int INT_KH = 38;
        static final int INT_CM = 39;
        static final int INT_CA = 40;
        static final int INT_CV = 41;
        static final int INT_KY = 42;
        static final int INT_CF = 43;
        static final int INT_TD = 44;
        static final int INT_CL = 45;
        static final int INT_CN = 46;
        static final int INT_CX = 47;
        static final int INT_CC = 48;
        static final int INT_CO = 49;
        static final int INT_KM = 50;
        static final int INT_CG = 51;
        static final int INT_CD = 52;
        static final int INT_CK = 53;
        static final int INT_CR = 54;
        static final int INT_CI = 55;
        static final int INT_HR = 56;
        static final int INT_CU = 57;
        static final int INT_CW = 58;
        static final int INT_CY = 59;
        static final int INT_CZ = 60;
        static final int INT_DK = 61;
        static final int INT_DJ = 62;
        static final int INT_DM = 63;
        static final int INT_DO = 64;
        static final int INT_EC = 65;
        static final int INT_EG = 66;
        static final int INT_SV = 67;
        static final int INT_GQ = 68;
        static final int INT_ER = 69;
        static final int INT_EE = 70;
        static final int INT_ET = 71;
        static final int INT_FK = 72;
        static final int INT_FO = 73;
        static final int INT_FJ = 74;
        static final int INT_FI = 75;
        static final int INT_FR = 76;
        static final int INT_GF = 77;
        static final int INT_PF = 78;
        static final int INT_TF = 79;
        static final int INT_GA = 80;
        static final int INT_GM = 81;
        static final int INT_GE = 82;
        static final int INT_DE = 83;
        static final int INT_GH = 84;
        static final int INT_GI = 85;
        static final int INT_GR = 86;
        static final int INT_GL = 87;
        static final int INT_GD = 88;
        static final int INT_GP = 89;
        static final int INT_GU = 90;
        static final int INT_GT = 91;
        static final int INT_GG = 92;
        static final int INT_GN = 93;
        static final int INT_GW = 94;
        static final int INT_GY = 95;
        static final int INT_HT = 96;
        static final int INT_HM = 97;
        static final int INT_VA = 98;
        static final int INT_HN = 99;
        static final int INT_HK = 100;
        static final int INT_HU = 101;
        static final int INT_IS = 102;
        static final int INT_IN = 103;
        static final int INT_ID = 104;
        static final int INT_IR = 105;
        static final int INT_IQ = 106;
        static final int INT_IE = 107;
        static final int INT_IM = 108;
        static final int INT_IL = 109;
        static final int INT_IT = 110;
        static final int INT_JM = 111;
        static final int INT_JP = 112;
        static final int INT_JE = 113;
        static final int INT_JO = 114;
        static final int INT_KZ = 115;
        static final int INT_KE = 116;
        static final int INT_KI = 117;
        static final int INT_KP = 118;
        static final int INT_KR = 119;
        static final int INT_KW = 120;
        static final int INT_KG = 121;
        static final int INT_LA = 122;
        static final int INT_LV = 123;
        static final int INT_LB = 124;
        static final int INT_LS = 125;
        static final int INT_LR = 126;
        static final int INT_LY = 127;
        static final int INT_LI = 128;
        static final int INT_LT = 129;
        static final int INT_LU = 130;
        static final int INT_MO = 131;
        static final int INT_MK = 132;
        static final int INT_MG = 133;
        static final int INT_MW = 134;
        static final int INT_MY = 135;
        static final int INT_MV = 136;
        static final int INT_ML = 137;
        static final int INT_MT = 138;
        static final int INT_MH = 139;
        static final int INT_MQ = 140;
        static final int INT_MR = 141;
        static final int INT_MU = 142;
        static final int INT_YT = 143;
        static final int INT_MX = 144;
        static final int INT_FM = 145;
        static final int INT_MD = 146;
        static final int INT_MC = 147;
        static final int INT_MN = 148;
        static final int INT_ME = 149;
        static final int INT_MS = 150;
        static final int INT_MA = 151;
        static final int INT_MZ = 152;
        static final int INT_MM = 153;
        static final int INT_NA = 154;
        static final int INT_NR = 155;
        static final int INT_NP = 156;
        static final int INT_NL = 157;
        static final int INT_NC = 158;
        static final int INT_NZ = 159;
        static final int INT_NI = 160;
        static final int INT_NE = 161;
        static final int INT_NG = 162;
        static final int INT_NU = 163;
        static final int INT_NF = 164;
        static final int INT_MP = 165;
        static final int INT_NO = 166;
        static final int INT_OM = 167;
        static final int INT_PK = 168;
        static final int INT_PW = 169;
        static final int INT_PS = 170;
        static final int INT_PA = 171;
        static final int INT_PG = 172;
        static final int INT_PY = 173;
        static final int INT_PE = 174;
        static final int INT_PH = 175;
        static final int INT_PN = 176;
        static final int INT_PL = 177;
        static final int INT_PT = 178;
        static final int INT_PR = 179;
        static final int INT_QA = 180;
        static final int INT_RE = 181;
        static final int INT_RO = 182;
        static final int INT_RU = 183;
        static final int INT_RW = 184;
        static final int INT_BL = 185;
        static final int INT_SH = 186;
        static final int INT_KN = 187;
        static final int INT_LC = 188;
        static final int INT_MF = 189;
        static final int INT_PM = 190;
        static final int INT_VC = 191;
        static final int INT_WS = 192;
        static final int INT_SM = 193;
        static final int INT_ST = 194;
        static final int INT_SA = 195;
        static final int INT_SN = 196;
        static final int INT_RS = 197;
        static final int INT_SC = 198;
        static final int INT_SL = 199;
        static final int INT_SG = 200;
        static final int INT_SX = 201;
        static final int INT_SK = 202;
        static final int INT_SI = 203;
        static final int INT_SB = 204;
        static final int INT_SO = 205;
        static final int INT_ZA = 206;
        static final int INT_GS = 207;
        static final int INT_SS = 208;
        static final int INT_ES = 209;
        static final int INT_LK = 210;
        static final int INT_SD = 211;
        static final int INT_SR = 212;
        static final int INT_SJ = 213;
        static final int INT_SZ = 214;
        static final int INT_SE = 215;
        static final int INT_CH = 216;
        static final int INT_SY = 217;
        static final int INT_TW = 218;
        static final int INT_TJ = 219;
        static final int INT_TZ = 220;
        static final int INT_TH = 221;
        static final int INT_TL = 222;
        static final int INT_TG = 223;
        static final int INT_TK = 224;
        static final int INT_TO = 225;
        static final int INT_TT = 226;
        static final int INT_TN = 227;
        static final int INT_TR = 228;
        static final int INT_TM = 229;
        static final int INT_TC = 230;
        static final int INT_TV = 231;
        static final int INT_UG = 232;
        static final int INT_UA = 233;
        static final int INT_AE = 234;
        static final int INT_GB = 235;
        static final int INT_US = 236;
        static final int INT_UM = 237;
        static final int INT_UY = 238;
        static final int INT_UZ = 239;
        static final int INT_VU = 240;
        static final int INT_VE = 241;
        static final int INT_VN = 242;
        static final int INT_VG = 243;
        static final int INT_VI = 244;
        static final int INT_WF = 245;
        static final int INT_EH = 246;
        static final int INT_YE = 247;
        static final int INT_ZM = 248;
        static final int INT_ZW = 249;
        static final int INT_X = 250;
        
        public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
            new org.apache.xmlbeans.StringEnumAbstractBase.Table
        (
            new Enum[]
            {
                new Enum("AF", INT_AF),
                new Enum("AX", INT_AX),
                new Enum("AL", INT_AL),
                new Enum("DZ", INT_DZ),
                new Enum("AS", INT_AS),
                new Enum("AD", INT_AD),
                new Enum("AO", INT_AO),
                new Enum("AI", INT_AI),
                new Enum("AQ", INT_AQ),
                new Enum("AG", INT_AG),
                new Enum("AR", INT_AR),
                new Enum("AM", INT_AM),
                new Enum("AW", INT_AW),
                new Enum("AU", INT_AU),
                new Enum("AT", INT_AT),
                new Enum("AZ", INT_AZ),
                new Enum("BS", INT_BS),
                new Enum("BH", INT_BH),
                new Enum("BD", INT_BD),
                new Enum("BB", INT_BB),
                new Enum("BY", INT_BY),
                new Enum("BE", INT_BE),
                new Enum("BZ", INT_BZ),
                new Enum("BJ", INT_BJ),
                new Enum("BM", INT_BM),
                new Enum("BT", INT_BT),
                new Enum("BO", INT_BO),
                new Enum("BQ", INT_BQ),
                new Enum("BA", INT_BA),
                new Enum("BW", INT_BW),
                new Enum("BV", INT_BV),
                new Enum("BR", INT_BR),
                new Enum("IO", INT_IO),
                new Enum("BN", INT_BN),
                new Enum("BG", INT_BG),
                new Enum("BF", INT_BF),
                new Enum("BI", INT_BI),
                new Enum("KH", INT_KH),
                new Enum("CM", INT_CM),
                new Enum("CA", INT_CA),
                new Enum("CV", INT_CV),
                new Enum("KY", INT_KY),
                new Enum("CF", INT_CF),
                new Enum("TD", INT_TD),
                new Enum("CL", INT_CL),
                new Enum("CN", INT_CN),
                new Enum("CX", INT_CX),
                new Enum("CC", INT_CC),
                new Enum("CO", INT_CO),
                new Enum("KM", INT_KM),
                new Enum("CG", INT_CG),
                new Enum("CD", INT_CD),
                new Enum("CK", INT_CK),
                new Enum("CR", INT_CR),
                new Enum("CI", INT_CI),
                new Enum("HR", INT_HR),
                new Enum("CU", INT_CU),
                new Enum("CW", INT_CW),
                new Enum("CY", INT_CY),
                new Enum("CZ", INT_CZ),
                new Enum("DK", INT_DK),
                new Enum("DJ", INT_DJ),
                new Enum("DM", INT_DM),
                new Enum("DO", INT_DO),
                new Enum("EC", INT_EC),
                new Enum("EG", INT_EG),
                new Enum("SV", INT_SV),
                new Enum("GQ", INT_GQ),
                new Enum("ER", INT_ER),
                new Enum("EE", INT_EE),
                new Enum("ET", INT_ET),
                new Enum("FK", INT_FK),
                new Enum("FO", INT_FO),
                new Enum("FJ", INT_FJ),
                new Enum("FI", INT_FI),
                new Enum("FR", INT_FR),
                new Enum("GF", INT_GF),
                new Enum("PF", INT_PF),
                new Enum("TF", INT_TF),
                new Enum("GA", INT_GA),
                new Enum("GM", INT_GM),
                new Enum("GE", INT_GE),
                new Enum("DE", INT_DE),
                new Enum("GH", INT_GH),
                new Enum("GI", INT_GI),
                new Enum("GR", INT_GR),
                new Enum("GL", INT_GL),
                new Enum("GD", INT_GD),
                new Enum("GP", INT_GP),
                new Enum("GU", INT_GU),
                new Enum("GT", INT_GT),
                new Enum("GG", INT_GG),
                new Enum("GN", INT_GN),
                new Enum("GW", INT_GW),
                new Enum("GY", INT_GY),
                new Enum("HT", INT_HT),
                new Enum("HM", INT_HM),
                new Enum("VA", INT_VA),
                new Enum("HN", INT_HN),
                new Enum("HK", INT_HK),
                new Enum("HU", INT_HU),
                new Enum("IS", INT_IS),
                new Enum("IN", INT_IN),
                new Enum("ID", INT_ID),
                new Enum("IR", INT_IR),
                new Enum("IQ", INT_IQ),
                new Enum("IE", INT_IE),
                new Enum("IM", INT_IM),
                new Enum("IL", INT_IL),
                new Enum("IT", INT_IT),
                new Enum("JM", INT_JM),
                new Enum("JP", INT_JP),
                new Enum("JE", INT_JE),
                new Enum("JO", INT_JO),
                new Enum("KZ", INT_KZ),
                new Enum("KE", INT_KE),
                new Enum("KI", INT_KI),
                new Enum("KP", INT_KP),
                new Enum("KR", INT_KR),
                new Enum("KW", INT_KW),
                new Enum("KG", INT_KG),
                new Enum("LA", INT_LA),
                new Enum("LV", INT_LV),
                new Enum("LB", INT_LB),
                new Enum("LS", INT_LS),
                new Enum("LR", INT_LR),
                new Enum("LY", INT_LY),
                new Enum("LI", INT_LI),
                new Enum("LT", INT_LT),
                new Enum("LU", INT_LU),
                new Enum("MO", INT_MO),
                new Enum("MK", INT_MK),
                new Enum("MG", INT_MG),
                new Enum("MW", INT_MW),
                new Enum("MY", INT_MY),
                new Enum("MV", INT_MV),
                new Enum("ML", INT_ML),
                new Enum("MT", INT_MT),
                new Enum("MH", INT_MH),
                new Enum("MQ", INT_MQ),
                new Enum("MR", INT_MR),
                new Enum("MU", INT_MU),
                new Enum("YT", INT_YT),
                new Enum("MX", INT_MX),
                new Enum("FM", INT_FM),
                new Enum("MD", INT_MD),
                new Enum("MC", INT_MC),
                new Enum("MN", INT_MN),
                new Enum("ME", INT_ME),
                new Enum("MS", INT_MS),
                new Enum("MA", INT_MA),
                new Enum("MZ", INT_MZ),
                new Enum("MM", INT_MM),
                new Enum("NA", INT_NA),
                new Enum("NR", INT_NR),
                new Enum("NP", INT_NP),
                new Enum("NL", INT_NL),
                new Enum("NC", INT_NC),
                new Enum("NZ", INT_NZ),
                new Enum("NI", INT_NI),
                new Enum("NE", INT_NE),
                new Enum("NG", INT_NG),
                new Enum("NU", INT_NU),
                new Enum("NF", INT_NF),
                new Enum("MP", INT_MP),
                new Enum("NO", INT_NO),
                new Enum("OM", INT_OM),
                new Enum("PK", INT_PK),
                new Enum("PW", INT_PW),
                new Enum("PS", INT_PS),
                new Enum("PA", INT_PA),
                new Enum("PG", INT_PG),
                new Enum("PY", INT_PY),
                new Enum("PE", INT_PE),
                new Enum("PH", INT_PH),
                new Enum("PN", INT_PN),
                new Enum("PL", INT_PL),
                new Enum("PT", INT_PT),
                new Enum("PR", INT_PR),
                new Enum("QA", INT_QA),
                new Enum("RE", INT_RE),
                new Enum("RO", INT_RO),
                new Enum("RU", INT_RU),
                new Enum("RW", INT_RW),
                new Enum("BL", INT_BL),
                new Enum("SH", INT_SH),
                new Enum("KN", INT_KN),
                new Enum("LC", INT_LC),
                new Enum("MF", INT_MF),
                new Enum("PM", INT_PM),
                new Enum("VC", INT_VC),
                new Enum("WS", INT_WS),
                new Enum("SM", INT_SM),
                new Enum("ST", INT_ST),
                new Enum("SA", INT_SA),
                new Enum("SN", INT_SN),
                new Enum("RS", INT_RS),
                new Enum("SC", INT_SC),
                new Enum("SL", INT_SL),
                new Enum("SG", INT_SG),
                new Enum("SX", INT_SX),
                new Enum("SK", INT_SK),
                new Enum("SI", INT_SI),
                new Enum("SB", INT_SB),
                new Enum("SO", INT_SO),
                new Enum("ZA", INT_ZA),
                new Enum("GS", INT_GS),
                new Enum("SS", INT_SS),
                new Enum("ES", INT_ES),
                new Enum("LK", INT_LK),
                new Enum("SD", INT_SD),
                new Enum("SR", INT_SR),
                new Enum("SJ", INT_SJ),
                new Enum("SZ", INT_SZ),
                new Enum("SE", INT_SE),
                new Enum("CH", INT_CH),
                new Enum("SY", INT_SY),
                new Enum("TW", INT_TW),
                new Enum("TJ", INT_TJ),
                new Enum("TZ", INT_TZ),
                new Enum("TH", INT_TH),
                new Enum("TL", INT_TL),
                new Enum("TG", INT_TG),
                new Enum("TK", INT_TK),
                new Enum("TO", INT_TO),
                new Enum("TT", INT_TT),
                new Enum("TN", INT_TN),
                new Enum("TR", INT_TR),
                new Enum("TM", INT_TM),
                new Enum("TC", INT_TC),
                new Enum("TV", INT_TV),
                new Enum("UG", INT_UG),
                new Enum("UA", INT_UA),
                new Enum("AE", INT_AE),
                new Enum("GB", INT_GB),
                new Enum("US", INT_US),
                new Enum("UM", INT_UM),
                new Enum("UY", INT_UY),
                new Enum("UZ", INT_UZ),
                new Enum("VU", INT_VU),
                new Enum("VE", INT_VE),
                new Enum("VN", INT_VN),
                new Enum("VG", INT_VG),
                new Enum("VI", INT_VI),
                new Enum("WF", INT_WF),
                new Enum("EH", INT_EH),
                new Enum("YE", INT_YE),
                new Enum("ZM", INT_ZM),
                new Enum("ZW", INT_ZW),
                new Enum("", INT_X),
            }
        );
        private static final long serialVersionUID = 1L;
        private java.lang.Object readResolve() { return forInt(intValue()); } 
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.CountryType newValue(java.lang.Object obj) {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) type.newValue( obj ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.CountryType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.CountryType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
