/*
 * XML Type:  CurrencyHistory
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.CurrencyHistory
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML CurrencyHistory(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface CurrencyHistory extends org.xvergabe.xsd.forms.components.x20.AbstractComponent
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(CurrencyHistory.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("currencyhistory3cfetype");
    
    /**
     * Gets array of all "year" elements
     */
    org.xvergabe.xsd.forms.components.x20.Integer[] getYearArray();
    
    /**
     * Gets ith "year" element
     */
    org.xvergabe.xsd.forms.components.x20.Integer getYearArray(int i);
    
    /**
     * Returns number of "year" element
     */
    int sizeOfYearArray();
    
    /**
     * Sets array of all "year" element
     */
    void setYearArray(org.xvergabe.xsd.forms.components.x20.Integer[] yearArray);
    
    /**
     * Sets ith "year" element
     */
    void setYearArray(int i, org.xvergabe.xsd.forms.components.x20.Integer year);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "year" element
     */
    org.xvergabe.xsd.forms.components.x20.Integer insertNewYear(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "year" element
     */
    org.xvergabe.xsd.forms.components.x20.Integer addNewYear();
    
    /**
     * Removes the ith "year" element
     */
    void removeYear(int i);
    
    /**
     * Gets array of all "amount" elements
     */
    org.xvergabe.xsd.forms.components.x20.Decimal[] getAmountArray();
    
    /**
     * Gets ith "amount" element
     */
    org.xvergabe.xsd.forms.components.x20.Decimal getAmountArray(int i);
    
    /**
     * Returns number of "amount" element
     */
    int sizeOfAmountArray();
    
    /**
     * Sets array of all "amount" element
     */
    void setAmountArray(org.xvergabe.xsd.forms.components.x20.Decimal[] amountArray);
    
    /**
     * Sets ith "amount" element
     */
    void setAmountArray(int i, org.xvergabe.xsd.forms.components.x20.Decimal amount);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "amount" element
     */
    org.xvergabe.xsd.forms.components.x20.Decimal insertNewAmount(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "amount" element
     */
    org.xvergabe.xsd.forms.components.x20.Decimal addNewAmount();
    
    /**
     * Removes the ith "amount" element
     */
    void removeAmount(int i);
    
    /**
     * Gets array of all "currency" elements
     */
    org.xvergabe.xsd.forms.components.x20.CurrencyChoice[] getCurrencyArray();
    
    /**
     * Gets ith "currency" element
     */
    org.xvergabe.xsd.forms.components.x20.CurrencyChoice getCurrencyArray(int i);
    
    /**
     * Returns number of "currency" element
     */
    int sizeOfCurrencyArray();
    
    /**
     * Sets array of all "currency" element
     */
    void setCurrencyArray(org.xvergabe.xsd.forms.components.x20.CurrencyChoice[] currencyArray);
    
    /**
     * Sets ith "currency" element
     */
    void setCurrencyArray(int i, org.xvergabe.xsd.forms.components.x20.CurrencyChoice currency);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "currency" element
     */
    org.xvergabe.xsd.forms.components.x20.CurrencyChoice insertNewCurrency(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "currency" element
     */
    org.xvergabe.xsd.forms.components.x20.CurrencyChoice addNewCurrency();
    
    /**
     * Removes the ith "currency" element
     */
    void removeCurrency(int i);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.CurrencyHistory parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.CurrencyHistory) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
