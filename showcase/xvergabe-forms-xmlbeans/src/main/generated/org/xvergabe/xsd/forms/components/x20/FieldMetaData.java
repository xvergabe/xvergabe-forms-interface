/*
 * XML Type:  FieldMetaData
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.FieldMetaData
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML FieldMetaData(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface FieldMetaData extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(FieldMetaData.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("fieldmetadata80d8type");
    
    /**
     * Gets the "fillingRole" element
     */
    org.xvergabe.xsd.forms.components.x20.RoleType.Enum getFillingRole();
    
    /**
     * Gets (as xml) the "fillingRole" element
     */
    org.xvergabe.xsd.forms.components.x20.RoleType xgetFillingRole();
    
    /**
     * Sets the "fillingRole" element
     */
    void setFillingRole(org.xvergabe.xsd.forms.components.x20.RoleType.Enum fillingRole);
    
    /**
     * Sets (as xml) the "fillingRole" element
     */
    void xsetFillingRole(org.xvergabe.xsd.forms.components.x20.RoleType fillingRole);
    
    /**
     * Gets the "required" element
     */
    boolean getRequired();
    
    /**
     * Gets (as xml) the "required" element
     */
    org.apache.xmlbeans.XmlBoolean xgetRequired();
    
    /**
     * True if has "required" element
     */
    boolean isSetRequired();
    
    /**
     * Sets the "required" element
     */
    void setRequired(boolean required);
    
    /**
     * Sets (as xml) the "required" element
     */
    void xsetRequired(org.apache.xmlbeans.XmlBoolean required);
    
    /**
     * Unsets the "required" element
     */
    void unsetRequired();
    
    /**
     * Gets array of all "displayInformation" elements
     */
    org.xvergabe.xsd.forms.components.x20.DisplayInformation[] getDisplayInformationArray();
    
    /**
     * Gets ith "displayInformation" element
     */
    org.xvergabe.xsd.forms.components.x20.DisplayInformation getDisplayInformationArray(int i);
    
    /**
     * Returns number of "displayInformation" element
     */
    int sizeOfDisplayInformationArray();
    
    /**
     * Sets array of all "displayInformation" element
     */
    void setDisplayInformationArray(org.xvergabe.xsd.forms.components.x20.DisplayInformation[] displayInformationArray);
    
    /**
     * Sets ith "displayInformation" element
     */
    void setDisplayInformationArray(int i, org.xvergabe.xsd.forms.components.x20.DisplayInformation displayInformation);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "displayInformation" element
     */
    org.xvergabe.xsd.forms.components.x20.DisplayInformation insertNewDisplayInformation(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "displayInformation" element
     */
    org.xvergabe.xsd.forms.components.x20.DisplayInformation addNewDisplayInformation();
    
    /**
     * Removes the ith "displayInformation" element
     */
    void removeDisplayInformation(int i);
    
    /**
     * Gets the "visible" element
     */
    boolean getVisible();
    
    /**
     * Gets (as xml) the "visible" element
     */
    org.apache.xmlbeans.XmlBoolean xgetVisible();
    
    /**
     * True if has "visible" element
     */
    boolean isSetVisible();
    
    /**
     * Sets the "visible" element
     */
    void setVisible(boolean visible);
    
    /**
     * Sets (as xml) the "visible" element
     */
    void xsetVisible(org.apache.xmlbeans.XmlBoolean visible);
    
    /**
     * Unsets the "visible" element
     */
    void unsetVisible();
    
    /**
     * Gets the "active" element
     */
    boolean getActive();
    
    /**
     * Gets (as xml) the "active" element
     */
    org.apache.xmlbeans.XmlBoolean xgetActive();
    
    /**
     * True if has "active" element
     */
    boolean isSetActive();
    
    /**
     * Sets the "active" element
     */
    void setActive(boolean active);
    
    /**
     * Sets (as xml) the "active" element
     */
    void xsetActive(org.apache.xmlbeans.XmlBoolean active);
    
    /**
     * Unsets the "active" element
     */
    void unsetActive();
    
    /**
     * Gets the "static" element
     */
    boolean getStatic();
    
    /**
     * Gets (as xml) the "static" element
     */
    org.apache.xmlbeans.XmlBoolean xgetStatic();
    
    /**
     * True if has "static" element
     */
    boolean isSetStatic();
    
    /**
     * Sets the "static" element
     */
    void setStatic(boolean xstatic);
    
    /**
     * Sets (as xml) the "static" element
     */
    void xsetStatic(org.apache.xmlbeans.XmlBoolean xstatic);
    
    /**
     * Unsets the "static" element
     */
    void unsetStatic();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.FieldMetaData parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.FieldMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
