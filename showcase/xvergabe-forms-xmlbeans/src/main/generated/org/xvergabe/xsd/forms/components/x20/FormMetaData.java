/*
 * XML Type:  FormMetaData
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.FormMetaData
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML FormMetaData(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface FormMetaData extends org.apache.xmlbeans.XmlObject
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(FormMetaData.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("formmetadatacd9ctype");
    
    /**
     * Gets array of all "displayInformation" elements
     */
    org.xvergabe.xsd.forms.components.x20.DisplayInformation[] getDisplayInformationArray();
    
    /**
     * Gets ith "displayInformation" element
     */
    org.xvergabe.xsd.forms.components.x20.DisplayInformation getDisplayInformationArray(int i);
    
    /**
     * Returns number of "displayInformation" element
     */
    int sizeOfDisplayInformationArray();
    
    /**
     * Sets array of all "displayInformation" element
     */
    void setDisplayInformationArray(org.xvergabe.xsd.forms.components.x20.DisplayInformation[] displayInformationArray);
    
    /**
     * Sets ith "displayInformation" element
     */
    void setDisplayInformationArray(int i, org.xvergabe.xsd.forms.components.x20.DisplayInformation displayInformation);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "displayInformation" element
     */
    org.xvergabe.xsd.forms.components.x20.DisplayInformation insertNewDisplayInformation(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "displayInformation" element
     */
    org.xvergabe.xsd.forms.components.x20.DisplayInformation addNewDisplayInformation();
    
    /**
     * Removes the ith "displayInformation" element
     */
    void removeDisplayInformation(int i);
    
    /**
     * Gets array of all "requiredDisjunction" elements
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction[] getRequiredDisjunctionArray();
    
    /**
     * Gets ith "requiredDisjunction" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction getRequiredDisjunctionArray(int i);
    
    /**
     * Returns number of "requiredDisjunction" element
     */
    int sizeOfRequiredDisjunctionArray();
    
    /**
     * Sets array of all "requiredDisjunction" element
     */
    void setRequiredDisjunctionArray(org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction[] requiredDisjunctionArray);
    
    /**
     * Sets ith "requiredDisjunction" element
     */
    void setRequiredDisjunctionArray(int i, org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction requiredDisjunction);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "requiredDisjunction" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction insertNewRequiredDisjunction(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "requiredDisjunction" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction addNewRequiredDisjunction();
    
    /**
     * Removes the ith "requiredDisjunction" element
     */
    void removeRequiredDisjunction(int i);
    
    /**
     * Gets array of all "requiredConjunction" elements
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction[] getRequiredConjunctionArray();
    
    /**
     * Gets ith "requiredConjunction" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction getRequiredConjunctionArray(int i);
    
    /**
     * Returns number of "requiredConjunction" element
     */
    int sizeOfRequiredConjunctionArray();
    
    /**
     * Sets array of all "requiredConjunction" element
     */
    void setRequiredConjunctionArray(org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction[] requiredConjunctionArray);
    
    /**
     * Sets ith "requiredConjunction" element
     */
    void setRequiredConjunctionArray(int i, org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction requiredConjunction);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "requiredConjunction" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction insertNewRequiredConjunction(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "requiredConjunction" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction addNewRequiredConjunction();
    
    /**
     * Removes the ith "requiredConjunction" element
     */
    void removeRequiredConjunction(int i);
    
    /**
     * Gets array of all "activationToggle" elements
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle[] getActivationToggleArray();
    
    /**
     * Gets ith "activationToggle" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle getActivationToggleArray(int i);
    
    /**
     * Returns number of "activationToggle" element
     */
    int sizeOfActivationToggleArray();
    
    /**
     * Sets array of all "activationToggle" element
     */
    void setActivationToggleArray(org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle[] activationToggleArray);
    
    /**
     * Sets ith "activationToggle" element
     */
    void setActivationToggleArray(int i, org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle activationToggle);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "activationToggle" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle insertNewActivationToggle(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "activationToggle" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle addNewActivationToggle();
    
    /**
     * Removes the ith "activationToggle" element
     */
    void removeActivationToggle(int i);
    
    /**
     * Gets array of all "valueTransfer" elements
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer[] getValueTransferArray();
    
    /**
     * Gets ith "valueTransfer" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer getValueTransferArray(int i);
    
    /**
     * Returns number of "valueTransfer" element
     */
    int sizeOfValueTransferArray();
    
    /**
     * Sets array of all "valueTransfer" element
     */
    void setValueTransferArray(org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer[] valueTransferArray);
    
    /**
     * Sets ith "valueTransfer" element
     */
    void setValueTransferArray(int i, org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer valueTransfer);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "valueTransfer" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer insertNewValueTransfer(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "valueTransfer" element
     */
    org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer addNewValueTransfer();
    
    /**
     * Removes the ith "valueTransfer" element
     */
    void removeValueTransfer(int i);
    
    /**
     * An XML requiredDisjunction(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public interface RequiredDisjunction extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(RequiredDisjunction.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("requireddisjunctiona6cbelemtype");
        
        /**
         * Gets the "triggerField" element
         */
        java.lang.String getTriggerField();
        
        /**
         * Gets (as xml) the "triggerField" element
         */
        org.apache.xmlbeans.XmlIDREF xgetTriggerField();
        
        /**
         * True if has "triggerField" element
         */
        boolean isSetTriggerField();
        
        /**
         * Sets the "triggerField" element
         */
        void setTriggerField(java.lang.String triggerField);
        
        /**
         * Sets (as xml) the "triggerField" element
         */
        void xsetTriggerField(org.apache.xmlbeans.XmlIDREF triggerField);
        
        /**
         * Unsets the "triggerField" element
         */
        void unsetTriggerField();
        
        /**
         * Gets the "triggerValue" element
         */
        org.apache.xmlbeans.XmlObject getTriggerValue();
        
        /**
         * True if has "triggerValue" element
         */
        boolean isSetTriggerValue();
        
        /**
         * Sets the "triggerValue" element
         */
        void setTriggerValue(org.apache.xmlbeans.XmlObject triggerValue);
        
        /**
         * Appends and returns a new empty "triggerValue" element
         */
        org.apache.xmlbeans.XmlObject addNewTriggerValue();
        
        /**
         * Unsets the "triggerValue" element
         */
        void unsetTriggerValue();
        
        /**
         * Gets the "targetFields" element
         */
        org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields getTargetFields();
        
        /**
         * Sets the "targetFields" element
         */
        void setTargetFields(org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields targetFields);
        
        /**
         * Appends and returns a new empty "targetFields" element
         */
        org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields addNewTargetFields();
        
        /**
         * An XML targetFields(@http://www.xvergabe.org/xsd/forms/components/2_0).
         *
         * This is a complex type.
         */
        public interface TargetFields extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(TargetFields.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("targetfieldsee91elemtype");
            
            /**
             * Gets array of all "targetField" elements
             */
            java.lang.String[] getTargetFieldArray();
            
            /**
             * Gets ith "targetField" element
             */
            java.lang.String getTargetFieldArray(int i);
            
            /**
             * Gets (as xml) array of all "targetField" elements
             */
            org.apache.xmlbeans.XmlIDREF[] xgetTargetFieldArray();
            
            /**
             * Gets (as xml) ith "targetField" element
             */
            org.apache.xmlbeans.XmlIDREF xgetTargetFieldArray(int i);
            
            /**
             * Returns number of "targetField" element
             */
            int sizeOfTargetFieldArray();
            
            /**
             * Sets array of all "targetField" element
             */
            void setTargetFieldArray(java.lang.String[] targetFieldArray);
            
            /**
             * Sets ith "targetField" element
             */
            void setTargetFieldArray(int i, java.lang.String targetField);
            
            /**
             * Sets (as xml) array of all "targetField" element
             */
            void xsetTargetFieldArray(org.apache.xmlbeans.XmlIDREF[] targetFieldArray);
            
            /**
             * Sets (as xml) ith "targetField" element
             */
            void xsetTargetFieldArray(int i, org.apache.xmlbeans.XmlIDREF targetField);
            
            /**
             * Inserts the value as the ith "targetField" element
             */
            void insertTargetField(int i, java.lang.String targetField);
            
            /**
             * Appends the value as the last "targetField" element
             */
            void addTargetField(java.lang.String targetField);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "targetField" element
             */
            org.apache.xmlbeans.XmlIDREF insertNewTargetField(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "targetField" element
             */
            org.apache.xmlbeans.XmlIDREF addNewTargetField();
            
            /**
             * Removes the ith "targetField" element
             */
            void removeTargetField(int i);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields newInstance() {
                  return (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction newInstance() {
              return (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML requiredConjunction(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public interface RequiredConjunction extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(RequiredConjunction.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("requiredconjunctioncfbfelemtype");
        
        /**
         * Gets the "triggerField" element
         */
        java.lang.String getTriggerField();
        
        /**
         * Gets (as xml) the "triggerField" element
         */
        org.apache.xmlbeans.XmlIDREF xgetTriggerField();
        
        /**
         * Sets the "triggerField" element
         */
        void setTriggerField(java.lang.String triggerField);
        
        /**
         * Sets (as xml) the "triggerField" element
         */
        void xsetTriggerField(org.apache.xmlbeans.XmlIDREF triggerField);
        
        /**
         * Gets the "triggerValue" element
         */
        org.apache.xmlbeans.XmlObject getTriggerValue();
        
        /**
         * True if has "triggerValue" element
         */
        boolean isSetTriggerValue();
        
        /**
         * Sets the "triggerValue" element
         */
        void setTriggerValue(org.apache.xmlbeans.XmlObject triggerValue);
        
        /**
         * Appends and returns a new empty "triggerValue" element
         */
        org.apache.xmlbeans.XmlObject addNewTriggerValue();
        
        /**
         * Unsets the "triggerValue" element
         */
        void unsetTriggerValue();
        
        /**
         * Gets the "targetFields" element
         */
        org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields getTargetFields();
        
        /**
         * Sets the "targetFields" element
         */
        void setTargetFields(org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields targetFields);
        
        /**
         * Appends and returns a new empty "targetFields" element
         */
        org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields addNewTargetFields();
        
        /**
         * An XML targetFields(@http://www.xvergabe.org/xsd/forms/components/2_0).
         *
         * This is a complex type.
         */
        public interface TargetFields extends org.apache.xmlbeans.XmlObject
        {
            public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
                org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(TargetFields.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("targetfields1785elemtype");
            
            /**
             * Gets array of all "targetField" elements
             */
            java.lang.String[] getTargetFieldArray();
            
            /**
             * Gets ith "targetField" element
             */
            java.lang.String getTargetFieldArray(int i);
            
            /**
             * Gets (as xml) array of all "targetField" elements
             */
            org.apache.xmlbeans.XmlIDREF[] xgetTargetFieldArray();
            
            /**
             * Gets (as xml) ith "targetField" element
             */
            org.apache.xmlbeans.XmlIDREF xgetTargetFieldArray(int i);
            
            /**
             * Returns number of "targetField" element
             */
            int sizeOfTargetFieldArray();
            
            /**
             * Sets array of all "targetField" element
             */
            void setTargetFieldArray(java.lang.String[] targetFieldArray);
            
            /**
             * Sets ith "targetField" element
             */
            void setTargetFieldArray(int i, java.lang.String targetField);
            
            /**
             * Sets (as xml) array of all "targetField" element
             */
            void xsetTargetFieldArray(org.apache.xmlbeans.XmlIDREF[] targetFieldArray);
            
            /**
             * Sets (as xml) ith "targetField" element
             */
            void xsetTargetFieldArray(int i, org.apache.xmlbeans.XmlIDREF targetField);
            
            /**
             * Inserts the value as the ith "targetField" element
             */
            void insertTargetField(int i, java.lang.String targetField);
            
            /**
             * Appends the value as the last "targetField" element
             */
            void addTargetField(java.lang.String targetField);
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "targetField" element
             */
            org.apache.xmlbeans.XmlIDREF insertNewTargetField(int i);
            
            /**
             * Appends and returns a new empty value (as xml) as the last "targetField" element
             */
            org.apache.xmlbeans.XmlIDREF addNewTargetField();
            
            /**
             * Removes the ith "targetField" element
             */
            void removeTargetField(int i);
            
            /**
             * A factory class with static methods for creating instances
             * of this type.
             */
            
            public static final class Factory
            {
                public static org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields newInstance() {
                  return (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
                
                public static org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields newInstance(org.apache.xmlbeans.XmlOptions options) {
                  return (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
                
                private Factory() { } // No instance of this class allowed
            }
        }
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction newInstance() {
              return (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML activationToggle(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public interface ActivationToggle extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ActivationToggle.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("activationtogglef642elemtype");
        
        /**
         * Gets the "triggerField" element
         */
        java.lang.String getTriggerField();
        
        /**
         * Gets (as xml) the "triggerField" element
         */
        org.apache.xmlbeans.XmlIDREF xgetTriggerField();
        
        /**
         * Sets the "triggerField" element
         */
        void setTriggerField(java.lang.String triggerField);
        
        /**
         * Sets (as xml) the "triggerField" element
         */
        void xsetTriggerField(org.apache.xmlbeans.XmlIDREF triggerField);
        
        /**
         * Gets the "triggerValue" element
         */
        java.lang.String getTriggerValue();
        
        /**
         * Gets (as xml) the "triggerValue" element
         */
        org.apache.xmlbeans.XmlString xgetTriggerValue();
        
        /**
         * True if has "triggerValue" element
         */
        boolean isSetTriggerValue();
        
        /**
         * Sets the "triggerValue" element
         */
        void setTriggerValue(java.lang.String triggerValue);
        
        /**
         * Sets (as xml) the "triggerValue" element
         */
        void xsetTriggerValue(org.apache.xmlbeans.XmlString triggerValue);
        
        /**
         * Unsets the "triggerValue" element
         */
        void unsetTriggerValue();
        
        /**
         * Gets the "targetField" element
         */
        java.lang.String getTargetField();
        
        /**
         * Gets (as xml) the "targetField" element
         */
        org.apache.xmlbeans.XmlIDREF xgetTargetField();
        
        /**
         * Sets the "targetField" element
         */
        void setTargetField(java.lang.String targetField);
        
        /**
         * Sets (as xml) the "targetField" element
         */
        void xsetTargetField(org.apache.xmlbeans.XmlIDREF targetField);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle newInstance() {
              return (org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * An XML valueTransfer(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public interface ValueTransfer extends org.apache.xmlbeans.XmlObject
    {
        public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
            org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(ValueTransfer.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("valuetransfere884elemtype");
        
        /**
         * Gets the "sourceFieldId" element
         */
        java.lang.String getSourceFieldId();
        
        /**
         * Gets (as xml) the "sourceFieldId" element
         */
        org.apache.xmlbeans.XmlIDREF xgetSourceFieldId();
        
        /**
         * Sets the "sourceFieldId" element
         */
        void setSourceFieldId(java.lang.String sourceFieldId);
        
        /**
         * Sets (as xml) the "sourceFieldId" element
         */
        void xsetSourceFieldId(org.apache.xmlbeans.XmlIDREF sourceFieldId);
        
        /**
         * Gets the "targetFieldId" element
         */
        java.lang.String getTargetFieldId();
        
        /**
         * Gets (as xml) the "targetFieldId" element
         */
        org.apache.xmlbeans.XmlIDREF xgetTargetFieldId();
        
        /**
         * Sets the "targetFieldId" element
         */
        void setTargetFieldId(java.lang.String targetFieldId);
        
        /**
         * Sets (as xml) the "targetFieldId" element
         */
        void xsetTargetFieldId(org.apache.xmlbeans.XmlIDREF targetFieldId);
        
        /**
         * A factory class with static methods for creating instances
         * of this type.
         */
        
        public static final class Factory
        {
            public static org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer newInstance() {
              return (org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
            
            public static org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer newInstance(org.apache.xmlbeans.XmlOptions options) {
              return (org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
            
            private Factory() { } // No instance of this class allowed
        }
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.FormMetaData parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.FormMetaData) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
