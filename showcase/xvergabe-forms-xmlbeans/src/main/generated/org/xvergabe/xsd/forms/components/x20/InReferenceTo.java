/*
 * XML Type:  InReferenceTo
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.InReferenceTo
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML InReferenceTo(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface InReferenceTo extends org.xvergabe.xsd.forms.components.x20.AbstractComponent
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(InReferenceTo.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("inreferenceto3180type");
    
    /**
     * Gets the "dateOfReference" element
     */
    org.xvergabe.xsd.forms.components.x20.Date getDateOfReference();
    
    /**
     * True if has "dateOfReference" element
     */
    boolean isSetDateOfReference();
    
    /**
     * Sets the "dateOfReference" element
     */
    void setDateOfReference(org.xvergabe.xsd.forms.components.x20.Date dateOfReference);
    
    /**
     * Appends and returns a new empty "dateOfReference" element
     */
    org.xvergabe.xsd.forms.components.x20.Date addNewDateOfReference();
    
    /**
     * Unsets the "dateOfReference" element
     */
    void unsetDateOfReference();
    
    /**
     * Gets the "referenceNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String getReferenceNumber();
    
    /**
     * True if has "referenceNumber" element
     */
    boolean isSetReferenceNumber();
    
    /**
     * Sets the "referenceNumber" element
     */
    void setReferenceNumber(org.xvergabe.xsd.forms.components.x20.String referenceNumber);
    
    /**
     * Appends and returns a new empty "referenceNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewReferenceNumber();
    
    /**
     * Unsets the "referenceNumber" element
     */
    void unsetReferenceNumber();
    
    /**
     * Gets the "activityNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String getActivityNumber();
    
    /**
     * True if has "activityNumber" element
     */
    boolean isSetActivityNumber();
    
    /**
     * Sets the "activityNumber" element
     */
    void setActivityNumber(org.xvergabe.xsd.forms.components.x20.String activityNumber);
    
    /**
     * Appends and returns a new empty "activityNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewActivityNumber();
    
    /**
     * Unsets the "activityNumber" element
     */
    void unsetActivityNumber();
    
    /**
     * Gets the "activityName" element
     */
    org.xvergabe.xsd.forms.components.x20.String getActivityName();
    
    /**
     * True if has "activityName" element
     */
    boolean isSetActivityName();
    
    /**
     * Sets the "activityName" element
     */
    void setActivityName(org.xvergabe.xsd.forms.components.x20.String activityName);
    
    /**
     * Appends and returns a new empty "activityName" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewActivityName();
    
    /**
     * Unsets the "activityName" element
     */
    void unsetActivityName();
    
    /**
     * Gets the "tenderingNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String getTenderingNumber();
    
    /**
     * True if has "tenderingNumber" element
     */
    boolean isSetTenderingNumber();
    
    /**
     * Sets the "tenderingNumber" element
     */
    void setTenderingNumber(org.xvergabe.xsd.forms.components.x20.String tenderingNumber);
    
    /**
     * Appends and returns a new empty "tenderingNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewTenderingNumber();
    
    /**
     * Unsets the "tenderingNumber" element
     */
    void unsetTenderingNumber();
    
    /**
     * Gets the "serviceDescription" element
     */
    org.xvergabe.xsd.forms.components.x20.Description getServiceDescription();
    
    /**
     * True if has "serviceDescription" element
     */
    boolean isSetServiceDescription();
    
    /**
     * Sets the "serviceDescription" element
     */
    void setServiceDescription(org.xvergabe.xsd.forms.components.x20.Description serviceDescription);
    
    /**
     * Appends and returns a new empty "serviceDescription" element
     */
    org.xvergabe.xsd.forms.components.x20.Description addNewServiceDescription();
    
    /**
     * Unsets the "serviceDescription" element
     */
    void unsetServiceDescription();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.InReferenceTo parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.InReferenceTo) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
