/*
 * XML Type:  NaturalPerson
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.NaturalPerson
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML NaturalPerson(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface NaturalPerson extends org.xvergabe.xsd.forms.components.x20.AbstractComponent
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(NaturalPerson.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("naturalpersoneb03type");
    
    /**
     * Gets the "familyName" element
     */
    org.xvergabe.xsd.forms.components.x20.String getFamilyName();
    
    /**
     * True if has "familyName" element
     */
    boolean isSetFamilyName();
    
    /**
     * Sets the "familyName" element
     */
    void setFamilyName(org.xvergabe.xsd.forms.components.x20.String familyName);
    
    /**
     * Appends and returns a new empty "familyName" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewFamilyName();
    
    /**
     * Unsets the "familyName" element
     */
    void unsetFamilyName();
    
    /**
     * Gets the "firstName" element
     */
    org.xvergabe.xsd.forms.components.x20.String getFirstName();
    
    /**
     * True if has "firstName" element
     */
    boolean isSetFirstName();
    
    /**
     * Sets the "firstName" element
     */
    void setFirstName(org.xvergabe.xsd.forms.components.x20.String firstName);
    
    /**
     * Appends and returns a new empty "firstName" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewFirstName();
    
    /**
     * Unsets the "firstName" element
     */
    void unsetFirstName();
    
    /**
     * Gets the "salutation" element
     */
    org.xvergabe.xsd.forms.components.x20.String getSalutation();
    
    /**
     * True if has "salutation" element
     */
    boolean isSetSalutation();
    
    /**
     * Sets the "salutation" element
     */
    void setSalutation(org.xvergabe.xsd.forms.components.x20.String salutation);
    
    /**
     * Appends and returns a new empty "salutation" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewSalutation();
    
    /**
     * Unsets the "salutation" element
     */
    void unsetSalutation();
    
    /**
     * Gets the "academicTitle" element
     */
    org.xvergabe.xsd.forms.components.x20.String getAcademicTitle();
    
    /**
     * True if has "academicTitle" element
     */
    boolean isSetAcademicTitle();
    
    /**
     * Sets the "academicTitle" element
     */
    void setAcademicTitle(org.xvergabe.xsd.forms.components.x20.String academicTitle);
    
    /**
     * Appends and returns a new empty "academicTitle" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewAcademicTitle();
    
    /**
     * Unsets the "academicTitle" element
     */
    void unsetAcademicTitle();
    
    /**
     * Gets the "organizationalUnit" element
     */
    org.xvergabe.xsd.forms.components.x20.String getOrganizationalUnit();
    
    /**
     * True if has "organizationalUnit" element
     */
    boolean isSetOrganizationalUnit();
    
    /**
     * Sets the "organizationalUnit" element
     */
    void setOrganizationalUnit(org.xvergabe.xsd.forms.components.x20.String organizationalUnit);
    
    /**
     * Appends and returns a new empty "organizationalUnit" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewOrganizationalUnit();
    
    /**
     * Unsets the "organizationalUnit" element
     */
    void unsetOrganizationalUnit();
    
    /**
     * Gets the "officeAddress" element
     */
    org.xvergabe.xsd.forms.components.x20.Address getOfficeAddress();
    
    /**
     * True if has "officeAddress" element
     */
    boolean isSetOfficeAddress();
    
    /**
     * Sets the "officeAddress" element
     */
    void setOfficeAddress(org.xvergabe.xsd.forms.components.x20.Address officeAddress);
    
    /**
     * Appends and returns a new empty "officeAddress" element
     */
    org.xvergabe.xsd.forms.components.x20.Address addNewOfficeAddress();
    
    /**
     * Unsets the "officeAddress" element
     */
    void unsetOfficeAddress();
    
    /**
     * Gets array of all "contactData" elements
     */
    org.xvergabe.xsd.forms.components.x20.Contact[] getContactDataArray();
    
    /**
     * Gets ith "contactData" element
     */
    org.xvergabe.xsd.forms.components.x20.Contact getContactDataArray(int i);
    
    /**
     * Returns number of "contactData" element
     */
    int sizeOfContactDataArray();
    
    /**
     * Sets array of all "contactData" element
     */
    void setContactDataArray(org.xvergabe.xsd.forms.components.x20.Contact[] contactDataArray);
    
    /**
     * Sets ith "contactData" element
     */
    void setContactDataArray(int i, org.xvergabe.xsd.forms.components.x20.Contact contactData);
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "contactData" element
     */
    org.xvergabe.xsd.forms.components.x20.Contact insertNewContactData(int i);
    
    /**
     * Appends and returns a new empty value (as xml) as the last "contactData" element
     */
    org.xvergabe.xsd.forms.components.x20.Contact addNewContactData();
    
    /**
     * Removes the ith "contactData" element
     */
    void removeContactData(int i);
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.NaturalPerson parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.NaturalPerson) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
