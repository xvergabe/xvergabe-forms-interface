/*
 * XML Type:  Price
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Price
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML Price(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface Price extends org.xvergabe.xsd.forms.components.x20.AbstractComponent
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Price.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("price1ef8type");
    
    /**
     * Gets the "netValue" element
     */
    org.xvergabe.xsd.forms.components.x20.Decimal getNetValue();
    
    /**
     * True if has "netValue" element
     */
    boolean isSetNetValue();
    
    /**
     * Sets the "netValue" element
     */
    void setNetValue(org.xvergabe.xsd.forms.components.x20.Decimal netValue);
    
    /**
     * Appends and returns a new empty "netValue" element
     */
    org.xvergabe.xsd.forms.components.x20.Decimal addNewNetValue();
    
    /**
     * Unsets the "netValue" element
     */
    void unsetNetValue();
    
    /**
     * Gets the "grossValue" element
     */
    org.xvergabe.xsd.forms.components.x20.Decimal getGrossValue();
    
    /**
     * True if has "grossValue" element
     */
    boolean isSetGrossValue();
    
    /**
     * Sets the "grossValue" element
     */
    void setGrossValue(org.xvergabe.xsd.forms.components.x20.Decimal grossValue);
    
    /**
     * Appends and returns a new empty "grossValue" element
     */
    org.xvergabe.xsd.forms.components.x20.Decimal addNewGrossValue();
    
    /**
     * Unsets the "grossValue" element
     */
    void unsetGrossValue();
    
    /**
     * Gets the "taxRate" element
     */
    org.xvergabe.xsd.forms.components.x20.Percentage getTaxRate();
    
    /**
     * True if has "taxRate" element
     */
    boolean isSetTaxRate();
    
    /**
     * Sets the "taxRate" element
     */
    void setTaxRate(org.xvergabe.xsd.forms.components.x20.Percentage taxRate);
    
    /**
     * Appends and returns a new empty "taxRate" element
     */
    org.xvergabe.xsd.forms.components.x20.Percentage addNewTaxRate();
    
    /**
     * Unsets the "taxRate" element
     */
    void unsetTaxRate();
    
    /**
     * Gets the "currency" element
     */
    org.xvergabe.xsd.forms.components.x20.CurrencyChoice getCurrency();
    
    /**
     * True if has "currency" element
     */
    boolean isSetCurrency();
    
    /**
     * Sets the "currency" element
     */
    void setCurrency(org.xvergabe.xsd.forms.components.x20.CurrencyChoice currency);
    
    /**
     * Appends and returns a new empty "currency" element
     */
    org.xvergabe.xsd.forms.components.x20.CurrencyChoice addNewCurrency();
    
    /**
     * Unsets the "currency" element
     */
    void unsetCurrency();
    
    /**
     * Gets the "discount" element
     */
    org.xvergabe.xsd.forms.components.x20.Discount getDiscount();
    
    /**
     * True if has "discount" element
     */
    boolean isSetDiscount();
    
    /**
     * Sets the "discount" element
     */
    void setDiscount(org.xvergabe.xsd.forms.components.x20.Discount discount);
    
    /**
     * Appends and returns a new empty "discount" element
     */
    org.xvergabe.xsd.forms.components.x20.Discount addNewDiscount();
    
    /**
     * Unsets the "discount" element
     */
    void unsetDiscount();
    
    /**
     * Gets the "remission" element
     */
    org.xvergabe.xsd.forms.components.x20.Remission getRemission();
    
    /**
     * True if has "remission" element
     */
    boolean isSetRemission();
    
    /**
     * Sets the "remission" element
     */
    void setRemission(org.xvergabe.xsd.forms.components.x20.Remission remission);
    
    /**
     * Appends and returns a new empty "remission" element
     */
    org.xvergabe.xsd.forms.components.x20.Remission addNewRemission();
    
    /**
     * Unsets the "remission" element
     */
    void unsetRemission();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.Price newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Price parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Price parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Price parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Price) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
