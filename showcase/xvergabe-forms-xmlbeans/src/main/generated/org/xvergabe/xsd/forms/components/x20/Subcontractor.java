/*
 * XML Type:  Subcontractor
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Subcontractor
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML Subcontractor(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public interface Subcontractor extends org.xvergabe.xsd.forms.components.x20.AbstractComponent
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(Subcontractor.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("subcontractordc0ctype");
    
    /**
     * Gets the "subcontractorName" element
     */
    org.xvergabe.xsd.forms.components.x20.String getSubcontractorName();
    
    /**
     * True if has "subcontractorName" element
     */
    boolean isSetSubcontractorName();
    
    /**
     * Sets the "subcontractorName" element
     */
    void setSubcontractorName(org.xvergabe.xsd.forms.components.x20.String subcontractorName);
    
    /**
     * Appends and returns a new empty "subcontractorName" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewSubcontractorName();
    
    /**
     * Unsets the "subcontractorName" element
     */
    void unsetSubcontractorName();
    
    /**
     * Gets the "serviceDescription" element
     */
    org.xvergabe.xsd.forms.components.x20.String getServiceDescription();
    
    /**
     * True if has "serviceDescription" element
     */
    boolean isSetServiceDescription();
    
    /**
     * Sets the "serviceDescription" element
     */
    void setServiceDescription(org.xvergabe.xsd.forms.components.x20.String serviceDescription);
    
    /**
     * Appends and returns a new empty "serviceDescription" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewServiceDescription();
    
    /**
     * Unsets the "serviceDescription" element
     */
    void unsetServiceDescription();
    
    /**
     * Gets the "serviceReferenceNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String getServiceReferenceNumber();
    
    /**
     * True if has "serviceReferenceNumber" element
     */
    boolean isSetServiceReferenceNumber();
    
    /**
     * Sets the "serviceReferenceNumber" element
     */
    void setServiceReferenceNumber(org.xvergabe.xsd.forms.components.x20.String serviceReferenceNumber);
    
    /**
     * Appends and returns a new empty "serviceReferenceNumber" element
     */
    org.xvergabe.xsd.forms.components.x20.String addNewServiceReferenceNumber();
    
    /**
     * Unsets the "serviceReferenceNumber" element
     */
    void unsetServiceReferenceNumber();
    
    /**
     * Gets the "bidderCouldSupplyHimself" element
     */
    org.xvergabe.xsd.forms.components.x20.Boolean getBidderCouldSupplyHimself();
    
    /**
     * True if has "bidderCouldSupplyHimself" element
     */
    boolean isSetBidderCouldSupplyHimself();
    
    /**
     * Sets the "bidderCouldSupplyHimself" element
     */
    void setBidderCouldSupplyHimself(org.xvergabe.xsd.forms.components.x20.Boolean bidderCouldSupplyHimself);
    
    /**
     * Appends and returns a new empty "bidderCouldSupplyHimself" element
     */
    org.xvergabe.xsd.forms.components.x20.Boolean addNewBidderCouldSupplyHimself();
    
    /**
     * Unsets the "bidderCouldSupplyHimself" element
     */
    void unsetBidderCouldSupplyHimself();
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.Subcontractor parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.Subcontractor) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
