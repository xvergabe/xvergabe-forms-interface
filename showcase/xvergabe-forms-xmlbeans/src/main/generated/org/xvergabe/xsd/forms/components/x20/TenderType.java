/*
 * XML Type:  TenderType
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.TenderType
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20;


/**
 * An XML TenderType(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.TenderType.
 */
public interface TenderType extends org.apache.xmlbeans.XmlToken
{
    public static final org.apache.xmlbeans.SchemaType type = (org.apache.xmlbeans.SchemaType)
        org.apache.xmlbeans.XmlBeans.typeSystemForClassLoader(TenderType.class.getClassLoader(), "schemaorg_apache_xmlbeans.system.sF98CB973EEFF94948F950ED0174C7ADB").resolveHandle("tendertype18c1type");
    
    org.apache.xmlbeans.StringEnumAbstractBase enumValue();
    void set(org.apache.xmlbeans.StringEnumAbstractBase e);
    
    static final Enum OPEN_REQUEST = Enum.forString("open request");
    static final Enum LIMITED_REQUEST = Enum.forString("limited request");
    static final Enum LIMITED_REQUEST_WITH_PARTICIPATION_CONTEST = Enum.forString("limited request with participation contest");
    static final Enum FREE_AWARDING = Enum.forString("free awarding");
    static final Enum FREE_AWARDING_WITH_PARTICIPATION_CONTEST = Enum.forString("free awarding with participation contest");
    static final Enum OPEN_AWARDING = Enum.forString("open awarding");
    static final Enum NICHT_OFFENES_VERFAHREN = Enum.forString("Nicht-offenes Verfahren");
    static final Enum NEGOTIATION_METHOD = Enum.forString("negotiation method");
    static final Enum WETTBEWERBLICHER_DIALOG = Enum.forString("Wettbewerblicher Dialog");
    static final Enum LIMITED_AUCTION = Enum.forString("limited auction");
    static final Enum UNLIMITED_AUCTION = Enum.forString("unlimited auction");
    static final Enum QUOTE_REQUEST = Enum.forString("quote request");
    static final Enum PUBLIC_QUOTE_REQUEST = Enum.forString("public quote request");
    static final Enum EXPRESSION_OF_INTEREST = Enum.forString("expression of interest");
    
    static final int INT_OPEN_REQUEST = Enum.INT_OPEN_REQUEST;
    static final int INT_LIMITED_REQUEST = Enum.INT_LIMITED_REQUEST;
    static final int INT_LIMITED_REQUEST_WITH_PARTICIPATION_CONTEST = Enum.INT_LIMITED_REQUEST_WITH_PARTICIPATION_CONTEST;
    static final int INT_FREE_AWARDING = Enum.INT_FREE_AWARDING;
    static final int INT_FREE_AWARDING_WITH_PARTICIPATION_CONTEST = Enum.INT_FREE_AWARDING_WITH_PARTICIPATION_CONTEST;
    static final int INT_OPEN_AWARDING = Enum.INT_OPEN_AWARDING;
    static final int INT_NICHT_OFFENES_VERFAHREN = Enum.INT_NICHT_OFFENES_VERFAHREN;
    static final int INT_NEGOTIATION_METHOD = Enum.INT_NEGOTIATION_METHOD;
    static final int INT_WETTBEWERBLICHER_DIALOG = Enum.INT_WETTBEWERBLICHER_DIALOG;
    static final int INT_LIMITED_AUCTION = Enum.INT_LIMITED_AUCTION;
    static final int INT_UNLIMITED_AUCTION = Enum.INT_UNLIMITED_AUCTION;
    static final int INT_QUOTE_REQUEST = Enum.INT_QUOTE_REQUEST;
    static final int INT_PUBLIC_QUOTE_REQUEST = Enum.INT_PUBLIC_QUOTE_REQUEST;
    static final int INT_EXPRESSION_OF_INTEREST = Enum.INT_EXPRESSION_OF_INTEREST;
    
    /**
     * Enumeration value class for org.xvergabe.xsd.forms.components.x20.TenderType.
     * These enum values can be used as follows:
     * <pre>
     * enum.toString(); // returns the string value of the enum
     * enum.intValue(); // returns an int value, useful for switches
     * // e.g., case Enum.INT_OPEN_REQUEST
     * Enum.forString(s); // returns the enum value for a string
     * Enum.forInt(i); // returns the enum value for an int
     * </pre>
     * Enumeration objects are immutable singleton objects that
     * can be compared using == object equality. They have no
     * public constructor. See the constants defined within this
     * class for all the valid values.
     */
    static final class Enum extends org.apache.xmlbeans.StringEnumAbstractBase
    {
        /**
         * Returns the enum value for a string, or null if none.
         */
        public static Enum forString(java.lang.String s)
            { return (Enum)table.forString(s); }
        /**
         * Returns the enum value corresponding to an int, or null if none.
         */
        public static Enum forInt(int i)
            { return (Enum)table.forInt(i); }
        
        private Enum(java.lang.String s, int i)
            { super(s, i); }
        
        static final int INT_OPEN_REQUEST = 1;
        static final int INT_LIMITED_REQUEST = 2;
        static final int INT_LIMITED_REQUEST_WITH_PARTICIPATION_CONTEST = 3;
        static final int INT_FREE_AWARDING = 4;
        static final int INT_FREE_AWARDING_WITH_PARTICIPATION_CONTEST = 5;
        static final int INT_OPEN_AWARDING = 6;
        static final int INT_NICHT_OFFENES_VERFAHREN = 7;
        static final int INT_NEGOTIATION_METHOD = 8;
        static final int INT_WETTBEWERBLICHER_DIALOG = 9;
        static final int INT_LIMITED_AUCTION = 10;
        static final int INT_UNLIMITED_AUCTION = 11;
        static final int INT_QUOTE_REQUEST = 12;
        static final int INT_PUBLIC_QUOTE_REQUEST = 13;
        static final int INT_EXPRESSION_OF_INTEREST = 14;
        
        public static final org.apache.xmlbeans.StringEnumAbstractBase.Table table =
            new org.apache.xmlbeans.StringEnumAbstractBase.Table
        (
            new Enum[]
            {
                new Enum("open request", INT_OPEN_REQUEST),
                new Enum("limited request", INT_LIMITED_REQUEST),
                new Enum("limited request with participation contest", INT_LIMITED_REQUEST_WITH_PARTICIPATION_CONTEST),
                new Enum("free awarding", INT_FREE_AWARDING),
                new Enum("free awarding with participation contest", INT_FREE_AWARDING_WITH_PARTICIPATION_CONTEST),
                new Enum("open awarding", INT_OPEN_AWARDING),
                new Enum("Nicht-offenes Verfahren", INT_NICHT_OFFENES_VERFAHREN),
                new Enum("negotiation method", INT_NEGOTIATION_METHOD),
                new Enum("Wettbewerblicher Dialog", INT_WETTBEWERBLICHER_DIALOG),
                new Enum("limited auction", INT_LIMITED_AUCTION),
                new Enum("unlimited auction", INT_UNLIMITED_AUCTION),
                new Enum("quote request", INT_QUOTE_REQUEST),
                new Enum("public quote request", INT_PUBLIC_QUOTE_REQUEST),
                new Enum("expression of interest", INT_EXPRESSION_OF_INTEREST),
            }
        );
        private static final long serialVersionUID = 1L;
        private java.lang.Object readResolve() { return forInt(intValue()); } 
    }
    
    /**
     * A factory class with static methods for creating instances
     * of this type.
     */
    
    public static final class Factory
    {
        public static org.xvergabe.xsd.forms.components.x20.TenderType newValue(java.lang.Object obj) {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) type.newValue( obj ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType newInstance() {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType newInstance(org.apache.xmlbeans.XmlOptions options) {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newInstance( type, options ); }
        
        /** @param xmlAsString the string value to parse */
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.lang.String xmlAsString) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.lang.String xmlAsString, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xmlAsString, type, options ); }
        
        /** @param file the file from which to load an xml document */
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.io.File file) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.io.File file, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( file, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.net.URL u) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.net.URL u, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( u, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.io.InputStream is) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.io.InputStream is, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( is, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.io.Reader r) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(java.io.Reader r, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, java.io.IOException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( r, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(javax.xml.stream.XMLStreamReader sr) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(javax.xml.stream.XMLStreamReader sr, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( sr, type, options ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(org.w3c.dom.Node node) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, null ); }
        
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(org.w3c.dom.Node node, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( node, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.xvergabe.xsd.forms.components.x20.TenderType parse(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return (org.xvergabe.xsd.forms.components.x20.TenderType) org.apache.xmlbeans.XmlBeans.getContextTypeLoader().parse( xis, type, options ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, null ); }
        
        /** @deprecated {@link org.apache.xmlbeans.xml.stream.XMLInputStream} */
        public static org.apache.xmlbeans.xml.stream.XMLInputStream newValidatingXMLInputStream(org.apache.xmlbeans.xml.stream.XMLInputStream xis, org.apache.xmlbeans.XmlOptions options) throws org.apache.xmlbeans.XmlException, org.apache.xmlbeans.xml.stream.XMLStreamException {
          return org.apache.xmlbeans.XmlBeans.getContextTypeLoader().newValidatingXMLInputStream( xis, type, options ); }
        
        private Factory() { } // No instance of this class allowed
    }
}
