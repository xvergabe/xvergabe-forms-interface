/*
 * XML Type:  AbstractField
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.AbstractField
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML AbstractField(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class AbstractFieldImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractElementImpl implements org.xvergabe.xsd.forms.components.x20.AbstractField
{
    private static final long serialVersionUID = 1L;
    
    public AbstractFieldImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName METADATA$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "metadata");
    
    
    /**
     * Gets the "metadata" element
     */
    public org.xvergabe.xsd.forms.components.x20.FieldMetaData getMetadata()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FieldMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FieldMetaData)get_store().find_element_user(METADATA$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "metadata" element
     */
    public boolean isSetMetadata()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(METADATA$0) != 0;
        }
    }
    
    /**
     * Sets the "metadata" element
     */
    public void setMetadata(org.xvergabe.xsd.forms.components.x20.FieldMetaData metadata)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FieldMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FieldMetaData)get_store().find_element_user(METADATA$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.FieldMetaData)get_store().add_element_user(METADATA$0);
            }
            target.set(metadata);
        }
    }
    
    /**
     * Appends and returns a new empty "metadata" element
     */
    public org.xvergabe.xsd.forms.components.x20.FieldMetaData addNewMetadata()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FieldMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FieldMetaData)get_store().add_element_user(METADATA$0);
            return target;
        }
    }
    
    /**
     * Unsets the "metadata" element
     */
    public void unsetMetadata()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(METADATA$0, 0);
        }
    }
}
