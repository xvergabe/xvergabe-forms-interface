/*
 * XML Type:  Address
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Address
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Address(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class AddressImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Address
{
    private static final long serialVersionUID = 1L;
    
    public AddressImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName STREET$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "street");
    private static final javax.xml.namespace.QName NUMBER$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "number");
    private static final javax.xml.namespace.QName POSTALCODE$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "postalCode");
    private static final javax.xml.namespace.QName POSTOFFICEBOX$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "postOfficeBox");
    private static final javax.xml.namespace.QName POSTOFFICEBOXCODE$8 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "postOfficeBoxCode");
    private static final javax.xml.namespace.QName CITY$10 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "city");
    private static final javax.xml.namespace.QName DISTRICT$12 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "district");
    private static final javax.xml.namespace.QName FEDERALSTATE$14 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "federalState");
    private static final javax.xml.namespace.QName STATE$16 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "state");
    private static final javax.xml.namespace.QName FURTHERINFORMATION$18 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "furtherInformation");
    
    
    /**
     * Gets the "street" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getStreet()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(STREET$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "street" element
     */
    public boolean isSetStreet()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STREET$0) != 0;
        }
    }
    
    /**
     * Sets the "street" element
     */
    public void setStreet(org.xvergabe.xsd.forms.components.x20.String street)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(STREET$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(STREET$0);
            }
            target.set(street);
        }
    }
    
    /**
     * Appends and returns a new empty "street" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewStreet()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(STREET$0);
            return target;
        }
    }
    
    /**
     * Unsets the "street" element
     */
    public void unsetStreet()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STREET$0, 0);
        }
    }
    
    /**
     * Gets the "number" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(NUMBER$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "number" element
     */
    public boolean isSetNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(NUMBER$2) != 0;
        }
    }
    
    /**
     * Sets the "number" element
     */
    public void setNumber(org.xvergabe.xsd.forms.components.x20.String number)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(NUMBER$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(NUMBER$2);
            }
            target.set(number);
        }
    }
    
    /**
     * Appends and returns a new empty "number" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(NUMBER$2);
            return target;
        }
    }
    
    /**
     * Unsets the "number" element
     */
    public void unsetNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(NUMBER$2, 0);
        }
    }
    
    /**
     * Gets the "postalCode" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getPostalCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(POSTALCODE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "postalCode" element
     */
    public boolean isSetPostalCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(POSTALCODE$4) != 0;
        }
    }
    
    /**
     * Sets the "postalCode" element
     */
    public void setPostalCode(org.xvergabe.xsd.forms.components.x20.String postalCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(POSTALCODE$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(POSTALCODE$4);
            }
            target.set(postalCode);
        }
    }
    
    /**
     * Appends and returns a new empty "postalCode" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewPostalCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(POSTALCODE$4);
            return target;
        }
    }
    
    /**
     * Unsets the "postalCode" element
     */
    public void unsetPostalCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(POSTALCODE$4, 0);
        }
    }
    
    /**
     * Gets the "postOfficeBox" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getPostOfficeBox()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(POSTOFFICEBOX$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "postOfficeBox" element
     */
    public boolean isSetPostOfficeBox()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(POSTOFFICEBOX$6) != 0;
        }
    }
    
    /**
     * Sets the "postOfficeBox" element
     */
    public void setPostOfficeBox(org.xvergabe.xsd.forms.components.x20.String postOfficeBox)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(POSTOFFICEBOX$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(POSTOFFICEBOX$6);
            }
            target.set(postOfficeBox);
        }
    }
    
    /**
     * Appends and returns a new empty "postOfficeBox" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewPostOfficeBox()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(POSTOFFICEBOX$6);
            return target;
        }
    }
    
    /**
     * Unsets the "postOfficeBox" element
     */
    public void unsetPostOfficeBox()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(POSTOFFICEBOX$6, 0);
        }
    }
    
    /**
     * Gets the "postOfficeBoxCode" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getPostOfficeBoxCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(POSTOFFICEBOXCODE$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "postOfficeBoxCode" element
     */
    public boolean isSetPostOfficeBoxCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(POSTOFFICEBOXCODE$8) != 0;
        }
    }
    
    /**
     * Sets the "postOfficeBoxCode" element
     */
    public void setPostOfficeBoxCode(org.xvergabe.xsd.forms.components.x20.String postOfficeBoxCode)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(POSTOFFICEBOXCODE$8, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(POSTOFFICEBOXCODE$8);
            }
            target.set(postOfficeBoxCode);
        }
    }
    
    /**
     * Appends and returns a new empty "postOfficeBoxCode" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewPostOfficeBoxCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(POSTOFFICEBOXCODE$8);
            return target;
        }
    }
    
    /**
     * Unsets the "postOfficeBoxCode" element
     */
    public void unsetPostOfficeBoxCode()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(POSTOFFICEBOXCODE$8, 0);
        }
    }
    
    /**
     * Gets the "city" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CITY$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "city" element
     */
    public boolean isSetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CITY$10) != 0;
        }
    }
    
    /**
     * Sets the "city" element
     */
    public void setCity(org.xvergabe.xsd.forms.components.x20.String city)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CITY$10, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CITY$10);
            }
            target.set(city);
        }
    }
    
    /**
     * Appends and returns a new empty "city" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CITY$10);
            return target;
        }
    }
    
    /**
     * Unsets the "city" element
     */
    public void unsetCity()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CITY$10, 0);
        }
    }
    
    /**
     * Gets the "district" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getDistrict()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(DISTRICT$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "district" element
     */
    public boolean isSetDistrict()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DISTRICT$12) != 0;
        }
    }
    
    /**
     * Sets the "district" element
     */
    public void setDistrict(org.xvergabe.xsd.forms.components.x20.String district)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(DISTRICT$12, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(DISTRICT$12);
            }
            target.set(district);
        }
    }
    
    /**
     * Appends and returns a new empty "district" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewDistrict()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(DISTRICT$12);
            return target;
        }
    }
    
    /**
     * Unsets the "district" element
     */
    public void unsetDistrict()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DISTRICT$12, 0);
        }
    }
    
    /**
     * Gets the "federalState" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getFederalState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FEDERALSTATE$14, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "federalState" element
     */
    public boolean isSetFederalState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FEDERALSTATE$14) != 0;
        }
    }
    
    /**
     * Sets the "federalState" element
     */
    public void setFederalState(org.xvergabe.xsd.forms.components.x20.String federalState)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FEDERALSTATE$14, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FEDERALSTATE$14);
            }
            target.set(federalState);
        }
    }
    
    /**
     * Appends and returns a new empty "federalState" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewFederalState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FEDERALSTATE$14);
            return target;
        }
    }
    
    /**
     * Unsets the "federalState" element
     */
    public void unsetFederalState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FEDERALSTATE$14, 0);
        }
    }
    
    /**
     * Gets the "state" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(STATE$16, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "state" element
     */
    public boolean isSetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STATE$16) != 0;
        }
    }
    
    /**
     * Sets the "state" element
     */
    public void setState(org.xvergabe.xsd.forms.components.x20.String state)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(STATE$16, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(STATE$16);
            }
            target.set(state);
        }
    }
    
    /**
     * Appends and returns a new empty "state" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(STATE$16);
            return target;
        }
    }
    
    /**
     * Unsets the "state" element
     */
    public void unsetState()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STATE$16, 0);
        }
    }
    
    /**
     * Gets the "furtherInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getFurtherInformation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FURTHERINFORMATION$18, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "furtherInformation" element
     */
    public boolean isSetFurtherInformation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FURTHERINFORMATION$18) != 0;
        }
    }
    
    /**
     * Sets the "furtherInformation" element
     */
    public void setFurtherInformation(org.xvergabe.xsd.forms.components.x20.String furtherInformation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FURTHERINFORMATION$18, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FURTHERINFORMATION$18);
            }
            target.set(furtherInformation);
        }
    }
    
    /**
     * Appends and returns a new empty "furtherInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewFurtherInformation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FURTHERINFORMATION$18);
            return target;
        }
    }
    
    /**
     * Unsets the "furtherInformation" element
     */
    public void unsetFurtherInformation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FURTHERINFORMATION$18, 0);
        }
    }
}
