/*
 * XML Type:  Agreement
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Agreement
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Agreement(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class AgreementImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Agreement
{
    private static final long serialVersionUID = 1L;
    
    public AgreementImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AGREED$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "agreed");
    private static final javax.xml.namespace.QName EXPLANATION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "explanation");
    private static final javax.xml.namespace.QName TYPE$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "type");
    private static final javax.xml.namespace.QName ADDITIONALFIELD$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "additionalField");
    
    
    /**
     * Gets the "agreed" element
     */
    public org.xvergabe.xsd.forms.components.x20.Boolean getAgreed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().find_element_user(AGREED$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "agreed" element
     */
    public boolean isSetAgreed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AGREED$0) != 0;
        }
    }
    
    /**
     * Sets the "agreed" element
     */
    public void setAgreed(org.xvergabe.xsd.forms.components.x20.Boolean agreed)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().find_element_user(AGREED$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().add_element_user(AGREED$0);
            }
            target.set(agreed);
        }
    }
    
    /**
     * Appends and returns a new empty "agreed" element
     */
    public org.xvergabe.xsd.forms.components.x20.Boolean addNewAgreed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().add_element_user(AGREED$0);
            return target;
        }
    }
    
    /**
     * Unsets the "agreed" element
     */
    public void unsetAgreed()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AGREED$0, 0);
        }
    }
    
    /**
     * Gets the "explanation" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getExplanation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(EXPLANATION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "explanation" element
     */
    public void setExplanation(org.xvergabe.xsd.forms.components.x20.String explanation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(EXPLANATION$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(EXPLANATION$2);
            }
            target.set(explanation);
        }
    }
    
    /**
     * Appends and returns a new empty "explanation" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewExplanation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(EXPLANATION$2);
            return target;
        }
    }
    
    /**
     * Gets the "type" element
     */
    public java.lang.String getType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$4, 0);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "type" element
     */
    public org.apache.xmlbeans.XmlString xgetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "type" element
     */
    public boolean isSetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TYPE$4) != 0;
        }
    }
    
    /**
     * Sets the "type" element
     */
    public void setType(java.lang.String type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TYPE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TYPE$4);
            }
            target.setStringValue(type);
        }
    }
    
    /**
     * Sets (as xml) the "type" element
     */
    public void xsetType(org.apache.xmlbeans.XmlString type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlString target = null;
            target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TYPE$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TYPE$4);
            }
            target.set(type);
        }
    }
    
    /**
     * Unsets the "type" element
     */
    public void unsetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TYPE$4, 0);
        }
    }
    
    /**
     * Gets array of all "additionalField" elements
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractField[] getAdditionalFieldArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ADDITIONALFIELD$6, targetList);
            org.xvergabe.xsd.forms.components.x20.AbstractField[] result = new org.xvergabe.xsd.forms.components.x20.AbstractField[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "additionalField" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractField getAdditionalFieldArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractField target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractField)get_store().find_element_user(ADDITIONALFIELD$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "additionalField" element
     */
    public int sizeOfAdditionalFieldArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDITIONALFIELD$6);
        }
    }
    
    /**
     * Sets array of all "additionalField" element
     */
    public void setAdditionalFieldArray(org.xvergabe.xsd.forms.components.x20.AbstractField[] additionalFieldArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(additionalFieldArray, ADDITIONALFIELD$6);
        }
    }
    
    /**
     * Sets ith "additionalField" element
     */
    public void setAdditionalFieldArray(int i, org.xvergabe.xsd.forms.components.x20.AbstractField additionalField)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractField target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractField)get_store().find_element_user(ADDITIONALFIELD$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(additionalField);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "additionalField" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractField insertNewAdditionalField(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractField target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractField)get_store().insert_element_user(ADDITIONALFIELD$6, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "additionalField" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractField addNewAdditionalField()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractField target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractField)get_store().add_element_user(ADDITIONALFIELD$6);
            return target;
        }
    }
    
    /**
     * Removes the ith "additionalField" element
     */
    public void removeAdditionalField(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDITIONALFIELD$6, i);
        }
    }
}
