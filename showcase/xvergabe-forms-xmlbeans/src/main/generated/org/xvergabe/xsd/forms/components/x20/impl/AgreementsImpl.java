/*
 * XML Type:  Agreements
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Agreements
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Agreements(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class AgreementsImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Agreements
{
    private static final long serialVersionUID = 1L;
    
    public AgreementsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AGREEMENT$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "agreement");
    
    
    /**
     * Gets array of all "agreement" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Agreement[] getAgreementArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(AGREEMENT$0, targetList);
            org.xvergabe.xsd.forms.components.x20.Agreement[] result = new org.xvergabe.xsd.forms.components.x20.Agreement[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "agreement" element
     */
    public org.xvergabe.xsd.forms.components.x20.Agreement getAgreementArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Agreement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Agreement)get_store().find_element_user(AGREEMENT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "agreement" element
     */
    public int sizeOfAgreementArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AGREEMENT$0);
        }
    }
    
    /**
     * Sets array of all "agreement" element
     */
    public void setAgreementArray(org.xvergabe.xsd.forms.components.x20.Agreement[] agreementArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(agreementArray, AGREEMENT$0);
        }
    }
    
    /**
     * Sets ith "agreement" element
     */
    public void setAgreementArray(int i, org.xvergabe.xsd.forms.components.x20.Agreement agreement)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Agreement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Agreement)get_store().find_element_user(AGREEMENT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(agreement);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "agreement" element
     */
    public org.xvergabe.xsd.forms.components.x20.Agreement insertNewAgreement(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Agreement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Agreement)get_store().insert_element_user(AGREEMENT$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "agreement" element
     */
    public org.xvergabe.xsd.forms.components.x20.Agreement addNewAgreement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Agreement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Agreement)get_store().add_element_user(AGREEMENT$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "agreement" element
     */
    public void removeAgreement(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AGREEMENT$0, i);
        }
    }
}
