/*
 * XML Type:  Bidder
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Bidder
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Bidder(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class BidderImpl extends org.xvergabe.xsd.forms.components.x20.impl.OrganizationImpl implements org.xvergabe.xsd.forms.components.x20.Bidder
{
    private static final long serialVersionUID = 1L;
    
    public BidderImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CUSTOMSNUMBER$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "customsNumber");
    private static final javax.xml.namespace.QName TAXID$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "taxId");
    private static final javax.xml.namespace.QName EMPLOYEEHISTORY$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "employeeHistory");
    private static final javax.xml.namespace.QName BUSINESSBRANCH$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "businessBranch");
    private static final javax.xml.namespace.QName CUSTOMERNUMBER$8 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "customerNumber");
    private static final javax.xml.namespace.QName CUSTOMERNUMBER2$10 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "customerNumber2");
    private static final javax.xml.namespace.QName BUSINESSTYPE$12 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "businessType");
    private static final javax.xml.namespace.QName REVENUEHISTORY$14 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "revenueHistory");
    private static final javax.xml.namespace.QName TRADEREGISTERNUMBER$16 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "tradeRegisterNumber");
    private static final javax.xml.namespace.QName HEADQUARTERSADDRESS$18 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "headQuartersAddress");
    private static final javax.xml.namespace.QName CONTACT$20 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "contact");
    
    
    /**
     * Gets the "customsNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getCustomsNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CUSTOMSNUMBER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "customsNumber" element
     */
    public boolean isSetCustomsNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CUSTOMSNUMBER$0) != 0;
        }
    }
    
    /**
     * Sets the "customsNumber" element
     */
    public void setCustomsNumber(org.xvergabe.xsd.forms.components.x20.String customsNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CUSTOMSNUMBER$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CUSTOMSNUMBER$0);
            }
            target.set(customsNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "customsNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewCustomsNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CUSTOMSNUMBER$0);
            return target;
        }
    }
    
    /**
     * Unsets the "customsNumber" element
     */
    public void unsetCustomsNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CUSTOMSNUMBER$0, 0);
        }
    }
    
    /**
     * Gets the "taxId" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getTaxId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(TAXID$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "taxId" element
     */
    public boolean isSetTaxId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TAXID$2) != 0;
        }
    }
    
    /**
     * Sets the "taxId" element
     */
    public void setTaxId(org.xvergabe.xsd.forms.components.x20.String taxId)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(TAXID$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(TAXID$2);
            }
            target.set(taxId);
        }
    }
    
    /**
     * Appends and returns a new empty "taxId" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewTaxId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(TAXID$2);
            return target;
        }
    }
    
    /**
     * Unsets the "taxId" element
     */
    public void unsetTaxId()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TAXID$2, 0);
        }
    }
    
    /**
     * Gets the "employeeHistory" element
     */
    public org.xvergabe.xsd.forms.components.x20.CountHistory getEmployeeHistory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CountHistory target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CountHistory)get_store().find_element_user(EMPLOYEEHISTORY$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "employeeHistory" element
     */
    public boolean isSetEmployeeHistory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EMPLOYEEHISTORY$4) != 0;
        }
    }
    
    /**
     * Sets the "employeeHistory" element
     */
    public void setEmployeeHistory(org.xvergabe.xsd.forms.components.x20.CountHistory employeeHistory)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CountHistory target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CountHistory)get_store().find_element_user(EMPLOYEEHISTORY$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.CountHistory)get_store().add_element_user(EMPLOYEEHISTORY$4);
            }
            target.set(employeeHistory);
        }
    }
    
    /**
     * Appends and returns a new empty "employeeHistory" element
     */
    public org.xvergabe.xsd.forms.components.x20.CountHistory addNewEmployeeHistory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CountHistory target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CountHistory)get_store().add_element_user(EMPLOYEEHISTORY$4);
            return target;
        }
    }
    
    /**
     * Unsets the "employeeHistory" element
     */
    public void unsetEmployeeHistory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EMPLOYEEHISTORY$4, 0);
        }
    }
    
    /**
     * Gets the "businessBranch" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getBusinessBranch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(BUSINESSBRANCH$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "businessBranch" element
     */
    public boolean isSetBusinessBranch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BUSINESSBRANCH$6) != 0;
        }
    }
    
    /**
     * Sets the "businessBranch" element
     */
    public void setBusinessBranch(org.xvergabe.xsd.forms.components.x20.String businessBranch)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(BUSINESSBRANCH$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(BUSINESSBRANCH$6);
            }
            target.set(businessBranch);
        }
    }
    
    /**
     * Appends and returns a new empty "businessBranch" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewBusinessBranch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(BUSINESSBRANCH$6);
            return target;
        }
    }
    
    /**
     * Unsets the "businessBranch" element
     */
    public void unsetBusinessBranch()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BUSINESSBRANCH$6, 0);
        }
    }
    
    /**
     * Gets the "customerNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getCustomerNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CUSTOMERNUMBER$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "customerNumber" element
     */
    public boolean isSetCustomerNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CUSTOMERNUMBER$8) != 0;
        }
    }
    
    /**
     * Sets the "customerNumber" element
     */
    public void setCustomerNumber(org.xvergabe.xsd.forms.components.x20.String customerNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CUSTOMERNUMBER$8, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CUSTOMERNUMBER$8);
            }
            target.set(customerNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "customerNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewCustomerNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CUSTOMERNUMBER$8);
            return target;
        }
    }
    
    /**
     * Unsets the "customerNumber" element
     */
    public void unsetCustomerNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CUSTOMERNUMBER$8, 0);
        }
    }
    
    /**
     * Gets the "customerNumber2" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getCustomerNumber2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CUSTOMERNUMBER2$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "customerNumber2" element
     */
    public boolean isSetCustomerNumber2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CUSTOMERNUMBER2$10) != 0;
        }
    }
    
    /**
     * Sets the "customerNumber2" element
     */
    public void setCustomerNumber2(org.xvergabe.xsd.forms.components.x20.String customerNumber2)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CUSTOMERNUMBER2$10, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CUSTOMERNUMBER2$10);
            }
            target.set(customerNumber2);
        }
    }
    
    /**
     * Appends and returns a new empty "customerNumber2" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewCustomerNumber2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CUSTOMERNUMBER2$10);
            return target;
        }
    }
    
    /**
     * Unsets the "customerNumber2" element
     */
    public void unsetCustomerNumber2()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CUSTOMERNUMBER2$10, 0);
        }
    }
    
    /**
     * Gets the "businessType" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getBusinessType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(BUSINESSTYPE$12, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "businessType" element
     */
    public boolean isSetBusinessType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BUSINESSTYPE$12) != 0;
        }
    }
    
    /**
     * Sets the "businessType" element
     */
    public void setBusinessType(org.xvergabe.xsd.forms.components.x20.String businessType)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(BUSINESSTYPE$12, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(BUSINESSTYPE$12);
            }
            target.set(businessType);
        }
    }
    
    /**
     * Appends and returns a new empty "businessType" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewBusinessType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(BUSINESSTYPE$12);
            return target;
        }
    }
    
    /**
     * Unsets the "businessType" element
     */
    public void unsetBusinessType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BUSINESSTYPE$12, 0);
        }
    }
    
    /**
     * Gets the "revenueHistory" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getRevenueHistory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(REVENUEHISTORY$14, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "revenueHistory" element
     */
    public boolean isSetRevenueHistory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REVENUEHISTORY$14) != 0;
        }
    }
    
    /**
     * Sets the "revenueHistory" element
     */
    public void setRevenueHistory(org.xvergabe.xsd.forms.components.x20.String revenueHistory)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(REVENUEHISTORY$14, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(REVENUEHISTORY$14);
            }
            target.set(revenueHistory);
        }
    }
    
    /**
     * Appends and returns a new empty "revenueHistory" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewRevenueHistory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(REVENUEHISTORY$14);
            return target;
        }
    }
    
    /**
     * Unsets the "revenueHistory" element
     */
    public void unsetRevenueHistory()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REVENUEHISTORY$14, 0);
        }
    }
    
    /**
     * Gets the "tradeRegisterNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getTradeRegisterNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(TRADEREGISTERNUMBER$16, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "tradeRegisterNumber" element
     */
    public boolean isSetTradeRegisterNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TRADEREGISTERNUMBER$16) != 0;
        }
    }
    
    /**
     * Sets the "tradeRegisterNumber" element
     */
    public void setTradeRegisterNumber(org.xvergabe.xsd.forms.components.x20.String tradeRegisterNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(TRADEREGISTERNUMBER$16, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(TRADEREGISTERNUMBER$16);
            }
            target.set(tradeRegisterNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "tradeRegisterNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewTradeRegisterNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(TRADEREGISTERNUMBER$16);
            return target;
        }
    }
    
    /**
     * Unsets the "tradeRegisterNumber" element
     */
    public void unsetTradeRegisterNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TRADEREGISTERNUMBER$16, 0);
        }
    }
    
    /**
     * Gets the "headQuartersAddress" element
     */
    public org.xvergabe.xsd.forms.components.x20.Address getHeadQuartersAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().find_element_user(HEADQUARTERSADDRESS$18, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "headQuartersAddress" element
     */
    public boolean isSetHeadQuartersAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(HEADQUARTERSADDRESS$18) != 0;
        }
    }
    
    /**
     * Sets the "headQuartersAddress" element
     */
    public void setHeadQuartersAddress(org.xvergabe.xsd.forms.components.x20.Address headQuartersAddress)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().find_element_user(HEADQUARTERSADDRESS$18, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().add_element_user(HEADQUARTERSADDRESS$18);
            }
            target.set(headQuartersAddress);
        }
    }
    
    /**
     * Appends and returns a new empty "headQuartersAddress" element
     */
    public org.xvergabe.xsd.forms.components.x20.Address addNewHeadQuartersAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().add_element_user(HEADQUARTERSADDRESS$18);
            return target;
        }
    }
    
    /**
     * Unsets the "headQuartersAddress" element
     */
    public void unsetHeadQuartersAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(HEADQUARTERSADDRESS$18, 0);
        }
    }
    
    /**
     * Gets the "contact" element
     */
    public org.xvergabe.xsd.forms.components.x20.Contact getContact()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().find_element_user(CONTACT$20, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "contact" element
     */
    public boolean isSetContact()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CONTACT$20) != 0;
        }
    }
    
    /**
     * Sets the "contact" element
     */
    public void setContact(org.xvergabe.xsd.forms.components.x20.Contact contact)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().find_element_user(CONTACT$20, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().add_element_user(CONTACT$20);
            }
            target.set(contact);
        }
    }
    
    /**
     * Appends and returns a new empty "contact" element
     */
    public org.xvergabe.xsd.forms.components.x20.Contact addNewContact()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().add_element_user(CONTACT$20);
            return target;
        }
    }
    
    /**
     * Unsets the "contact" element
     */
    public void unsetContact()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CONTACT$20, 0);
        }
    }
}
