/*
 * XML Type:  Buyer
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Buyer
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Buyer(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class BuyerImpl extends org.xvergabe.xsd.forms.components.x20.impl.OrganizationImpl implements org.xvergabe.xsd.forms.components.x20.Buyer
{
    private static final long serialVersionUID = 1L;
    
    public BuyerImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
