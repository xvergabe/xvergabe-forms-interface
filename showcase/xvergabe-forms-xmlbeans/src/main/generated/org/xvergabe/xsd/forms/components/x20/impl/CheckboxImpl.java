/*
 * XML Type:  Checkbox
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Checkbox
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Checkbox(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class CheckboxImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl implements org.xvergabe.xsd.forms.components.x20.Checkbox
{
    private static final long serialVersionUID = 1L;
    
    public CheckboxImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VALUE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "value");
    private static final javax.xml.namespace.QName CHECKED$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "checked");
    
    
    /**
     * Gets the "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "value" element
     */
    public boolean isSetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VALUE$0) != 0;
        }
    }
    
    /**
     * Sets the "value" element
     */
    public void setValue(org.xvergabe.xsd.forms.components.x20.String value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(VALUE$0);
            }
            target.set(value);
        }
    }
    
    /**
     * Appends and returns a new empty "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(VALUE$0);
            return target;
        }
    }
    
    /**
     * Unsets the "value" element
     */
    public void unsetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VALUE$0, 0);
        }
    }
    
    /**
     * Gets the "checked" element
     */
    public org.xvergabe.xsd.forms.components.x20.Boolean getChecked()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().find_element_user(CHECKED$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "checked" element
     */
    public boolean isSetChecked()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CHECKED$2) != 0;
        }
    }
    
    /**
     * Sets the "checked" element
     */
    public void setChecked(org.xvergabe.xsd.forms.components.x20.Boolean checked)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().find_element_user(CHECKED$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().add_element_user(CHECKED$2);
            }
            target.set(checked);
        }
    }
    
    /**
     * Appends and returns a new empty "checked" element
     */
    public org.xvergabe.xsd.forms.components.x20.Boolean addNewChecked()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().add_element_user(CHECKED$2);
            return target;
        }
    }
    
    /**
     * Unsets the "checked" element
     */
    public void unsetChecked()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CHECKED$2, 0);
        }
    }
}
