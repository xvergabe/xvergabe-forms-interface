/*
 * XML Type:  ComponentMetaData
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.ComponentMetaData
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML ComponentMetaData(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class ComponentMetaDataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.ComponentMetaData
{
    private static final long serialVersionUID = 1L;
    
    public ComponentMetaDataImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DISPLAYINFORMATION$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "displayInformation");
    private static final javax.xml.namespace.QName VISIBLE$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "visible");
    
    
    /**
     * Gets array of all "displayInformation" elements
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation[] getDisplayInformationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DISPLAYINFORMATION$0, targetList);
            org.xvergabe.xsd.forms.components.x20.DisplayInformation[] result = new org.xvergabe.xsd.forms.components.x20.DisplayInformation[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation getDisplayInformationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().find_element_user(DISPLAYINFORMATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "displayInformation" element
     */
    public int sizeOfDisplayInformationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DISPLAYINFORMATION$0);
        }
    }
    
    /**
     * Sets array of all "displayInformation" element
     */
    public void setDisplayInformationArray(org.xvergabe.xsd.forms.components.x20.DisplayInformation[] displayInformationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(displayInformationArray, DISPLAYINFORMATION$0);
        }
    }
    
    /**
     * Sets ith "displayInformation" element
     */
    public void setDisplayInformationArray(int i, org.xvergabe.xsd.forms.components.x20.DisplayInformation displayInformation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().find_element_user(DISPLAYINFORMATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(displayInformation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation insertNewDisplayInformation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().insert_element_user(DISPLAYINFORMATION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation addNewDisplayInformation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().add_element_user(DISPLAYINFORMATION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "displayInformation" element
     */
    public void removeDisplayInformation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DISPLAYINFORMATION$0, i);
        }
    }
    
    /**
     * Gets the "visible" element
     */
    public boolean getVisible()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VISIBLE$2, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "visible" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetVisible()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(VISIBLE$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "visible" element
     */
    public boolean isSetVisible()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VISIBLE$2) != 0;
        }
    }
    
    /**
     * Sets the "visible" element
     */
    public void setVisible(boolean visible)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VISIBLE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VISIBLE$2);
            }
            target.setBooleanValue(visible);
        }
    }
    
    /**
     * Sets (as xml) the "visible" element
     */
    public void xsetVisible(org.apache.xmlbeans.XmlBoolean visible)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(VISIBLE$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(VISIBLE$2);
            }
            target.set(visible);
        }
    }
    
    /**
     * Unsets the "visible" element
     */
    public void unsetVisible()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VISIBLE$2, 0);
        }
    }
}
