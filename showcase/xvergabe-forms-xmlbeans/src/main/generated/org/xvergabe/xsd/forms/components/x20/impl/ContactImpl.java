/*
 * XML Type:  Contact
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Contact
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Contact(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class ContactImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Contact
{
    private static final long serialVersionUID = 1L;
    
    public ContactImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PHONE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "phone");
    private static final javax.xml.namespace.QName FAX$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "fax");
    private static final javax.xml.namespace.QName MAIL$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "mail");
    private static final javax.xml.namespace.QName URL$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "url");
    
    
    /**
     * Gets the "phone" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(PHONE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "phone" element
     */
    public boolean isSetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PHONE$0) != 0;
        }
    }
    
    /**
     * Sets the "phone" element
     */
    public void setPhone(org.xvergabe.xsd.forms.components.x20.String phone)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(PHONE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(PHONE$0);
            }
            target.set(phone);
        }
    }
    
    /**
     * Appends and returns a new empty "phone" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(PHONE$0);
            return target;
        }
    }
    
    /**
     * Unsets the "phone" element
     */
    public void unsetPhone()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PHONE$0, 0);
        }
    }
    
    /**
     * Gets the "fax" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getFax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FAX$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "fax" element
     */
    public boolean isSetFax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FAX$2) != 0;
        }
    }
    
    /**
     * Sets the "fax" element
     */
    public void setFax(org.xvergabe.xsd.forms.components.x20.String fax)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FAX$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FAX$2);
            }
            target.set(fax);
        }
    }
    
    /**
     * Appends and returns a new empty "fax" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewFax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FAX$2);
            return target;
        }
    }
    
    /**
     * Unsets the "fax" element
     */
    public void unsetFax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FAX$2, 0);
        }
    }
    
    /**
     * Gets the "mail" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getMail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(MAIL$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "mail" element
     */
    public boolean isSetMail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MAIL$4) != 0;
        }
    }
    
    /**
     * Sets the "mail" element
     */
    public void setMail(org.xvergabe.xsd.forms.components.x20.String mail)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(MAIL$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(MAIL$4);
            }
            target.set(mail);
        }
    }
    
    /**
     * Appends and returns a new empty "mail" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewMail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(MAIL$4);
            return target;
        }
    }
    
    /**
     * Unsets the "mail" element
     */
    public void unsetMail()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MAIL$4, 0);
        }
    }
    
    /**
     * Gets the "url" element
     */
    public org.xvergabe.xsd.forms.components.x20.Url getUrl()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Url target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Url)get_store().find_element_user(URL$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "url" element
     */
    public boolean isSetUrl()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(URL$6) != 0;
        }
    }
    
    /**
     * Sets the "url" element
     */
    public void setUrl(org.xvergabe.xsd.forms.components.x20.Url url)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Url target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Url)get_store().find_element_user(URL$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Url)get_store().add_element_user(URL$6);
            }
            target.set(url);
        }
    }
    
    /**
     * Appends and returns a new empty "url" element
     */
    public org.xvergabe.xsd.forms.components.x20.Url addNewUrl()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Url target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Url)get_store().add_element_user(URL$6);
            return target;
        }
    }
    
    /**
     * Unsets the "url" element
     */
    public void unsetUrl()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(URL$6, 0);
        }
    }
}
