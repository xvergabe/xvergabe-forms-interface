/*
 * XML Type:  ContractingAuthority
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.ContractingAuthority
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML ContractingAuthority(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class ContractingAuthorityImpl extends org.xvergabe.xsd.forms.components.x20.impl.OrganizationImpl implements org.xvergabe.xsd.forms.components.x20.ContractingAuthority
{
    private static final long serialVersionUID = 1L;
    
    public ContractingAuthorityImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
