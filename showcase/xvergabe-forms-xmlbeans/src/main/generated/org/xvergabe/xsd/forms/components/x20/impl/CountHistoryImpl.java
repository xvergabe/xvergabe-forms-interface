/*
 * XML Type:  CountHistory
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.CountHistory
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML CountHistory(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class CountHistoryImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.CountHistory
{
    private static final long serialVersionUID = 1L;
    
    public CountHistoryImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName YEAR$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "year");
    private static final javax.xml.namespace.QName COUNT$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "count");
    
    
    /**
     * Gets array of all "year" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Integer[] getYearArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(YEAR$0, targetList);
            org.xvergabe.xsd.forms.components.x20.Integer[] result = new org.xvergabe.xsd.forms.components.x20.Integer[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "year" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer getYearArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(YEAR$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "year" element
     */
    public int sizeOfYearArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(YEAR$0);
        }
    }
    
    /**
     * Sets array of all "year" element
     */
    public void setYearArray(org.xvergabe.xsd.forms.components.x20.Integer[] yearArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(yearArray, YEAR$0);
        }
    }
    
    /**
     * Sets ith "year" element
     */
    public void setYearArray(int i, org.xvergabe.xsd.forms.components.x20.Integer year)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(YEAR$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(year);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "year" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer insertNewYear(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().insert_element_user(YEAR$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "year" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer addNewYear()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(YEAR$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "year" element
     */
    public void removeYear(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(YEAR$0, i);
        }
    }
    
    /**
     * Gets array of all "count" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Integer[] getCountArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(COUNT$2, targetList);
            org.xvergabe.xsd.forms.components.x20.Integer[] result = new org.xvergabe.xsd.forms.components.x20.Integer[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "count" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer getCountArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(COUNT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "count" element
     */
    public int sizeOfCountArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(COUNT$2);
        }
    }
    
    /**
     * Sets array of all "count" element
     */
    public void setCountArray(org.xvergabe.xsd.forms.components.x20.Integer[] countArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(countArray, COUNT$2);
        }
    }
    
    /**
     * Sets ith "count" element
     */
    public void setCountArray(int i, org.xvergabe.xsd.forms.components.x20.Integer count)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(COUNT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(count);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "count" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer insertNewCount(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().insert_element_user(COUNT$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "count" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer addNewCount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(COUNT$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "count" element
     */
    public void removeCount(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(COUNT$2, i);
        }
    }
}
