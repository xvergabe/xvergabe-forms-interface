/*
 * XML Type:  CountryType
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.CountryType
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML CountryType(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.CountryType.
 */
public class CountryTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements org.xvergabe.xsd.forms.components.x20.CountryType
{
    private static final long serialVersionUID = 1L;
    
    public CountryTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected CountryTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
