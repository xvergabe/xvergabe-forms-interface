/*
 * XML Type:  CurrencyChoice
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.CurrencyChoice
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML CurrencyChoice(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class CurrencyChoiceImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl implements org.xvergabe.xsd.forms.components.x20.CurrencyChoice
{
    private static final long serialVersionUID = 1L;
    
    public CurrencyChoiceImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VALUE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "value");
    private static final javax.xml.namespace.QName CURRENCYMETADATA$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "currencyMetaData");
    
    
    /**
     * Gets the "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum getValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                return null;
            }
            return (org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyType xgetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyType target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyType)get_store().find_element_user(VALUE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "value" element
     */
    public boolean isSetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VALUE$0) != 0;
        }
    }
    
    /**
     * Sets the "value" element
     */
    public void setValue(org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALUE$0);
            }
            target.setEnumValue(value);
        }
    }
    
    /**
     * Sets (as xml) the "value" element
     */
    public void xsetValue(org.xvergabe.xsd.forms.components.x20.CurrencyType value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyType target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyType)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.CurrencyType)get_store().add_element_user(VALUE$0);
            }
            target.set(value);
        }
    }
    
    /**
     * Unsets the "value" element
     */
    public void unsetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VALUE$0, 0);
        }
    }
    
    /**
     * Gets the "currencyMetaData" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyMetaData getCurrencyMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyMetaData)get_store().find_element_user(CURRENCYMETADATA$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "currencyMetaData" element
     */
    public boolean isSetCurrencyMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CURRENCYMETADATA$2) != 0;
        }
    }
    
    /**
     * Sets the "currencyMetaData" element
     */
    public void setCurrencyMetaData(org.xvergabe.xsd.forms.components.x20.CurrencyMetaData currencyMetaData)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyMetaData)get_store().find_element_user(CURRENCYMETADATA$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.CurrencyMetaData)get_store().add_element_user(CURRENCYMETADATA$2);
            }
            target.set(currencyMetaData);
        }
    }
    
    /**
     * Appends and returns a new empty "currencyMetaData" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyMetaData addNewCurrencyMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyMetaData)get_store().add_element_user(CURRENCYMETADATA$2);
            return target;
        }
    }
    
    /**
     * Unsets the "currencyMetaData" element
     */
    public void unsetCurrencyMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CURRENCYMETADATA$2, 0);
        }
    }
}
