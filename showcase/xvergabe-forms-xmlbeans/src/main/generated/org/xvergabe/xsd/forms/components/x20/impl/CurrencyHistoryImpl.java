/*
 * XML Type:  CurrencyHistory
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.CurrencyHistory
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML CurrencyHistory(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class CurrencyHistoryImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.CurrencyHistory
{
    private static final long serialVersionUID = 1L;
    
    public CurrencyHistoryImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName YEAR$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "year");
    private static final javax.xml.namespace.QName AMOUNT$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "amount");
    private static final javax.xml.namespace.QName CURRENCY$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "currency");
    
    
    /**
     * Gets array of all "year" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Integer[] getYearArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(YEAR$0, targetList);
            org.xvergabe.xsd.forms.components.x20.Integer[] result = new org.xvergabe.xsd.forms.components.x20.Integer[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "year" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer getYearArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(YEAR$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "year" element
     */
    public int sizeOfYearArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(YEAR$0);
        }
    }
    
    /**
     * Sets array of all "year" element
     */
    public void setYearArray(org.xvergabe.xsd.forms.components.x20.Integer[] yearArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(yearArray, YEAR$0);
        }
    }
    
    /**
     * Sets ith "year" element
     */
    public void setYearArray(int i, org.xvergabe.xsd.forms.components.x20.Integer year)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(YEAR$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(year);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "year" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer insertNewYear(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().insert_element_user(YEAR$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "year" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer addNewYear()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(YEAR$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "year" element
     */
    public void removeYear(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(YEAR$0, i);
        }
    }
    
    /**
     * Gets array of all "amount" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Decimal[] getAmountArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(AMOUNT$2, targetList);
            org.xvergabe.xsd.forms.components.x20.Decimal[] result = new org.xvergabe.xsd.forms.components.x20.Decimal[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "amount" element
     */
    public org.xvergabe.xsd.forms.components.x20.Decimal getAmountArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Decimal target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Decimal)get_store().find_element_user(AMOUNT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "amount" element
     */
    public int sizeOfAmountArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AMOUNT$2);
        }
    }
    
    /**
     * Sets array of all "amount" element
     */
    public void setAmountArray(org.xvergabe.xsd.forms.components.x20.Decimal[] amountArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(amountArray, AMOUNT$2);
        }
    }
    
    /**
     * Sets ith "amount" element
     */
    public void setAmountArray(int i, org.xvergabe.xsd.forms.components.x20.Decimal amount)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Decimal target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Decimal)get_store().find_element_user(AMOUNT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(amount);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "amount" element
     */
    public org.xvergabe.xsd.forms.components.x20.Decimal insertNewAmount(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Decimal target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Decimal)get_store().insert_element_user(AMOUNT$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "amount" element
     */
    public org.xvergabe.xsd.forms.components.x20.Decimal addNewAmount()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Decimal target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Decimal)get_store().add_element_user(AMOUNT$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "amount" element
     */
    public void removeAmount(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AMOUNT$2, i);
        }
    }
    
    /**
     * Gets array of all "currency" elements
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyChoice[] getCurrencyArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CURRENCY$4, targetList);
            org.xvergabe.xsd.forms.components.x20.CurrencyChoice[] result = new org.xvergabe.xsd.forms.components.x20.CurrencyChoice[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "currency" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyChoice getCurrencyArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyChoice target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyChoice)get_store().find_element_user(CURRENCY$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "currency" element
     */
    public int sizeOfCurrencyArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CURRENCY$4);
        }
    }
    
    /**
     * Sets array of all "currency" element
     */
    public void setCurrencyArray(org.xvergabe.xsd.forms.components.x20.CurrencyChoice[] currencyArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(currencyArray, CURRENCY$4);
        }
    }
    
    /**
     * Sets ith "currency" element
     */
    public void setCurrencyArray(int i, org.xvergabe.xsd.forms.components.x20.CurrencyChoice currency)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyChoice target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyChoice)get_store().find_element_user(CURRENCY$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(currency);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "currency" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyChoice insertNewCurrency(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyChoice target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyChoice)get_store().insert_element_user(CURRENCY$4, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "currency" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyChoice addNewCurrency()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyChoice target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyChoice)get_store().add_element_user(CURRENCY$4);
            return target;
        }
    }
    
    /**
     * Removes the ith "currency" element
     */
    public void removeCurrency(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CURRENCY$4, i);
        }
    }
}
