/*
 * XML Type:  CurrencyMetaData
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.CurrencyMetaData
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML CurrencyMetaData(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class CurrencyMetaDataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.CurrencyMetaData
{
    private static final long serialVersionUID = 1L;
    
    public CurrencyMetaDataImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName RESTRICTION$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "restriction");
    
    
    /**
     * Gets the "restriction" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction getRestriction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction)get_store().find_element_user(RESTRICTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "restriction" element
     */
    public boolean isSetRestriction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(RESTRICTION$0) != 0;
        }
    }
    
    /**
     * Sets the "restriction" element
     */
    public void setRestriction(org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction restriction)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction)get_store().find_element_user(RESTRICTION$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction)get_store().add_element_user(RESTRICTION$0);
            }
            target.set(restriction);
        }
    }
    
    /**
     * Appends and returns a new empty "restriction" element
     */
    public org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction addNewRestriction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction)get_store().add_element_user(RESTRICTION$0);
            return target;
        }
    }
    
    /**
     * Unsets the "restriction" element
     */
    public void unsetRestriction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(RESTRICTION$0, 0);
        }
    }
    /**
     * An XML restriction(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public static class RestrictionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.CurrencyMetaData.Restriction
    {
        private static final long serialVersionUID = 1L;
        
        public RestrictionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName VALID$0 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "valid");
        
        
        /**
         * Gets array of all "valid" elements
         */
        public org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum[] getValidArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(VALID$0, targetList);
                org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum[] result = new org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum[targetList.size()];
                for (int i = 0, len = targetList.size() ; i < len ; i++)
                    result[i] = (org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum)((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getEnumValue();
                return result;
            }
        }
        
        /**
         * Gets ith "valid" element
         */
        public org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum getValidArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALID$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return (org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum)target.getEnumValue();
            }
        }
        
        /**
         * Gets (as xml) array of all "valid" elements
         */
        public org.xvergabe.xsd.forms.components.x20.CurrencyType[] xgetValidArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(VALID$0, targetList);
                org.xvergabe.xsd.forms.components.x20.CurrencyType[] result = new org.xvergabe.xsd.forms.components.x20.CurrencyType[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets (as xml) ith "valid" element
         */
        public org.xvergabe.xsd.forms.components.x20.CurrencyType xgetValidArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.CurrencyType target = null;
                target = (org.xvergabe.xsd.forms.components.x20.CurrencyType)get_store().find_element_user(VALID$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return (org.xvergabe.xsd.forms.components.x20.CurrencyType)target;
            }
        }
        
        /**
         * Returns number of "valid" element
         */
        public int sizeOfValidArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(VALID$0);
            }
        }
        
        /**
         * Sets array of all "valid" element
         */
        public void setValidArray(org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum[] validArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(validArray, VALID$0);
            }
        }
        
        /**
         * Sets ith "valid" element
         */
        public void setValidArray(int i, org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum valid)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALID$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.setEnumValue(valid);
            }
        }
        
        /**
         * Sets (as xml) array of all "valid" element
         */
        public void xsetValidArray(org.xvergabe.xsd.forms.components.x20.CurrencyType[]validArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(validArray, VALID$0);
            }
        }
        
        /**
         * Sets (as xml) ith "valid" element
         */
        public void xsetValidArray(int i, org.xvergabe.xsd.forms.components.x20.CurrencyType valid)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.CurrencyType target = null;
                target = (org.xvergabe.xsd.forms.components.x20.CurrencyType)get_store().find_element_user(VALID$0, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(valid);
            }
        }
        
        /**
         * Inserts the value as the ith "valid" element
         */
        public void insertValid(int i, org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum valid)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = 
                    (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(VALID$0, i);
                target.setEnumValue(valid);
            }
        }
        
        /**
         * Appends the value as the last "valid" element
         */
        public void addValid(org.xvergabe.xsd.forms.components.x20.CurrencyType.Enum valid)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALID$0);
                target.setEnumValue(valid);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "valid" element
         */
        public org.xvergabe.xsd.forms.components.x20.CurrencyType insertNewValid(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.CurrencyType target = null;
                target = (org.xvergabe.xsd.forms.components.x20.CurrencyType)get_store().insert_element_user(VALID$0, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "valid" element
         */
        public org.xvergabe.xsd.forms.components.x20.CurrencyType addNewValid()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.CurrencyType target = null;
                target = (org.xvergabe.xsd.forms.components.x20.CurrencyType)get_store().add_element_user(VALID$0);
                return target;
            }
        }
        
        /**
         * Removes the ith "valid" element
         */
        public void removeValid(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(VALID$0, i);
            }
        }
    }
}
