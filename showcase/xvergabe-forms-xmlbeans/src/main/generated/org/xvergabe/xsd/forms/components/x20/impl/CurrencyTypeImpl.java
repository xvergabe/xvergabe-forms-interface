/*
 * XML Type:  CurrencyType
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.CurrencyType
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML CurrencyType(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.CurrencyType.
 */
public class CurrencyTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements org.xvergabe.xsd.forms.components.x20.CurrencyType
{
    private static final long serialVersionUID = 1L;
    
    public CurrencyTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected CurrencyTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
