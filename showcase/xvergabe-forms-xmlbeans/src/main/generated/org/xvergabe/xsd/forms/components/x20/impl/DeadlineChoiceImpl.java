/*
 * XML Type:  DeadlineChoice
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.DeadlineChoice
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML DeadlineChoice(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class DeadlineChoiceImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl implements org.xvergabe.xsd.forms.components.x20.DeadlineChoice
{
    private static final long serialVersionUID = 1L;
    
    public DeadlineChoiceImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VALUE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "value");
    
    
    /**
     * Gets the "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.DeadlineType.Enum getValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                return null;
            }
            return (org.xvergabe.xsd.forms.components.x20.DeadlineType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.DeadlineType xgetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DeadlineType target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DeadlineType)get_store().find_element_user(VALUE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "value" element
     */
    public void setValue(org.xvergabe.xsd.forms.components.x20.DeadlineType.Enum value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALUE$0);
            }
            target.setEnumValue(value);
        }
    }
    
    /**
     * Sets (as xml) the "value" element
     */
    public void xsetValue(org.xvergabe.xsd.forms.components.x20.DeadlineType value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DeadlineType target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DeadlineType)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.DeadlineType)get_store().add_element_user(VALUE$0);
            }
            target.set(value);
        }
    }
}
