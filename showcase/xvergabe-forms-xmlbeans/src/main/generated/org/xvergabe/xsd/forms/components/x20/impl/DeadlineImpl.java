/*
 * XML Type:  Deadline
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Deadline
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Deadline(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class DeadlineImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Deadline
{
    private static final long serialVersionUID = 1L;
    
    public DeadlineImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName EXCLUSIONCRITERION$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "exclusionCriterion");
    private static final javax.xml.namespace.QName DATE$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "date");
    private static final javax.xml.namespace.QName TIME$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "time");
    private static final javax.xml.namespace.QName TYPE$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "type");
    private static final javax.xml.namespace.QName DESCRIPTION$8 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "description");
    
    
    /**
     * Gets the "exclusionCriterion" element
     */
    public org.xvergabe.xsd.forms.components.x20.Boolean getExclusionCriterion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().find_element_user(EXCLUSIONCRITERION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "exclusionCriterion" element
     */
    public boolean isSetExclusionCriterion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(EXCLUSIONCRITERION$0) != 0;
        }
    }
    
    /**
     * Sets the "exclusionCriterion" element
     */
    public void setExclusionCriterion(org.xvergabe.xsd.forms.components.x20.Boolean exclusionCriterion)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().find_element_user(EXCLUSIONCRITERION$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().add_element_user(EXCLUSIONCRITERION$0);
            }
            target.set(exclusionCriterion);
        }
    }
    
    /**
     * Appends and returns a new empty "exclusionCriterion" element
     */
    public org.xvergabe.xsd.forms.components.x20.Boolean addNewExclusionCriterion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().add_element_user(EXCLUSIONCRITERION$0);
            return target;
        }
    }
    
    /**
     * Unsets the "exclusionCriterion" element
     */
    public void unsetExclusionCriterion()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(EXCLUSIONCRITERION$0, 0);
        }
    }
    
    /**
     * Gets the "date" element
     */
    public org.xvergabe.xsd.forms.components.x20.Date getDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().find_element_user(DATE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "date" element
     */
    public void setDate(org.xvergabe.xsd.forms.components.x20.Date date)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().find_element_user(DATE$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().add_element_user(DATE$2);
            }
            target.set(date);
        }
    }
    
    /**
     * Appends and returns a new empty "date" element
     */
    public org.xvergabe.xsd.forms.components.x20.Date addNewDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().add_element_user(DATE$2);
            return target;
        }
    }
    
    /**
     * Gets the "time" element
     */
    public org.xvergabe.xsd.forms.components.x20.Time getTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Time target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Time)get_store().find_element_user(TIME$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "time" element
     */
    public boolean isSetTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TIME$4) != 0;
        }
    }
    
    /**
     * Sets the "time" element
     */
    public void setTime(org.xvergabe.xsd.forms.components.x20.Time time)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Time target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Time)get_store().find_element_user(TIME$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Time)get_store().add_element_user(TIME$4);
            }
            target.set(time);
        }
    }
    
    /**
     * Appends and returns a new empty "time" element
     */
    public org.xvergabe.xsd.forms.components.x20.Time addNewTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Time target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Time)get_store().add_element_user(TIME$4);
            return target;
        }
    }
    
    /**
     * Unsets the "time" element
     */
    public void unsetTime()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TIME$4, 0);
        }
    }
    
    /**
     * Gets the "type" element
     */
    public org.xvergabe.xsd.forms.components.x20.DeadlineChoice getType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DeadlineChoice target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DeadlineChoice)get_store().find_element_user(TYPE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "type" element
     */
    public void setType(org.xvergabe.xsd.forms.components.x20.DeadlineChoice type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DeadlineChoice target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DeadlineChoice)get_store().find_element_user(TYPE$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.DeadlineChoice)get_store().add_element_user(TYPE$6);
            }
            target.set(type);
        }
    }
    
    /**
     * Appends and returns a new empty "type" element
     */
    public org.xvergabe.xsd.forms.components.x20.DeadlineChoice addNewType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DeadlineChoice target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DeadlineChoice)get_store().add_element_user(TYPE$6);
            return target;
        }
    }
    
    /**
     * Gets the "description" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(DESCRIPTION$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "description" element
     */
    public boolean isSetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DESCRIPTION$8) != 0;
        }
    }
    
    /**
     * Sets the "description" element
     */
    public void setDescription(org.xvergabe.xsd.forms.components.x20.String description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(DESCRIPTION$8, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(DESCRIPTION$8);
            }
            target.set(description);
        }
    }
    
    /**
     * Appends and returns a new empty "description" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(DESCRIPTION$8);
            return target;
        }
    }
    
    /**
     * Unsets the "description" element
     */
    public void unsetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DESCRIPTION$8, 0);
        }
    }
}
