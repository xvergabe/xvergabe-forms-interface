/*
 * XML Type:  DeadlineType
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.DeadlineType
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML DeadlineType(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.DeadlineType.
 */
public class DeadlineTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements org.xvergabe.xsd.forms.components.x20.DeadlineType
{
    private static final long serialVersionUID = 1L;
    
    public DeadlineTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected DeadlineTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
