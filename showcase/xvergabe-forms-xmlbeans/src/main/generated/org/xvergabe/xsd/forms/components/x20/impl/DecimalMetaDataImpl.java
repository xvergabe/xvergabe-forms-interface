/*
 * XML Type:  DecimalMetaData
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.DecimalMetaData
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML DecimalMetaData(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class DecimalMetaDataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.DecimalMetaData
{
    private static final long serialVersionUID = 1L;
    
    public DecimalMetaDataImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MIN$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "min");
    private static final javax.xml.namespace.QName MAX$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "max");
    private static final javax.xml.namespace.QName DECIMALS$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "decimals");
    
    
    /**
     * Gets the "min" element
     */
    public float getMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIN$0, 0);
            if (target == null)
            {
                return 0.0f;
            }
            return target.getFloatValue();
        }
    }
    
    /**
     * Gets (as xml) the "min" element
     */
    public org.apache.xmlbeans.XmlFloat xgetMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlFloat target = null;
            target = (org.apache.xmlbeans.XmlFloat)get_store().find_element_user(MIN$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "min" element
     */
    public boolean isSetMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MIN$0) != 0;
        }
    }
    
    /**
     * Sets the "min" element
     */
    public void setMin(float min)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MIN$0);
            }
            target.setFloatValue(min);
        }
    }
    
    /**
     * Sets (as xml) the "min" element
     */
    public void xsetMin(org.apache.xmlbeans.XmlFloat min)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlFloat target = null;
            target = (org.apache.xmlbeans.XmlFloat)get_store().find_element_user(MIN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlFloat)get_store().add_element_user(MIN$0);
            }
            target.set(min);
        }
    }
    
    /**
     * Unsets the "min" element
     */
    public void unsetMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MIN$0, 0);
        }
    }
    
    /**
     * Gets the "max" element
     */
    public float getMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAX$2, 0);
            if (target == null)
            {
                return 0.0f;
            }
            return target.getFloatValue();
        }
    }
    
    /**
     * Gets (as xml) the "max" element
     */
    public org.apache.xmlbeans.XmlFloat xgetMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlFloat target = null;
            target = (org.apache.xmlbeans.XmlFloat)get_store().find_element_user(MAX$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "max" element
     */
    public boolean isSetMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MAX$2) != 0;
        }
    }
    
    /**
     * Sets the "max" element
     */
    public void setMax(float max)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAX$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MAX$2);
            }
            target.setFloatValue(max);
        }
    }
    
    /**
     * Sets (as xml) the "max" element
     */
    public void xsetMax(org.apache.xmlbeans.XmlFloat max)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlFloat target = null;
            target = (org.apache.xmlbeans.XmlFloat)get_store().find_element_user(MAX$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlFloat)get_store().add_element_user(MAX$2);
            }
            target.set(max);
        }
    }
    
    /**
     * Unsets the "max" element
     */
    public void unsetMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MAX$2, 0);
        }
    }
    
    /**
     * Gets the "decimals" element
     */
    public int getDecimals()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DECIMALS$4, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "decimals" element
     */
    public org.apache.xmlbeans.XmlInt xgetDecimals()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(DECIMALS$4, 0);
            return target;
        }
    }
    
    /**
     * True if has "decimals" element
     */
    public boolean isSetDecimals()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DECIMALS$4) != 0;
        }
    }
    
    /**
     * Sets the "decimals" element
     */
    public void setDecimals(int decimals)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(DECIMALS$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(DECIMALS$4);
            }
            target.setIntValue(decimals);
        }
    }
    
    /**
     * Sets (as xml) the "decimals" element
     */
    public void xsetDecimals(org.apache.xmlbeans.XmlInt decimals)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(DECIMALS$4, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(DECIMALS$4);
            }
            target.set(decimals);
        }
    }
    
    /**
     * Unsets the "decimals" element
     */
    public void unsetDecimals()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DECIMALS$4, 0);
        }
    }
}
