/*
 * XML Type:  Description
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Description
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Description(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class DescriptionImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Description
{
    private static final long serialVersionUID = 1L;
    
    public DescriptionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SHORTDESCRIPTION$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "shortDescription");
    private static final javax.xml.namespace.QName LONGDESCRIPTION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "longDescription");
    
    
    /**
     * Gets the "shortDescription" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getShortDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SHORTDESCRIPTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "shortDescription" element
     */
    public boolean isSetShortDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SHORTDESCRIPTION$0) != 0;
        }
    }
    
    /**
     * Sets the "shortDescription" element
     */
    public void setShortDescription(org.xvergabe.xsd.forms.components.x20.String shortDescription)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SHORTDESCRIPTION$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SHORTDESCRIPTION$0);
            }
            target.set(shortDescription);
        }
    }
    
    /**
     * Appends and returns a new empty "shortDescription" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewShortDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SHORTDESCRIPTION$0);
            return target;
        }
    }
    
    /**
     * Unsets the "shortDescription" element
     */
    public void unsetShortDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SHORTDESCRIPTION$0, 0);
        }
    }
    
    /**
     * Gets the "longDescription" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getLongDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LONGDESCRIPTION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "longDescription" element
     */
    public boolean isSetLongDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LONGDESCRIPTION$2) != 0;
        }
    }
    
    /**
     * Sets the "longDescription" element
     */
    public void setLongDescription(org.xvergabe.xsd.forms.components.x20.String longDescription)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LONGDESCRIPTION$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LONGDESCRIPTION$2);
            }
            target.set(longDescription);
        }
    }
    
    /**
     * Appends and returns a new empty "longDescription" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewLongDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LONGDESCRIPTION$2);
            return target;
        }
    }
    
    /**
     * Unsets the "longDescription" element
     */
    public void unsetLongDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LONGDESCRIPTION$2, 0);
        }
    }
}
