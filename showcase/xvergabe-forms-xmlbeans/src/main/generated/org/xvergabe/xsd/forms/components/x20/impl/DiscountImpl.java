/*
 * XML Type:  Discount
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Discount
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Discount(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class DiscountImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Discount
{
    private static final long serialVersionUID = 1L;
    
    public DiscountImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DAYS$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "days");
    private static final javax.xml.namespace.QName PERCENTAGE$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "percentage");
    
    
    /**
     * Gets the "days" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer getDays()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(DAYS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "days" element
     */
    public void setDays(org.xvergabe.xsd.forms.components.x20.Integer days)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(DAYS$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(DAYS$0);
            }
            target.set(days);
        }
    }
    
    /**
     * Appends and returns a new empty "days" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer addNewDays()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(DAYS$0);
            return target;
        }
    }
    
    /**
     * Gets the "percentage" element
     */
    public org.xvergabe.xsd.forms.components.x20.Percentage getPercentage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Percentage target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Percentage)get_store().find_element_user(PERCENTAGE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "percentage" element
     */
    public void setPercentage(org.xvergabe.xsd.forms.components.x20.Percentage percentage)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Percentage target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Percentage)get_store().find_element_user(PERCENTAGE$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Percentage)get_store().add_element_user(PERCENTAGE$2);
            }
            target.set(percentage);
        }
    }
    
    /**
     * Appends and returns a new empty "percentage" element
     */
    public org.xvergabe.xsd.forms.components.x20.Percentage addNewPercentage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Percentage target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Percentage)get_store().add_element_user(PERCENTAGE$2);
            return target;
        }
    }
}
