/*
 * XML Type:  DisplayInformation
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.DisplayInformation
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML DisplayInformation(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class DisplayInformationImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.DisplayInformation
{
    private static final long serialVersionUID = 1L;
    
    public DisplayInformationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName QUICKINFO$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "quickInfo");
    private static final javax.xml.namespace.QName LABEL$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "label");
    private static final javax.xml.namespace.QName LANG$4 = 
        new javax.xml.namespace.QName("", "lang");
    
    
    /**
     * Gets the "quickInfo" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo getQuickInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo)get_store().find_element_user(QUICKINFO$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "quickInfo" element
     */
    public boolean isSetQuickInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(QUICKINFO$0) != 0;
        }
    }
    
    /**
     * Sets the "quickInfo" element
     */
    public void setQuickInfo(org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo quickInfo)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo)get_store().find_element_user(QUICKINFO$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo)get_store().add_element_user(QUICKINFO$0);
            }
            target.set(quickInfo);
        }
    }
    
    /**
     * Appends and returns a new empty "quickInfo" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo addNewQuickInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo)get_store().add_element_user(QUICKINFO$0);
            return target;
        }
    }
    
    /**
     * Unsets the "quickInfo" element
     */
    public void unsetQuickInfo()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(QUICKINFO$0, 0);
        }
    }
    
    /**
     * Gets the "label" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label getLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label)get_store().find_element_user(LABEL$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "label" element
     */
    public void setLabel(org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label label)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label)get_store().find_element_user(LABEL$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label)get_store().add_element_user(LABEL$2);
            }
            target.set(label);
        }
    }
    
    /**
     * Appends and returns a new empty "label" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label addNewLabel()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label)get_store().add_element_user(LABEL$2);
            return target;
        }
    }
    
    /**
     * Gets the "lang" attribute
     */
    public java.lang.String getLang()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(LANG$4);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "lang" attribute
     */
    public org.apache.xmlbeans.XmlLanguage xgetLang()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlLanguage target = null;
            target = (org.apache.xmlbeans.XmlLanguage)get_store().find_attribute_user(LANG$4);
            return target;
        }
    }
    
    /**
     * Sets the "lang" attribute
     */
    public void setLang(java.lang.String lang)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(LANG$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(LANG$4);
            }
            target.setStringValue(lang);
        }
    }
    
    /**
     * Sets (as xml) the "lang" attribute
     */
    public void xsetLang(org.apache.xmlbeans.XmlLanguage lang)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlLanguage target = null;
            target = (org.apache.xmlbeans.XmlLanguage)get_store().find_attribute_user(LANG$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlLanguage)get_store().add_attribute_user(LANG$4);
            }
            target.set(lang);
        }
    }
    /**
     * An XML quickInfo(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.DisplayInformation$QuickInfo.
     */
    public static class QuickInfoImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements org.xvergabe.xsd.forms.components.x20.DisplayInformation.QuickInfo
    {
        private static final long serialVersionUID = 1L;
        
        public QuickInfoImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, true);
        }
        
        protected QuickInfoImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        
        
    }
    /**
     * An XML label(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.DisplayInformation$Label.
     */
    public static class LabelImpl extends org.apache.xmlbeans.impl.values.JavaStringHolderEx implements org.xvergabe.xsd.forms.components.x20.DisplayInformation.Label
    {
        private static final long serialVersionUID = 1L;
        
        public LabelImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType, true);
        }
        
        protected LabelImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
        {
            super(sType, b);
        }
        
        
    }
}
