/*
 * XML Type:  DropDown
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.DropDown
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML DropDown(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class DropDownImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl implements org.xvergabe.xsd.forms.components.x20.DropDown
{
    private static final long serialVersionUID = 1L;
    
    public DropDownImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OPTION$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "option");
    
    
    /**
     * Gets array of all "option" elements
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractField[] getOptionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(OPTION$0, targetList);
            org.xvergabe.xsd.forms.components.x20.AbstractField[] result = new org.xvergabe.xsd.forms.components.x20.AbstractField[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "option" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractField getOptionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractField target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractField)get_store().find_element_user(OPTION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "option" element
     */
    public int sizeOfOptionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OPTION$0);
        }
    }
    
    /**
     * Sets array of all "option" element
     */
    public void setOptionArray(org.xvergabe.xsd.forms.components.x20.AbstractField[] optionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(optionArray, OPTION$0);
        }
    }
    
    /**
     * Sets ith "option" element
     */
    public void setOptionArray(int i, org.xvergabe.xsd.forms.components.x20.AbstractField option)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractField target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractField)get_store().find_element_user(OPTION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(option);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "option" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractField insertNewOption(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractField target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractField)get_store().insert_element_user(OPTION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "option" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractField addNewOption()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractField target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractField)get_store().add_element_user(OPTION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "option" element
     */
    public void removeOption(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OPTION$0, i);
        }
    }
}
