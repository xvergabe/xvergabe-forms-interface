/*
 * XML Type:  DynamicCheckGroup
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML DynamicCheckGroup(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class DynamicCheckGroupImpl extends org.xvergabe.xsd.forms.components.x20.impl.CheckGroupImpl implements org.xvergabe.xsd.forms.components.x20.DynamicCheckGroup
{
    private static final long serialVersionUID = 1L;
    
    public DynamicCheckGroupImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
