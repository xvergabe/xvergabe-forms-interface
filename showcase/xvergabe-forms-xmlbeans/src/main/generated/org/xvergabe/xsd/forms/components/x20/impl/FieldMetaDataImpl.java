/*
 * XML Type:  FieldMetaData
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.FieldMetaData
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML FieldMetaData(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class FieldMetaDataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FieldMetaData
{
    private static final long serialVersionUID = 1L;
    
    public FieldMetaDataImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FILLINGROLE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "fillingRole");
    private static final javax.xml.namespace.QName REQUIRED$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "required");
    private static final javax.xml.namespace.QName DISPLAYINFORMATION$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "displayInformation");
    private static final javax.xml.namespace.QName VISIBLE$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "visible");
    private static final javax.xml.namespace.QName ACTIVE$8 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "active");
    private static final javax.xml.namespace.QName STATIC$10 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "static");
    
    
    /**
     * Gets the "fillingRole" element
     */
    public org.xvergabe.xsd.forms.components.x20.RoleType.Enum getFillingRole()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FILLINGROLE$0, 0);
            if (target == null)
            {
                return null;
            }
            return (org.xvergabe.xsd.forms.components.x20.RoleType.Enum)target.getEnumValue();
        }
    }
    
    /**
     * Gets (as xml) the "fillingRole" element
     */
    public org.xvergabe.xsd.forms.components.x20.RoleType xgetFillingRole()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.RoleType target = null;
            target = (org.xvergabe.xsd.forms.components.x20.RoleType)get_store().find_element_user(FILLINGROLE$0, 0);
            return target;
        }
    }
    
    /**
     * Sets the "fillingRole" element
     */
    public void setFillingRole(org.xvergabe.xsd.forms.components.x20.RoleType.Enum fillingRole)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(FILLINGROLE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(FILLINGROLE$0);
            }
            target.setEnumValue(fillingRole);
        }
    }
    
    /**
     * Sets (as xml) the "fillingRole" element
     */
    public void xsetFillingRole(org.xvergabe.xsd.forms.components.x20.RoleType fillingRole)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.RoleType target = null;
            target = (org.xvergabe.xsd.forms.components.x20.RoleType)get_store().find_element_user(FILLINGROLE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.RoleType)get_store().add_element_user(FILLINGROLE$0);
            }
            target.set(fillingRole);
        }
    }
    
    /**
     * Gets the "required" element
     */
    public boolean getRequired()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUIRED$2, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "required" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetRequired()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(REQUIRED$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "required" element
     */
    public boolean isSetRequired()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REQUIRED$2) != 0;
        }
    }
    
    /**
     * Sets the "required" element
     */
    public void setRequired(boolean required)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(REQUIRED$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(REQUIRED$2);
            }
            target.setBooleanValue(required);
        }
    }
    
    /**
     * Sets (as xml) the "required" element
     */
    public void xsetRequired(org.apache.xmlbeans.XmlBoolean required)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(REQUIRED$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(REQUIRED$2);
            }
            target.set(required);
        }
    }
    
    /**
     * Unsets the "required" element
     */
    public void unsetRequired()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REQUIRED$2, 0);
        }
    }
    
    /**
     * Gets array of all "displayInformation" elements
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation[] getDisplayInformationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DISPLAYINFORMATION$4, targetList);
            org.xvergabe.xsd.forms.components.x20.DisplayInformation[] result = new org.xvergabe.xsd.forms.components.x20.DisplayInformation[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation getDisplayInformationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().find_element_user(DISPLAYINFORMATION$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "displayInformation" element
     */
    public int sizeOfDisplayInformationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DISPLAYINFORMATION$4);
        }
    }
    
    /**
     * Sets array of all "displayInformation" element
     */
    public void setDisplayInformationArray(org.xvergabe.xsd.forms.components.x20.DisplayInformation[] displayInformationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(displayInformationArray, DISPLAYINFORMATION$4);
        }
    }
    
    /**
     * Sets ith "displayInformation" element
     */
    public void setDisplayInformationArray(int i, org.xvergabe.xsd.forms.components.x20.DisplayInformation displayInformation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().find_element_user(DISPLAYINFORMATION$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(displayInformation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation insertNewDisplayInformation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().insert_element_user(DISPLAYINFORMATION$4, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation addNewDisplayInformation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().add_element_user(DISPLAYINFORMATION$4);
            return target;
        }
    }
    
    /**
     * Removes the ith "displayInformation" element
     */
    public void removeDisplayInformation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DISPLAYINFORMATION$4, i);
        }
    }
    
    /**
     * Gets the "visible" element
     */
    public boolean getVisible()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VISIBLE$6, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "visible" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetVisible()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(VISIBLE$6, 0);
            return target;
        }
    }
    
    /**
     * True if has "visible" element
     */
    public boolean isSetVisible()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VISIBLE$6) != 0;
        }
    }
    
    /**
     * Sets the "visible" element
     */
    public void setVisible(boolean visible)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VISIBLE$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VISIBLE$6);
            }
            target.setBooleanValue(visible);
        }
    }
    
    /**
     * Sets (as xml) the "visible" element
     */
    public void xsetVisible(org.apache.xmlbeans.XmlBoolean visible)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(VISIBLE$6, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(VISIBLE$6);
            }
            target.set(visible);
        }
    }
    
    /**
     * Unsets the "visible" element
     */
    public void unsetVisible()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VISIBLE$6, 0);
        }
    }
    
    /**
     * Gets the "active" element
     */
    public boolean getActive()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACTIVE$8, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "active" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetActive()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(ACTIVE$8, 0);
            return target;
        }
    }
    
    /**
     * True if has "active" element
     */
    public boolean isSetActive()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACTIVE$8) != 0;
        }
    }
    
    /**
     * Sets the "active" element
     */
    public void setActive(boolean active)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(ACTIVE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(ACTIVE$8);
            }
            target.setBooleanValue(active);
        }
    }
    
    /**
     * Sets (as xml) the "active" element
     */
    public void xsetActive(org.apache.xmlbeans.XmlBoolean active)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(ACTIVE$8, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(ACTIVE$8);
            }
            target.set(active);
        }
    }
    
    /**
     * Unsets the "active" element
     */
    public void unsetActive()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACTIVE$8, 0);
        }
    }
    
    /**
     * Gets the "static" element
     */
    public boolean getStatic()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATIC$10, 0);
            if (target == null)
            {
                return false;
            }
            return target.getBooleanValue();
        }
    }
    
    /**
     * Gets (as xml) the "static" element
     */
    public org.apache.xmlbeans.XmlBoolean xgetStatic()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(STATIC$10, 0);
            return target;
        }
    }
    
    /**
     * True if has "static" element
     */
    public boolean isSetStatic()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(STATIC$10) != 0;
        }
    }
    
    /**
     * Sets the "static" element
     */
    public void setStatic(boolean xstatic)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(STATIC$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(STATIC$10);
            }
            target.setBooleanValue(xstatic);
        }
    }
    
    /**
     * Sets (as xml) the "static" element
     */
    public void xsetStatic(org.apache.xmlbeans.XmlBoolean xstatic)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlBoolean target = null;
            target = (org.apache.xmlbeans.XmlBoolean)get_store().find_element_user(STATIC$10, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlBoolean)get_store().add_element_user(STATIC$10);
            }
            target.set(xstatic);
        }
    }
    
    /**
     * Unsets the "static" element
     */
    public void unsetStatic()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(STATIC$10, 0);
        }
    }
}
