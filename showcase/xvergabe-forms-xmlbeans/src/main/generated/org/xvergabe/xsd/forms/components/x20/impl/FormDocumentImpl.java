/*
 * An XML document type.
 * Localname: form
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.FormDocument
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * A document containing one form(@http://www.xvergabe.org/xsd/forms/components/2_0) element.
 *
 * This is a complex type.
 */
public class FormDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormDocument
{
    private static final long serialVersionUID = 1L;
    
    public FormDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FORM$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "form");
    
    
    /**
     * Gets the "form" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormDocument.Form getForm()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormDocument.Form target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormDocument.Form)get_store().find_element_user(FORM$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "form" element
     */
    public void setForm(org.xvergabe.xsd.forms.components.x20.FormDocument.Form form)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormDocument.Form target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormDocument.Form)get_store().find_element_user(FORM$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.FormDocument.Form)get_store().add_element_user(FORM$0);
            }
            target.set(form);
        }
    }
    
    /**
     * Appends and returns a new empty "form" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormDocument.Form addNewForm()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormDocument.Form target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormDocument.Form)get_store().add_element_user(FORM$0);
            return target;
        }
    }
    /**
     * An XML form(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public static class FormImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormDocument.Form
    {
        private static final long serialVersionUID = 1L;
        
        public FormImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName METADATA$0 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "metadata");
        private static final javax.xml.namespace.QName SECTION$2 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "section");
        
        
        /**
         * Gets the "metadata" element
         */
        public org.xvergabe.xsd.forms.components.x20.FormMetaData getMetadata()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData)get_store().find_element_user(METADATA$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "metadata" element
         */
        public boolean isSetMetadata()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(METADATA$0) != 0;
            }
        }
        
        /**
         * Sets the "metadata" element
         */
        public void setMetadata(org.xvergabe.xsd.forms.components.x20.FormMetaData metadata)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData)get_store().find_element_user(METADATA$0, 0);
                if (target == null)
                {
                    target = (org.xvergabe.xsd.forms.components.x20.FormMetaData)get_store().add_element_user(METADATA$0);
                }
                target.set(metadata);
            }
        }
        
        /**
         * Appends and returns a new empty "metadata" element
         */
        public org.xvergabe.xsd.forms.components.x20.FormMetaData addNewMetadata()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData)get_store().add_element_user(METADATA$0);
                return target;
            }
        }
        
        /**
         * Unsets the "metadata" element
         */
        public void unsetMetadata()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(METADATA$0, 0);
            }
        }
        
        /**
         * Gets array of all "section" elements
         */
        public org.xvergabe.xsd.forms.components.x20.Section[] getSectionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                java.util.List targetList = new java.util.ArrayList();
                get_store().find_all_element_users(SECTION$2, targetList);
                org.xvergabe.xsd.forms.components.x20.Section[] result = new org.xvergabe.xsd.forms.components.x20.Section[targetList.size()];
                targetList.toArray(result);
                return result;
            }
        }
        
        /**
         * Gets ith "section" element
         */
        public org.xvergabe.xsd.forms.components.x20.Section getSectionArray(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.Section target = null;
                target = (org.xvergabe.xsd.forms.components.x20.Section)get_store().find_element_user(SECTION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                return target;
            }
        }
        
        /**
         * Returns number of "section" element
         */
        public int sizeOfSectionArray()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(SECTION$2);
            }
        }
        
        /**
         * Sets array of all "section" element
         */
        public void setSectionArray(org.xvergabe.xsd.forms.components.x20.Section[] sectionArray)
        {
            synchronized (monitor())
            {
                check_orphaned();
                arraySetterHelper(sectionArray, SECTION$2);
            }
        }
        
        /**
         * Sets ith "section" element
         */
        public void setSectionArray(int i, org.xvergabe.xsd.forms.components.x20.Section section)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.Section target = null;
                target = (org.xvergabe.xsd.forms.components.x20.Section)get_store().find_element_user(SECTION$2, i);
                if (target == null)
                {
                    throw new IndexOutOfBoundsException();
                }
                target.set(section);
            }
        }
        
        /**
         * Inserts and returns a new empty value (as xml) as the ith "section" element
         */
        public org.xvergabe.xsd.forms.components.x20.Section insertNewSection(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.Section target = null;
                target = (org.xvergabe.xsd.forms.components.x20.Section)get_store().insert_element_user(SECTION$2, i);
                return target;
            }
        }
        
        /**
         * Appends and returns a new empty value (as xml) as the last "section" element
         */
        public org.xvergabe.xsd.forms.components.x20.Section addNewSection()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.Section target = null;
                target = (org.xvergabe.xsd.forms.components.x20.Section)get_store().add_element_user(SECTION$2);
                return target;
            }
        }
        
        /**
         * Removes the ith "section" element
         */
        public void removeSection(int i)
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(SECTION$2, i);
            }
        }
    }
}
