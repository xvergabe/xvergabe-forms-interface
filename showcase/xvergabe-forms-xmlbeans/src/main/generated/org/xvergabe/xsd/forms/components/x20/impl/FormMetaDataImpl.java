/*
 * XML Type:  FormMetaData
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.FormMetaData
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML FormMetaData(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class FormMetaDataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormMetaData
{
    private static final long serialVersionUID = 1L;
    
    public FormMetaDataImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DISPLAYINFORMATION$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "displayInformation");
    private static final javax.xml.namespace.QName REQUIREDDISJUNCTION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "requiredDisjunction");
    private static final javax.xml.namespace.QName REQUIREDCONJUNCTION$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "requiredConjunction");
    private static final javax.xml.namespace.QName ACTIVATIONTOGGLE$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "activationToggle");
    private static final javax.xml.namespace.QName VALUETRANSFER$8 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "valueTransfer");
    
    
    /**
     * Gets array of all "displayInformation" elements
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation[] getDisplayInformationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DISPLAYINFORMATION$0, targetList);
            org.xvergabe.xsd.forms.components.x20.DisplayInformation[] result = new org.xvergabe.xsd.forms.components.x20.DisplayInformation[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation getDisplayInformationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().find_element_user(DISPLAYINFORMATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "displayInformation" element
     */
    public int sizeOfDisplayInformationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DISPLAYINFORMATION$0);
        }
    }
    
    /**
     * Sets array of all "displayInformation" element
     */
    public void setDisplayInformationArray(org.xvergabe.xsd.forms.components.x20.DisplayInformation[] displayInformationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(displayInformationArray, DISPLAYINFORMATION$0);
        }
    }
    
    /**
     * Sets ith "displayInformation" element
     */
    public void setDisplayInformationArray(int i, org.xvergabe.xsd.forms.components.x20.DisplayInformation displayInformation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().find_element_user(DISPLAYINFORMATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(displayInformation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation insertNewDisplayInformation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().insert_element_user(DISPLAYINFORMATION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "displayInformation" element
     */
    public org.xvergabe.xsd.forms.components.x20.DisplayInformation addNewDisplayInformation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DisplayInformation target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DisplayInformation)get_store().add_element_user(DISPLAYINFORMATION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "displayInformation" element
     */
    public void removeDisplayInformation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DISPLAYINFORMATION$0, i);
        }
    }
    
    /**
     * Gets array of all "requiredDisjunction" elements
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction[] getRequiredDisjunctionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(REQUIREDDISJUNCTION$2, targetList);
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction[] result = new org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "requiredDisjunction" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction getRequiredDisjunctionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction)get_store().find_element_user(REQUIREDDISJUNCTION$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "requiredDisjunction" element
     */
    public int sizeOfRequiredDisjunctionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REQUIREDDISJUNCTION$2);
        }
    }
    
    /**
     * Sets array of all "requiredDisjunction" element
     */
    public void setRequiredDisjunctionArray(org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction[] requiredDisjunctionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(requiredDisjunctionArray, REQUIREDDISJUNCTION$2);
        }
    }
    
    /**
     * Sets ith "requiredDisjunction" element
     */
    public void setRequiredDisjunctionArray(int i, org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction requiredDisjunction)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction)get_store().find_element_user(REQUIREDDISJUNCTION$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(requiredDisjunction);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "requiredDisjunction" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction insertNewRequiredDisjunction(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction)get_store().insert_element_user(REQUIREDDISJUNCTION$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "requiredDisjunction" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction addNewRequiredDisjunction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction)get_store().add_element_user(REQUIREDDISJUNCTION$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "requiredDisjunction" element
     */
    public void removeRequiredDisjunction(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REQUIREDDISJUNCTION$2, i);
        }
    }
    
    /**
     * Gets array of all "requiredConjunction" elements
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction[] getRequiredConjunctionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(REQUIREDCONJUNCTION$4, targetList);
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction[] result = new org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "requiredConjunction" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction getRequiredConjunctionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction)get_store().find_element_user(REQUIREDCONJUNCTION$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "requiredConjunction" element
     */
    public int sizeOfRequiredConjunctionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REQUIREDCONJUNCTION$4);
        }
    }
    
    /**
     * Sets array of all "requiredConjunction" element
     */
    public void setRequiredConjunctionArray(org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction[] requiredConjunctionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(requiredConjunctionArray, REQUIREDCONJUNCTION$4);
        }
    }
    
    /**
     * Sets ith "requiredConjunction" element
     */
    public void setRequiredConjunctionArray(int i, org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction requiredConjunction)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction)get_store().find_element_user(REQUIREDCONJUNCTION$4, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(requiredConjunction);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "requiredConjunction" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction insertNewRequiredConjunction(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction)get_store().insert_element_user(REQUIREDCONJUNCTION$4, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "requiredConjunction" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction addNewRequiredConjunction()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction)get_store().add_element_user(REQUIREDCONJUNCTION$4);
            return target;
        }
    }
    
    /**
     * Removes the ith "requiredConjunction" element
     */
    public void removeRequiredConjunction(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REQUIREDCONJUNCTION$4, i);
        }
    }
    
    /**
     * Gets array of all "activationToggle" elements
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle[] getActivationToggleArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ACTIVATIONTOGGLE$6, targetList);
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle[] result = new org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "activationToggle" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle getActivationToggleArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle)get_store().find_element_user(ACTIVATIONTOGGLE$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "activationToggle" element
     */
    public int sizeOfActivationToggleArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACTIVATIONTOGGLE$6);
        }
    }
    
    /**
     * Sets array of all "activationToggle" element
     */
    public void setActivationToggleArray(org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle[] activationToggleArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(activationToggleArray, ACTIVATIONTOGGLE$6);
        }
    }
    
    /**
     * Sets ith "activationToggle" element
     */
    public void setActivationToggleArray(int i, org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle activationToggle)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle)get_store().find_element_user(ACTIVATIONTOGGLE$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(activationToggle);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "activationToggle" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle insertNewActivationToggle(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle)get_store().insert_element_user(ACTIVATIONTOGGLE$6, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "activationToggle" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle addNewActivationToggle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle)get_store().add_element_user(ACTIVATIONTOGGLE$6);
            return target;
        }
    }
    
    /**
     * Removes the ith "activationToggle" element
     */
    public void removeActivationToggle(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACTIVATIONTOGGLE$6, i);
        }
    }
    
    /**
     * Gets array of all "valueTransfer" elements
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer[] getValueTransferArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(VALUETRANSFER$8, targetList);
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer[] result = new org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "valueTransfer" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer getValueTransferArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer)get_store().find_element_user(VALUETRANSFER$8, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "valueTransfer" element
     */
    public int sizeOfValueTransferArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VALUETRANSFER$8);
        }
    }
    
    /**
     * Sets array of all "valueTransfer" element
     */
    public void setValueTransferArray(org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer[] valueTransferArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(valueTransferArray, VALUETRANSFER$8);
        }
    }
    
    /**
     * Sets ith "valueTransfer" element
     */
    public void setValueTransferArray(int i, org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer valueTransfer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer)get_store().find_element_user(VALUETRANSFER$8, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(valueTransfer);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "valueTransfer" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer insertNewValueTransfer(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer)get_store().insert_element_user(VALUETRANSFER$8, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "valueTransfer" element
     */
    public org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer addNewValueTransfer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer)get_store().add_element_user(VALUETRANSFER$8);
            return target;
        }
    }
    
    /**
     * Removes the ith "valueTransfer" element
     */
    public void removeValueTransfer(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VALUETRANSFER$8, i);
        }
    }
    /**
     * An XML requiredDisjunction(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public static class RequiredDisjunctionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction
    {
        private static final long serialVersionUID = 1L;
        
        public RequiredDisjunctionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName TRIGGERFIELD$0 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "triggerField");
        private static final javax.xml.namespace.QName TRIGGERVALUE$2 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "triggerValue");
        private static final javax.xml.namespace.QName TARGETFIELDS$4 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "targetFields");
        
        
        /**
         * Gets the "triggerField" element
         */
        public java.lang.String getTriggerField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "triggerField" element
         */
        public org.apache.xmlbeans.XmlIDREF xgetTriggerField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TRIGGERFIELD$0, 0);
                return target;
            }
        }
        
        /**
         * True if has "triggerField" element
         */
        public boolean isSetTriggerField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(TRIGGERFIELD$0) != 0;
            }
        }
        
        /**
         * Sets the "triggerField" element
         */
        public void setTriggerField(java.lang.String triggerField)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TRIGGERFIELD$0);
                }
                target.setStringValue(triggerField);
            }
        }
        
        /**
         * Sets (as xml) the "triggerField" element
         */
        public void xsetTriggerField(org.apache.xmlbeans.XmlIDREF triggerField)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_element_user(TRIGGERFIELD$0);
                }
                target.set(triggerField);
            }
        }
        
        /**
         * Unsets the "triggerField" element
         */
        public void unsetTriggerField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(TRIGGERFIELD$0, 0);
            }
        }
        
        /**
         * Gets the "triggerValue" element
         */
        public org.apache.xmlbeans.XmlObject getTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlObject target = null;
                target = (org.apache.xmlbeans.XmlObject)get_store().find_element_user(TRIGGERVALUE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "triggerValue" element
         */
        public boolean isSetTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(TRIGGERVALUE$2) != 0;
            }
        }
        
        /**
         * Sets the "triggerValue" element
         */
        public void setTriggerValue(org.apache.xmlbeans.XmlObject triggerValue)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlObject target = null;
                target = (org.apache.xmlbeans.XmlObject)get_store().find_element_user(TRIGGERVALUE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlObject)get_store().add_element_user(TRIGGERVALUE$2);
                }
                target.set(triggerValue);
            }
        }
        
        /**
         * Appends and returns a new empty "triggerValue" element
         */
        public org.apache.xmlbeans.XmlObject addNewTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlObject target = null;
                target = (org.apache.xmlbeans.XmlObject)get_store().add_element_user(TRIGGERVALUE$2);
                return target;
            }
        }
        
        /**
         * Unsets the "triggerValue" element
         */
        public void unsetTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(TRIGGERVALUE$2, 0);
            }
        }
        
        /**
         * Gets the "targetFields" element
         */
        public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields getTargetFields()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields)get_store().find_element_user(TARGETFIELDS$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "targetFields" element
         */
        public void setTargetFields(org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields targetFields)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields)get_store().find_element_user(TARGETFIELDS$4, 0);
                if (target == null)
                {
                    target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields)get_store().add_element_user(TARGETFIELDS$4);
                }
                target.set(targetFields);
            }
        }
        
        /**
         * Appends and returns a new empty "targetFields" element
         */
        public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields addNewTargetFields()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields)get_store().add_element_user(TARGETFIELDS$4);
                return target;
            }
        }
        /**
         * An XML targetFields(@http://www.xvergabe.org/xsd/forms/components/2_0).
         *
         * This is a complex type.
         */
        public static class TargetFieldsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredDisjunction.TargetFields
        {
            private static final long serialVersionUID = 1L;
            
            public TargetFieldsImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName TARGETFIELD$0 = 
                new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "targetField");
            
            
            /**
             * Gets array of all "targetField" elements
             */
            public java.lang.String[] getTargetFieldArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(TARGETFIELD$0, targetList);
                    java.lang.String[] result = new java.lang.String[targetList.size()];
                    for (int i = 0, len = targetList.size() ; i < len ; i++)
                        result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                    return result;
                }
            }
            
            /**
             * Gets ith "targetField" element
             */
            public java.lang.String getTargetFieldArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TARGETFIELD$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) array of all "targetField" elements
             */
            public org.apache.xmlbeans.XmlIDREF[] xgetTargetFieldArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(TARGETFIELD$0, targetList);
                    org.apache.xmlbeans.XmlIDREF[] result = new org.apache.xmlbeans.XmlIDREF[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets (as xml) ith "targetField" element
             */
            public org.apache.xmlbeans.XmlIDREF xgetTargetFieldArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlIDREF target = null;
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TARGETFIELD$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return (org.apache.xmlbeans.XmlIDREF)target;
                }
            }
            
            /**
             * Returns number of "targetField" element
             */
            public int sizeOfTargetFieldArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(TARGETFIELD$0);
                }
            }
            
            /**
             * Sets array of all "targetField" element
             */
            public void setTargetFieldArray(java.lang.String[] targetFieldArray)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    arraySetterHelper(targetFieldArray, TARGETFIELD$0);
                }
            }
            
            /**
             * Sets ith "targetField" element
             */
            public void setTargetFieldArray(int i, java.lang.String targetField)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TARGETFIELD$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.setStringValue(targetField);
                }
            }
            
            /**
             * Sets (as xml) array of all "targetField" element
             */
            public void xsetTargetFieldArray(org.apache.xmlbeans.XmlIDREF[]targetFieldArray)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    arraySetterHelper(targetFieldArray, TARGETFIELD$0);
                }
            }
            
            /**
             * Sets (as xml) ith "targetField" element
             */
            public void xsetTargetFieldArray(int i, org.apache.xmlbeans.XmlIDREF targetField)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlIDREF target = null;
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TARGETFIELD$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(targetField);
                }
            }
            
            /**
             * Inserts the value as the ith "targetField" element
             */
            public void insertTargetField(int i, java.lang.String targetField)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = 
                      (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(TARGETFIELD$0, i);
                    target.setStringValue(targetField);
                }
            }
            
            /**
             * Appends the value as the last "targetField" element
             */
            public void addTargetField(java.lang.String targetField)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TARGETFIELD$0);
                    target.setStringValue(targetField);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "targetField" element
             */
            public org.apache.xmlbeans.XmlIDREF insertNewTargetField(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlIDREF target = null;
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().insert_element_user(TARGETFIELD$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "targetField" element
             */
            public org.apache.xmlbeans.XmlIDREF addNewTargetField()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlIDREF target = null;
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_element_user(TARGETFIELD$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "targetField" element
             */
            public void removeTargetField(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(TARGETFIELD$0, i);
                }
            }
        }
    }
    /**
     * An XML requiredConjunction(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public static class RequiredConjunctionImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction
    {
        private static final long serialVersionUID = 1L;
        
        public RequiredConjunctionImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName TRIGGERFIELD$0 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "triggerField");
        private static final javax.xml.namespace.QName TRIGGERVALUE$2 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "triggerValue");
        private static final javax.xml.namespace.QName TARGETFIELDS$4 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "targetFields");
        
        
        /**
         * Gets the "triggerField" element
         */
        public java.lang.String getTriggerField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "triggerField" element
         */
        public org.apache.xmlbeans.XmlIDREF xgetTriggerField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TRIGGERFIELD$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "triggerField" element
         */
        public void setTriggerField(java.lang.String triggerField)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TRIGGERFIELD$0);
                }
                target.setStringValue(triggerField);
            }
        }
        
        /**
         * Sets (as xml) the "triggerField" element
         */
        public void xsetTriggerField(org.apache.xmlbeans.XmlIDREF triggerField)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_element_user(TRIGGERFIELD$0);
                }
                target.set(triggerField);
            }
        }
        
        /**
         * Gets the "triggerValue" element
         */
        public org.apache.xmlbeans.XmlObject getTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlObject target = null;
                target = (org.apache.xmlbeans.XmlObject)get_store().find_element_user(TRIGGERVALUE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * True if has "triggerValue" element
         */
        public boolean isSetTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(TRIGGERVALUE$2) != 0;
            }
        }
        
        /**
         * Sets the "triggerValue" element
         */
        public void setTriggerValue(org.apache.xmlbeans.XmlObject triggerValue)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlObject target = null;
                target = (org.apache.xmlbeans.XmlObject)get_store().find_element_user(TRIGGERVALUE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlObject)get_store().add_element_user(TRIGGERVALUE$2);
                }
                target.set(triggerValue);
            }
        }
        
        /**
         * Appends and returns a new empty "triggerValue" element
         */
        public org.apache.xmlbeans.XmlObject addNewTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlObject target = null;
                target = (org.apache.xmlbeans.XmlObject)get_store().add_element_user(TRIGGERVALUE$2);
                return target;
            }
        }
        
        /**
         * Unsets the "triggerValue" element
         */
        public void unsetTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(TRIGGERVALUE$2, 0);
            }
        }
        
        /**
         * Gets the "targetFields" element
         */
        public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields getTargetFields()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields)get_store().find_element_user(TARGETFIELDS$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target;
            }
        }
        
        /**
         * Sets the "targetFields" element
         */
        public void setTargetFields(org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields targetFields)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields)get_store().find_element_user(TARGETFIELDS$4, 0);
                if (target == null)
                {
                    target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields)get_store().add_element_user(TARGETFIELDS$4);
                }
                target.set(targetFields);
            }
        }
        
        /**
         * Appends and returns a new empty "targetFields" element
         */
        public org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields addNewTargetFields()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields target = null;
                target = (org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields)get_store().add_element_user(TARGETFIELDS$4);
                return target;
            }
        }
        /**
         * An XML targetFields(@http://www.xvergabe.org/xsd/forms/components/2_0).
         *
         * This is a complex type.
         */
        public static class TargetFieldsImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormMetaData.RequiredConjunction.TargetFields
        {
            private static final long serialVersionUID = 1L;
            
            public TargetFieldsImpl(org.apache.xmlbeans.SchemaType sType)
            {
                super(sType);
            }
            
            private static final javax.xml.namespace.QName TARGETFIELD$0 = 
                new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "targetField");
            
            
            /**
             * Gets array of all "targetField" elements
             */
            public java.lang.String[] getTargetFieldArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(TARGETFIELD$0, targetList);
                    java.lang.String[] result = new java.lang.String[targetList.size()];
                    for (int i = 0, len = targetList.size() ; i < len ; i++)
                        result[i] = ((org.apache.xmlbeans.SimpleValue)targetList.get(i)).getStringValue();
                    return result;
                }
            }
            
            /**
             * Gets ith "targetField" element
             */
            public java.lang.String getTargetFieldArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TARGETFIELD$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return target.getStringValue();
                }
            }
            
            /**
             * Gets (as xml) array of all "targetField" elements
             */
            public org.apache.xmlbeans.XmlIDREF[] xgetTargetFieldArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    java.util.List targetList = new java.util.ArrayList();
                    get_store().find_all_element_users(TARGETFIELD$0, targetList);
                    org.apache.xmlbeans.XmlIDREF[] result = new org.apache.xmlbeans.XmlIDREF[targetList.size()];
                    targetList.toArray(result);
                    return result;
                }
            }
            
            /**
             * Gets (as xml) ith "targetField" element
             */
            public org.apache.xmlbeans.XmlIDREF xgetTargetFieldArray(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlIDREF target = null;
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TARGETFIELD$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    return (org.apache.xmlbeans.XmlIDREF)target;
                }
            }
            
            /**
             * Returns number of "targetField" element
             */
            public int sizeOfTargetFieldArray()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    return get_store().count_elements(TARGETFIELD$0);
                }
            }
            
            /**
             * Sets array of all "targetField" element
             */
            public void setTargetFieldArray(java.lang.String[] targetFieldArray)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    arraySetterHelper(targetFieldArray, TARGETFIELD$0);
                }
            }
            
            /**
             * Sets ith "targetField" element
             */
            public void setTargetFieldArray(int i, java.lang.String targetField)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TARGETFIELD$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.setStringValue(targetField);
                }
            }
            
            /**
             * Sets (as xml) array of all "targetField" element
             */
            public void xsetTargetFieldArray(org.apache.xmlbeans.XmlIDREF[]targetFieldArray)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    arraySetterHelper(targetFieldArray, TARGETFIELD$0);
                }
            }
            
            /**
             * Sets (as xml) ith "targetField" element
             */
            public void xsetTargetFieldArray(int i, org.apache.xmlbeans.XmlIDREF targetField)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlIDREF target = null;
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TARGETFIELD$0, i);
                    if (target == null)
                    {
                      throw new IndexOutOfBoundsException();
                    }
                    target.set(targetField);
                }
            }
            
            /**
             * Inserts the value as the ith "targetField" element
             */
            public void insertTargetField(int i, java.lang.String targetField)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = 
                      (org.apache.xmlbeans.SimpleValue)get_store().insert_element_user(TARGETFIELD$0, i);
                    target.setStringValue(targetField);
                }
            }
            
            /**
             * Appends the value as the last "targetField" element
             */
            public void addTargetField(java.lang.String targetField)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.SimpleValue target = null;
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TARGETFIELD$0);
                    target.setStringValue(targetField);
                }
            }
            
            /**
             * Inserts and returns a new empty value (as xml) as the ith "targetField" element
             */
            public org.apache.xmlbeans.XmlIDREF insertNewTargetField(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlIDREF target = null;
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().insert_element_user(TARGETFIELD$0, i);
                    return target;
                }
            }
            
            /**
             * Appends and returns a new empty value (as xml) as the last "targetField" element
             */
            public org.apache.xmlbeans.XmlIDREF addNewTargetField()
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    org.apache.xmlbeans.XmlIDREF target = null;
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_element_user(TARGETFIELD$0);
                    return target;
                }
            }
            
            /**
             * Removes the ith "targetField" element
             */
            public void removeTargetField(int i)
            {
                synchronized (monitor())
                {
                    check_orphaned();
                    get_store().remove_element(TARGETFIELD$0, i);
                }
            }
        }
    }
    /**
     * An XML activationToggle(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public static class ActivationToggleImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormMetaData.ActivationToggle
    {
        private static final long serialVersionUID = 1L;
        
        public ActivationToggleImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName TRIGGERFIELD$0 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "triggerField");
        private static final javax.xml.namespace.QName TRIGGERVALUE$2 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "triggerValue");
        private static final javax.xml.namespace.QName TARGETFIELD$4 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "targetField");
        
        
        /**
         * Gets the "triggerField" element
         */
        public java.lang.String getTriggerField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "triggerField" element
         */
        public org.apache.xmlbeans.XmlIDREF xgetTriggerField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TRIGGERFIELD$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "triggerField" element
         */
        public void setTriggerField(java.lang.String triggerField)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TRIGGERFIELD$0);
                }
                target.setStringValue(triggerField);
            }
        }
        
        /**
         * Sets (as xml) the "triggerField" element
         */
        public void xsetTriggerField(org.apache.xmlbeans.XmlIDREF triggerField)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TRIGGERFIELD$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_element_user(TRIGGERFIELD$0);
                }
                target.set(triggerField);
            }
        }
        
        /**
         * Gets the "triggerValue" element
         */
        public java.lang.String getTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRIGGERVALUE$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "triggerValue" element
         */
        public org.apache.xmlbeans.XmlString xgetTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TRIGGERVALUE$2, 0);
                return target;
            }
        }
        
        /**
         * True if has "triggerValue" element
         */
        public boolean isSetTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                return get_store().count_elements(TRIGGERVALUE$2) != 0;
            }
        }
        
        /**
         * Sets the "triggerValue" element
         */
        public void setTriggerValue(java.lang.String triggerValue)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TRIGGERVALUE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TRIGGERVALUE$2);
                }
                target.setStringValue(triggerValue);
            }
        }
        
        /**
         * Sets (as xml) the "triggerValue" element
         */
        public void xsetTriggerValue(org.apache.xmlbeans.XmlString triggerValue)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlString target = null;
                target = (org.apache.xmlbeans.XmlString)get_store().find_element_user(TRIGGERVALUE$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlString)get_store().add_element_user(TRIGGERVALUE$2);
                }
                target.set(triggerValue);
            }
        }
        
        /**
         * Unsets the "triggerValue" element
         */
        public void unsetTriggerValue()
        {
            synchronized (monitor())
            {
                check_orphaned();
                get_store().remove_element(TRIGGERVALUE$2, 0);
            }
        }
        
        /**
         * Gets the "targetField" element
         */
        public java.lang.String getTargetField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TARGETFIELD$4, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "targetField" element
         */
        public org.apache.xmlbeans.XmlIDREF xgetTargetField()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TARGETFIELD$4, 0);
                return target;
            }
        }
        
        /**
         * Sets the "targetField" element
         */
        public void setTargetField(java.lang.String targetField)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TARGETFIELD$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TARGETFIELD$4);
                }
                target.setStringValue(targetField);
            }
        }
        
        /**
         * Sets (as xml) the "targetField" element
         */
        public void xsetTargetField(org.apache.xmlbeans.XmlIDREF targetField)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TARGETFIELD$4, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_element_user(TARGETFIELD$4);
                }
                target.set(targetField);
            }
        }
    }
    /**
     * An XML valueTransfer(@http://www.xvergabe.org/xsd/forms/components/2_0).
     *
     * This is a complex type.
     */
    public static class ValueTransferImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.FormMetaData.ValueTransfer
    {
        private static final long serialVersionUID = 1L;
        
        public ValueTransferImpl(org.apache.xmlbeans.SchemaType sType)
        {
            super(sType);
        }
        
        private static final javax.xml.namespace.QName SOURCEFIELDID$0 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "sourceFieldId");
        private static final javax.xml.namespace.QName TARGETFIELDID$2 = 
            new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "targetFieldId");
        
        
        /**
         * Gets the "sourceFieldId" element
         */
        public java.lang.String getSourceFieldId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOURCEFIELDID$0, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "sourceFieldId" element
         */
        public org.apache.xmlbeans.XmlIDREF xgetSourceFieldId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(SOURCEFIELDID$0, 0);
                return target;
            }
        }
        
        /**
         * Sets the "sourceFieldId" element
         */
        public void setSourceFieldId(java.lang.String sourceFieldId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(SOURCEFIELDID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(SOURCEFIELDID$0);
                }
                target.setStringValue(sourceFieldId);
            }
        }
        
        /**
         * Sets (as xml) the "sourceFieldId" element
         */
        public void xsetSourceFieldId(org.apache.xmlbeans.XmlIDREF sourceFieldId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(SOURCEFIELDID$0, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_element_user(SOURCEFIELDID$0);
                }
                target.set(sourceFieldId);
            }
        }
        
        /**
         * Gets the "targetFieldId" element
         */
        public java.lang.String getTargetFieldId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TARGETFIELDID$2, 0);
                if (target == null)
                {
                    return null;
                }
                return target.getStringValue();
            }
        }
        
        /**
         * Gets (as xml) the "targetFieldId" element
         */
        public org.apache.xmlbeans.XmlIDREF xgetTargetFieldId()
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TARGETFIELDID$2, 0);
                return target;
            }
        }
        
        /**
         * Sets the "targetFieldId" element
         */
        public void setTargetFieldId(java.lang.String targetFieldId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.SimpleValue target = null;
                target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(TARGETFIELDID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(TARGETFIELDID$2);
                }
                target.setStringValue(targetFieldId);
            }
        }
        
        /**
         * Sets (as xml) the "targetFieldId" element
         */
        public void xsetTargetFieldId(org.apache.xmlbeans.XmlIDREF targetFieldId)
        {
            synchronized (monitor())
            {
                check_orphaned();
                org.apache.xmlbeans.XmlIDREF target = null;
                target = (org.apache.xmlbeans.XmlIDREF)get_store().find_element_user(TARGETFIELDID$2, 0);
                if (target == null)
                {
                    target = (org.apache.xmlbeans.XmlIDREF)get_store().add_element_user(TARGETFIELDID$2);
                }
                target.set(targetFieldId);
            }
        }
    }
}
