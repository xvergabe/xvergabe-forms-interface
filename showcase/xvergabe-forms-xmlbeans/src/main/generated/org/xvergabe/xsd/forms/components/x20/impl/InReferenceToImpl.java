/*
 * XML Type:  InReferenceTo
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.InReferenceTo
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML InReferenceTo(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class InReferenceToImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.InReferenceTo
{
    private static final long serialVersionUID = 1L;
    
    public InReferenceToImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DATEOFREFERENCE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "dateOfReference");
    private static final javax.xml.namespace.QName REFERENCENUMBER$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "referenceNumber");
    private static final javax.xml.namespace.QName ACTIVITYNUMBER$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "activityNumber");
    private static final javax.xml.namespace.QName ACTIVITYNAME$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "activityName");
    private static final javax.xml.namespace.QName TENDERINGNUMBER$8 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "tenderingNumber");
    private static final javax.xml.namespace.QName SERVICEDESCRIPTION$10 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "serviceDescription");
    
    
    /**
     * Gets the "dateOfReference" element
     */
    public org.xvergabe.xsd.forms.components.x20.Date getDateOfReference()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().find_element_user(DATEOFREFERENCE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "dateOfReference" element
     */
    public boolean isSetDateOfReference()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DATEOFREFERENCE$0) != 0;
        }
    }
    
    /**
     * Sets the "dateOfReference" element
     */
    public void setDateOfReference(org.xvergabe.xsd.forms.components.x20.Date dateOfReference)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().find_element_user(DATEOFREFERENCE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().add_element_user(DATEOFREFERENCE$0);
            }
            target.set(dateOfReference);
        }
    }
    
    /**
     * Appends and returns a new empty "dateOfReference" element
     */
    public org.xvergabe.xsd.forms.components.x20.Date addNewDateOfReference()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().add_element_user(DATEOFREFERENCE$0);
            return target;
        }
    }
    
    /**
     * Unsets the "dateOfReference" element
     */
    public void unsetDateOfReference()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DATEOFREFERENCE$0, 0);
        }
    }
    
    /**
     * Gets the "referenceNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(REFERENCENUMBER$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "referenceNumber" element
     */
    public boolean isSetReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REFERENCENUMBER$2) != 0;
        }
    }
    
    /**
     * Sets the "referenceNumber" element
     */
    public void setReferenceNumber(org.xvergabe.xsd.forms.components.x20.String referenceNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(REFERENCENUMBER$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(REFERENCENUMBER$2);
            }
            target.set(referenceNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "referenceNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(REFERENCENUMBER$2);
            return target;
        }
    }
    
    /**
     * Unsets the "referenceNumber" element
     */
    public void unsetReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REFERENCENUMBER$2, 0);
        }
    }
    
    /**
     * Gets the "activityNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getActivityNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ACTIVITYNUMBER$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "activityNumber" element
     */
    public boolean isSetActivityNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACTIVITYNUMBER$4) != 0;
        }
    }
    
    /**
     * Sets the "activityNumber" element
     */
    public void setActivityNumber(org.xvergabe.xsd.forms.components.x20.String activityNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ACTIVITYNUMBER$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ACTIVITYNUMBER$4);
            }
            target.set(activityNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "activityNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewActivityNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ACTIVITYNUMBER$4);
            return target;
        }
    }
    
    /**
     * Unsets the "activityNumber" element
     */
    public void unsetActivityNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACTIVITYNUMBER$4, 0);
        }
    }
    
    /**
     * Gets the "activityName" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getActivityName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ACTIVITYNAME$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "activityName" element
     */
    public boolean isSetActivityName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACTIVITYNAME$6) != 0;
        }
    }
    
    /**
     * Sets the "activityName" element
     */
    public void setActivityName(org.xvergabe.xsd.forms.components.x20.String activityName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ACTIVITYNAME$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ACTIVITYNAME$6);
            }
            target.set(activityName);
        }
    }
    
    /**
     * Appends and returns a new empty "activityName" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewActivityName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ACTIVITYNAME$6);
            return target;
        }
    }
    
    /**
     * Unsets the "activityName" element
     */
    public void unsetActivityName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACTIVITYNAME$6, 0);
        }
    }
    
    /**
     * Gets the "tenderingNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getTenderingNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(TENDERINGNUMBER$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "tenderingNumber" element
     */
    public boolean isSetTenderingNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(TENDERINGNUMBER$8) != 0;
        }
    }
    
    /**
     * Sets the "tenderingNumber" element
     */
    public void setTenderingNumber(org.xvergabe.xsd.forms.components.x20.String tenderingNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(TENDERINGNUMBER$8, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(TENDERINGNUMBER$8);
            }
            target.set(tenderingNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "tenderingNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewTenderingNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(TENDERINGNUMBER$8);
            return target;
        }
    }
    
    /**
     * Unsets the "tenderingNumber" element
     */
    public void unsetTenderingNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(TENDERINGNUMBER$8, 0);
        }
    }
    
    /**
     * Gets the "serviceDescription" element
     */
    public org.xvergabe.xsd.forms.components.x20.Description getServiceDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Description target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Description)get_store().find_element_user(SERVICEDESCRIPTION$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "serviceDescription" element
     */
    public boolean isSetServiceDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SERVICEDESCRIPTION$10) != 0;
        }
    }
    
    /**
     * Sets the "serviceDescription" element
     */
    public void setServiceDescription(org.xvergabe.xsd.forms.components.x20.Description serviceDescription)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Description target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Description)get_store().find_element_user(SERVICEDESCRIPTION$10, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Description)get_store().add_element_user(SERVICEDESCRIPTION$10);
            }
            target.set(serviceDescription);
        }
    }
    
    /**
     * Appends and returns a new empty "serviceDescription" element
     */
    public org.xvergabe.xsd.forms.components.x20.Description addNewServiceDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Description target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Description)get_store().add_element_user(SERVICEDESCRIPTION$10);
            return target;
        }
    }
    
    /**
     * Unsets the "serviceDescription" element
     */
    public void unsetServiceDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SERVICEDESCRIPTION$10, 0);
        }
    }
}
