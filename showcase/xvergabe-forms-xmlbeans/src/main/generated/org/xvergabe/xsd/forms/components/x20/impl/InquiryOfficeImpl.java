/*
 * XML Type:  InquiryOffice
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.InquiryOffice
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML InquiryOffice(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class InquiryOfficeImpl extends org.xvergabe.xsd.forms.components.x20.impl.OrganizationImpl implements org.xvergabe.xsd.forms.components.x20.InquiryOffice
{
    private static final long serialVersionUID = 1L;
    
    public InquiryOfficeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
