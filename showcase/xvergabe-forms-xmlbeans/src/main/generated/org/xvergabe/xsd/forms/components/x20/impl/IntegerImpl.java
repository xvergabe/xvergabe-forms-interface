/*
 * XML Type:  Integer
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Integer
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Integer(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class IntegerImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl implements org.xvergabe.xsd.forms.components.x20.Integer
{
    private static final long serialVersionUID = 1L;
    
    public IntegerImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VALUE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "value");
    private static final javax.xml.namespace.QName INTEGERMETADATA$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "integerMetaData");
    
    
    /**
     * Gets the "value" element
     */
    public int getValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "value" element
     */
    public org.apache.xmlbeans.XmlInt xgetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(VALUE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "value" element
     */
    public boolean isSetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VALUE$0) != 0;
        }
    }
    
    /**
     * Sets the "value" element
     */
    public void setValue(int value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALUE$0);
            }
            target.setIntValue(value);
        }
    }
    
    /**
     * Sets (as xml) the "value" element
     */
    public void xsetValue(org.apache.xmlbeans.XmlInt value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(VALUE$0);
            }
            target.set(value);
        }
    }
    
    /**
     * Unsets the "value" element
     */
    public void unsetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VALUE$0, 0);
        }
    }
    
    /**
     * Gets the "integerMetaData" element
     */
    public org.xvergabe.xsd.forms.components.x20.IntegerMetaData getIntegerMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.IntegerMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.IntegerMetaData)get_store().find_element_user(INTEGERMETADATA$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "integerMetaData" element
     */
    public boolean isSetIntegerMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(INTEGERMETADATA$2) != 0;
        }
    }
    
    /**
     * Sets the "integerMetaData" element
     */
    public void setIntegerMetaData(org.xvergabe.xsd.forms.components.x20.IntegerMetaData integerMetaData)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.IntegerMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.IntegerMetaData)get_store().find_element_user(INTEGERMETADATA$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.IntegerMetaData)get_store().add_element_user(INTEGERMETADATA$2);
            }
            target.set(integerMetaData);
        }
    }
    
    /**
     * Appends and returns a new empty "integerMetaData" element
     */
    public org.xvergabe.xsd.forms.components.x20.IntegerMetaData addNewIntegerMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.IntegerMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.IntegerMetaData)get_store().add_element_user(INTEGERMETADATA$2);
            return target;
        }
    }
    
    /**
     * Unsets the "integerMetaData" element
     */
    public void unsetIntegerMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(INTEGERMETADATA$2, 0);
        }
    }
}
