/*
 * XML Type:  IntegerMetaData
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.IntegerMetaData
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML IntegerMetaData(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class IntegerMetaDataImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.xvergabe.xsd.forms.components.x20.IntegerMetaData
{
    private static final long serialVersionUID = 1L;
    
    public IntegerMetaDataImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName MIN$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "min");
    private static final javax.xml.namespace.QName MAX$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "max");
    
    
    /**
     * Gets the "min" element
     */
    public int getMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIN$0, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "min" element
     */
    public org.apache.xmlbeans.XmlInt xgetMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(MIN$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "min" element
     */
    public boolean isSetMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MIN$0) != 0;
        }
    }
    
    /**
     * Sets the "min" element
     */
    public void setMin(int min)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MIN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MIN$0);
            }
            target.setIntValue(min);
        }
    }
    
    /**
     * Sets (as xml) the "min" element
     */
    public void xsetMin(org.apache.xmlbeans.XmlInt min)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(MIN$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(MIN$0);
            }
            target.set(min);
        }
    }
    
    /**
     * Unsets the "min" element
     */
    public void unsetMin()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MIN$0, 0);
        }
    }
    
    /**
     * Gets the "max" element
     */
    public int getMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAX$2, 0);
            if (target == null)
            {
                return 0;
            }
            return target.getIntValue();
        }
    }
    
    /**
     * Gets (as xml) the "max" element
     */
    public org.apache.xmlbeans.XmlInt xgetMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(MAX$2, 0);
            return target;
        }
    }
    
    /**
     * True if has "max" element
     */
    public boolean isSetMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MAX$2) != 0;
        }
    }
    
    /**
     * Sets the "max" element
     */
    public void setMax(int max)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(MAX$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(MAX$2);
            }
            target.setIntValue(max);
        }
    }
    
    /**
     * Sets (as xml) the "max" element
     */
    public void xsetMax(org.apache.xmlbeans.XmlInt max)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlInt target = null;
            target = (org.apache.xmlbeans.XmlInt)get_store().find_element_user(MAX$2, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlInt)get_store().add_element_user(MAX$2);
            }
            target.set(max);
        }
    }
    
    /**
     * Unsets the "max" element
     */
    public void unsetMax()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MAX$2, 0);
        }
    }
}
