/*
 * XML Type:  Lot
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Lot
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Lot(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class LotImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Lot
{
    private static final long serialVersionUID = 1L;
    
    public LotImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LOTNUMBER$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "lotNumber");
    private static final javax.xml.namespace.QName PRICE$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "price");
    private static final javax.xml.namespace.QName ALTERNATIVEOFFERS$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "alternativeOffers");
    private static final javax.xml.namespace.QName DISCOUNTS$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "discounts");
    
    
    /**
     * Gets the "lotNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer getLotNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(LOTNUMBER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "lotNumber" element
     */
    public void setLotNumber(org.xvergabe.xsd.forms.components.x20.Integer lotNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(LOTNUMBER$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(LOTNUMBER$0);
            }
            target.set(lotNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "lotNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer addNewLotNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(LOTNUMBER$0);
            return target;
        }
    }
    
    /**
     * Gets the "price" element
     */
    public org.xvergabe.xsd.forms.components.x20.Price getPrice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Price target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Price)get_store().find_element_user(PRICE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "price" element
     */
    public void setPrice(org.xvergabe.xsd.forms.components.x20.Price price)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Price target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Price)get_store().find_element_user(PRICE$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Price)get_store().add_element_user(PRICE$2);
            }
            target.set(price);
        }
    }
    
    /**
     * Appends and returns a new empty "price" element
     */
    public org.xvergabe.xsd.forms.components.x20.Price addNewPrice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Price target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Price)get_store().add_element_user(PRICE$2);
            return target;
        }
    }
    
    /**
     * Gets the "alternativeOffers" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer getAlternativeOffers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(ALTERNATIVEOFFERS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "alternativeOffers" element
     */
    public void setAlternativeOffers(org.xvergabe.xsd.forms.components.x20.Integer alternativeOffers)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(ALTERNATIVEOFFERS$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(ALTERNATIVEOFFERS$4);
            }
            target.set(alternativeOffers);
        }
    }
    
    /**
     * Appends and returns a new empty "alternativeOffers" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer addNewAlternativeOffers()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(ALTERNATIVEOFFERS$4);
            return target;
        }
    }
    
    /**
     * Gets the "discounts" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer getDiscounts()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(DISCOUNTS$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "discounts" element
     */
    public boolean isSetDiscounts()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DISCOUNTS$6) != 0;
        }
    }
    
    /**
     * Sets the "discounts" element
     */
    public void setDiscounts(org.xvergabe.xsd.forms.components.x20.Integer discounts)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().find_element_user(DISCOUNTS$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(DISCOUNTS$6);
            }
            target.set(discounts);
        }
    }
    
    /**
     * Appends and returns a new empty "discounts" element
     */
    public org.xvergabe.xsd.forms.components.x20.Integer addNewDiscounts()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Integer target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Integer)get_store().add_element_user(DISCOUNTS$6);
            return target;
        }
    }
    
    /**
     * Unsets the "discounts" element
     */
    public void unsetDiscounts()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DISCOUNTS$6, 0);
        }
    }
}
