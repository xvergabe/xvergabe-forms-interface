/*
 * XML Type:  Lots
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Lots
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Lots(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class LotsImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Lots
{
    private static final long serialVersionUID = 1L;
    
    public LotsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LOT$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "lot");
    
    
    /**
     * Gets array of all "lot" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Lot[] getLotArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(LOT$0, targetList);
            org.xvergabe.xsd.forms.components.x20.Lot[] result = new org.xvergabe.xsd.forms.components.x20.Lot[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "lot" element
     */
    public org.xvergabe.xsd.forms.components.x20.Lot getLotArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Lot target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Lot)get_store().find_element_user(LOT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "lot" element
     */
    public int sizeOfLotArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LOT$0);
        }
    }
    
    /**
     * Sets array of all "lot" element
     */
    public void setLotArray(org.xvergabe.xsd.forms.components.x20.Lot[] lotArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(lotArray, LOT$0);
        }
    }
    
    /**
     * Sets ith "lot" element
     */
    public void setLotArray(int i, org.xvergabe.xsd.forms.components.x20.Lot lot)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Lot target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Lot)get_store().find_element_user(LOT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(lot);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "lot" element
     */
    public org.xvergabe.xsd.forms.components.x20.Lot insertNewLot(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Lot target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Lot)get_store().insert_element_user(LOT$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "lot" element
     */
    public org.xvergabe.xsd.forms.components.x20.Lot addNewLot()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Lot target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Lot)get_store().add_element_user(LOT$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "lot" element
     */
    public void removeLot(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LOT$0, i);
        }
    }
}
