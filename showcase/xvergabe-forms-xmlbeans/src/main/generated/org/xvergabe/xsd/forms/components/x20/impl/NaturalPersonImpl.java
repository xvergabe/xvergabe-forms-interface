/*
 * XML Type:  NaturalPerson
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.NaturalPerson
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML NaturalPerson(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class NaturalPersonImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.NaturalPerson
{
    private static final long serialVersionUID = 1L;
    
    public NaturalPersonImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName FAMILYNAME$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "familyName");
    private static final javax.xml.namespace.QName FIRSTNAME$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "firstName");
    private static final javax.xml.namespace.QName SALUTATION$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "salutation");
    private static final javax.xml.namespace.QName ACADEMICTITLE$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "academicTitle");
    private static final javax.xml.namespace.QName ORGANIZATIONALUNIT$8 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "organizationalUnit");
    private static final javax.xml.namespace.QName OFFICEADDRESS$10 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "officeAddress");
    private static final javax.xml.namespace.QName CONTACTDATA$12 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "contactData");
    
    
    /**
     * Gets the "familyName" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getFamilyName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FAMILYNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "familyName" element
     */
    public boolean isSetFamilyName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FAMILYNAME$0) != 0;
        }
    }
    
    /**
     * Sets the "familyName" element
     */
    public void setFamilyName(org.xvergabe.xsd.forms.components.x20.String familyName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FAMILYNAME$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FAMILYNAME$0);
            }
            target.set(familyName);
        }
    }
    
    /**
     * Appends and returns a new empty "familyName" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewFamilyName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FAMILYNAME$0);
            return target;
        }
    }
    
    /**
     * Unsets the "familyName" element
     */
    public void unsetFamilyName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FAMILYNAME$0, 0);
        }
    }
    
    /**
     * Gets the "firstName" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FIRSTNAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "firstName" element
     */
    public boolean isSetFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FIRSTNAME$2) != 0;
        }
    }
    
    /**
     * Sets the "firstName" element
     */
    public void setFirstName(org.xvergabe.xsd.forms.components.x20.String firstName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(FIRSTNAME$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FIRSTNAME$2);
            }
            target.set(firstName);
        }
    }
    
    /**
     * Appends and returns a new empty "firstName" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(FIRSTNAME$2);
            return target;
        }
    }
    
    /**
     * Unsets the "firstName" element
     */
    public void unsetFirstName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FIRSTNAME$2, 0);
        }
    }
    
    /**
     * Gets the "salutation" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getSalutation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SALUTATION$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "salutation" element
     */
    public boolean isSetSalutation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SALUTATION$4) != 0;
        }
    }
    
    /**
     * Sets the "salutation" element
     */
    public void setSalutation(org.xvergabe.xsd.forms.components.x20.String salutation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SALUTATION$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SALUTATION$4);
            }
            target.set(salutation);
        }
    }
    
    /**
     * Appends and returns a new empty "salutation" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewSalutation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SALUTATION$4);
            return target;
        }
    }
    
    /**
     * Unsets the "salutation" element
     */
    public void unsetSalutation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SALUTATION$4, 0);
        }
    }
    
    /**
     * Gets the "academicTitle" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getAcademicTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ACADEMICTITLE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "academicTitle" element
     */
    public boolean isSetAcademicTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ACADEMICTITLE$6) != 0;
        }
    }
    
    /**
     * Sets the "academicTitle" element
     */
    public void setAcademicTitle(org.xvergabe.xsd.forms.components.x20.String academicTitle)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ACADEMICTITLE$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ACADEMICTITLE$6);
            }
            target.set(academicTitle);
        }
    }
    
    /**
     * Appends and returns a new empty "academicTitle" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewAcademicTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ACADEMICTITLE$6);
            return target;
        }
    }
    
    /**
     * Unsets the "academicTitle" element
     */
    public void unsetAcademicTitle()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ACADEMICTITLE$6, 0);
        }
    }
    
    /**
     * Gets the "organizationalUnit" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getOrganizationalUnit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ORGANIZATIONALUNIT$8, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "organizationalUnit" element
     */
    public boolean isSetOrganizationalUnit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ORGANIZATIONALUNIT$8) != 0;
        }
    }
    
    /**
     * Sets the "organizationalUnit" element
     */
    public void setOrganizationalUnit(org.xvergabe.xsd.forms.components.x20.String organizationalUnit)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ORGANIZATIONALUNIT$8, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ORGANIZATIONALUNIT$8);
            }
            target.set(organizationalUnit);
        }
    }
    
    /**
     * Appends and returns a new empty "organizationalUnit" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewOrganizationalUnit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ORGANIZATIONALUNIT$8);
            return target;
        }
    }
    
    /**
     * Unsets the "organizationalUnit" element
     */
    public void unsetOrganizationalUnit()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ORGANIZATIONALUNIT$8, 0);
        }
    }
    
    /**
     * Gets the "officeAddress" element
     */
    public org.xvergabe.xsd.forms.components.x20.Address getOfficeAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().find_element_user(OFFICEADDRESS$10, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "officeAddress" element
     */
    public boolean isSetOfficeAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OFFICEADDRESS$10) != 0;
        }
    }
    
    /**
     * Sets the "officeAddress" element
     */
    public void setOfficeAddress(org.xvergabe.xsd.forms.components.x20.Address officeAddress)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().find_element_user(OFFICEADDRESS$10, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().add_element_user(OFFICEADDRESS$10);
            }
            target.set(officeAddress);
        }
    }
    
    /**
     * Appends and returns a new empty "officeAddress" element
     */
    public org.xvergabe.xsd.forms.components.x20.Address addNewOfficeAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().add_element_user(OFFICEADDRESS$10);
            return target;
        }
    }
    
    /**
     * Unsets the "officeAddress" element
     */
    public void unsetOfficeAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OFFICEADDRESS$10, 0);
        }
    }
    
    /**
     * Gets array of all "contactData" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Contact[] getContactDataArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CONTACTDATA$12, targetList);
            org.xvergabe.xsd.forms.components.x20.Contact[] result = new org.xvergabe.xsd.forms.components.x20.Contact[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "contactData" element
     */
    public org.xvergabe.xsd.forms.components.x20.Contact getContactDataArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().find_element_user(CONTACTDATA$12, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "contactData" element
     */
    public int sizeOfContactDataArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CONTACTDATA$12);
        }
    }
    
    /**
     * Sets array of all "contactData" element
     */
    public void setContactDataArray(org.xvergabe.xsd.forms.components.x20.Contact[] contactDataArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(contactDataArray, CONTACTDATA$12);
        }
    }
    
    /**
     * Sets ith "contactData" element
     */
    public void setContactDataArray(int i, org.xvergabe.xsd.forms.components.x20.Contact contactData)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().find_element_user(CONTACTDATA$12, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(contactData);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "contactData" element
     */
    public org.xvergabe.xsd.forms.components.x20.Contact insertNewContactData(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().insert_element_user(CONTACTDATA$12, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "contactData" element
     */
    public org.xvergabe.xsd.forms.components.x20.Contact addNewContactData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().add_element_user(CONTACTDATA$12);
            return target;
        }
    }
    
    /**
     * Removes the ith "contactData" element
     */
    public void removeContactData(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CONTACTDATA$12, i);
        }
    }
}
