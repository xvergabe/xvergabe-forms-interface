/*
 * XML Type:  Note
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Note
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Note(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class NoteImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl implements org.xvergabe.xsd.forms.components.x20.Note
{
    private static final long serialVersionUID = 1L;
    
    public NoteImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VALUE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "value");
    
    
    /**
     * Gets the "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "value" element
     */
    public void setValue(org.xvergabe.xsd.forms.components.x20.String value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(VALUE$0);
            }
            target.set(value);
        }
    }
    
    /**
     * Appends and returns a new empty "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(VALUE$0);
            return target;
        }
    }
}
