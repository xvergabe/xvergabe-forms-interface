/*
 * XML Type:  OrSection
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.OrSection
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML OrSection(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class OrSectionImpl extends org.xvergabe.xsd.forms.components.x20.impl.SectionImpl implements org.xvergabe.xsd.forms.components.x20.OrSection
{
    private static final long serialVersionUID = 1L;
    
    public OrSectionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
