/*
 * XML Type:  Organization
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Organization
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Organization(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class OrganizationImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Organization
{
    private static final long serialVersionUID = 1L;
    
    public OrganizationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName NAME$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "name");
    private static final javax.xml.namespace.QName DESCRIPTION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "description");
    private static final javax.xml.namespace.QName ADDRESS$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "address");
    private static final javax.xml.namespace.QName CONTACTDATA$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "contactData");
    private static final javax.xml.namespace.QName CONTACTPERSON$8 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "contactPerson");
    
    
    /**
     * Gets the "name" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "name" element
     */
    public boolean isSetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(NAME$0) != 0;
        }
    }
    
    /**
     * Sets the "name" element
     */
    public void setName(org.xvergabe.xsd.forms.components.x20.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(NAME$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(NAME$0);
            }
            target.set(name);
        }
    }
    
    /**
     * Appends and returns a new empty "name" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(NAME$0);
            return target;
        }
    }
    
    /**
     * Unsets the "name" element
     */
    public void unsetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(NAME$0, 0);
        }
    }
    
    /**
     * Gets the "description" element
     */
    public org.xvergabe.xsd.forms.components.x20.Description getDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Description target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Description)get_store().find_element_user(DESCRIPTION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "description" element
     */
    public boolean isSetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DESCRIPTION$2) != 0;
        }
    }
    
    /**
     * Sets the "description" element
     */
    public void setDescription(org.xvergabe.xsd.forms.components.x20.Description description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Description target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Description)get_store().find_element_user(DESCRIPTION$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Description)get_store().add_element_user(DESCRIPTION$2);
            }
            target.set(description);
        }
    }
    
    /**
     * Appends and returns a new empty "description" element
     */
    public org.xvergabe.xsd.forms.components.x20.Description addNewDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Description target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Description)get_store().add_element_user(DESCRIPTION$2);
            return target;
        }
    }
    
    /**
     * Unsets the "description" element
     */
    public void unsetDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DESCRIPTION$2, 0);
        }
    }
    
    /**
     * Gets the "address" element
     */
    public org.xvergabe.xsd.forms.components.x20.Address getAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().find_element_user(ADDRESS$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "address" element
     */
    public boolean isSetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDRESS$4) != 0;
        }
    }
    
    /**
     * Sets the "address" element
     */
    public void setAddress(org.xvergabe.xsd.forms.components.x20.Address address)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().find_element_user(ADDRESS$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().add_element_user(ADDRESS$4);
            }
            target.set(address);
        }
    }
    
    /**
     * Appends and returns a new empty "address" element
     */
    public org.xvergabe.xsd.forms.components.x20.Address addNewAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Address target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Address)get_store().add_element_user(ADDRESS$4);
            return target;
        }
    }
    
    /**
     * Unsets the "address" element
     */
    public void unsetAddress()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDRESS$4, 0);
        }
    }
    
    /**
     * Gets array of all "contactData" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Contact[] getContactDataArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CONTACTDATA$6, targetList);
            org.xvergabe.xsd.forms.components.x20.Contact[] result = new org.xvergabe.xsd.forms.components.x20.Contact[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "contactData" element
     */
    public org.xvergabe.xsd.forms.components.x20.Contact getContactDataArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().find_element_user(CONTACTDATA$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "contactData" element
     */
    public int sizeOfContactDataArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CONTACTDATA$6);
        }
    }
    
    /**
     * Sets array of all "contactData" element
     */
    public void setContactDataArray(org.xvergabe.xsd.forms.components.x20.Contact[] contactDataArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(contactDataArray, CONTACTDATA$6);
        }
    }
    
    /**
     * Sets ith "contactData" element
     */
    public void setContactDataArray(int i, org.xvergabe.xsd.forms.components.x20.Contact contactData)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().find_element_user(CONTACTDATA$6, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(contactData);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "contactData" element
     */
    public org.xvergabe.xsd.forms.components.x20.Contact insertNewContactData(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().insert_element_user(CONTACTDATA$6, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "contactData" element
     */
    public org.xvergabe.xsd.forms.components.x20.Contact addNewContactData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Contact target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Contact)get_store().add_element_user(CONTACTDATA$6);
            return target;
        }
    }
    
    /**
     * Removes the ith "contactData" element
     */
    public void removeContactData(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CONTACTDATA$6, i);
        }
    }
    
    /**
     * Gets array of all "contactPerson" elements
     */
    public org.xvergabe.xsd.forms.components.x20.NaturalPerson[] getContactPersonArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(CONTACTPERSON$8, targetList);
            org.xvergabe.xsd.forms.components.x20.NaturalPerson[] result = new org.xvergabe.xsd.forms.components.x20.NaturalPerson[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "contactPerson" element
     */
    public org.xvergabe.xsd.forms.components.x20.NaturalPerson getContactPersonArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.NaturalPerson target = null;
            target = (org.xvergabe.xsd.forms.components.x20.NaturalPerson)get_store().find_element_user(CONTACTPERSON$8, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "contactPerson" element
     */
    public int sizeOfContactPersonArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(CONTACTPERSON$8);
        }
    }
    
    /**
     * Sets array of all "contactPerson" element
     */
    public void setContactPersonArray(org.xvergabe.xsd.forms.components.x20.NaturalPerson[] contactPersonArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(contactPersonArray, CONTACTPERSON$8);
        }
    }
    
    /**
     * Sets ith "contactPerson" element
     */
    public void setContactPersonArray(int i, org.xvergabe.xsd.forms.components.x20.NaturalPerson contactPerson)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.NaturalPerson target = null;
            target = (org.xvergabe.xsd.forms.components.x20.NaturalPerson)get_store().find_element_user(CONTACTPERSON$8, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(contactPerson);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "contactPerson" element
     */
    public org.xvergabe.xsd.forms.components.x20.NaturalPerson insertNewContactPerson(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.NaturalPerson target = null;
            target = (org.xvergabe.xsd.forms.components.x20.NaturalPerson)get_store().insert_element_user(CONTACTPERSON$8, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "contactPerson" element
     */
    public org.xvergabe.xsd.forms.components.x20.NaturalPerson addNewContactPerson()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.NaturalPerson target = null;
            target = (org.xvergabe.xsd.forms.components.x20.NaturalPerson)get_store().add_element_user(CONTACTPERSON$8);
            return target;
        }
    }
    
    /**
     * Removes the ith "contactPerson" element
     */
    public void removeContactPerson(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(CONTACTPERSON$8, i);
        }
    }
}
