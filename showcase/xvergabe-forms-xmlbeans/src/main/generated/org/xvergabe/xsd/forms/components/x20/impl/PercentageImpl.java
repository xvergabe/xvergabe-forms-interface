/*
 * XML Type:  Percentage
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Percentage
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Percentage(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class PercentageImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl implements org.xvergabe.xsd.forms.components.x20.Percentage
{
    private static final long serialVersionUID = 1L;
    
    public PercentageImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName VALUE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "value");
    private static final javax.xml.namespace.QName DECIMALMETADATA$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "decimalMetaData");
    
    
    /**
     * Gets the "value" element
     */
    public float getValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                return 0.0f;
            }
            return target.getFloatValue();
        }
    }
    
    /**
     * Gets (as xml) the "value" element
     */
    public org.xvergabe.xsd.forms.components.x20.PercentageType xgetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.PercentageType target = null;
            target = (org.xvergabe.xsd.forms.components.x20.PercentageType)get_store().find_element_user(VALUE$0, 0);
            return target;
        }
    }
    
    /**
     * True if has "value" element
     */
    public boolean isSetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(VALUE$0) != 0;
        }
    }
    
    /**
     * Sets the "value" element
     */
    public void setValue(float value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_element_user(VALUE$0);
            }
            target.setFloatValue(value);
        }
    }
    
    /**
     * Sets (as xml) the "value" element
     */
    public void xsetValue(org.xvergabe.xsd.forms.components.x20.PercentageType value)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.PercentageType target = null;
            target = (org.xvergabe.xsd.forms.components.x20.PercentageType)get_store().find_element_user(VALUE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.PercentageType)get_store().add_element_user(VALUE$0);
            }
            target.set(value);
        }
    }
    
    /**
     * Unsets the "value" element
     */
    public void unsetValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(VALUE$0, 0);
        }
    }
    
    /**
     * Gets the "decimalMetaData" element
     */
    public org.xvergabe.xsd.forms.components.x20.DecimalMetaData getDecimalMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DecimalMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DecimalMetaData)get_store().find_element_user(DECIMALMETADATA$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "decimalMetaData" element
     */
    public boolean isSetDecimalMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DECIMALMETADATA$2) != 0;
        }
    }
    
    /**
     * Sets the "decimalMetaData" element
     */
    public void setDecimalMetaData(org.xvergabe.xsd.forms.components.x20.DecimalMetaData decimalMetaData)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DecimalMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DecimalMetaData)get_store().find_element_user(DECIMALMETADATA$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.DecimalMetaData)get_store().add_element_user(DECIMALMETADATA$2);
            }
            target.set(decimalMetaData);
        }
    }
    
    /**
     * Appends and returns a new empty "decimalMetaData" element
     */
    public org.xvergabe.xsd.forms.components.x20.DecimalMetaData addNewDecimalMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.DecimalMetaData target = null;
            target = (org.xvergabe.xsd.forms.components.x20.DecimalMetaData)get_store().add_element_user(DECIMALMETADATA$2);
            return target;
        }
    }
    
    /**
     * Unsets the "decimalMetaData" element
     */
    public void unsetDecimalMetaData()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DECIMALMETADATA$2, 0);
        }
    }
}
