/*
 * XML Type:  PercentageType
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.PercentageType
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML PercentageType(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.PercentageType.
 */
public class PercentageTypeImpl extends org.apache.xmlbeans.impl.values.JavaFloatHolderEx implements org.xvergabe.xsd.forms.components.x20.PercentageType
{
    private static final long serialVersionUID = 1L;
    
    public PercentageTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected PercentageTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
