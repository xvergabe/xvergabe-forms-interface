/*
 * XML Type:  Prequalification
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Prequalification
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Prequalification(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class PrequalificationImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Prequalification
{
    private static final long serialVersionUID = 1L;
    
    public PrequalificationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName PREQUALIFICATIONINSTANCE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "prequalificationInstance");
    private static final javax.xml.namespace.QName PREQUALIFIEDORGANIZATION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "prequalifiedOrganization");
    private static final javax.xml.namespace.QName PQNUMBER$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "pqNumber");
    private static final javax.xml.namespace.QName ULVNUMBER$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "ulvNumber");
    
    
    /**
     * Gets the "prequalificationInstance" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getPrequalificationInstance()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(PREQUALIFICATIONINSTANCE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "prequalificationInstance" element
     */
    public void setPrequalificationInstance(org.xvergabe.xsd.forms.components.x20.String prequalificationInstance)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(PREQUALIFICATIONINSTANCE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(PREQUALIFICATIONINSTANCE$0);
            }
            target.set(prequalificationInstance);
        }
    }
    
    /**
     * Appends and returns a new empty "prequalificationInstance" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewPrequalificationInstance()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(PREQUALIFICATIONINSTANCE$0);
            return target;
        }
    }
    
    /**
     * Gets the "prequalifiedOrganization" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getPrequalifiedOrganization()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(PREQUALIFIEDORGANIZATION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "prequalifiedOrganization" element
     */
    public boolean isSetPrequalifiedOrganization()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREQUALIFIEDORGANIZATION$2) != 0;
        }
    }
    
    /**
     * Sets the "prequalifiedOrganization" element
     */
    public void setPrequalifiedOrganization(org.xvergabe.xsd.forms.components.x20.String prequalifiedOrganization)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(PREQUALIFIEDORGANIZATION$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(PREQUALIFIEDORGANIZATION$2);
            }
            target.set(prequalifiedOrganization);
        }
    }
    
    /**
     * Appends and returns a new empty "prequalifiedOrganization" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewPrequalifiedOrganization()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(PREQUALIFIEDORGANIZATION$2);
            return target;
        }
    }
    
    /**
     * Unsets the "prequalifiedOrganization" element
     */
    public void unsetPrequalifiedOrganization()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREQUALIFIEDORGANIZATION$2, 0);
        }
    }
    
    /**
     * Gets the "pqNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getPqNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(PQNUMBER$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "pqNumber" element
     */
    public void setPqNumber(org.xvergabe.xsd.forms.components.x20.String pqNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(PQNUMBER$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(PQNUMBER$4);
            }
            target.set(pqNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "pqNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewPqNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(PQNUMBER$4);
            return target;
        }
    }
    
    /**
     * Gets the "ulvNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getUlvNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ULVNUMBER$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "ulvNumber" element
     */
    public void setUlvNumber(org.xvergabe.xsd.forms.components.x20.String ulvNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ULVNUMBER$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ULVNUMBER$6);
            }
            target.set(ulvNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "ulvNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewUlvNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ULVNUMBER$6);
            return target;
        }
    }
}
