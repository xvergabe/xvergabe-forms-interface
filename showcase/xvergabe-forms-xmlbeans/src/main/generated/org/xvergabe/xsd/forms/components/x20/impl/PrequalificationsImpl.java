/*
 * XML Type:  Prequalifications
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Prequalifications
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Prequalifications(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class PrequalificationsImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Prequalifications
{
    private static final long serialVersionUID = 1L;
    
    public PrequalificationsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName AGREEMENT$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "agreement");
    private static final javax.xml.namespace.QName PREQUALIFICATION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "prequalification");
    
    
    /**
     * Gets the "agreement" element
     */
    public org.xvergabe.xsd.forms.components.x20.Agreement getAgreement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Agreement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Agreement)get_store().find_element_user(AGREEMENT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "agreement" element
     */
    public boolean isSetAgreement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AGREEMENT$0) != 0;
        }
    }
    
    /**
     * Sets the "agreement" element
     */
    public void setAgreement(org.xvergabe.xsd.forms.components.x20.Agreement agreement)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Agreement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Agreement)get_store().find_element_user(AGREEMENT$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Agreement)get_store().add_element_user(AGREEMENT$0);
            }
            target.set(agreement);
        }
    }
    
    /**
     * Appends and returns a new empty "agreement" element
     */
    public org.xvergabe.xsd.forms.components.x20.Agreement addNewAgreement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Agreement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Agreement)get_store().add_element_user(AGREEMENT$0);
            return target;
        }
    }
    
    /**
     * Unsets the "agreement" element
     */
    public void unsetAgreement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AGREEMENT$0, 0);
        }
    }
    
    /**
     * Gets array of all "prequalification" elements
     */
    public org.xvergabe.xsd.forms.components.x20.Prequalification[] getPrequalificationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(PREQUALIFICATION$2, targetList);
            org.xvergabe.xsd.forms.components.x20.Prequalification[] result = new org.xvergabe.xsd.forms.components.x20.Prequalification[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "prequalification" element
     */
    public org.xvergabe.xsd.forms.components.x20.Prequalification getPrequalificationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Prequalification target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Prequalification)get_store().find_element_user(PREQUALIFICATION$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "prequalification" element
     */
    public int sizeOfPrequalificationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PREQUALIFICATION$2);
        }
    }
    
    /**
     * Sets array of all "prequalification" element
     */
    public void setPrequalificationArray(org.xvergabe.xsd.forms.components.x20.Prequalification[] prequalificationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(prequalificationArray, PREQUALIFICATION$2);
        }
    }
    
    /**
     * Sets ith "prequalification" element
     */
    public void setPrequalificationArray(int i, org.xvergabe.xsd.forms.components.x20.Prequalification prequalification)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Prequalification target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Prequalification)get_store().find_element_user(PREQUALIFICATION$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(prequalification);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "prequalification" element
     */
    public org.xvergabe.xsd.forms.components.x20.Prequalification insertNewPrequalification(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Prequalification target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Prequalification)get_store().insert_element_user(PREQUALIFICATION$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "prequalification" element
     */
    public org.xvergabe.xsd.forms.components.x20.Prequalification addNewPrequalification()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Prequalification target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Prequalification)get_store().add_element_user(PREQUALIFICATION$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "prequalification" element
     */
    public void removePrequalification(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PREQUALIFICATION$2, i);
        }
    }
}
