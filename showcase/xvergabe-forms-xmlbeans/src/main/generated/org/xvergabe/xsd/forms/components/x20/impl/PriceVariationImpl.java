/*
 * XML Type:  PriceVariation
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.PriceVariation
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML PriceVariation(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class PriceVariationImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.PriceVariation
{
    private static final long serialVersionUID = 1L;
    
    public PriceVariationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LEISTUNGSBEREICHNUMMER$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "leistungsbereichNummer");
    private static final javax.xml.namespace.QName LEISTUNGSBEREICHBESCHREIBUNG$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "leistungsbereichBeschreibung");
    private static final javax.xml.namespace.QName ABGEBOT$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "abgebot");
    private static final javax.xml.namespace.QName AUFGEBOT$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "aufgebot");
    
    
    /**
     * Gets the "leistungsbereichNummer" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getLeistungsbereichNummer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LEISTUNGSBEREICHNUMMER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "leistungsbereichNummer" element
     */
    public void setLeistungsbereichNummer(org.xvergabe.xsd.forms.components.x20.String leistungsbereichNummer)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LEISTUNGSBEREICHNUMMER$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LEISTUNGSBEREICHNUMMER$0);
            }
            target.set(leistungsbereichNummer);
        }
    }
    
    /**
     * Appends and returns a new empty "leistungsbereichNummer" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewLeistungsbereichNummer()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LEISTUNGSBEREICHNUMMER$0);
            return target;
        }
    }
    
    /**
     * Gets the "leistungsbereichBeschreibung" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getLeistungsbereichBeschreibung()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LEISTUNGSBEREICHBESCHREIBUNG$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "leistungsbereichBeschreibung" element
     */
    public void setLeistungsbereichBeschreibung(org.xvergabe.xsd.forms.components.x20.String leistungsbereichBeschreibung)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LEISTUNGSBEREICHBESCHREIBUNG$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LEISTUNGSBEREICHBESCHREIBUNG$2);
            }
            target.set(leistungsbereichBeschreibung);
        }
    }
    
    /**
     * Appends and returns a new empty "leistungsbereichBeschreibung" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewLeistungsbereichBeschreibung()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LEISTUNGSBEREICHBESCHREIBUNG$2);
            return target;
        }
    }
    
    /**
     * Gets the "abgebot" element
     */
    public org.xvergabe.xsd.forms.components.x20.Remission getAbgebot()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Remission target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Remission)get_store().find_element_user(ABGEBOT$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "abgebot" element
     */
    public void setAbgebot(org.xvergabe.xsd.forms.components.x20.Remission abgebot)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Remission target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Remission)get_store().find_element_user(ABGEBOT$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Remission)get_store().add_element_user(ABGEBOT$4);
            }
            target.set(abgebot);
        }
    }
    
    /**
     * Appends and returns a new empty "abgebot" element
     */
    public org.xvergabe.xsd.forms.components.x20.Remission addNewAbgebot()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Remission target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Remission)get_store().add_element_user(ABGEBOT$4);
            return target;
        }
    }
    
    /**
     * Gets the "aufgebot" element
     */
    public org.xvergabe.xsd.forms.components.x20.Remission getAufgebot()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Remission target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Remission)get_store().find_element_user(AUFGEBOT$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "aufgebot" element
     */
    public void setAufgebot(org.xvergabe.xsd.forms.components.x20.Remission aufgebot)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Remission target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Remission)get_store().find_element_user(AUFGEBOT$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Remission)get_store().add_element_user(AUFGEBOT$6);
            }
            target.set(aufgebot);
        }
    }
    
    /**
     * Appends and returns a new empty "aufgebot" element
     */
    public org.xvergabe.xsd.forms.components.x20.Remission addNewAufgebot()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Remission target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Remission)get_store().add_element_user(AUFGEBOT$6);
            return target;
        }
    }
}
