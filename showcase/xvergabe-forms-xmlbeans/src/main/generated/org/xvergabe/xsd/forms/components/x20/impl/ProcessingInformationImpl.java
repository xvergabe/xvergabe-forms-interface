/*
 * XML Type:  ProcessingInformation
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.ProcessingInformation
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML ProcessingInformation(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class ProcessingInformationImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.ProcessingInformation
{
    private static final long serialVersionUID = 1L;
    
    public ProcessingInformationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SENDINGDATE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "sendingDate");
    private static final javax.xml.namespace.QName LOCATION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "location");
    
    
    /**
     * Gets the "sendingDate" element
     */
    public org.xvergabe.xsd.forms.components.x20.Date getSendingDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().find_element_user(SENDINGDATE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "sendingDate" element
     */
    public boolean isSetSendingDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SENDINGDATE$0) != 0;
        }
    }
    
    /**
     * Sets the "sendingDate" element
     */
    public void setSendingDate(org.xvergabe.xsd.forms.components.x20.Date sendingDate)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().find_element_user(SENDINGDATE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().add_element_user(SENDINGDATE$0);
            }
            target.set(sendingDate);
        }
    }
    
    /**
     * Appends and returns a new empty "sendingDate" element
     */
    public org.xvergabe.xsd.forms.components.x20.Date addNewSendingDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Date target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Date)get_store().add_element_user(SENDINGDATE$0);
            return target;
        }
    }
    
    /**
     * Unsets the "sendingDate" element
     */
    public void unsetSendingDate()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SENDINGDATE$0, 0);
        }
    }
    
    /**
     * Gets the "location" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getLocation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LOCATION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "location" element
     */
    public boolean isSetLocation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LOCATION$2) != 0;
        }
    }
    
    /**
     * Sets the "location" element
     */
    public void setLocation(org.xvergabe.xsd.forms.components.x20.String location)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LOCATION$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LOCATION$2);
            }
            target.set(location);
        }
    }
    
    /**
     * Appends and returns a new empty "location" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewLocation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LOCATION$2);
            return target;
        }
    }
    
    /**
     * Unsets the "location" element
     */
    public void unsetLocation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LOCATION$2, 0);
        }
    }
}
