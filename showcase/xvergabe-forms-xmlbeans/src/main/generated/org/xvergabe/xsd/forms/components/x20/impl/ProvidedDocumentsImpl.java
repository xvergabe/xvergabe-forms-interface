/*
 * XML Type:  ProvidedDocuments
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.ProvidedDocuments
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML ProvidedDocuments(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class ProvidedDocumentsImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.ProvidedDocuments
{
    private static final long serialVersionUID = 1L;
    
    public ProvidedDocumentsImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName GIVENDOCUMENTS$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "givenDocuments");
    private static final javax.xml.namespace.QName DOCUMENT$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "document");
    
    
    /**
     * Gets the "givenDocuments" element
     */
    public org.xvergabe.xsd.forms.components.x20.CheckGroup getGivenDocuments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CheckGroup target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CheckGroup)get_store().find_element_user(GIVENDOCUMENTS$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "givenDocuments" element
     */
    public boolean isSetGivenDocuments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(GIVENDOCUMENTS$0) != 0;
        }
    }
    
    /**
     * Sets the "givenDocuments" element
     */
    public void setGivenDocuments(org.xvergabe.xsd.forms.components.x20.CheckGroup givenDocuments)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CheckGroup target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CheckGroup)get_store().find_element_user(GIVENDOCUMENTS$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.CheckGroup)get_store().add_element_user(GIVENDOCUMENTS$0);
            }
            target.set(givenDocuments);
        }
    }
    
    /**
     * Appends and returns a new empty "givenDocuments" element
     */
    public org.xvergabe.xsd.forms.components.x20.CheckGroup addNewGivenDocuments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.CheckGroup target = null;
            target = (org.xvergabe.xsd.forms.components.x20.CheckGroup)get_store().add_element_user(GIVENDOCUMENTS$0);
            return target;
        }
    }
    
    /**
     * Unsets the "givenDocuments" element
     */
    public void unsetGivenDocuments()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(GIVENDOCUMENTS$0, 0);
        }
    }
    
    /**
     * Gets array of all "document" elements
     */
    public org.xvergabe.xsd.forms.components.x20.String[] getDocumentArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(DOCUMENT$2, targetList);
            org.xvergabe.xsd.forms.components.x20.String[] result = new org.xvergabe.xsd.forms.components.x20.String[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "document" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getDocumentArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(DOCUMENT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "document" element
     */
    public int sizeOfDocumentArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(DOCUMENT$2);
        }
    }
    
    /**
     * Sets array of all "document" element
     */
    public void setDocumentArray(org.xvergabe.xsd.forms.components.x20.String[] documentArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(documentArray, DOCUMENT$2);
        }
    }
    
    /**
     * Sets ith "document" element
     */
    public void setDocumentArray(int i, org.xvergabe.xsd.forms.components.x20.String document)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(DOCUMENT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(document);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "document" element
     */
    public org.xvergabe.xsd.forms.components.x20.String insertNewDocument(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().insert_element_user(DOCUMENT$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "document" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewDocument()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(DOCUMENT$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "document" element
     */
    public void removeDocument(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(DOCUMENT$2, i);
        }
    }
}
