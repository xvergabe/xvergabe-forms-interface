/*
 * XML Type:  RadioOption
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.RadioOption
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML RadioOption(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class RadioOptionImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractFieldImpl implements org.xvergabe.xsd.forms.components.x20.RadioOption
{
    private static final long serialVersionUID = 1L;
    
    public RadioOptionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName CHOICE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "choice");
    private static final javax.xml.namespace.QName ADDITION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "addition");
    
    
    /**
     * Gets the "choice" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getChoice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CHOICE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "choice" element
     */
    public void setChoice(org.xvergabe.xsd.forms.components.x20.String choice)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(CHOICE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CHOICE$0);
            }
            target.set(choice);
        }
    }
    
    /**
     * Appends and returns a new empty "choice" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewChoice()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(CHOICE$0);
            return target;
        }
    }
    
    /**
     * Gets array of all "addition" elements
     */
    public org.xvergabe.xsd.forms.components.x20.String[] getAdditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ADDITION$2, targetList);
            org.xvergabe.xsd.forms.components.x20.String[] result = new org.xvergabe.xsd.forms.components.x20.String[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "addition" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getAdditionArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ADDITION$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "addition" element
     */
    public int sizeOfAdditionArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ADDITION$2);
        }
    }
    
    /**
     * Sets array of all "addition" element
     */
    public void setAdditionArray(org.xvergabe.xsd.forms.components.x20.String[] additionArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(additionArray, ADDITION$2);
        }
    }
    
    /**
     * Sets ith "addition" element
     */
    public void setAdditionArray(int i, org.xvergabe.xsd.forms.components.x20.String addition)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(ADDITION$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(addition);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "addition" element
     */
    public org.xvergabe.xsd.forms.components.x20.String insertNewAddition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().insert_element_user(ADDITION$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "addition" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewAddition()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(ADDITION$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "addition" element
     */
    public void removeAddition(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ADDITION$2, i);
        }
    }
}
