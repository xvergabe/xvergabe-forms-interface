/*
 * XML Type:  ReferenceInformation
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.ReferenceInformation
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML ReferenceInformation(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class ReferenceInformationImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.ReferenceInformation
{
    private static final long serialVersionUID = 1L;
    
    public ReferenceInformationImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName REFERENCENUMBER$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "referenceNumber");
    private static final javax.xml.namespace.QName MASSNAHMEBEREICH$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "Massnahmebereich");
    
    
    /**
     * Gets the "referenceNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(REFERENCENUMBER$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "referenceNumber" element
     */
    public boolean isSetReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(REFERENCENUMBER$0) != 0;
        }
    }
    
    /**
     * Sets the "referenceNumber" element
     */
    public void setReferenceNumber(org.xvergabe.xsd.forms.components.x20.String referenceNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(REFERENCENUMBER$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(REFERENCENUMBER$0);
            }
            target.set(referenceNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "referenceNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(REFERENCENUMBER$0);
            return target;
        }
    }
    
    /**
     * Unsets the "referenceNumber" element
     */
    public void unsetReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(REFERENCENUMBER$0, 0);
        }
    }
    
    /**
     * Gets the "Massnahmebereich" element
     */
    public org.xvergabe.xsd.forms.components.x20.TextArea getMassnahmebereich()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.TextArea target = null;
            target = (org.xvergabe.xsd.forms.components.x20.TextArea)get_store().find_element_user(MASSNAHMEBEREICH$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "Massnahmebereich" element
     */
    public boolean isSetMassnahmebereich()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(MASSNAHMEBEREICH$2) != 0;
        }
    }
    
    /**
     * Sets the "Massnahmebereich" element
     */
    public void setMassnahmebereich(org.xvergabe.xsd.forms.components.x20.TextArea massnahmebereich)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.TextArea target = null;
            target = (org.xvergabe.xsd.forms.components.x20.TextArea)get_store().find_element_user(MASSNAHMEBEREICH$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.TextArea)get_store().add_element_user(MASSNAHMEBEREICH$2);
            }
            target.set(massnahmebereich);
        }
    }
    
    /**
     * Appends and returns a new empty "Massnahmebereich" element
     */
    public org.xvergabe.xsd.forms.components.x20.TextArea addNewMassnahmebereich()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.TextArea target = null;
            target = (org.xvergabe.xsd.forms.components.x20.TextArea)get_store().add_element_user(MASSNAHMEBEREICH$2);
            return target;
        }
    }
    
    /**
     * Unsets the "Massnahmebereich" element
     */
    public void unsetMassnahmebereich()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(MASSNAHMEBEREICH$2, 0);
        }
    }
}
