/*
 * XML Type:  Remission
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Remission
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Remission(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class RemissionImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Remission
{
    private static final long serialVersionUID = 1L;
    
    public RemissionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ABSOLUTEVALUE$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "absoluteValue");
    private static final javax.xml.namespace.QName PERCENTAGE$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "percentage");
    
    
    /**
     * Gets the "absoluteValue" element
     */
    public org.xvergabe.xsd.forms.components.x20.Decimal getAbsoluteValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Decimal target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Decimal)get_store().find_element_user(ABSOLUTEVALUE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "absoluteValue" element
     */
    public boolean isSetAbsoluteValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ABSOLUTEVALUE$0) != 0;
        }
    }
    
    /**
     * Sets the "absoluteValue" element
     */
    public void setAbsoluteValue(org.xvergabe.xsd.forms.components.x20.Decimal absoluteValue)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Decimal target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Decimal)get_store().find_element_user(ABSOLUTEVALUE$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Decimal)get_store().add_element_user(ABSOLUTEVALUE$0);
            }
            target.set(absoluteValue);
        }
    }
    
    /**
     * Appends and returns a new empty "absoluteValue" element
     */
    public org.xvergabe.xsd.forms.components.x20.Decimal addNewAbsoluteValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Decimal target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Decimal)get_store().add_element_user(ABSOLUTEVALUE$0);
            return target;
        }
    }
    
    /**
     * Unsets the "absoluteValue" element
     */
    public void unsetAbsoluteValue()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ABSOLUTEVALUE$0, 0);
        }
    }
    
    /**
     * Gets the "percentage" element
     */
    public org.xvergabe.xsd.forms.components.x20.Percentage getPercentage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Percentage target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Percentage)get_store().find_element_user(PERCENTAGE$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "percentage" element
     */
    public boolean isSetPercentage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(PERCENTAGE$2) != 0;
        }
    }
    
    /**
     * Sets the "percentage" element
     */
    public void setPercentage(org.xvergabe.xsd.forms.components.x20.Percentage percentage)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Percentage target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Percentage)get_store().find_element_user(PERCENTAGE$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Percentage)get_store().add_element_user(PERCENTAGE$2);
            }
            target.set(percentage);
        }
    }
    
    /**
     * Appends and returns a new empty "percentage" element
     */
    public org.xvergabe.xsd.forms.components.x20.Percentage addNewPercentage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Percentage target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Percentage)get_store().add_element_user(PERCENTAGE$2);
            return target;
        }
    }
    
    /**
     * Unsets the "percentage" element
     */
    public void unsetPercentage()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(PERCENTAGE$2, 0);
        }
    }
}
