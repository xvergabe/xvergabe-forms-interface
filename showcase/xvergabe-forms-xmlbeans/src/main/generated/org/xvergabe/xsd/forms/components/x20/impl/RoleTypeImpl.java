/*
 * XML Type:  RoleType
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.RoleType
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML RoleType(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.RoleType.
 */
public class RoleTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements org.xvergabe.xsd.forms.components.x20.RoleType
{
    private static final long serialVersionUID = 1L;
    
    public RoleTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected RoleTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
