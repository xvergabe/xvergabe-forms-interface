/*
 * XML Type:  Section
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Section
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Section(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class SectionImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Section
{
    private static final long serialVersionUID = 1L;
    
    public SectionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName ELEMENT$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "element");
    
    
    /**
     * Gets array of all "element" elements
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractElement[] getElementArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(ELEMENT$0, targetList);
            org.xvergabe.xsd.forms.components.x20.AbstractElement[] result = new org.xvergabe.xsd.forms.components.x20.AbstractElement[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "element" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractElement getElementArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractElement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractElement)get_store().find_element_user(ELEMENT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "element" element
     */
    public int sizeOfElementArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(ELEMENT$0);
        }
    }
    
    /**
     * Sets array of all "element" element
     */
    public void setElementArray(org.xvergabe.xsd.forms.components.x20.AbstractElement[] elementArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(elementArray, ELEMENT$0);
        }
    }
    
    /**
     * Sets ith "element" element
     */
    public void setElementArray(int i, org.xvergabe.xsd.forms.components.x20.AbstractElement element)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractElement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractElement)get_store().find_element_user(ELEMENT$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(element);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "element" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractElement insertNewElement(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractElement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractElement)get_store().insert_element_user(ELEMENT$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "element" element
     */
    public org.xvergabe.xsd.forms.components.x20.AbstractElement addNewElement()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.AbstractElement target = null;
            target = (org.xvergabe.xsd.forms.components.x20.AbstractElement)get_store().add_element_user(ELEMENT$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "element" element
     */
    public void removeElement(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(ELEMENT$0, i);
        }
    }
}
