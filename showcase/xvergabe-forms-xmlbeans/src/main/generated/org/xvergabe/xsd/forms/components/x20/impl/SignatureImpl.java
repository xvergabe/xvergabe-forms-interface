/*
 * XML Type:  Signature
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Signature
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Signature(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class SignatureImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Signature
{
    private static final long serialVersionUID = 1L;
    
    public SignatureImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName LOCATION$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "location");
    private static final javax.xml.namespace.QName NAME$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "name");
    private static final javax.xml.namespace.QName AUTHORIZATION$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "authorization");
    private static final javax.xml.namespace.QName XMLSIGNATURE$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "xmlSignature");
    
    
    /**
     * Gets the "location" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getLocation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LOCATION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "location" element
     */
    public boolean isSetLocation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(LOCATION$0) != 0;
        }
    }
    
    /**
     * Sets the "location" element
     */
    public void setLocation(org.xvergabe.xsd.forms.components.x20.String location)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(LOCATION$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LOCATION$0);
            }
            target.set(location);
        }
    }
    
    /**
     * Appends and returns a new empty "location" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewLocation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(LOCATION$0);
            return target;
        }
    }
    
    /**
     * Unsets the "location" element
     */
    public void unsetLocation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(LOCATION$0, 0);
        }
    }
    
    /**
     * Gets the "name" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(NAME$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "name" element
     */
    public boolean isSetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(NAME$2) != 0;
        }
    }
    
    /**
     * Sets the "name" element
     */
    public void setName(org.xvergabe.xsd.forms.components.x20.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(NAME$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(NAME$2);
            }
            target.set(name);
        }
    }
    
    /**
     * Appends and returns a new empty "name" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(NAME$2);
            return target;
        }
    }
    
    /**
     * Unsets the "name" element
     */
    public void unsetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(NAME$2, 0);
        }
    }
    
    /**
     * Gets the "authorization" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getAuthorization()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(AUTHORIZATION$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "authorization" element
     */
    public boolean isSetAuthorization()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(AUTHORIZATION$4) != 0;
        }
    }
    
    /**
     * Sets the "authorization" element
     */
    public void setAuthorization(org.xvergabe.xsd.forms.components.x20.String authorization)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(AUTHORIZATION$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(AUTHORIZATION$4);
            }
            target.set(authorization);
        }
    }
    
    /**
     * Appends and returns a new empty "authorization" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewAuthorization()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(AUTHORIZATION$4);
            return target;
        }
    }
    
    /**
     * Unsets the "authorization" element
     */
    public void unsetAuthorization()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(AUTHORIZATION$4, 0);
        }
    }
    
    /**
     * Gets the "xmlSignature" element
     */
    public org.w3.x2000.x09.xmldsig.SignatureType getXmlSignature()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.x2000.x09.xmldsig.SignatureType target = null;
            target = (org.w3.x2000.x09.xmldsig.SignatureType)get_store().find_element_user(XMLSIGNATURE$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "xmlSignature" element
     */
    public boolean isSetXmlSignature()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(XMLSIGNATURE$6) != 0;
        }
    }
    
    /**
     * Sets the "xmlSignature" element
     */
    public void setXmlSignature(org.w3.x2000.x09.xmldsig.SignatureType xmlSignature)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.x2000.x09.xmldsig.SignatureType target = null;
            target = (org.w3.x2000.x09.xmldsig.SignatureType)get_store().find_element_user(XMLSIGNATURE$6, 0);
            if (target == null)
            {
                target = (org.w3.x2000.x09.xmldsig.SignatureType)get_store().add_element_user(XMLSIGNATURE$6);
            }
            target.set(xmlSignature);
        }
    }
    
    /**
     * Appends and returns a new empty "xmlSignature" element
     */
    public org.w3.x2000.x09.xmldsig.SignatureType addNewXmlSignature()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.x2000.x09.xmldsig.SignatureType target = null;
            target = (org.w3.x2000.x09.xmldsig.SignatureType)get_store().add_element_user(XMLSIGNATURE$6);
            return target;
        }
    }
    
    /**
     * Unsets the "xmlSignature" element
     */
    public void unsetXmlSignature()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(XMLSIGNATURE$6, 0);
        }
    }
}
