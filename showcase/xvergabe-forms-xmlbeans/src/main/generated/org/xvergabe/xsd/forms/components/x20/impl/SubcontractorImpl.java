/*
 * XML Type:  Subcontractor
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.Subcontractor
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML Subcontractor(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class SubcontractorImpl extends org.xvergabe.xsd.forms.components.x20.impl.AbstractComponentImpl implements org.xvergabe.xsd.forms.components.x20.Subcontractor
{
    private static final long serialVersionUID = 1L;
    
    public SubcontractorImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SUBCONTRACTORNAME$0 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "subcontractorName");
    private static final javax.xml.namespace.QName SERVICEDESCRIPTION$2 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "serviceDescription");
    private static final javax.xml.namespace.QName SERVICEREFERENCENUMBER$4 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "serviceReferenceNumber");
    private static final javax.xml.namespace.QName BIDDERCOULDSUPPLYHIMSELF$6 = 
        new javax.xml.namespace.QName("http://www.xvergabe.org/xsd/forms/components/2_0", "bidderCouldSupplyHimself");
    
    
    /**
     * Gets the "subcontractorName" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getSubcontractorName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SUBCONTRACTORNAME$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "subcontractorName" element
     */
    public boolean isSetSubcontractorName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SUBCONTRACTORNAME$0) != 0;
        }
    }
    
    /**
     * Sets the "subcontractorName" element
     */
    public void setSubcontractorName(org.xvergabe.xsd.forms.components.x20.String subcontractorName)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SUBCONTRACTORNAME$0, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SUBCONTRACTORNAME$0);
            }
            target.set(subcontractorName);
        }
    }
    
    /**
     * Appends and returns a new empty "subcontractorName" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewSubcontractorName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SUBCONTRACTORNAME$0);
            return target;
        }
    }
    
    /**
     * Unsets the "subcontractorName" element
     */
    public void unsetSubcontractorName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SUBCONTRACTORNAME$0, 0);
        }
    }
    
    /**
     * Gets the "serviceDescription" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getServiceDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SERVICEDESCRIPTION$2, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "serviceDescription" element
     */
    public boolean isSetServiceDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SERVICEDESCRIPTION$2) != 0;
        }
    }
    
    /**
     * Sets the "serviceDescription" element
     */
    public void setServiceDescription(org.xvergabe.xsd.forms.components.x20.String serviceDescription)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SERVICEDESCRIPTION$2, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SERVICEDESCRIPTION$2);
            }
            target.set(serviceDescription);
        }
    }
    
    /**
     * Appends and returns a new empty "serviceDescription" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewServiceDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SERVICEDESCRIPTION$2);
            return target;
        }
    }
    
    /**
     * Unsets the "serviceDescription" element
     */
    public void unsetServiceDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SERVICEDESCRIPTION$2, 0);
        }
    }
    
    /**
     * Gets the "serviceReferenceNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String getServiceReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SERVICEREFERENCENUMBER$4, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "serviceReferenceNumber" element
     */
    public boolean isSetServiceReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(SERVICEREFERENCENUMBER$4) != 0;
        }
    }
    
    /**
     * Sets the "serviceReferenceNumber" element
     */
    public void setServiceReferenceNumber(org.xvergabe.xsd.forms.components.x20.String serviceReferenceNumber)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().find_element_user(SERVICEREFERENCENUMBER$4, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SERVICEREFERENCENUMBER$4);
            }
            target.set(serviceReferenceNumber);
        }
    }
    
    /**
     * Appends and returns a new empty "serviceReferenceNumber" element
     */
    public org.xvergabe.xsd.forms.components.x20.String addNewServiceReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.String target = null;
            target = (org.xvergabe.xsd.forms.components.x20.String)get_store().add_element_user(SERVICEREFERENCENUMBER$4);
            return target;
        }
    }
    
    /**
     * Unsets the "serviceReferenceNumber" element
     */
    public void unsetServiceReferenceNumber()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(SERVICEREFERENCENUMBER$4, 0);
        }
    }
    
    /**
     * Gets the "bidderCouldSupplyHimself" element
     */
    public org.xvergabe.xsd.forms.components.x20.Boolean getBidderCouldSupplyHimself()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().find_element_user(BIDDERCOULDSUPPLYHIMSELF$6, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * True if has "bidderCouldSupplyHimself" element
     */
    public boolean isSetBidderCouldSupplyHimself()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(BIDDERCOULDSUPPLYHIMSELF$6) != 0;
        }
    }
    
    /**
     * Sets the "bidderCouldSupplyHimself" element
     */
    public void setBidderCouldSupplyHimself(org.xvergabe.xsd.forms.components.x20.Boolean bidderCouldSupplyHimself)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().find_element_user(BIDDERCOULDSUPPLYHIMSELF$6, 0);
            if (target == null)
            {
                target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().add_element_user(BIDDERCOULDSUPPLYHIMSELF$6);
            }
            target.set(bidderCouldSupplyHimself);
        }
    }
    
    /**
     * Appends and returns a new empty "bidderCouldSupplyHimself" element
     */
    public org.xvergabe.xsd.forms.components.x20.Boolean addNewBidderCouldSupplyHimself()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.xvergabe.xsd.forms.components.x20.Boolean target = null;
            target = (org.xvergabe.xsd.forms.components.x20.Boolean)get_store().add_element_user(BIDDERCOULDSUPPLYHIMSELF$6);
            return target;
        }
    }
    
    /**
     * Unsets the "bidderCouldSupplyHimself" element
     */
    public void unsetBidderCouldSupplyHimself()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(BIDDERCOULDSUPPLYHIMSELF$6, 0);
        }
    }
}
