/*
 * XML Type:  TenderType
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.TenderType
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML TenderType(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is an atomic type that is a restriction of org.xvergabe.xsd.forms.components.x20.TenderType.
 */
public class TenderTypeImpl extends org.apache.xmlbeans.impl.values.JavaStringEnumerationHolderEx implements org.xvergabe.xsd.forms.components.x20.TenderType
{
    private static final long serialVersionUID = 1L;
    
    public TenderTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType, false);
    }
    
    protected TenderTypeImpl(org.apache.xmlbeans.SchemaType sType, boolean b)
    {
        super(sType, b);
    }
}
