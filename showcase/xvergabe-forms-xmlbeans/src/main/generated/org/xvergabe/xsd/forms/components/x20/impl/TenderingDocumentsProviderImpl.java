/*
 * XML Type:  TenderingDocumentsProvider
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.TenderingDocumentsProvider
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML TenderingDocumentsProvider(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class TenderingDocumentsProviderImpl extends org.xvergabe.xsd.forms.components.x20.impl.OrganizationImpl implements org.xvergabe.xsd.forms.components.x20.TenderingDocumentsProvider
{
    private static final long serialVersionUID = 1L;
    
    public TenderingDocumentsProviderImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
