/*
 * XML Type:  XorSection
 * Namespace: http://www.xvergabe.org/xsd/forms/components/2_0
 * Java type: org.xvergabe.xsd.forms.components.x20.XorSection
 *
 * Automatically generated - do not modify.
 */
package org.xvergabe.xsd.forms.components.x20.impl;
/**
 * An XML XorSection(@http://www.xvergabe.org/xsd/forms/components/2_0).
 *
 * This is a complex type.
 */
public class XorSectionImpl extends org.xvergabe.xsd.forms.components.x20.impl.SectionImpl implements org.xvergabe.xsd.forms.components.x20.XorSection
{
    private static final long serialVersionUID = 1L;
    
    public XorSectionImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
